<?php

	require_once 'site-header.php';



	$count_budget =
	sql("SELECT COUNT(id_user)
		 FROM budget
		 WHERE id_user = :_iduser
		", Array(
			'_iduser' => (int)$user['id']
		), 'count');

	if($count_budget != 0) {
		$get_budget =
		sql("SELECT *
			 FROM budget
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			));

		$arr_budget = [];
		foreach($get_budget AS $list) {
			$arr_budget[] = [
				'id' => (int)$list['id'],
				'name' => endecrypt($list['data_name'], false),
				'goal' => (float)endecrypt($list['data_sum_goal'], false),
				'current' => (empty($list['data_sum_current']) ? null : (float)endecrypt($list['data_sum_current'], false)),
				'permonth' => (empty($list['data_sum_permonth']) ? null : (float)endecrypt($list['data_sum_permonth'], false)),
				'notes' => (empty($list['data_notes']) ? null : endecrypt($list['data_notes'], false)),
				'order' => (int)$list['data_order']
			];
		}

		usort($arr_budget, fn($a, $b) => $a['order'] <=> $b['order']);
	}







	echo '<section id="budget">';
		echo '<h1>'.$lang['titles']['budget'].'</h1>';

		echo '<nav>';
			echo '<a href="'.url('add-budget').'" id="add">'.svgicon('add') . $lang['nav']['sub']['add-budget'].'</a>';
		echo '</nav>';



		if($count_budget == 0) {
			echo '<div class="message">';
				echo $lang['messages']['no-items'];
			echo '</div>';


		} else {
			echo '<div class="updating">';
				echo '<div class="saving">'.$lang['messages']['budget-order-updating'].'</div>';
				echo '<div class="error">'.$lang['messages']['budget-order-error'].'</div>';
			echo '</div>';

			echo '<div class="side-by-side" id="items">';
				$order = 0;
				foreach($arr_budget AS $budget) {
					$order ++;
					$sum_collected = (float)$budget['current'];
					$percentage = number_format(($sum_collected * 100) / $budget['goal'], 0, '', '');

					if((float)$sum_collected >= (float)$budget['goal']) {
						$percentage = 100;
					} elseif(strpos(endecrypt($budget['goal'], false), '-') !== false) {
						$percentage = 0;
					} else {
						$percentage = $percentage;
					}

					if(!empty($budget['permonth'])) {
						$months = ($budget['goal'] / $budget['permonth']);
						$estimated_date = date('Y-m-d', strtotime('+'.format_number($months, 0).' months'));
					}



					echo '<div class="item id-'.(int)$budget['id'].'" data-iditem="'.(int)$budget['id'].'" data-order="'.$order.'">';
						echo '<div class="done'.((float)$sum_collected == (float)$budget['goal'] ? '' : ' hidden').'" data-idbudget="'.(int)$budget['id'].'">';
							echo svgicon('confetti');
						echo '</div>';

						echo '<h2>';
							echo '<span class="move" title="'.$lang['tooltips']['move-item'].'">'.svgicon('move').'</span>';
							echo $budget['name'];
						echo '</h2>';


						echo '<div class="sums" data-idbudget="'.(int)$budget['id'].'">';
							echo '<div class="current sum">';
								echo format_number($sum_collected);
							echo '</div>';

							echo '<div class="goal sum">';
								echo format_number($budget['goal']);
							echo '</div>';
						echo '</div>';

						echo '<div class="progress" data-idbudget="'.(int)$budget['id'].'">';
							echo '<div class="percentage" style="width: '.$percentage.'%;">';
								echo '<div class="text">'.$percentage.'%</div>';
							echo '</div>';
						echo '</div>';

						echo '<div class="months-left">';
							if(!empty($budget['permonth'])) {
								echo $lang['words']['estimated'].' '.mb_strtolower(date_(strtotime($estimated_date), 'month-year'));
							} else {
								echo '<span class="color-blue">';
									echo $lang['information']['no-monthlysum'];
								echo '</span>';
							}
						echo '</div>';

						echo '<div class="notes">';
							if(empty($budget['notes'])) {
								echo '<p class="color-blue">';
									echo $lang['information']['no-notes'];
								echo '</p>';

							} else {
								echo $Parsedown->text($budget['notes']);
							}
						echo '</div>';



						echo '<div class="msg" data-idbudget="'.(int)$budget['id'].'"></div>';

						echo '<div class="options side-by-side" data-idbudget="'.(int)$budget['id'].'" data-html2canvas-ignore>';
							echo '<div>';
								echo '<a href="javascript:void(0)" class="update" data-idbudget="'.(int)$budget['id'].'">';
									echo $lang['words']['cash-inout'];
								echo '</a>';
							echo '</div>';

							echo '<div class="side-by-side">';
								echo screenshot('.item.id-'.(int)$budget['id'].'', 'Budget - '.$budget['name'], null, true);

								echo '<a href="'.url('edit-budget:'.(int)$budget['id']).'" title="'.$lang['tooltips']['edit-budget'].'">';
									echo svgicon('edit');
								echo '</a>';

								echo '<a href="'.url('delete-budget:'.(int)$budget['id']).'" class="color-red" title="'.$lang['tooltips']['delete-budget'].'" onClick="return confirm(\''.$lang['modals']['remove-budget'].'\')"">';
									echo svgicon('trash');
								echo '</a>';
							echo '</div>';
						echo '</div>';



						echo '<form action="javascript:void(0)" method="POST" autocomplete="off" data-idbudget="'.(int)$budget['id'].'" novalidate>';
							echo '<input type="hidden" name="hidden-idbudget" value="'.(int)$budget['id'].'">';

							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['words']['sums']['amount'];
								echo '</div>';

								echo '<div class="field">';
									echo '<div class="icon">'.svgicon('money').'</div>';
									echo '<input type="text" inputmode="numeric" pattern="[0-9]{6}" name="field-sum" aria-label="sum">';
								echo '</div>';
							echo '</div>';


							echo '<div class="checkboxes">';
								echo radio($lang['words']['cash-in'], 'in-'.(int)$budget['id'], 'option', null, true);
								echo radio($lang['words']['cash-out'], 'out-'.(int)$budget['id'], 'option', null);
							echo '</div>';


							echo '<input type="submit" name="button-update" value="OK" data-idbudget="'.(int)$budget['id'].'">';
							echo '<a href="javascript:void(0)" class="cancel" data-idbudget="'.(int)$budget['id'].'">';
								echo $lang['words']['cancel'];
							echo '</a>';
						echo '</form>';
					echo '</div>';
				}
			echo '</div>';
		}
	echo '</section>';







	require_once 'site-footer.php';

?>