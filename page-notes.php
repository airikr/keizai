<?php

	require_once 'site-header.php';







	echo '<section id="notes">';
		echo '<h1>'.$lang['titles']['notes'].'</h1>';

		echo '<div class="msg"></div>';



		echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate>';
			echo '<textarea name="field-notes" aria-label="notes">';
				echo (empty($user['data_notes']) ? null : endecrypt($user['data_notes'], false));
			echo '</textarea>';

			echo '<div class="button">';
				echo '<input type="submit" name="button-save" value="'.$lang['words']['buttons']['save'].'">';
			echo '</div>';
		echo '</form>';
	echo '</section>';







	require_once 'site-footer.php';

?>