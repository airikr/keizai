<?php

	require_once 'site-header.php';



	$get_item = safetag($_GET['ite']);
	$get_account = ((!isset($_GET['ida']) OR $count_accounts == 0) ? null : (int)safetag($_GET['ida']));
	$get_filter = (!isset($_GET['fil']) ? null : safetag($_GET['fil']));
	$get_type = (!isset($_GET['typ']) ? null : safetag($_GET['typ']));
	$get_category = (!isset($_GET['cat']) ? null : safetag($_GET['cat']));
	$get_date_year = (!isset($_GET['yea']) ? null : safetag($_GET['yea']));
	$get_date_month = (!isset($_GET['mon']) ? null : safetag($_GET['mon']));
	$get_date_day = (!isset($_GET['day']) ? null : safetag($_GET['day']));

	$total_sum = 0;
	$is_anothercurrency = false;
	$subtitle = null;
	$period = null;



	if($get_item == 'all') {
		$title = 'Alla';
		$name = 'Alla';
		$object = 'all';

		if(!empty($get_date_year) AND empty($get_date_month) AND empty($get_date_day)) {
			$filter_date = "AND YEAR(FROM_UNIXTIME(timestamp_date)) = '".$get_date_year."'";
			$filter_date_title = $get_date_year;
	
		} elseif(!empty($get_date_year) AND !empty($get_date_month) AND empty($get_date_day)) {
			$filter_date = "AND YEAR(FROM_UNIXTIME(timestamp_date)) = '".$get_date_year."' AND MONTH(FROM_UNIXTIME(timestamp_date)) = '".$get_date_month."'";
			$filter_date_title = date_(strtotime($get_date_year.'-'.$get_date_month.'-01'), 'month-year');
	
		} elseif(!empty($get_date_year) AND !empty($get_date_month) AND !empty($get_date_day)) {
			$filter_date = "AND DATE(FROM_UNIXTIME(timestamp_date)) = '".$get_date_year."-".$get_date_month."-".$get_date_day."'";
			$filter_date_title = date_(strtotime($get_date_year.'-'.$get_date_month.'-'.$get_date_day), 'day-month-year');
		}


		$count_items =
		sql("SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 ".$filter_date."
			", Array(
				'_iduser' => (int)$user['id']
			), 'count');

		if($count_items != 0) {
			$list_items =
			sql("SELECT *
				 FROM items
				 WHERE id_user = :_iduser
				 ".$filter_date."
				", Array(
					'_iduser' => (int)$user['id']
				));
		}



	} elseif($get_item == 'expenses') {
		$title = $lang['titles']['expenses'];
		$name = $lang['words']['the-expense'];
		$object = 'expense';

		if(!empty($get_filter)) {
			if($get_filter == 'period') {
				$subtitle = $lang['subtitles']['current-period'];

				if(!empty($user['timestamp_period_start']) AND !empty($user['timestamp_period_end'])) {
					$subtitle .= ' ('.date_($period_start, 'day-month').' - '.date_($period_end, 'day-month').')';

				} elseif(!empty($user['data_incomeday'])) {
					$subtitle .= ' ('.date_($period_incomeday, 'day-month').' - '.date_($period_incomeday_nextmonth_1, 'day-month').')';
				}


			} elseif($get_filter == 'not-payed') {
				$subtitle = $lang['subtitles']['not-payed'];
			} elseif($get_filter == 'payed') {
				$subtitle = $lang['subtitles']['payed'];
			} elseif($get_filter == 'extra') {
				$subtitle = $lang['subtitles']['extra'];
			} elseif($get_filter == 'future') {
				$subtitle = $lang['subtitles']['future'];
			} elseif($get_filter == 'recurrent') {
				$subtitle = $lang['subtitles']['recurrent'];
			} elseif($get_filter == 'shared') {
				$subtitle = $lang['subtitles']['shared'];
			} elseif($get_filter == 'date') {
				$subtitle = $get_date_year.'-'.$get_date_month.'-'.$get_date_day;

			} elseif(!empty($get_type)) {
				$subtitle = $lang['types'][$get_type];

			} elseif(!empty($get_category)) {
				$subtitle = $lang['categories'][$get_category];
			}

		} else {
			$subtitle = $lang['words']['all'];
		}


		if($count_accounts != 0) {
			$get_accounts =
			sql("SELECT *
				 FROM item_accounts
				 WHERE id_user = :_iduser
				", Array(
					'_iduser' => (int)$user['id']
				));
		}

		if(!empty($get_account)) {
			$account =
			sql("SELECT *
				 FROM item_accounts
				 WHERE id = :_idaccount
				 AND id_user = :_iduser
				", Array(
					'_idaccount' => $get_account,
					'_iduser' => (int)$user['id']
				), 'fetch');
		}

		if(!empty($get_filter)) {
			if(!empty($user['timestamp_period_start']) AND !empty($user['timestamp_period_end'])) {
				$period = "AND timestamp_date >= '".$period_start."' AND timestamp_date < '".$period_end."'";

			} elseif(!empty($user['data_incomeday'])) {
				$period = "AND timestamp_date >= '".$period_incomeday."' AND timestamp_date < '".$period_incomeday_nextmonth_1."'";
			}
		}



		if(empty($get_type) AND empty($get_category) AND $get_filter != 'shared') {
			$count_items =
			sql("SELECT COUNT(id_user)
				 FROM items
				 WHERE id_user = :_iduser
				 ".(empty($get_account) ? "" : "AND id_account = '".$get_account."'")."
				 ".((!empty($get_filter) AND !empty($period) AND $get_filter == 'period') ? $period : '')."
				 ".((!empty($get_filter) AND $get_filter == 'not-payed') ? "AND check_expense_payed IS NULL ".$period : "")."
				 ".((!empty($get_filter) AND $get_filter == 'payed') ? "AND check_expense_payed IS NOT NULL ".$period : "")."
				 ".((!empty($get_filter) AND $get_filter == 'extra') ? "AND check_expense_extra IS NOT NULL ".$period : "")."
				 ".((!empty($get_filter) AND $get_filter == 'subscriptions') ? "AND check_expense_subscription IS NOT NULL" : "")."
				 ".((!empty($get_filter) AND $get_filter == 'future') ? "AND timestamp_date >= '".(!empty($user['timestamp_period_end']) ? $period_end : (!empty($user['data_incomeday']) ? $period_incomeday_nextmonth_1 : null))."'" : "")."
				 ".((!empty($get_filter) AND $get_filter == 'recurrent') ? "AND check_expense_recurrent IS NOT NULL" : "")."
				 ".((!empty($get_filter) AND $get_filter == 'date') ? "AND DATE(FROM_UNIXTIME(timestamp_date)) = '".$get_date_year."-".$get_date_month."-".$get_date_day."'" : "")."
				 AND is_expense = '1'
				", Array(
					'_iduser' => (int)$user['id']
				), 'count');

			if($count_items != 0) {
				$list_items =
				sql("SELECT *
					 FROM items
					 WHERE id_user = :_iduser
					 ".(empty($get_account) ? "" : "AND id_account = '".$get_account."'")."
					 ".((!empty($get_filter) AND isset($period) AND $get_filter == 'period') ? $period : '')."
					 ".((!empty($get_filter) AND $get_filter == 'not-payed') ? "AND check_expense_payed IS NULL ".$period : "")."
					 ".((!empty($get_filter) AND $get_filter == 'payed') ? "AND check_expense_payed IS NOT NULL ".$period : "")."
					 ".((!empty($get_filter) AND $get_filter == 'extra') ? "AND check_expense_extra IS NOT NULL ".$period : "")."
					 ".((!empty($get_filter) AND $get_filter == 'subscriptions') ? "AND check_expense_subscription IS NOT NULL" : "")."
					 ".((!empty($get_filter) AND $get_filter == 'future') ? "AND timestamp_date >= '".(!empty($user['timestamp_period_end']) ? $period_end : (!empty($user['data_incomeday']) ? $period_incomeday_nextmonth_1 : null))."'" : "")."
					 ".((!empty($get_filter) AND $get_filter == 'recurrent') ? "AND check_expense_recurrent IS NOT NULL" : "")."
					 ".((!empty($get_filter) AND $get_filter == 'date') ? "AND DATE(FROM_UNIXTIME(timestamp_date)) = '".$get_date_year."-".$get_date_month."-".$get_date_day."'" : "")."
					 AND is_expense = '1'
					 ORDER BY timestamp_date ASC
					", Array(
						'_iduser' => (int)$user['id']
					));
			}


		} else {
			if(!empty($get_type)) {
				$count_items =
				sql("SELECT COUNT(id_user)
					 FROM items
					 WHERE id_user = :_iduser
					 ".(empty($get_account) ? "" : "AND id_account = '".$get_account."'")."
					 AND data_type = :_type
					 AND is_expense = '1'
					", Array(
						'_iduser' => (int)$user['id'],
						'_type' => endecrypt($get_type)
					), 'count');

				if($count_items != 0) {
					$list_items =
					sql("SELECT *
						 FROM items
						 WHERE id_user = :_iduser
						 ".(empty($get_account) ? "" : "AND id_account = '".$get_account."'")."
						 AND data_type = :_type
						 AND is_expense = '1'
						 ORDER BY timestamp_date ASC
						", Array(
							'_iduser' => (int)$user['id'],
							'_type' => endecrypt($get_type)
						));
				}


			} elseif(!empty($get_category)) {
				$count_items =
				sql("SELECT COUNT(id_user)
					 FROM items
					 WHERE id_user = :_iduser
					 ".(empty($get_account) ? "" : "AND id_account = '".$get_account."'")."
					 AND data_category = :_category
					 AND is_expense = '1'
					", Array(
						'_iduser' => (int)$user['id'],
						'_category' => endecrypt($get_category)
					), 'count');

				if($count_items != 0) {
					$list_items =
					sql("SELECT *
						 FROM items
						 WHERE id_user = :_iduser
						 ".(empty($get_account) ? "" : "AND id_account = '".$get_account."'")."
						 AND data_category = :_category
						 AND is_expense = '1'
						 ORDER BY timestamp_date ASC
						", Array(
							'_iduser' => (int)$user['id'],
							'_category' => endecrypt($get_category)
						));
				}
			}
		}



	} elseif($get_item == 'debts') {
		$title = $lang['titles']['debts'];
		$name = $lang['words']['the-debt'];
		$object = 'debt';

		if(!empty($get_filter)) {
			if($get_filter == 'private') {
				$subtitle = $lang['words']['individuals'];
			} elseif($get_filter == 'companies') {
				$subtitle = $lang['words']['companies'];
			} elseif($get_filter == 'date') {
				$subtitle = $get_date_year.'-'.$get_date_month.'-'.$get_date_day;
			}
		}


		$count_items =
		sql("SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 AND is_debt = '1'
			 ".((!empty($get_filter) AND $get_filter == 'private') ? "AND check_debt_company IS NULL" : "")."
			 ".((!empty($get_filter) AND $get_filter == 'companies') ? "AND check_debt_company IS NOT NULL" : "")."
			 ".((!empty($get_filter) AND $get_filter == 'date') ? "AND timestamp_date = '".strtotime($get_date_year."-".$get_date_month."-".$get_date_day)."'" : "")."
			", Array(
				'_iduser' => (int)$user['id']
			), 'count');

		if($count_items != 0) {
			$list_items =
			sql("SELECT id, data_name, data_sum, data_currency, check_expense_payed, timestamp_date
				 FROM items
				 WHERE id_user = :_iduser
				 AND is_debt = '1'
				 ".((!empty($get_filter) AND $get_filter == 'private') ? "AND check_debt_company IS NULL" : "")."
				 ".((!empty($get_filter) AND $get_filter == 'companies') ? "AND check_debt_company IS NOT NULL" : "")."
				 ".((!empty($get_filter) AND $get_filter == 'date') ? "AND timestamp_date = '".strtotime($get_date_year."-".$get_date_month."-".$get_date_day)."'" : "")."
				 ORDER BY timestamp_date ASC
				", Array(
					'_iduser' => (int)$user['id']
				));
		}



	} elseif($get_item == 'loans') {
		$title = $lang['titles']['loan'];
		$object = 'loan';

		if(!empty($get_filter)) {
			if($get_filter == 'out') {
				$subtitle = $lang['words']['lend'];
			} elseif($get_filter == 'in') {
				$subtitle = $lang['words']['borrowed'];
			} elseif($get_filter == 'date') {
				$subtitle = $get_date_year.'-'.$get_date_month.'-'.$get_date_day;
			}
		}


		if($get_filter != 'shared') {
			$count_items =
			sql("SELECT COUNT(id_user)
				 FROM items
				 WHERE id_user = :_iduser
				 AND is_loan = '1'
				 ".((!empty($get_filter) AND $get_filter == 'out') ? "AND check_loan_out IS NOT NULL" : "")."
				 ".((!empty($get_filter) AND $get_filter == 'in') ? "AND check_loan_out IS NULL" : "")."
				 ".((!empty($get_filter) AND $get_filter == 'date') ? "AND timestamp_date = '".strtotime($get_date_year."-".$get_date_month."-".$get_date_day)."' OR timestamp_date_end = '".endecrypt(strtotime($get_date_year."-".$get_date_month."-".$get_date_day))."'" : "")."
				", Array(
					'_iduser' => (int)$user['id']
				), 'count');

			if($count_items != 0) {
				$list_items =
				sql("SELECT id, data_name, data_sum, data_currency, check_expense_payed, timestamp_date
					 FROM items
					 WHERE id_user = :_iduser
					 AND is_loan = '1'
					 ".((!empty($get_filter) AND $get_filter == 'out') ? "AND check_loan_out IS NOT NULL" : "")."
					 ".((!empty($get_filter) AND $get_filter == 'in') ? "AND check_loan_out IS NULL" : "")."
					 ".((!empty($get_filter) AND $get_filter == 'date') ? "AND timestamp_date = '".strtotime($get_date_year."-".$get_date_month."-".$get_date_day)."' OR timestamp_date_end = '".endecrypt(strtotime($get_date_year."-".$get_date_month."-".$get_date_day))."'" : "")."
					 ORDER BY timestamp_date ASC
					", Array(
						'_iduser' => (int)$user['id']
					));
			}
		}
	}



	if($object != 'all') {
		$count_shares =
		sql("SELECT COUNT(i.id_user)
			 FROM items i
			 JOIN shares s
			 ON i.id = s.id_item
			 WHERE s.id_user_with = :_iduser
			 AND i.is_".$object." IS NOT NULL
			", Array(
				'_iduser' => (int)$user['id']
			), 'count');

		if($get_filter == 'shared' AND $count_shares != 0) {
			$list_items =
			sql("SELECT *, i.id AS id_item, i.data_sum AS sum_item, s.data_sum AS sum_share
				 FROM items i
				 JOIN shares s
				 ON i.id = s.id_item
				 WHERE s.id_user_with = :_iduser
				 AND i.is_".$object." IS NOT NULL
				 ORDER BY i.timestamp_date ASC
				", Array(
					'_iduser' => (int)$user['id']
				));
		}
	}







	echo '<section id="list">';
		if($get_item == 'all') {
			echo '<h1>'.$filter_date_title.'</h1>';

			echo '<div>';
				if($count_items == 0) {
					echo '<div class="message">';
						echo $lang['messages']['no-items'];
					echo '</div>';


				} else {
					$arr_items = [];
					foreach($list_items AS $iteminfo) {
						if(!empty($iteminfo['is_expense'])) {
							$itemtype = 'expense';
							$name = $lang['words']['expense'];

						} elseif(!empty($iteminfo['is_debt'])) {
							$itemtype = 'debt';
							$name = $lang['words']['debt'];

						} elseif(!empty($iteminfo['is_loan'])) {
							$itemtype = 'loan';
							$name = $lang['words']['loan'];
						}

						if(empty($iteminfo['data_currency'])) {
							$sum = format_number(endecrypt($iteminfo['data_sum'], false));
							$sum .= svgicon('currency-'.mb_strtolower(endecrypt($user['data_currency'], false)));
	
						} else {
							if(endecrypt($iteminfo['data_currency'], false) == endecrypt($user['data_currency'], false)) {
								$sum = format_number(endecrypt($iteminfo['data_sum'], false));
								$sum .= svgicon('currency-'.mb_strtolower(endecrypt($user['data_currency'], false)));
	
							} else {
								$sum = currency(endecrypt($iteminfo['data_sum'], false), endecrypt($iteminfo['data_currency'], false));
							}
						}
	

						$arr_items[] = [
							'id' => (int)$iteminfo['id'],
							'date' => $iteminfo['timestamp_date'],
							'type' => [
								'id' => $itemtype,
								'name' => $name
							],
							'name' => endecrypt($iteminfo['data_name'], false),
							'sum' => $sum
						];
					}

					function cmp($a, $b){return strcmp($a['date'], $b['date']);}
					usort($arr_items, 'cmp');


					echo '<div class="list all">';

						echo '<div class="items">';
							echo '<div class="head">';
								echo '<div class="date">'.$lang['words']['date'].'</div>';
								echo '<div class="type">'.$lang['words']['type'].'</div>';
								echo '<div class="name">'.$lang['words']['name'].'</div>';
								echo '<div class="sum"></div>';
							echo '</div>';


							$c_items = 0;
							foreach($arr_items AS $item) {
								$c_items++;

								echo '<div class="data">';
									echo '<div class="date">';
										echo '<a href="'.url('filter/year:'.date('Y', $item['date'])).'">'.date('Y', $item['date']).'</a>-';
										echo '<a href="'.url('filter/year:'.date('Y', $item['date']).'/month:'.date('n', $item['date'])).'">'.date('m', $item['date']).'</a>-';
										echo '<a href="'.url('filter/year:'.date('Y', $item['date']).'/month:'.date('n', $item['date']).'/day:'.date('j', $item['date'])).'">'.date('d', $item['date']).'</a>';
									echo '</div>';

									echo '<div class="type">';
										echo $item['type']['name'];
									echo '</div>';


									echo '<div class="side-by-side">';
										echo '<div class="name">';
											echo '<a href="'.url($item['type']['id'].'s#'.$item['type']['id'].'-'.$item['id']).'">';
												echo $item['name'];
											echo '</a>';
										echo '</div>';

										echo '<div class="sum">'.$item['sum'].'</div>';
									echo '</div>';
								echo '</div>';
							}
						echo '</div>';

					echo '</div>';
				}
			echo '</div>';



		} else {
			echo '<h1'.($get_item == 'expenses' ? ' class="viewing-expenses"' : '').'>';
				echo $title;
				echo (empty($get_account) ? '' : svgicon('arrow-right-compact') . endecrypt($account['data_name'], false)) . (empty($subtitle) ? '' : svgicon('arrow-right-compact') . $subtitle);
			echo '</h1>';


			if($get_item == 'expenses') {
				echo '<div class="accounts">';
					echo '<div class="msg"></div>';

					echo '<div class="add-account">';
						echo '<div class="link side-by-side">';
							echo svgicon('account-add');
							echo '<a href="javascript:void(0)" class="add">'.$lang['nav']['sub']['create-directory'].'</a>';
						echo '</div>';

						echo '<form action="javascript:void(0)" method="POST">';
							echo '<input type="hidden" name="hidden-idaccount">';

							echo '<div class="item">';
								echo '<div class="field">';
									echo '<div class="icon">'.svgicon('name').'</div>';
									echo '<input type="text" name="field-account-name" placeholder="'.$lang['words']['name'].'">';
								echo '</div>';
							echo '</div>';

							echo '<div class="checkboxes">';
								echo checkbox($lang['forms']['checkboxes']['default-directory'], 'default', null, ($count_accounts == 0 ? true : false), ($count_accounts == 0 ? true : false));
							echo '</div>';

							echo '<div class="button add">';
								echo '<input type="submit" name="button-create" value="'.$lang['words']['buttons']['create'].'">';
								echo '<a href="javascript:void(0)" class="cancel">'.$lang['words']['cancel'].'</a>';
							echo '</div>';

							echo '<div class="button save">';
								echo '<input type="submit" name="button-save" value="'.$lang['words']['buttons']['save'].'">';
								echo '<a href="javascript:void(0)" class="cancel">'.$lang['words']['cancel'].'</a>';
							echo '</div>';
						echo '</form>';
					echo '</div>';


					echo '<div class="list-accounts side-by-side">';
						echo svgicon('account');

						echo '<div>';
							if($count_accounts == 0) {
								echo '<span class="empty">'.$lang['messages']['no-accounts'].'</span>';

							} else {
								echo '<a href="'.url('expenses/filter:period').'"';
								echo (empty($get_account) ? ' class="active"' : '');
								echo '>'.$lang['words']['all'].'</a>';

								foreach($get_accounts AS $accountlist) {
									echo '<a href="'.url('expenses/account:'.(int)$accountlist['id'].'/filter:period').'"';
									echo ($accountlist['id'] == $get_account ? ' class="active"' : '');
									echo '>';
										echo endecrypt($accountlist['data_name'], false);
									echo '</a>';
								}
							}
						echo '</div>';
					echo '</div>';


					if(!empty($get_account)) {
						echo '<div class="manage-account side-by-side">';
							echo svgicon('account-cog');

							echo '<div class="accounts">';
								echo '<a href="javascript:void(0)" class="edit" data-idaccount="'.(int)$account['id'].'" data-isdefault="'.(empty($account['is_default']) ? 'n' : 'y').'">'.$lang['words']['edit'].'</a>';
								echo '<a href="javascript:void(0)" class="delete color-red" data-idaccount="'.(int)$account['id'].'">'.$lang['words']['remove'].'</a>';
							echo '</div>';
						echo '</div>';
					}
				echo '</div>';
			}


			echo '<nav'.($get_item == 'expenses' ? ' class="viewing-expenses"' : '').'>';
				echo '<div class="manage">';
					echo '<a href="'.url('add-'.$object . (($object == 'expense' AND !empty($get_account)) ? '/account:'.$get_account : '')).'" id="add">'.svgicon('add') . $lang['nav']['sub']['add-'.$object].'</a>';
				echo '</div>';


				echo '<div class="links">';
					echo '<div>'.svgicon('filter').'</div>';

					echo '<div>';
						echo '<a href="'.url($get_item . (empty($get_account) ? '' : '/account:'.(int)$account['id'])).'"';
						if(empty($get_filter) AND $get_filter != 'date') {
							echo ' class="active"';
						}
						echo '>';
							echo $lang['nav']['filter']['all'];
						echo '</a>';


						if($get_item == 'expenses') {
							if((empty($user['timestamp_period_start']) AND empty($user['timestamp_period_start'])) AND empty($user['data_incomeday'])) {
								echo '<span class="inactive no-select" title="'.$lang['tooltips']['no-period'].'">'.$lang['nav']['filter']['period'].'</span>';

							} else {
								echo '<a href="'.url($get_item . (empty($get_account) ? '' : '/account:'.(int)$account['id']).'/filter:period').'"';
								echo ($get_filter == 'period' ? ' class="active"' : '');
								echo '>'.$lang['nav']['filter']['period'].'</a>';
							}


							echo '<a href="'.url($get_item . (empty($get_account) ? '' : '/account:'.(int)$account['id']).'/filter:not-payed').'"';
							echo ($get_filter == 'not-payed' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['not-payed'].'</a>';

							echo '<a href="'.url($get_item . (empty($get_account) ? '' : '/account:'.(int)$account['id']).'/filter:payed').'"';
							echo ($get_filter == 'payed' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['payed'].'</a>';

							echo '<a href="'.url($get_item . (empty($get_account) ? '' : '/account:'.(int)$account['id']).'/filter:extra').'"';
							echo ($get_filter == 'extra' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['extra'].'</a>';

							echo '<a href="'.url($get_item . (empty($get_account) ? '' : '/account:'.(int)$account['id']).'/filter:future').'"';
							echo ($get_filter == 'future' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['future'].'</a>';

							echo '<a href="'.url($get_item . (empty($get_account) ? '' : '/account:'.(int)$account['id']).'/filter:recurrent').'"';
							echo ($get_filter == 'recurrent' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['recurrent'].'</a>';

							echo '<a href="'.url($get_item . (empty($get_account) ? '' : '/account:'.(int)$account['id']).'/filter:subscriptions').'"';
							echo ($get_filter == 'subscriptions' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['subscriptions'].'</a>';

							echo '<a href="'.url($get_item . (empty($get_account) ? '' : '/account:'.(int)$account['id']).'/filter:shared').'"';
							echo ($get_filter == 'shared' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['shared'].' ('.format_number($count_shares, 0).')</a>';



						} elseif($get_item == 'debts') {
							echo '<a href="'.url($get_item.'/filter:private').'"';
							echo ($get_filter == 'private' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['private'].'</a>';

							echo '<a href="'.url($get_item.'/filter:companies').'"';
							echo ($get_filter == 'companies' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['companies'].'</a>';

							echo '<a href="'.url($get_item.'/filter:shared').'"';
							echo ($get_filter == 'shared' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['shared'].' ('.format_number($count_shares, 0).')</a>';



						} elseif($get_item == 'loans') {
							echo '<a href="'.url($get_item.'/filter:out').'"';
							echo ($get_filter == 'out' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['loans-out'].'</a>';

							echo '<a href="'.url($get_item.'/filter:in').'"';
							echo ($get_filter == 'in' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['loans-in'].'</a>';

							echo '<a href="'.url($get_item.'/filter:shared').'"';
							echo ($get_filter == 'shared' ? ' class="active"' : '');
							echo '>'.$lang['nav']['filter']['shared'].' ('.format_number($count_shares, 0).')</a>';
						}
					echo '</div>';
				echo '</div>';
			echo '</nav>';



			echo '<div>';
				if($get_filter != 'shared' AND $count_items == 0 OR $get_filter == 'shared' AND $count_shares == 0) {
					echo '<div class="message">';
						echo $lang['messages']['no-items'];
					echo '</div>';


				} else {
					echo '<div class="list">';

						echo '<div class="items">';
							echo '<div class="head">';
								echo '<div class="date';
								echo (($object == 'expense' AND !empty($user['check_option_hidedate_expenses'])) ? ' hidden' : '');
								echo (($object == 'debt' AND !empty($user['check_option_hidedate_debts'])) ? ' hidden' : '');
								echo (($object == 'loan' AND !empty($user['check_option_hidedate_loans'])) ? ' hidden' : '');
								echo '">';
									if($object == 'expense') {
										echo $lang['words']['dates']['expires-list'];
									} elseif($object == 'debt') {
										echo $lang['words']['dates']['started'];
									} elseif($object == 'loan') {
										echo $lang['words']['dates']['loan'];
									}
								echo '</div>';

								echo '<div class="name">'.$lang['words']['name'].'</div>';
								echo '<div class="sum"></div>';
							echo '</div>';


							$c_items = 0;
							foreach($list_items AS $item) {
								$is_shared = false;
								$c_items++;

								if($get_filter == 'shared') {
									$is_shared = true;

									$usershare =
									sql("SELECT id, data_saltedstring_1, data_saltedstring_2, data_currency
										FROM users
										WHERE id = :_iduser
										", Array(
											'_iduser' => (int)$item['id_user']
										), 'fetch');

									$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($usershare['data_saltedstring_1'] . (int)$usershare['id'] . $usershare['data_saltedstring_2']).'.json'), true);

									$share =
									sql("SELECT s.id_user,
												s.data_sum AS sum_share,
												s.data_payment,
												s.allow_markas_payed,
												s.share_notes,
												s.share_qrcodes,
												i.data_sum AS sum_item,
												i.data_currency,
												i.data_number_payment,
												i.data_number_ocr,
												i.data_notes AS item_notes

										FROM items i
										JOIN shares s
										ON i.id = s.id_item
										WHERE s.id_user_with = :_iduser
										AND s.id_item = :_iditem
										", Array(
											'_iduser' => (int)$user['id'],
											'_iditem' => (int)$item['id_item']
										), 'fetch');


								} else {
									$sharestatus =
									sql("SELECT COUNT(id_item)
										FROM shares
										WHERE id_item = :_iditem
										AND id_user = :_iduser
										", Array(
											'_iditem' => (int)$item['id'],
											'_iduser' => (int)$user['id']
										), 'count');
								}



								echo '<div class="data">';
									echo '<div class="date';
									echo (($object == 'expense' AND !empty($user['check_option_hidedate_expenses'])) ? ' hidden' : '');
									echo (($object == 'debt' AND !empty($user['check_option_hidedate_debts'])) ? ' hidden' : '');
									echo (($object == 'loan' AND !empty($user['check_option_hidedate_loans'])) ? ' hidden' : '');
									echo '">';
										echo '<a href="'.url($object.'s/filter-by-date:'.date_($item['timestamp_date'], 'date')).'" title="'.$lang['tooltips']['filter-by-date'].'">';
											echo date_($item['timestamp_date'], 'date');
										echo '</a>';
									echo '</div>';


									echo '<div class="side-by-side">';
										echo '<div class="name side-by-side';
										echo (($object == 'expense' AND !empty($user['check_option_hidedate_debts'])) ? ' no-date' : '');
										echo (($object == 'debt' AND !empty($user['check_option_hidedate_debts'])) ? ' no-date' : '');
										echo (($object == 'loan' AND !empty($user['check_option_hidedate_loans'])) ? ' no-date' : '');
										echo '">';
											if($object == 'expense' AND $get_filter != 'payed') {
												if($is_shared == false OR $is_shared == true AND !empty($share['allow_markas_payed'])) {
													echo '<div class="wait hidden" data-iditem="'.(int)($is_shared == true ? $item['id_item'] : $item['id']).'">'.svgicon('wait').'</div>';

													echo '<div class="status not-payed'.(($get_filter == 'payed' OR empty($item['check_expense_payed'])) ? '' : ' hidden').'" data-iditem="'.(int)($is_shared == true ? $item['id_item'] : $item['id']).'">';
														echo '<a href="javascript:void(0)" class="markas-payed" data-iditem="'.(int)($is_shared == true ? $share['id_item'] : $item['id']).'" title="'.$lang['tooltips']['markas-payed'].'">';
															echo svgicon('tick');
														echo '</a>';
													echo '</div>';

													echo '<div class="status payed'.(($get_filter == 'payed' OR empty($item['check_expense_payed'])) ? ' hidden' : null).'" data-iditem="'.(int)($is_shared == true ? $item['id_item'] : $item['id']).'">';
														echo '<a href="javascript:void(0)" class="markas-notpayed" data-iditem="'.(int)($is_shared == true ? $share['id_item'] : $item['id']).'" title="'.$lang['tooltips']['markas-notpayed'].'">';
															echo svgicon('tick');
														echo '</a>';
													echo '</div>';
												}
											}


											echo '<div>';
												echo '<div class="icons side-by-side'.((empty($item['data_notes']) AND ($is_shared == false AND $sharestatus == 0) AND (empty($item['data_number_payment']) AND empty($item['data_number_ocr']))) ? ' hidden' : null).'">';
													if($is_shared == true) {
														if(!empty($share['share_notes'])) {
															echo '<div class="notes'.(empty($share['item_notes']) ? ' hidden' : null).'" title="'.$lang['tooltips']['item-have-notes'].'">'.svgicon('notes').'</div>';
														}

													} else {
														echo '<div class="notes'.(empty($item['data_notes']) ? ' hidden' : null).'" title="'.$lang['tooltips']['item-have-notes'].'">'.svgicon('notes').'</div>';
													}

													if($is_shared == true) {
														if(!empty($share['share_qrcodes'])) {
															echo '<div class="qrcode'.((empty($share['data_number_payment']) AND empty($share['data_number_ocr'])) ? ' hidden' : null).'" title="'.$lang['tooltips']['item-has-qrcode'].'">'.svgicon('qrcode').'</div>';
														}

													} else {
														echo '<div class="qrcode'.((empty($item['data_number_payment']) AND empty($item['data_number_ocr'])) ? ' hidden' : null).'" title="'.$lang['tooltips']['item-has-qrcode'].'">'.svgicon('qrcode').'</div>';
													}

													echo '<div class="shared'.(($is_shared == false AND $sharestatus == 0) ? ' hidden' : null).'" title="'.$lang['tooltips']['item-has-been-shared'].'">'.svgicon('users').'</div>';
												echo '</div>';

												echo '<a href="javascript:void(0)" class="showinfo" data-object="'.$object.'" data-iditem="'.(int)($is_shared == true ? $item['id_item'] : $item['id']).'" title="'.$lang['tooltips']['show-details'].'">';
													echo endecrypt($item['data_name'], false);
												echo '</a>';
											echo '</div>';
										echo '</div>';


										echo '<div class="sum">';
											if(!empty($item['is_expense'])) {
												if($is_shared == false AND empty($item['data_currency']) OR $item['data_currency'] == $user['data_currency']) {
													$total_sum += endecrypt($item['data_sum'], false);
													echo format_number(endecrypt($item['data_sum'], false));
													echo svgicon('currency-'.mb_strtolower(endecrypt($item['data_currency'], false)));

												} else {
													if($is_shared == true) {
														if(endecrypt($share['data_payment'], false) == 'sum') {
															$total_sum += endecrypt($share['sum_share'], false);
															echo format_number(endecrypt($share['sum_share'], false));
					
														} elseif(endecrypt($share['data_payment'], false) == 'percent') {
															$total_sum += endecrypt($share['sum_item'], false) * (endecrypt($share['sum_share'], false) / 100);
															echo format_number(endecrypt($share['sum_item'], false) * (endecrypt($share['sum_share'], false) / 100));
					
														} else {
															$total_sum += endecrypt((empty($share['sum_share']) ? $share['sum_item'] : $share['sum_share']), false);
															echo format_number(endecrypt((empty($share['sum_share']) ? $share['sum_item'] : $share['sum_share']), false));
														}

														echo svgicon('currency-'.mb_strtolower(endecrypt(($is_shared == true ? $usershare['data_currency'] : $user['data_currency']), false)));


													} else {
														if(endecrypt($item['data_currency'], false) == endecrypt($user['data_currency'], false)) {
															$total_sum += endecrypt($item['data_sum'], false);
															echo format_number(endecrypt($item['data_sum'], false));
															echo svgicon('currency-'.mb_strtolower(endecrypt(($is_shared == true ? $usershare['data_currency'] : $user['data_currency']), false)));

														} else {
															$is_anothercurrency = true;
															$total_sum += currency(endecrypt($item['data_sum'], false), endecrypt($item['data_currency'], false), null, true, true);
															echo currency(endecrypt($item['data_sum'], false), endecrypt($item['data_currency'], false));
														}
													}
												}


											} else {
												if($is_shared == false) {
													$total_sum += endecrypt($item['data_sum'], false);
													echo format_number(endecrypt($item['data_sum'], false));
													echo svgicon('currency-'.mb_strtolower(endecrypt(($is_shared == true ? $usershare['data_currency'] : $user['data_currency']), false)));

												} else {
													if(endecrypt($share['data_payment'], false) == 'sum') {
														$total_sum += endecrypt($share['sum_share'], false);
														echo format_number(endecrypt($share['sum_share'], false));

													} elseif(endecrypt($share['data_payment'], false) == 'percent') {
														$total_sum += endecrypt($share['sum_item'], false) * (endecrypt($share['sum_share'], false) / 100);
														echo format_number(endecrypt($share['sum_item'], false) * (endecrypt($share['sum_share'], false) / 100));

													} else {
														$total_sum += endecrypt((empty($share['sum_share']) ? $share['sum_item'] : $share['sum_share']), false);
														echo format_number(endecrypt((empty($share['sum_share']) ? $share['sum_item'] : $share['sum_share']), false));
													}

													echo svgicon('currency-'.mb_strtolower(endecrypt(($is_shared == true ? $usershare['data_currency'] : $user['data_currency']), false)));
												}
											}
										echo '</div>';
									echo '</div>';
								echo '</div>';
							}



							echo '<div class="summary">';
								echo '<div class="total';
								echo (($object == 'expense' AND !empty($user['check_option_hidedate_expenses'])) ? ' no-date' : '');
								echo (($object == 'debt' AND !empty($user['check_option_hidedate_debts'])) ? ' no-date' : '');
								echo (($object == 'loan' AND !empty($user['check_option_hidedate_loans'])) ? ' no-date' : '');
								echo '">';
									echo $lang['words']['total'].' '.$c_items.' '.mb_strtolower(($c_items == 1 ? $lang['words']['piece'] : $lang['words']['pieces']));
								echo '</div>';

								echo '<div class="sum">';
									if($is_anothercurrency == true) {
										echo $lang['words']['approx'].' ';
									}

									echo format_number($total_sum) . svgicon('currency-'.mb_strtolower(endecrypt(($is_shared == true ? $usershare['data_currency'] : $user['data_currency']), false)));
								echo '</div>';
							echo '</div>';
						echo '</div>';

					echo '</div>';



					echo '<div class="details">';
						echo '<div class="message">';
							echo '<span class="choose">'.$lang['messages']['choose-item'].'</span>';
							echo '<span class="loading">'.$lang['messages']['loading-item-details'].'</span>';
						echo '</div>';

						echo '<div class="content"></div>';
					echo '</div>';
				}
			echo '</div>';
		}
	echo '</section>';







	require_once 'site-footer.php';

?>