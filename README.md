# Keizai
Keizai is a financial overview site where the focus on privacy is prioritized. The idea with the website is that nothing should be added automatically from, for example, one's bank. Instead, you enter all the expenses you have for the period in question, as well as your debts and if you have, for example, lent to someone.

## How it works
When you create a new account, Keizai will create a file containing your encryption keys. All data you enter, except for the username (which is encrypted using the website's own encryption keys), is encrypted using these keys.

Even if your encryption keys are stored on the same server managed by the developer, your data will be safe from prying eyes. The developer doesn't want to know your encryption keys - they're yours for a reason.

You can export your data in either clear text or encrypted at any time. You can also delete all your data and start over, or simply delete your account whenever you want. You decide!

## Features
- Works fully offline within a local network or desktop/laptop.
- Detailed overview of your economic situation.
  - See how much money you have left after all expenses for the selected period have been payed.
  - Weekly budget and daily budget.
- Add, edit and delete expenses, debts, loans, and/or budgets.
- Securing your data with AES-256.
- Export your data to an XLSX file.
- Export and import your data to a JSON file.
- Download a PDF file showing your expenses, debts and loans. Perfect for keeping track of how your finances have changed for better or worse.
- Select when you get your income between 2 dates (from and to) or a specific date (for an example 18th each month).
- Secure your account with two-step authentication (2FA).
- Share expenses, debts, or loans to other users.
  - Choose what the recipient will see.
  - Share the whole amount, selected amount, or percentage of the amount.
- Take screenshots of selected elements.
- Write notes for items or on the Notes page.
  - Create rich notes with Markdown.
- Enter the payment number and OCR number for an expense to let Keizai generate a QR code that you can scan to quickly pay the expense.
- Categorise your expenses.
- Create an account for expenses if you have 2 different bank accounts.
  - Select what expense that are for what account.
- Filter items after the following:
  - Date
  - Category
  - Type
  - Account
  - Shared
  - And for expenses only:
    - Period (if entered)
    - Not payed
    - Payed
    - Extra
    - Future
    - Recurrent
- ...and more.

## Hidden language switch when not signed in
By default, the start page is displayed in Swedish, as Keizai is a Swedish-developed website with a Swedish domain (.se). But if you add `?lang=en` to the end of the address bar (`keizai.se?lang=en`), the home page will be displayed in English. And when you create an account with this variable, your account will have English as the default language.

## The project is not finished
The development of Keizai is ongoing. Bugs and issues can occur.

## Install Keizai on your own server
If you don't trust the developer with your encryption keys, you can install Keizai on your own server. The [INSTALL.md](https://codeberg.org/airikr/Keizai/src/branch/main/INSTALL.md) file will explain the process.

## Demo account
Go to [keizai.se?lang=en](https://keizai.se?lang=en) and login with `demo` as username and `demo` as password. If Keizai switches to the Swedish language, go to `Inställningar` (Settings) in the menu and change the language for the `Språk` (Language) option.

A crontab deletes the account and creates it again every hour. [See the source code of the file](https://codeberg.org/airikr/Keizai/src/branch/main/cronjobs/demo-account.php) to learn how this works.

## Issues? Bugs?
If you encounter any problems, create an issue.