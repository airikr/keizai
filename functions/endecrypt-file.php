<?php

	function endecrypt_file($file, $encrypt = true) {
		global $enc, $dir_encfiles, $encryption_method;

		$encryption_key = base64_decode($enc['keys'][0]);

		if($encrypt == true) {
			$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($encryption_method));
			$encrypted = openssl_encrypt(file_get_contents($dir_encfiles.'/'.$file), $encryption_method, $encryption_key, 0, $iv);
			file_put_contents($dir_encfiles.'/'.$file.'.enc', base64_encode($encrypted.'::'.$iv));

		} else {
			list($encrypted_data, $iv) = explode('::', base64_decode(file_get_contents($dir_encfiles.'/'.$file.'.enc')), 2);
			$decrypt = openssl_decrypt($encrypted_data, $encryption_method, $encryption_key, 0, $iv);
			file_put_contents($dir_encfiles.'/'.$file, $decrypt);
		}
	}

?>