<?php

	function safetag($string) {
		return strip_tags(htmlspecialchars($string));
	}

?>