<?php

	function url($string, $changelang = false) {
		global $protocol, $host, $uri, $is_local, $config_folder, $get_language;

		$url_parsed = parse_url($protocol.'://'.$host . $uri);
		$url_exploded = explode('/', $url_parsed['path']);
		$basename = str_replace('?lang=en', '', basename($_SERVER['REQUEST_URI']));
		$change_language = (($changelang == false AND !empty($get_language)) ? '?lang='.$get_language : null) . (($changelang == true AND !empty($basename)) ? '/'.$basename : null);

		return (empty($config_folder) ? '/' : '/'.$config_folder.'/') . $string . $change_language;
	}

?>