<?php

	function format_number($string, $decimal = 2, $symbol = ',', $thousands = ' ') {
		return number_format($string, $decimal, $symbol, $thousands);
	}

?>