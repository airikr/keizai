<?php

	function simplepage($string, $is_email = true, $error = false) {
		global $site_title, $site_favicon;

		$content = '<!DOCTYPE html>';
		$content .= '<html lang="sv">';
			$content .= '<head>';
				$content .= '<title>'.$site_title.' - '.(empty($is_email) ? 'Oops' : 'E-postmeddelande').'</title>';

				$content .= '<meta charset="UTF-8">';
				$content .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, maximum-scale=3">';

				$content .= '<link rel="icon" href="'.$site_favicon.'">';

				$content .= '<style>';
					$content .= 'html{background-color:#1e2326;color:#e8e5d5;font-family:"Arial",sans-serif;font-size:0.980rem;margin:0;padding:0;}';
					$content .= 'body{max-width:700px;line-height:150%;padding:20px 40px;}';
					$content .= 'a{color:#d3c6aa;text-decoration:underline;}';
					$content .= 'p{hyphens:auto;line-height:1.5rem;margin:0;padding:10px 0;text-align:left;word-wrap:break-word;-moz-hyphens:auto;-webkit-hyphens:auto;-o-hyphens:auto;}';
					$content .= 'h1{font-size:190%;font-weight:700;line-height:160%;margin:0 0 5px 0;padding:0;}';
					$content .= 'code{font-family:"Victor Mono";font-size:90%;line-height:100%;}';
					$content .= '.color-red{color:#e67e80;}';
				$content .= '</style>';
			$content .= '</head>';

			$content .= '<body'.($error == false ? '' : ' class="color-red"').'>';
				$content .= $string;
			$content .= '</body>';
		$content .= '</html>';



		return $content;
	}

?>