<?php

	function period($string, $nextmonth = false, $extramonths = 0) {
		$option_allow_weekends = (empty($user['check_option_allow_weekends']) ? false : true);
		$incomeday = (strtotime($string) ? null : (date('d') > $string ? date('Y-m') : date('Y-m', strtotime('-1 month'))).'-');
		$get_date = date('Y-m-d', strtotime($incomeday . $string . ($nextmonth == false ? null : ' +'.($extramonths == 0 ? '1' : (int)$extramonths).' month')));
		$get_weekday = date('D', strtotime($incomeday . $string . ($nextmonth == false ? null : ' +'.($extramonths == 0 ? '1' : (int)$extramonths).' month')));

		if(strtotime($string)) {
			$is_weekend = (($get_weekday == 'Sat' OR $get_weekday == 'Sun') ? true : false);
		} else {
			$is_weekend = (($get_weekday == 'Sat' OR $get_weekday == 'Sun') ? true : false);
		}

		#echo $get_date.': '.($is_weekend == true ? 'Ja' : 'Nej').' ('.$get_weekday.')';

		if($option_allow_weekends == false AND $is_weekend == true AND $get_weekday == 'Sat') {
			return strtotime($get_date.' -1 day');
		} elseif($option_allow_weekends == false AND $is_weekend == true AND $get_weekday == 'Sun') {
			return strtotime($get_date.' +1 day');
		} else {
			return strtotime($get_date);
		}
	}

?>