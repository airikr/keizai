<?php

	function whatisthis($element) {
		global $lang;

		$wit = '<a href="javascript:void(0)" class="wit" data-element="'.$element.'" title="'.$lang['tooltips']['what-is-this'].'" data-html2canvas-ignore>';
			$wit .= svgicon('question');
		$wit .= '</a>';

		echo $wit;
	}

?>