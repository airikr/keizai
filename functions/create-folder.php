<?php

	function create_folder($string) {
		if(!file_exists($string)) {
			$oldmask = umask(0);
			mkdir($string, 0777, true);
			umask($oldmask);
		}
	}

?>