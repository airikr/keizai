<?php

	function x_left($date_1, $date_2, $x = 'days') {
		if($date_1 < time()) {
			$start_date = date('Y-m-d');
		} else {
			$start_date = date_($date_1, 'date');
		}

		$d1 = new DateTime($start_date);
		$d2 = new DateTime(date_($date_2, 'date'));
		$diff = $d2->diff($d1);

		if($x == 'days') {
			return $diff->days;

		} elseif($x == 'months') {
			return ($diff->format('%m') == 0 ? 1 : $diff->format('%m'));

		} else {
			return 'only days or months';
		}
	}

?>