<?php

	function currency($float, $currency, $usercurrency = null, $pure = false, $convert = false) {
		global $user, $lang;

		$check_existence =
		sql("SELECT COUNT(data_currency_from)
			 FROM currencies
			 WHERE data_currency_from = :_from
			", Array(
				'_from' => strtoupper((empty($usercurrency) ? endecrypt($user['data_currency'], false) : $usercurrency))
			), 'count');


		if($check_existence != 0) {
			$rate =
			sql("SELECT data_rate
				 FROM currencies
				 WHERE data_currency_from = :_from
				 AND data_currency_to = :_to
				", Array(
					'_from' => $currency,
					'_to' => strtoupper((empty($usercurrency) ? endecrypt($user['data_currency'], false) : $usercurrency))
				), 'fetch');


			if($pure == false) {
				if($check_existence == 0) {
					$currency_value = '<span class="currency-website">'.svgicon('currency-'.mb_strtolower($currency)).'</span>';
					$currency_value .= '<span class="currency-screenshot">'.mb_strtoupper($currency).'</span>';

					return format_number($float) . $currency_value;

				} else {
					$currency_value = '<span class="currency-website">'.$lang['words']['approx'].' '.format_number(($float * $rate['data_rate'])) . svgicon('currency-'.mb_strtolower(endecrypt($user['data_currency'], false))).'</span>';
					$currency_value .= '<span class="currency-screenshot">'.format_number($float).'<span class="currency">'.mb_strtoupper($currency).'</span></span>';

					return ($convert == false ? $currency_value : ($float * $rate['data_rate']));
				}


			} else {
				return ($check_existence == 0 ? $float : ($float * $rate['data_rate']));
			}


		} else {
			return ($pure == false ? format_number($float, 2, '.', '') : $float);
		}
	}

?>