<?php

	function date_($timestamp, $format) {
		global $get_dateformat, $get_daysuffix, $lang;

		$months = Array(
			1 => $lang['months']['january'],
			2 => $lang['months']['february'],
			3 => $lang['months']['march'],
			4 => $lang['months']['april'],
			5 => $lang['months']['may'],
			6 => $lang['months']['june'],
			7 => $lang['months']['july'],
			8 => $lang['months']['august'],
			9 => $lang['months']['september'],
			10 => $lang['months']['october'],
			11 => $lang['months']['november'],
			12 => $lang['months']['december']
		);


		if($format == 'datetime') {
			return date('Y-m-d, H:i', $timestamp);

		} elseif($format == 'date') {
			return date('Y-m-d', $timestamp);

		} elseif($format == 'time') {
			return date('H:i', $timestamp);

		} elseif($format == 'year') {
			return date('Y', $timestamp);

		} elseif($format == 'month') {
			return $months[date('n', $timestamp)];

		} elseif($format == 'day-month') {
			return date('j', $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]);

		} elseif($format == 'month-year') {
			return $months[date('n', $timestamp)].' '.date('Y', $timestamp);

		} elseif($format == 'day-month-year') {
			return date('j', $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]).', '.date('Y', $timestamp);

		} elseif($format == 'day-month-year-time') {
			return date('j', $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]).' '.date('Y', $timestamp).', kl. '.date('H:i', $timestamp);
		}
	}

?>