<?php

	/*use ParagonIE\Halite\KeyFactory;
	use ParagonIE\Halite\Symmetric\Crypto as Symmetric;
	use ParagonIE\HiddenString\HiddenString;*/

	function endecrypt($string, $encrypt = true, $websites = false) {
		global $dir_files, $dir_userfiles, $user, $site_title, $config_encryptionmethod;

		/*if(!empty($is_loggedin) AND file_exists($dir_userfiles.'/'.hash('sha256', $user['data_username']).'.key') AND !empty($user['is_upgraded'])) {
			if($websites == false) {
				$encryptionkey = KeyFactory::loadEncryptionKey($dir_userfiles.'/'.hash('sha256', $user['data_username']).'.key');
			} else {
				$encryptionkey = KeyFactory::loadEncryptionKey($dir_files.'/'.hash('sha256', $site_title).'.key');
			}

			if($encrypt == true) {
				$ciphertext = Symmetric::encrypt(new HiddenString($string), $encryptionkey);
				return $ciphertext;

			} else {
				$decrypted = Symmetric::decrypt($string, $encryptionkey);
				return $decrypted->getString();
			}



		} else {*/
			if($websites == false) {
				global $enc;

				if($encrypt == true) {
					if(empty($string)) {
						return 'encr.err';

					} else {
						return base64_encode(openssl_encrypt(
							$string,
							$config_encryptionmethod,
							hash('sha256', $enc['keys'][0]),
							0,
							substr(hash('sha256', $enc['keys'][1]), 0, 16)
						));
					}

				} else {
					if(empty($string)) {
						return 'dencr.err';

					} else {
						return openssl_decrypt(
							base64_decode($string),
							$config_encryptionmethod,
							hash('sha256', $enc['keys'][0]),
							0,
							substr(hash('sha256', $enc['keys'][1]), 0, 16)
						);
					}
				}



			} else {
				global $encryption_key_1, $encryption_key_2, $encryption_salt_1, $encryption_salt_2, $encryption_salt_3, $encryption_salt_4;

				if($encrypt == true) {
					return base64_encode(openssl_encrypt(
						$string,
						$config_encryptionmethod,
						hash('sha256', $encryption_salt_1 . $encryption_key_1 . $encryption_salt_2),
						0,
						substr(hash('sha256', $encryption_salt_4 . $encryption_key_2 . $encryption_salt_3), 0, 16)
					));

				} else {
					return openssl_decrypt(
						base64_decode($string),
						$config_encryptionmethod,
						hash('sha256', $encryption_salt_1 . $encryption_key_1 . $encryption_salt_2),
						0,
						substr(hash('sha256', $encryption_salt_4 . $encryption_key_2 . $encryption_salt_3), 0, 16)
					);
				}
			}
		#}
	}

?>