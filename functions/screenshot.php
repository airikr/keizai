<?php

	function screenshot($capture, $fname, $classes = null, $nopadding = false) {
		global $lang;

		echo '<div class="screenshot" data-html2canvas-ignore>';
			echo '<div class="wait">'.svgicon('wait').'</div>';
			echo '<div class="link">';
				echo '<a href="javascript:void(0)" class="download'.(empty($classes) ? '' : ' '.$classes).'" ';
				echo 'data-classname="'.$capture.'" ';
				echo 'data-filename="'.$fname.'" ';
				echo 'data-nopadding="'.($nopadding == false ? '0' : '1').'" ';
				echo 'title="'.$lang['tooltips']['take-screenshot'].'">';
					echo svgicon('screenshot');
				echo '</a>';
			echo '</div>';
		echo '</div>';
	}

?>