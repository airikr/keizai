<?php

	function log_action($code, $userid = null, $sessionid = null, $iditem = null, $isexpense = null, $isdebt = null, $isloan = null, $isnotes = null, $istfa_enabled = null, $istfa_disabled = null, $isexporting = null, $isimporting = null, $isdeletedata = null) {
		global $user, $session;

		sql("INSERT INTO sess_actions(
				 id_user,
				 id_session,
				 id_item,
				 data_code,
				 is_expense,
				 is_debt,
				 is_loan,
				 is_notes,
				 is_tfa_enabled,
				 is_tfa_disabled,
				 is_exporting,
				 is_importing,
				 is_delete_data,
				 timestamp_occurred
			 )

			 VALUES(
				 :_iduser,
				 :_idsession,
				 :_iditem,
				 :_code,
				 :_isexpense,
				 :_isdebt,
				 :_isloan,
				 :_isnotes,
				 :_istfa_enabled,
				 :_istfa_disabled,
				 :_isexporting,
				 :_isimporting,
				 :_isdeletedata,
				 :_occurred
			 )
			", Array(
				'_iduser' => (empty($userid) ? (int)$user['id'] : (int)$userid),
				'_idsession' => (empty($sessionid) ? (int)$session['id'] : (int)$sessionid),
				'_iditem' => (empty($iditem) ? null : (int)$iditem),
				'_code' => $code,
				'_isexpense' => (empty($isexpense) ? null : (int)$isexpense),
				'_isdebt' => (empty($isdebt) ? null : (int)$isdebt),
				'_isloan' => (empty($isloan) ? null : (int)$isloan),
				'_isnotes' => (empty($isnotes) ? null : (int)$isnotes),
				'_istfa_enabled' => (empty($istfa_enabled) ? null : (int)$istfa_enabled),
				'_istfa_disabled' => (empty($istfa_disabled) ? null : (int)$istfa_disabled),
				'_isexporting' => (empty($isexporting) ? null : (int)$isexporting),
				'_isimporting' => (empty($isimporting) ? null : (int)$isimporting),
				'_isdeletedata' => (empty($isdeletedata) ? null : (int)$isdeletedata),
				'_occurred' => time()
			), 'insert');
	}

?>