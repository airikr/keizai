<?php

	function sql($query, $array, $method = '') {

		global $sql;


		try {
			$prepare = $sql->prepare($query);

			if($method == 'count') {
				$prepare->execute($array);
				$data = $prepare->fetchColumn();
				return $data;


			} elseif($method == 'fetch') {
				$prepare->execute($array);
				$data = $prepare->fetch(PDO::FETCH_ASSOC);
				return $data;


			} elseif($method == 'insert') {
				foreach($array AS $data => $value) {
					$prepare->bindValue(':'.$data, $value);
				}

				$prepare->execute();
				return $value;


			} else {
				$prepare->execute($array);
				return $prepare;
			}


		} catch(Exception $e) {
			throw($e);
		}

	}

?>