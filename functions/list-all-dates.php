<?php

	function listalldates($from, $to) {
		$from = DateTime::createFromFormat('Y-m-d', $from);
		$to = DateTime::createFromFormat('Y-m-d', $to);

		return new DatePeriod(
			$from,
			new DateInterval('P1D'),
			$to->modify('+1 day')
		);
	}

?>