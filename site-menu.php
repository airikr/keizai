<?php

	if(!empty($is_loggedin)) {
		echo '<nav>';
			echo '<a href="'.url('overview').'" class="logo">';
				echo '<img src="'.url('images/logo.svg').'">';

				echo '<div class="sitename">';
					echo '<div class="title">';
						echo $site_title;
					echo '</div>';

					echo '<div class="meaning side-by-side">';
						echo '<span>'.$lang['words']['sitename-meaning'].'</span>';
					echo '</div>';
				echo '</div>';
			echo '</a>';



			echo '<div class="menu">';
				echo '<a href="'.url('overview').'">';
					echo $lang['nav']['signed-in']['overview'];
				echo '</a>';



				echo '<div class="space"></div>';



				echo '<div>';
					echo '<a href="'.url('expenses'.($count_accounts == 0 ? '' : '/account:'.(int)$default_account['id']) . (((empty($user['timestamp_period_start']) AND empty($user['timestamp_period_start'])) AND empty($user['data_incomeday'])) ? '' : '/filter:period')).'"';
					echo ' aria-label="'.$lang['aria-labels']['menu']['overview'].'"';
					echo '>';
						echo $lang['nav']['signed-in']['expenses'];
						echo '<div class="line"></div>';
						echo '<div class="count">';
							echo format_number($count_m['expenses'], 0);
						echo '</div>';
					echo '</a>';
				echo '</div>';

				echo '<div>';
					echo '<a href="'.url('debts').'"';
					echo ' aria-label="'.$lang['aria-labels']['menu']['debts'].'"';
					echo '>';
						echo $lang['nav']['signed-in']['debts'];
						echo '<div class="line"></div>';
						echo '<div class="count">';
							echo format_number($count_m['debts'], 0);
						echo '</div>';
					echo '</a>';
				echo '</div>';

				echo '<div>';
					echo '<a href="'.url('loans').'"';
					echo ' aria-label="'.$lang['aria-labels']['menu']['loans'].'"';
					echo '>';
						echo $lang['nav']['signed-in']['loans'];
						echo '<div class="line"></div>';
						echo '<div class="count">';
							echo format_number($count_m['loans'], 0);
						echo '</div>';
					echo '</a>';
				echo '</div>';

				echo '<a href="'.url('budget').'"';
				echo ' aria-label="'.$lang['aria-labels']['menu']['budget'].'"';
				echo '>';
					echo $lang['nav']['signed-in']['budget'];
				echo '</a>';



				echo '<div class="space"></div>';



				echo '<a href="'.url('notes').'"';
				echo ' aria-label="'.$lang['aria-labels']['menu']['notes'].'"';
				echo '>';
					echo (empty($user['data_notes']) ? null : '<div class="dot"></div>');
					echo $lang['nav']['signed-in']['notes'];
				echo '</a>';

				echo '<div>';
					if($config_solomember == false) {
						echo '<a href="'.url('shares').'"';
						echo ' aria-label="'.$lang['aria-labels']['menu']['shares'].'"';
						echo '>';
							echo $lang['nav']['signed-in']['shares'];
							echo '<div class="line"></div>';
							echo '<div class="count">';
								echo number_format($count_newshares, 0, '', ' ');
							echo '</div>';
						echo '</a>';

					} else {
						echo '<div class="disabled" title="'.$lang['tooltips']['shared-navigation-disabled-solo'].'"';
						echo ' aria-label="'.$lang['aria-labels']['menu']['shares'].'"';
						echo '>';
							echo $lang['nav']['signed-in']['shares'];
							echo '<div class="line"></div>';
							echo '<div class="count">';
								echo number_format($count_newshares, 0, '', ' ');
							echo '</div>';
						echo '</div>';
					}
				echo '</div>';

				echo '<a href="'.url('settings').'"';
				echo ' aria-label="'.$lang['aria-labels']['menu']['settings'].'"';
				echo '>';
					echo $lang['nav']['signed-in']['settings'];
				echo '</a>';



				echo '<div class="space"></div>';



				if(!empty($user['is_admin'])) {
					echo '<a href="'.url('admin').'"';
					echo ' aria-label="'.$lang['aria-labels']['menu']['admin'].'"';
					echo '>';
						echo $lang['nav']['signed-in']['admin'];
					echo '</a>';
				}



				echo '<div class="space"></div>';



				echo '<div>';
					echo '<div class="signed-in">';
						echo '<a href="javascript:void(0)" class="signout" data-signout="'.$lang['modals']['sign-out'].'"';
						echo ' aria-label="'.$lang['aria-labels']['menu']['logout'].'"';
						echo '>';
							echo '<div class="color-red">'.$lang['nav']['signed-in']['sign-out'].'</div>';
							echo '<div class="line"></div>';
							echo '<div class="timer time">';
								echo '00:00';
							echo '</div>';
						echo '</a>';
					echo '</div>';

					echo '<div class="signing-out">';
						echo $lang['messages']['signing-out'];
					echo '</div>';
				echo '</div>';
			echo '</div>';

		echo '</nav>';
	}

?>