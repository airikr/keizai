$(document).ready(function() {


	$('body').on('click', '.check > a', function() {
		$.ajax({
			url: folder_name + 'ajax/check-version.php',
			method: 'GET',
			async: true,
			beforeSend: function() {
				$('.check').hide();
				$('.working').show();
			},
			success: function(s) {
				setTimeout(function() {
					$('.check-for-updates > .working').html(s);
				}, 100);
			},
			error: function(e) {
				console.log(e)
			}
		});
	});


});