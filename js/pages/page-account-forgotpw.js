$(document).ready(function() {


	$('body').on('click', '[name="button-finish"]', function() {
		let form = new FormData($('form')[0]);

		$.ajax({
			url: folder_name + 'ajax/account-forgotpw.php?act=update',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					$('[name*="password"]').val('');

					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error');

					} else if(s == 'user-notexists') {
						msg(lang_msg_errors_username_notexists, 'error');

					} else if(s == 'email-notexists') {
						msg(lang_msg_errors_email_notexists, 'error');

					} else if(s == 'password-notsame') {
						msg(lang_msg_errors_pw_notsame, 'error');

					} else {
						msg(lang_msg_done_updatedpw, 'done');

						setTimeout(function() {
							window.location = folder_name;
						}, 800);
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
				console.log(e);
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});


});