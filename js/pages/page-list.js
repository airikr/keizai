$(document).ready(function() {


	$('body').on('click', '.showinfo', function() {
		let object = $(this).data('object');
		let iditem = $(this).data('iditem');

		$.ajax({
			url: folder_name + 'ajax/item-details.php?ite=' + object + '&idi=' + iditem,
			method: 'GET',
			beforeSend: function() {
				$('.message').show();
				$('.message > .choose').hide();
				$('.message > .loading').show();
				$('.details > .content').hide();

				$('html,body').animate({
					scrollTop: ($('.details').offset().top - scrollto)
				}, 10);
			},

			success: function(s) {
				setTimeout(function() {
					if(s == 'not-found') {
						$('.message > .loading').hide();
						$('.message > .choose').show();
						history.pushState('', document.title, window.location.pathname);

					} else {
						$('.message').hide();
						$('.details > .content').show().html(s);
						if(is_portable == false) { $('.details > .highlight').show(); }

						setTimeout(function() {
							if(is_portable == false) { $('.details > .highlight').fadeOut(250); }

							history.pushState('', document.title, window.location.pathname + (iditem == 0 ? '' : '#' + object + '-' + iditem));
						}, 100);

						$('html,body').animate({
							scrollTop: ($('.details').offset().top - scrollto)
						}, 100);



						if(($('.code').is(':visible') && $('.item.payment').is(':visible') && $('.item.payment > .value').text() != '-') &&
							($('.code').is(':visible') && $('.item.ocr').is(':visible') && $('.item.ocr > .value').text() != '-')) {
							let item_class = '.code';
							let qr_nme = $(item_class).data('nme');
							let qr_ocr = $(item_class).data('ocr');
							let qr_ddt = $(item_class).data('ddt');
							let qr_acc = $(item_class).data('acc');
							let qr_pt = $(item_class).data('pt');
							let qr_due = $(item_class).data('due');

							qrcode(item_class, 200, '{"uqr":1,"tp":1,"nme":"' + qr_nme + '","cid":"' + qr_ocr + '","iref":"' + qr_ocr + '","ddt":"' + qr_ddt + '","due":' + qr_due + ',"pt":"' + qr_pt + '","acc":"' + qr_acc + '"}');
						}

						if($('.item.phone > .value').text() != '-') {
							qrcode('.swish', 170, 'https://app.swish.nu/1/p/sw/?sw=' + $('.swish').data('sw') + '&amt=' + $('.swish').data('amt') + '&cur=SEK&msg=&edit=amt,msg&src=qr', folder_name + 'images/swish.svg');
						}
					}
				}, 200);
			},

			error: function(e) {
				$('.message > .loading').hide();
				$('.details > .content').show().html('<div class="message color-red">' + lang_msg_error_cantfetchinfo + '</div>');
			}
		});
	});



	if(window.location.hash) {
		let hash_split = window.location.hash.split('-');
		let object = hash_split[0].substring(1);
		let iditem = hash_split[1];



		$.ajax({
			url: folder_name + 'ajax/item-details.php?ite=' + object + '&idi=' + iditem,
			method: 'GET',
			beforeSend: function() {
				$('.message').show();
				$('.message > .choose').hide();
				$('.message > .loading').show();
				$('.details > .content').hide();

				$('html,body').animate({
					scrollTop: ($('.details').offset().top - scrollto)
				}, 10);
			},

			success: function(s) {
				setTimeout(function() {
					if(s == 'not-found') {
						$('.message > .loading').hide();
						$('.message > .choose').show();
						history.pushState('', document.title, window.location.pathname);

					} else {
						$('.message').hide();
						$('.details > .content').show().html(s);
						if(is_portable == false) { $('.details > .highlight').show(); }

						setTimeout(function() {
							if(is_portable == false) { $('.details > .highlight').fadeOut(250); }
						}, 100);

						$('html,body').animate({
							scrollTop: ($('.details').offset().top - scrollto)
						}, 100);



						if(($('.code').is(':visible') && $('.item.payment').is(':visible') && $('.item.payment > .value').text() != '-') &&
							($('.code').is(':visible') && $('.item.ocr').is(':visible') && $('.item.ocr > .value').text() != '-')) {
							let item_class = '.code';
							let qr_nme = $(item_class).data('nme');
							let qr_ocr = $(item_class).data('ocr');
							let qr_ddt = $(item_class).data('ddt');
							let qr_acc = $(item_class).data('acc');
							let qr_pt = $(item_class).data('pt');
							let qr_due = $(item_class).data('due');

							qrcode(item_class, 200, '{"uqr":1,"tp":1,"nme":"' + qr_nme + '","cid":"' + qr_ocr + '","iref":"' + qr_ocr + '","ddt":"' + qr_ddt + '","due":' + qr_due + ',"pt":"' + qr_pt + '","acc":"' + qr_acc + '"}');
						}

						if($('.item.phone > .value').text() != '-') {
							qrcode('.swish', 170, 'https://app.swish.nu/1/p/sw/?sw=' + $('.swish').data('sw') + '&amt=' + $('.swish').data('amt') + '&cur=SEK&msg=&edit=amt,msg&src=qr', folder_name + 'images/swish.svg');
						}
					}
				}, 200);
			},

			error: function(e) {
				$('.details > .content').show().html('<div class="message color-red">' + lang_msg_error_cantfetchinfo + '</div>');
			}
		});
	}







	$('body').on('click', '.close', function() {
		$('.message').show();
		$('.message > .choose').show();
		$('.message > .loading').hide();
		$('.details > .message > .choose').show();
		$('.details > .content').hide();
		history.pushState('', document.title, window.location.pathname);
	});



	$('body').on('click', '.details > div > .nav.info', function() {
		$('.details > div > .info:not(.nav)').show();
		$('.details > div > .share:not(.nav)').hide();
		$('.details > div > .share-edit').hide();
		$('.details > div > .users:not(.nav)').hide();
		$('.details > div > .nav').removeClass('active');
		$('.details > div > .nav.info').addClass('active');
	});

	$('body').on('click', '.details > div > .nav.share', function() {
		$('.details > div > .info:not(.nav)').hide();
		$('.details > div > .share:not(.nav)').show();
		$('.details > div > .share-edit').hide();
		$('.details > div > .users:not(.nav)').hide();
		$('.details > div > .nav').removeClass('active');
		$('.details > div > .nav.share').addClass('active');
	});

	$('body').on('click', '.details > div > .nav.users', function() {
		$('.details > div > .info:not(.nav)').hide();
		$('.details > div > .share:not(.nav)').hide();
		$('.details > div > .share-edit').hide();
		$('.details > div > .users:not(.nav)').show();
		$('.details > div > .nav').removeClass('active');
		$('.details > div > .nav.users').addClass('active');
	});







	$('body').on('click', '.edit-share', function() {
		let id_share = $(this).data('idshare');
		let id_item = $(this).data('iditem');

		$.ajax({
			url: folder_name + 'ajax/share-edit.php?ids=' + id_share + '&idi=' + id_item,
			method: 'GET',
			beforeSend: function() {
				//
			},
			success: function(s) {
				$('.details > div > .info:not(.nav)').hide();
				$('.details > div > .share:not(.nav)').hide();
				$('.details > div > .share-edit').show().html(s);
				$('.details > div > .users:not(.nav)').hide();
				$('.details > div > .nav').removeClass('active');
				$('.details > div > .nav.share').addClass('active');

				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);
				}
			},
			error: function(e) {
				msg(lang_msg_error_othererror, 'info');
				console.log('error', e)
			}
		});
	});


	$('body').on('click', '.cancel-editshare', function() {
		$('.details > div > .info:not(.nav)').hide();
		$('.details > div > .share:not(.nav)').hide();
		$('.details > div > .share-edit').hide();
		$('.details > div > .users:not(.nav)').show();
		$('.details > div > .nav').removeClass('active');
		$('.details > div > .nav.users').addClass('active');
	});


	$('body').on('click', '[name="check-share-number-payment"]', function() {
		if($('[name="check-share-number-payment"]').is(':checked') && $('[name="check-share-number-ocr"]').is(':checked') || $('[name="check-share-number-phone"]').is(':checked')) {
			$('[name="check-share-qrcodes"]').attr({ 'disabled': false });

		} else {
			$('[name="check-share-qrcodes"]').attr({ 'disabled': true }).prop({ 'checked': false });
		}
	});

	$('body').on('click', '[name="check-share-number-ocr"]', function() {
		if($('[name="check-share-number-payment"]').is(':checked') && $('[name="check-share-number-ocr"]').is(':checked') || $('[name="check-share-number-phone"]').is(':checked')) {
			$('[name="check-share-qrcodes"]').attr({ 'disabled': false });

		} else {
			$('[name="check-share-qrcodes"]').attr({ 'disabled': true }).prop({ 'checked': false });
		}
	});

	$('body').on('click', '[name="check-share-number-phone"]', function() {
		if($('.checkboxes').prop('class') != 'checkboxes debt' && $('[name="check-share-number-phone"]').is(':checked')) {
			$('[name="check-share-qrcodes"]').attr({ 'disabled': false });

		} else {
			$('[name="check-share-qrcodes"]').attr({ 'disabled': true }).prop({ 'checked': false });
		}
	});


	$('body').on('click', '[name="button-save-share"]', function() {
		let form_editshare = new FormData($('form#share-edit')[0]);

		$.ajax({
			url: folder_name + 'ajax/share-save.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait', 'share-edit');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info', 'share-edit');
					console.log(s);


				} else {
					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error', 'share-edit');

					} else if(s == 'user-not-exists') {
						msg(lang_msg_error_user_notexists, 'error', 'share-edit');

					} else if(s == 'already-shared') {
						msg(lang_msg_error_alreadyshared, 'error', 'share-edit');

					} else if(s == 'cant-share-to-self') {
						msg(lang_msg_error_cantsharetoself, 'error', 'share-edit');

					} else if(s == 'privacy-sharing-notallowed') {
						msg(lang_msg_error_privacy_sharing_notallowed, 'error', 'share-edit');

					} else if(s == 'sum-aboveactual') {
						msg(lang_msg_error_sum_aboveactual, 'error', 'share-edit');

					} else if(s == 'sum-below5') {
						msg(lang_msg_error_sum_below5, 'error', 'share-edit');

					} else if(s == 'sum-percent-above100') {
						msg(lang_msg_error_sum_percent_above100, 'error', 'share-edit');

					} else if(s == 'sum-percent-below5') {
						msg(lang_msg_error_sum_percent_below5, 'error', 'share-edit');


					} else {
						msg(lang_msg_done_itemshared + ' ' + $('[name="field-username"]').val(), 'done', 'share-edit');
						$('#share')[0].reset();
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
				console.log(e);
			},
			data: form_editshare,
			cache: false,
			contentType: false,
			processData: false
		});
	});







	$('body').on('click', '.add-account > .link > .add', function() {
		$('.add-account > .link').hide();
		$('.add-account > form').show();
		$('.add-account > form > .item > .field > [name="field-account-name"]').focus();
	});

	$('body').on('click', '.edit', function() {
		$('[name="hidden-idaccount"]').val($(this).data('idaccount'));

		$('.add-account > .link').hide();
		$('.add-account > form').show();
		$('.add-account > form > .item > .field > [name="field-account-name"]').val($('.list-accounts > div > .active').text()).select();
		$('.add-account > form > .button > [type="submit"]').prop({
			'name': 'button-save',
			'value': lang_button_save
		});

		if($(this).data('isdefault') == 'y') {
			$('[name="check-default"]').prop({ 'checked': true });
		}
	});

	$('body').on('click', '.add-account > form > div > .cancel', function() {
		$('.add-account > .link').show();
		$('.add-account > form').hide();
		$('.add-account > form > .item > .field > [name="field-account-name"]').val('');
		$('.add-account > form > .button > [type="submit"]').prop({
			'name': 'button-create',
			'value': lang_button_create
		});

		$('.msg').hide();
	});





	$('body').on('click', '[name="button-create"]', function() {
		let form = new FormData($('form')[0]);

		$.ajax({
			url: folder_name + 'ajax/item-account-create.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error');

					} else if(s == 'account-exists') {
						msg(lang_msg_error_accountexists, 'error');

					} else {
						msg(lang_msg_done_accountcreated, 'done');

						setTimeout(function() {
							window.location = folder_name + 'expenses';
						}, 1000);
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
				console.log(e);
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});



	$('body').on('click', '[name="button-save"]', function() {
		let form = new FormData($('form')[0]);

		$.ajax({
			url: folder_name + 'ajax/item-account-save.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error');

					} else if(s == 'account-exists') {
						msg(lang_msg_error_accountexists, 'error');

					} else {
						msg(lang_msg_done_accountsaved, 'done');

						setTimeout(function() {
							window.location = folder_name + 'expenses/account:' + $('[name="hidden-idaccount"]').val();
						}, 1000);
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
				console.log(e);
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});



	$('body').on('click', '.manage-account > div > .delete', function() {
		if(confirm(lang_modal_deleteitemaccount)) {
			$.ajax({
				url: folder_name + 'ajax/item-account-delete.php?ida=' + $(this).data('idaccount'),
				method: 'GET',
				beforeSend: function() {
					msg(lang_msg_working, 'wait');
				},

				success: function(s) {
					if(debugging == true) {
						msg(lang_msg_debugging, 'info');
						console.log(s);


					} else {
						if(s == 'account-default') {
							msg(lang_msg_error_accountdefault, 'error');

						} else {
							msg(lang_msg_done_accountdeleted, 'done');

							setTimeout(function() {
								window.location = folder_name + 'expenses';
							}, 1000);
						}
					}
				},

				error: function(e) {
					msg(lang_msg_error_othererror, 'error');
					console.log(e);
				}
			});
		}
	});







	$('body').on('click', '[class*="markas"]', function() {
		let id_item = $(this).data('iditem');
		let type = $(this).prop('class') == 'markas-payed' ? 'payed' : 'not-payed';

		$.ajax({
			url: folder_name + 'ajax/mark-as.php?mar=' + type + '&idi=' + id_item,
			method: 'GET',
			async: true,
			beforeSend: function() {
				$('.wait[data-iditem="' + id_item + '"]').removeClass('hidden');
				$('.status.not-payed[data-iditem="' + id_item + '"]').addClass('hidden');
				$('.status.payed[data-iditem="' + id_item + '"]').addClass('hidden');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					setTimeout(function() {
						$('.wait[data-iditem="' + id_item + '"]').addClass('hidden');

						if(type == 'payed') {
							$('.not-payed[data-iditem="' + id_item + '"]').addClass('hidden');
							$('.payed[data-iditem="' + id_item + '"]').removeClass('hidden');

							if($('.details > .content > .info[data-iditem="' + id_item + '"]').is(':visible')) {
								$('.payed[data-iditem="' + id_item + '"]').removeClass('hidden');
								$('.item.payedstatus[data-iditem="' + id_item + '"] > .value > .not-payed').addClass('hidden');
								$('.item.payedstatus[data-iditem="' + id_item + '"] > .value > .payed').removeClass('hidden').html('<a href="' + folder_name + 'expenses/filter-by-date:' + year + '-' + month + '-' + day + '">' + year + '-' + month + '-' + day + '</a>');
							}

						} else {
							$('.not-payed[data-iditem="' + id_item + '"]').removeClass('hidden');
							$('.payed[data-iditem="' + id_item + '"]').addClass('hidden');

							if($('.details > .content > .info[data-iditem="' + id_item + '"]').is(':visible')) {
								$('.payed[data-iditem="' + id_item + '"]').addClass('hidden');
								$('.item.payedstatus[data-iditem="' + id_item + '"] > .value > .not-payed').removeClass('hidden');
								$('.item.payedstatus[data-iditem="' + id_item + '"] > .value > .payed').addClass('hidden');
							}

							if(($('.code').is(':visible') && $('.item.payment').is(':visible') && $('.item.payment > .value').text() != '-') &&
							   ($('.code').is(':visible') && $('.item.ocr').is(':visible') && $('.item.ocr > .value').text() != '-')) {
								let item_class = '.code';
								let qr_nme = $(item_class).data('nme');
								let qr_ocr = $(item_class).data('ocr');
								let qr_ddt = $(item_class).data('ddt');
								let qr_acc = $(item_class).data('acc');
								let qr_pt = $(item_class).data('pt');
								let qr_due = $(item_class).data('due');

								$('canvas').remove();
								qrcode(item_class, 200, '{"uqr":1,"tp":1,"nme":"' + qr_nme + '","cid":"' + qr_ocr + '","iref":"' + qr_ocr + '","ddt":"' + qr_ddt + '","due":' + qr_due + ',"pt":"' + qr_pt + '","acc":"' + qr_acc + '"}');
							}

							if($('.swish').is(':visible') && $('.item.phone').is(':visible')) {
								$('canvas').remove();
								qrcode('.swish', 170, 'https://app.swish.nu/1/p/sw/?sw=' + $('.swish').data('sw') + '&amt=' + $('.swish').data('amt') + '&cur=SEK&msg=&edit=amt,msg&src=qr', folder_name + 'images/swish.svg');
							}
						}
					}, 100);
				}
			},

			error: function(e) {
				if(type == 'payed') {
					$('.not-payed[data-iditem="' + id_item + '"]').removeClass('hidden');
					$('.payed[data-iditem="' + id_item + '"]').addClass('hidden');

				} else {
					$('.not-payed[data-iditem="' + id_item + '"]').addClass('hidden');
					$('.payed[data-iditem="' + id_item + '"]').removeClass('hidden');
				}
			}
		});
	});







	$('body').on('click', '[name="button-share"]', function() {
		let form = new FormData($('#share')[0]);

		$.ajax({
			url: folder_name + 'ajax/item-share.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait', 'share');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error', 'share');

					} else if(s == 'user-not-exists') {
						msg(lang_msg_error_user_notexists, 'error', 'share');

					} else if(s == 'already-shared') {
						msg(lang_msg_error_alreadyshared, 'error', 'share');

					} else if(s == 'cant-share-to-self') {
						msg(lang_msg_error_cantsharetoself, 'error', 'share');

					} else if(s == 'privacy-sharing-notallowed') {
						msg(lang_msg_error_privacy_sharing_notallowed, 'error', 'share');

					} else if(s == 'sum-aboveactual') {
						msg(lang_msg_error_sum_aboveactual, 'error', 'share');

					} else if(s == 'sum-below5') {
						msg(lang_msg_error_sum_below5, 'error', 'share');

					} else if(s == 'sum-above-100-percent') {
						msg(lang_msg_error_sum_percent_above100, 'error', 'share');


					} else {
						msg(lang_msg_done_itemshared + ' ' + $('[name="field-username"]').val(), 'done', 'share');
						$('#share')[0].reset();
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
				console.log(e);
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});


});