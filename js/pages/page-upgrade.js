$(document).ready(function() {


	$('body').on('click', '#upgrade > a', function() {
		$.ajax({
			url: folder_name + '/ajax/account-upgrade.php?ste=1',
			method: 'GET',
			beforeSend: function() {
				$('.continue').hide();
				$('.working').show();
			},
			success: function(s) {
				$('.working > .step-2 > .wait').show();
				$('.working > .step-2 > .empty').hide();
				$('.working > .step-1 > .empty').hide();
				$('.working > .step-1 > .wait').hide();
				$('.working > .step-1 > .success').show();

				$('.working > .step-1').addClass('color-green');

				$('.working > .step-2 > .inactive').hide();
				$('.working > .step-2 > .active').show();

				console.log(s);


				$.ajax({
					url: folder_name + '/ajax/account-upgrade.php?ste=2',
					method: 'GET',
					success: function(s) {
						$('.working > .step-3 > .wait').show();
						$('.working > .step-3 > .empty').hide();
						$('.working > .step-2 > .empty').hide();
						$('.working > .step-2 > .wait').hide();
						$('.working > .step-2 > .success').show();

						$('.working > .step-2').addClass('color-green');

						$('.working > .step-3 > .inactive').hide();
						$('.working > .step-3 > .active').show();

						console.log(s);


						$.ajax({
							url: folder_name + '/ajax/account-upgrade.php?ste=3',
							method: 'GET',
							success: function(s) {
								$('.working > .step-4 > .wait').show();
								$('.working > .step-4 > .empty').hide();
								$('.working > .step-3 > .empty').hide();
								$('.working > .step-3 > .wait').hide();
								$('.working > .step-3 > .success').show();

								$('.working > .step-3').addClass('color-green');

								$('.working > .step-4 > .inactive').hide();
								$('.working > .step-4 > .active').show();

								console.log(s);


								$.ajax({
									url: folder_name + '/ajax/account-upgrade.php?ste=4',
									method: 'GET',
									success: function(s) {
										$('.working > .step-4 > .empty').hide();
										$('.working > .step-4 > .wait').hide();
										$('.working > .step-4 > .success').show();

										$('.working > .step-4').addClass('color-green');

										console.log(s);
									},
									error: function(e) {
										//
									}
								});


							},
							error: function(e) {
								//
							}
						});


					},
					error: function(e) {
						//
					}
				});


			},
			error: function(e) {
				//
			}
		});
	});


});