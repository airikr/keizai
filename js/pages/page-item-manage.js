$(document).ready(function() {


	$('body').on('click', '[name="button-add"]', function() {
		let form = new FormData($('form')[0]);
		let hidden_object = $('[name="hidden-object"]').val();
		let object = (hidden_object == 'expense' ? 'expenses' : (hidden_object == 'debt' ? 'debts' : hidden_object == 'loan' ? 'loans' : null));

		$.ajax({
			url: folder_name + 'ajax/item-create.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					let splitted = s.split('|');

					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error');

					} else if(s == 'item-exists') {
						msg(lang_msg_error_itemexists, 'error');

					} else if(s == 'onlydigits-permonth') {
						msg(lang_msg_error_sum_onlydigits_permonth, 'error');

					} else if(s == 'onlydigits-interest') {
						msg(lang_msg_error_sum_onlydigits_interest, 'error');

					} else if(s == 'onlydigits-adminfee') {
						msg(lang_msg_error_sum_onlydigits_adminfee, 'error');

					} else if(s == 'onlydigits-recurrence') {
						msg(lang_msg_error_onlydigits_recurrence, 'error');

					} else if(s == 'onlybetween-1-12-recurrence') {
						msg(lang_msg_error_onlybetween_1_12_recurrence, 'error');

					} else if(s == 'sum-invalid-nonegative') {
						msg(lang_msg_error_sum_invalid_nonegative, 'error');


					} else {
						msg(lang_msg_done_itemadded, 'done');

						setTimeout(function() {
							window.location = folder_name + object + (splitted[0] == 'expense' ? (splitted[1].length == 0 ? '' : '/account:' + splitted[1]) : '');
						}, 800);
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
				console.log(e);
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});





	$('body').on('click', '[name="button-save"]', function() {
		let form = new FormData($('form')[0]);
		let hidden_object = $('[name="hidden-object"]').val();
		let hidden_iditem = ($('[name="hidden-iditem"]').val().length == 0 ? 0 : $('[name="hidden-iditem"]').val());
		let object = (hidden_object == 'expense' ? 'expenses' : (hidden_object == 'debt' ? 'debts' : hidden_object == 'loan' ? 'loans' : null));

		$.ajax({
			url: folder_name + 'ajax/item-save.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					let splitted = s.split('|');

					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error');

					} else if(s == 'item-exists') {
						msg(lang_msg_error_itemexists, 'error');

					} else if(s == 'onlydigits-permonth') {
						msg(lang_msg_error_sum_onlydigits_permonth, 'error');

					} else if(s == 'onlydigits-interest') {
						msg(lang_msg_error_sum_onlydigits_interest, 'error');

					} else if(s == 'onlydigits-adminfee') {
						msg(lang_msg_error_sum_onlydigits_adminfee, 'error');

					} else if(s == 'onlydigits-recurrence') {
						msg(lang_msg_error_onlydigits_recurrence, 'error');

					} else if(s == 'onlybetween-1-12-recurrence') {
						msg(lang_msg_error_onlybetween_1_12_recurrence, 'error');

					} else if(s == 'sum-invalid-nonegative') {
						msg(lang_msg_error_sum_invalid_nonegative, 'error');


					} else {
						msg(lang_msg_done_saved, 'done');

						if(splitted[1] != 'shared') {
							setting = (splitted[0] == 'expense' ? (splitted[1].length != 0 ? '/account:' + splitted[1] : '') : '');
						} else {
							setting = '/filter:shared';
						}

						setTimeout(function() {
							window.location = folder_name + object + setting + (hidden_iditem == 0 ? '' : '#' + hidden_object + '-' + hidden_iditem);
						}, 800);
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
				console.log(e);
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});







	/*$('body').on('click', '[name="check-loan-out"]', function() {
		if($(this).is(':checked')) {
			$('[name="field-number-phone"]').val('').prop({
				'disabled': true
			});

		} else {
			$('[name="field-number-phone"]').prop({
				'disabled': false
			});
		}
	});*/



	$('body').on('change', '[name="list-types"]', function() {
		let type = $(this).val();

		if(type == 'swish') {
			$('[name="field-number-payment"]').prop({ 'disabled': true });
			$('[name="field-number-ocr"]').prop({ 'disabled': true });
			$('[name="field-number-phone"]').prop({ 'disabled': false });

		} else {
			$('[name="field-number-payment"]').prop({ 'disabled': false });
			$('[name="field-number-ocr"]').prop({ 'disabled': false });
			$('[name="field-number-phone"]').prop({ 'disabled': true });
		}
	});







	$('body').on('focus', '[name="field-sum"]:not([readonly])', function() {
		let sum = ($(this).val().length == 0 ? '0' : $(this).val().replace(',', '.'));
		let currency_exists = ($('[name="list-currencies"]').is(':visible') ? true : false);

		$('.field.sum > div > .icon').html(svgicon('calculator'));

		if(sum != '0') {
			$('.math').show();
			$('.math > .sum').text(format_number(math.evaluate(sum), 2));
		}

		if(is_portable == false) {
			$('.name').hide();
			$('.item.sum').removeClass('marginleft-10').addClass('margin-0');
			$(this).addClass('width-sum-' + (currency_exists == false ? '' : 'withcurrency-') + 'extended');
		}
	});

	$('body').on('blur', '[name="field-sum"]', function() {
		let currency_exists = ($('[name="list-currencies"]').is(':visible') ? true : false);

		$('.field.sum > div > .icon').html(svgicon('money'));

		if(is_portable == false) {
			$('.name').show();
			$('.item.sum').removeClass('margin-0').addClass('marginleft-10');
			$(this).removeClass('width-sum-' + (currency_exists == false ? '' : 'withcurrency-') + 'extended');
		}
	});

	$('body').on('input paste', '[name="field-sum"]', function() {
		let sum = $(this).val().replace(',', '.');
		let lastchar = sum.substr(sum.length - 1);

		if(sum.length == 0) {
			$('.math').hide();

		} else {
			$('.math').show();

			if(!arr_chars.includes(lastchar)) {
				$('.math > .sum').text(format_number(math.evaluate(sum.replace(',', '.')), 2));
			}
		}
	});


	$('body').on('click', '#expense-payed', function() {
		if($(this).is(':checked')) {
			$('[name="field-date-payed"]').prop({ 'disabled': false }).val(year + '-' + month + '-' + day);
		} else {
			$('[name="field-date-payed"]').prop({ 'disabled': true }).val('');
		}
	});


	$('body').on('click', '#expense-subscription', function() {
		if($(this).is(':checked')) {
			$('[name="field-recurrence"]').prop({ 'disabled': false });
			$('[name="list-recurrence-types"]').prop({ 'disabled': false });
			$('#expense-recurrent').prop({ 'checked': true });
		} else {
			$('[name="field-recurrence"]').prop({ 'disabled': true });
			$('[name="list-recurrence-types"]').prop({ 'disabled': true });
			$('#expense-recurrent').prop({ 'checked': false });
		}
	});


	$('body').on('click', '#expense-recurrent', function() {
		if($(this).is(':checked')) {
			$('[name="field-recurrence"]').prop({ 'disabled': false });
			$('[name="list-recurrence-types"]').prop({ 'disabled': false });
		} else {
			$('[name="field-recurrence"]').prop({ 'disabled': true });
			$('[name="list-recurrence-types"]').prop({ 'disabled': true });
			$('#expense-subscription').prop({ 'checked': false });
		}
	});


	$('body').on('paste input', '[name="field-number-payment"]', function() {
		if($(this).val().length <= 5 || $('[name="field-number-ocr"]').val().length < 5) {
			$('#expense-correctinfo').prop({ 'disabled': true, 'checked': false });
		} else {
			$('#expense-correctinfo').prop({ 'disabled': false });
		}
	});


	$('body').on('paste input', '[name="field-number-ocr"]', function() {
		if($(this).val().length < 5 || $('[name="field-number-payment"]').val().length <= 5) {
			$('#expense-correctinfo').prop({ 'disabled': true, 'checked': false });
		} else {
			$('#expense-correctinfo').prop({ 'disabled': false });
		}
	});


});