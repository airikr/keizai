$(document).ready(function() {


	new Sortable(document.getElementById('items'), {
		swapThreshold: 1,
		animation: 500,
		forceFallback: true,
		fallbackClass: 'dragged-item',
		chosenClass: 'chosen-item',
		handle: '.move',

		onUpdate: function(evt) {
			let order = 0;
			$('#items > .item').each(function() {
				order++;
				let id_item = $(this).data('iditem');

				$.ajax({
					url: folder_name + 'ajax/budget-save-order.php?idi=' + id_item + '&ord=' + order,
					method: 'GET',
					beforeSend: function() {
						$('.updating').fadeIn(100).css({
							'height': 'calc(' + $('#items').height() + 'px - 200px)'
						});
					},
					success: function(s) {
						setTimeout(function() {
							$('.updating').fadeOut(100);
						}, 200);
					},
					error: function(e) {
						$('.updating > .saving').hide();
						$('.updating > .error').show();

						setTimeout(function() {
							$('.updating').fadeOut(100);
						}, 1500);
					}
				});
			});
		}
	});



	$('body').on('click', '.update', function() {
		let type = $(this).data('type');
		let id_budget = $(this).data('idbudget');

		$('.options[data-idbudget="' + id_budget + '"]').hide();
		$('form[data-idbudget="' + id_budget + '"]').show();
		$('form[data-idbudget="' + id_budget + '"] > .msg[data-idbudget="' + id_budget + '"]').hide();

		$('form[data-idbudget="' + id_budget + '"] > [name="button-update"]').attr({ 'data-type': type });
	});

	$('body').on('click', '.cancel', function() {
		let id_budget = $(this).data('idbudget');

		$('.msg').hide();
		$('.options[data-idbudget="' + id_budget + '"]').show();
		$('form[data-idbudget="' + id_budget + '"]').hide();
		$('form[data-idbudget="' + id_budget + '"]')[0].reset();
		$('form[data-idbudget="' + id_budget + '"] > .msg[data-idbudget="' + id_budget + '"]').hide();

		$('form[data-idbudget="' + id_budget + '"] > [name="button-update"]').removeAttr('data-type');
	});





	$('body').on('click', '[name="button-update"]', function() {
		let id_budget = $(this).data('idbudget');
		let form = new FormData($('form[data-idbudget="' + id_budget + '"]')[0]);

		$.ajax({
			url: folder_name + 'ajax/budget-update.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait', null, '[data-idbudget="' + id_budget + '"]', false);
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info', null, '[data-idbudget="' + id_budget + '"]', false);
					console.log(s);


				} else {
					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error', null, '[data-idbudget="' + id_budget + '"]', false);

					} else if(s == 'onlydigits-sum') {
						msg(lang_msg_error_sum_onlydigits_budget, 'error', null, '[data-idbudget="' + id_budget + '"]', false);

					} else if(s == 'sum-belowzero') {
						msg(lang_msg_error_sum_belowzero, 'error', null, '[data-idbudget="' + id_budget + '"]', false);

					} else if(s == 'sum-aboveonehundred') {
						msg(lang_msg_error_sum_aboveonehundred, 'error', null, '[data-idbudget="' + id_budget + '"]', false);

					} else {
						let splitted = s.split('|');

						msg(lang_msg_done_budgetupdated, 'done', null, '[data-idbudget="' + id_budget + '"]', false);

						$('.sums[data-idbudget="' + id_budget + '"] > .current').text(splitted[0]);
						$('.progress[data-idbudget="' + id_budget + '"] > .percentage').css({ 'width': splitted[1] + '%' });
						$('.progress[data-idbudget="' + id_budget + '"] > .percentage > .text').text(splitted[1] + '%');

						$('.options[data-idbudget="' + id_budget + '"]').show();
						$('form[data-idbudget="' + id_budget + '"]').hide();
						$('form[data-idbudget="' + id_budget + '"]')[0].reset();
				
						$('form[data-idbudget="' + id_budget + '"] > [name="button-update"]').removeAttr('data-type');

						if(splitted[2] == 'done') {
							$('.done[data-idbudget="' + id_budget + '"]').removeClass('hidden');
						} else {
							$('.done[data-idbudget="' + id_budget + '"]').addClass('hidden');
						}
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error', '[data-idbudget="' + id_budget + '"]', false);
				console.log(e);
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});


});