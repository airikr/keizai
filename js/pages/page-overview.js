$(document).ready(function() {


	$('body').on('click', '.nav:not(.wait)', function() {
		let date = $(this).data('date');
		let year = $(this).data('year');
		let side = $(this).prop('class').replace(' ', '.');

		$.ajax({
			url: folder_name + 'ajax/navigate-calendar.php?dat=' + date,
			method: 'GET',
			beforeSend: function() {
				$('.dim').show();
				$('.' + side).hide();
				$('.' + side + '.wait').show();
			},
			success: function(s) {
				$('.calendar').html(s);
				$('.dim').hide();
			},
			error: function(e) {
				console.log(e)
			}
		});
	});


	$('body').on('click', '.month:not(.inactive)', function() {
		let date = $(this).data('date');

		$.ajax({
			url: folder_name + 'ajax/navigate-calendar.php?dat=' + date,
			method: 'GET',
			beforeSend: function() {
				$('.dim').show();
				$('.month').hide();
				$('.month.' + date + '.wait').show();
				$('.month.inactive:not(.' + date + ')').show();
			},
			success: function(s) {
				$('.calendar').html(s);
				$('.dim').hide();
			},
			error: function(e) {
				console.log(e)
			}
		});
	});


	$('body').on('click', '.view-month', function() {
		let year = $(this).data('year');
		let month = $(this).data('month');

		$.ajax({
			url: folder_name + 'ajax/navigate-calendar.php?yea=' + year + '&mon=' + month + '&vie',
			method: 'GET',
			beforeSend: function() {
				$('.dim').show();
				$('.' + month).hide();
				$('.' + month + '.wait').show();
			},
			success: function(s) {
				$('.calendar').html(s);
				$('.dim').hide();
			},
			error: function(e) {
				console.log(e)
			}
		});
	});

});