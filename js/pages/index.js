$(document).ready(function() {


	if(debugging == true) {
		$('.item.tfa').addClass('debugging');
	}


	$('body').on('click', '.form > nav > a', function() {
		let tab = $(this).data('tab');

		$('.form > nav > a').removeClass('active');
		$('.form > div').hide();
		$('input:not([type="submit"]):not([name="hidden-language"])').blur().val('');
		$('.tfa').hide();

		if(tab == 'login') {
			$(this).addClass('active');
			$('.form > .login').show();

			if(debugging == true) {
				$('.tfa').show();
			}

		} else if(tab == 'create') {
			$(this).addClass('active');
			$('.form > .create').show();

		} else if(tab == 'forgot-pw') {
			$(this).addClass('active');
			$('.form > .forgot-pw').show();
		}
	});


	$('body').on('click', '.remove-tfa > a', function() {
		$('nav > a').removeClass('active');
		$('.login').hide();
		$('.msg').hide();
		$('.remove-tfa').show();
	});

	$('body').on('click', '.cancel', function() {
		$('nav > [data-tab="login"]').addClass('active');
		$('.login').show();
		$('.remove-tfa').hide();
		$('.remove-tfa.link').show();
	});







	$('body').on('click', '[name="button-login"]', function() {
		let form = new FormData($('.login > form')[0]);

		$.ajax({
			url: folder_name + 'ajax/account-login.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error');
						$('[name*="password"]').val('');

					} else if(s == 'user-notfound') {
						msg(lang_msg_error_username_notexists, 'error');
						$('[name*="password"]').val('');

					} else if(s == 'too-many-attempts') {
						msg(lang_msg_error_toomanyattempts, 'error');
						$('[name*="password"]').val('');
						$('[name="field-tfa"]').val('');
						$('.item.tfa').hide();

					} else if(s == 'password-incorrect') {
						msg(lang_msg_error_pw_incorrect, 'error');
						$('[name*="password"]').val('');

					} else if(s == 'tfa-enabled') {
						msg(lang_msg_info_tfa_enabled, 'info');
						$('.login > form > .item.tfa').show();
						$('[name="field-tfa"]').focus();

					} else if(s == 'tfa-nodigits') {
						msg(lang_msg_error_tfa_onlydigits, 'error');
						$('[name="field-tfa"]').val('').focus();

					} else if(s == 'tfa-incorrect') {
						msg(lang_msg_error_tfa_incorrect, 'error');
						$('[name="field-tfa"]').val('').focus();

					} else {
						localStorage.removeItem('theme');
						localStorage.removeItem('align');

						msg(lang_msg_done_signedin, 'done');
						$('[name*="password"]').val('');
						$('[name="field-tfa"]').val('').blur();

						setTimeout(function() {
							window.location = folder_name + 'overview';
						}, 800);
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});







	$('body').on('click', '[name="button-create"]', function() {
		let form = new FormData($('.create > form')[0]);

		$.ajax({
			url: folder_name + 'ajax/account-create.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					$('[name*="password"]').val('');

					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error');

					} else if(s == 'user-exists') {
						msg(lang_msg_error_username_taken, 'error');

					} else if(s == 'password-tooshort') {
						msg(lang_msg_error_pw_tooshort, 'error');

					} else if(s == 'passwords-notmatching') {
						msg(lang_msg_error_pw_notsame, 'error');

					} else if(s == 'email-notvalid') {
						msg(lang_msg_error_email_notvalid, 'error');

					} else if(s == 'email-exists') {
						msg(lang_msg_error_email_taken, 'error');

					} else if(s == 'permission-denied') {
						msg(lang_msg_error_permission_denied, 'error');

					} else {
						msg(lang_msg_done_account_created, 'done');

						$('.create > form')[0].reset();

						setTimeout(function() {
							location.reload();
						}, 800);
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});







	$('body').on('click', '[name="button-send"]', function() {
		let form = new FormData($('.forgot-pw > form')[0]);

		$.ajax({
			url: folder_name + 'ajax/account-forgotpw.php?act=email',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					$('[name*="password"]').val('');

					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error');

					} else if(s == 'user-notexists') {
						msg(lang_msg_error_username_notexists, 'error');

					} else if(s == 'email-notexists') {
						msg(lang_msg_error_email_notexists, 'error');

					} else if(s == 'email-notvalid') {
						msg(lang_msg_error_email_notvalid, 'error');

					} else {
						msg(lang_msg_done_emailsent, 'done');

						$('.forgot-pw > form')[0].reset();
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});







	$('body').on('click', '[name="button-remove-tfa"]', function() {
		let form = new FormData($('.remove-tfa > form')[0]);

		$.ajax({
			url: folder_name + 'ajax/account-remove-tfa-resetcode.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					$('[name="field-resetcode"]').val('');

					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error');

					} else if(s == 'user-notexists') {
						msg(lang_msg_error_username_notexists, 'error');

					} else if(s == 'resetcode-incorrect') {
						msg(lang_msg_error_resetcode_incorrect, 'error');

					} else if(s == 'resetcode-length') {
						msg(lang_msg_error_resetcode_length, 'error');

					} else {
						msg(lang_msg_done_tfa_disabled, 'done');

						$('nav > [data-tab="login"]').removeClass('active');
						$('.remove-tfa').hide();
						$('.login').show();
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});


});