$(document).ready(function() {


	$('body').on('click', '[name="button-add"]', function() {
		let form = new FormData($('form')[0]);

		$.ajax({
			url: folder_name + 'ajax/budget-create.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error');

					} else if(s == 'item-exists') {
						msg(lang_msg_error_itemexists, 'error');

					} else if(s == 'onlydigits-sum-goal') {
						msg(lang_msg_error_sum_onlydigits_goal, 'error');

					} else if(s == 'onlydigits-sum-monthly') {
						msg(lang_msg_error_sum_onlydigits_permonth, 'error');


					} else {
						msg(lang_msg_done_itemadded, 'done');

						setTimeout(function() {
							window.location = folder_name + 'budget';
						}, 800);
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
				console.log(e);
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});





	$('body').on('click', '[name="button-save"]', function() {
		let form = new FormData($('form')[0]);

		$.ajax({
			url: folder_name + 'ajax/budget-save.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					if(s == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error');

					} else if(s == 'budget-exists') {
						msg(lang_msg_error_budgetexists, 'error');

					} else if(s == 'onlydigits-sum-goal') {
						msg(lang_msg_error_sum_onlydigits_goal, 'error');

					} else if(s == 'onlydigits-sum-monthly') {
						msg(lang_msg_error_sum_onlydigits_permonth, 'error');


					} else {
						msg(lang_msg_done_budgetupdated, 'done');

						setTimeout(function() {
							window.location = folder_name + 'budget';
						}, 800);
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
				console.log(e);
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});


});