$(document).ready(function() {


	if(window.location.hash) {
		let hash = window.location.hash.substring(1);
		let hash_element = '.item.' + hash;
		let sum = ($(this).val().length == 0 ? '0' : $(this).val());

		goto(hash_element, 10);

		$(hash_element + ' > div > div > input').select();
		$(hash_element + ' > div > div > .icon:not(.wit)').html(svgicon('calculator'));

		if(sum != '0') {
			$(hash_element + ' > div > .math').show();
			$(hash_element + ' > div > .math > .sum').text(format_number(math.evaluate(sum.replace(',', '.')), 2));
		}

		if(is_portable == false) {
			$('.income').hide();
			$('.balance').hide();
			$('.savings').hide();
			$(hash_element).show().removeClass('width-sum-original').addClass('width-sum-extended');
			$(hash_element).removeClass('marginleft-10').addClass('margin-0');
		}
	}



	$('body').on('change', '[name="list-theme"]', function() {
		let theme = $(this).val();
		let arr_themes = {
			0: 'light-everforest',
			1: 'dark-everforest',
			2: 'dark-dracula',
			3: 'dark-tokyonight'
		};

		$('link.theme').prop({
			'href': folder_name + 'css/themes/' + arr_themes[theme] + '.css'
		});
	});



	$('body').on('click', '[name="button-save"]', function() {
		let form = new FormData($('form')[0]);

		$.ajax({
			url: folder_name + 'ajax/account-save.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait', 'settings');
			},

			success: function(s) {
				let splitted = s.split('|');

				if(debugging == true) {
					msg(lang_msg_debugging, 'info', 'settings');
					console.log(s);


				} else {
					$('[name*="password"]').val('');

					if(s == 'fields-empty' || splitted[0] == 'fields-empty') {
						msg(lang_msg_error_fieldsempty, 'error', 'settings');

					} else if(s == 'email-notvalid' || splitted[0] == 'email-notvalid') {
						msg(lang_msg_error_email_notvalid, 'error', 'settings');

					} else if(s == 'email-exists' || splitted[0] == 'email-exists') {
						msg(lang_msg_error_email_taken, 'error', 'settings');

					} else if(s == 'password-notsame' || splitted[0] == 'password-notsame') {
						msg(lang_msg_error_pw_notsame, 'error', 'settings');

					} else if(s == 'password-incorrect' || splitted[0] == 'password-incorrect') {
						msg(lang_msg_error_pw_incorrect_current, 'error', 'settings');

					} else if(s == 'password-tooshort' || splitted[0] == 'password-tooshort') {
						msg(lang_msg_error_pw_tooshort, 'error');

					} else if(s == 'tfa-nodigits' || splitted[0] == 'tfa-nodigits') {
						msg(lang_msg_error_tfa_onlydigits, 'error');

					} else if(s == 'tfa-incorrect' || splitted[0] == 'tfa-incorrect') {
						msg(lang_msg_error_tfa_incorrect, 'error', 'settings');

					} else if(s == 'period-wrongorder' || splitted[0] == 'period-wrongorder') {
						msg(lang_msg_error_period_wrongorder, 'error', 'settings');

					} else if(s == 'period-missingdate-start' || splitted[0] == 'period-missingdate-start') {
						msg(lang_msg_error_period_missingdate_start, 'error', 'settings');

					} else if(s == 'period-missingdate-end' || splitted[0] == 'period-missingdate-end') {
						msg(lang_msg_error_period_missingdate_end, 'error', 'settings');

					} else if(s == 'sum-invalid-nonegative' || splitted[0] == 'sum-invalid-nonegative') {
						msg(lang_msg_error_sum_invalid_nonegative, 'error', 'settings');

					} else {
						if(s.indexOf('|') > -1 && splitted[1].indexOf('tfa') > -1) {
							let tfa_split = splitted[1].split('-');
							$('[name="field-tfa"]').val('');
							$('.tfa-disabled').hide();
							$('.tfa-backup').show();
							$('.tfa-backup > .code').text(tfa_split[1]);
						}


						if(s == 'success' || splitted[0] == 'success') {
							msg(lang_msg_done_saved, 'done', 'settings');

						} else if(s == 'success-pwc' || splitted[0] == 'success-pwc') {
							msg(lang_msg_done_saved_pw, 'done', 'settings');

							setTimeout(function() {
								window.location = folder_name + 'sign-out';
							}, 1500);
						}
					}
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error', 'settings');
				console.log(e);
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});







	$('body').on('click', '.update-expiration-dates > .update', function() {
		if(confirm($(this).data('modal'))) {
			$.ajax({
				url: folder_name + 'ajax/update-expiration-dates.php',
				method: 'GET',
				async: true,
				beforeSend: function() {
					$('.update').hide();
					$('.updating').show();
				},

				success: function(s) {
					if(debugging == true) {
						$('.update').hide();
						$('.updating').hide();
						$('.updated').hide();
						$('.debugging').show();

						console.log(s);


					} else {
						setTimeout(function() {
							$('.update').hide();
							$('.updating').hide();

							if(s == 'no-update') {
								$('.update-not-needed').show();
							} else {
								$('.updated').show();
							}

							setTimeout(function() {
								$('.update').show();
								$('.updating').hide();
								$('.updated').hide();
								$('.update-not-needed').hide();
							}, 1500);
						}, 200);
					}
				},

				error: function(e) {
					msg(lang_msg_error_othererror, 'error');
					console.log(e);
				}
			});
		}
	});







	$('body').on('click', '#delete-data', function() {
		if(confirm(lang_modal_deletedata)) {
			$.ajax({
				url: folder_name + 'ajax/account-delete-data.php?del=',
				method: 'GET',
				beforeSend: function() {
					msg(lang_msg_working_deletingyourdata, 'wait', 'data');
				},
				success: function(s) {
					(debugging == true ? console.log(s) : null);

					setTimeout(function() {
						$.ajax({
							url: folder_name + 'ajax/account-delete-data.php?con=',
							method: 'GET',
							beforeSend: function() {
								msg(lang_msg_working_checkingdeletion, 'wait', 'data');
							},
							success: function(s) {
								if(debugging == true) {
									msg(lang_msg_debugging, 'info', 'data');
									console.log(s);


								} else {
									setTimeout(function() {
										if(s == 'retry') {
											msg(lang_msg_error_cantdeletedata, 'error', 'data');

										} else if(s == 'success') {
											msg(lang_msg_done_datadeleted, 'done', 'data');

											setTimeout(function() {
												location.reload();
											}, 800);
										}
									}, 500);
								}
							},
							error: function(e) {
								//
							}
						});
					}, 500);
				},
				error: function(e) {
					msg(lang_msg_error_othererror, 'error', 'data');
					console.log(e);
				}
			});
		}
	});



	$('body').on('click', '#delete-account', function() {
		if(confirm(lang_modal_deleteaccount)) {
			$.ajax({
				url: folder_name + 'ajax/account-delete-account.php?del&idu=' + $('#delete-account').data('iduser'),
				method: 'GET',
				beforeSend: function() {
					msg(lang_msg_working_deletingdata, 'wait', 'data');
				},
				success: function(s) {
					(debugging == true ? console.log(s) : null);

					setTimeout(function() {
						$.ajax({
							url: folder_name + 'ajax/account-delete-account.php?con&idu=' + $('#delete-account').data('iduser'),
							method: 'GET',
							beforeSend: function() {
								msg(lang_msg_working_checkingdeletion, 'wait', 'data');
							},
							success: function(s) {
								if(debugging == true) {
									msg(lang_msg_debugging, 'info', 'data');
									console.log(s);


								} else {
									setTimeout(function() {
										if(s == 'retry') {
											msg(lang_msg_errors_cantdeletedata, 'error', 'data');

										} else if(s == 'success') {
											msg(lang_msg_done_deletedaccount, 'done', 'data');

											setTimeout(function() {
												window.location = folder_name + 'sign-out';
											}, 800);
										}
									}, 500);
								}
							},
							error: function(e) {
								msg(lang_msg_error_othererror, 'error', 'data');
								console.log(e);
							}
						});
					}, 500);
				},
				error: function(e) {
					msg(lang_msg_error_othererror, 'error', 'data');
					console.log(e);
				}
			});
		}
	});



	$('body').on('click', '.options > .show', function() {
		let idsession = $(this).data('idsession');
		let class_moreinfo = '.moreinfo[data-idsession="' + idsession + '"]';

		if($(class_moreinfo).is(':hidden')) {
			$('.options > .show.more[data-idsession="' + idsession + '"]').hide();
			$('.options > .show.less[data-idsession="' + idsession + '"]').show();
			$(class_moreinfo).show();

		} else {
			$('.options > .show.more[data-idsession="' + idsession + '"]').show();
			$('.options > .show.less[data-idsession="' + idsession + '"]').hide();
			$(class_moreinfo).hide();
		}
	});







	$('body').on('click', '[name="button-import"]', function() {
		if(confirm(lang_modal_import)) {
			let form = new FormData($('form#import')[0]);

			$.ajax({
				url: folder_name + 'ajax/account-import.php',
				method: 'POST',
				beforeSend: function() {
					msg(lang_msg_working, 'wait', 'import');
				},

				success: function(s) {
					if(debugging == true) {
						msg(lang_msg_debugging, 'info', 'import');
						console.log(s);


					} else {
						if(s == 'server-size-ini') {
							msg(lang_msg_error_file_serversize_ini, 'error', 'import');

						} else if(s == 'server-size-form') {
							msg(lang_msg_error_file_serversize_form, 'error', 'import');

						} else if(s == 'partial') {
							msg(lang_msg_error_file_partial, 'error', 'import');

						} else if(s == 'no-file') {
							msg(lang_msg_error_file_nofile, 'error', 'import');

						} else if(s == 'no-tmpdir') {
							msg(lang_msg_error_file_notmpdir, 'error', 'import');

						} else if(s == 'cant-write') {
							msg(lang_msg_error_file_cantwrite, 'error', 'import');

						} else if(s == 'extension') {
							msg(lang_msg_error_file_extension, 'error', 'import');

						} else if(s == 'unknown') {
							msg(lang_msg_error_file_unknown, 'error', 'import');

						} else if(s == 'success') {
							msg(lang_msg_done_imported, 'done', 'import');

							setTimeout(function() {
								window.location = folder_name;
							}, 1500);
						}
					}
				},

				error: function(e) {
					msg(lang_msg_error_othererror, 'error', 'import');
					console.log(e);
				},
				data: form,
				cache: false,
				contentType: false,
				processData: false
			});
		}
	});







	if($('.tfa-disabled').is(':visible')) {
		qrcode('.qr-code', 200, 'otpauth://totp/' + $('.qr-code').data('username') + '?secret=' + $('.qr-code').data('secret') + '&issuer=Keizai');

	} else if($('.tfa-enabled').is(':visible')) {
		$('body').on('click', '.tfa-enabled > .remove', function() {
			if(confirm(lang_modal_deletetfa)) {
				$.ajax({
					url: folder_name + 'ajax/account-remove-tfa.php',
					method: 'GET',
					beforeSend: function() {
						$('.tfa-enabled').hide();
						$('.tfa-status').show();
						$('.tfa-status > .working').show();
					},
					success: function(s) {
						setTimeout(function() {
							$('.tfa-enabled').hide();
							$('.tfa-status').show();
							$('.tfa-status > .working').hide();

							if(s == 'retry') {
								$('.tfa-status > .msg-tfa.retry').show();

							} else if(s == 'success') {
								$('.tfa-status > .removed').show();
								$('.tfa-status > .removed > .removed').show();
							}
						}, 200);
					},
					error: function(e) {
						setTimeout(function() {
							$('.tfa-enabled').hide();
							$('.tfa-status').show();
							$('.tfa-status > .working').hide();
							$('.tfa-status > .retry').show();
						}, 200);

						setTimeout(function() {
							$('.tfa-enabled').show();
							$('.tfa-status').hide();
							$('.tfa-status > .retry').hide();
						}, 1500);
					}
				});
			}
		});
	}







	$('body').on('focus', '[name="field-sum-income"]', function() {
		let sum = ($(this).val().length == 0 ? '0' : $(this).val());

		$('.item.income > .field.sum > div > .icon:not(.wit)').html(svgicon('calculator'));

		if(sum != '0') {
			$('.item.income > .field > .math').show();
			$('.item.income > .field > .math > .sum').text(format_number(math.evaluate(sum.replace(',', '.')), 2));
		}

		if(is_portable == false) {
			$('.balance').hide();
			$('.savings').hide();
			$(this).removeClass('width-sum-original').addClass('width-sum-extended');
		}
	});

	$('body').on('blur', '[name="field-sum-income"]', function() {
		$('.item.income > .field > .math').hide();
		$('.sum > div > .icon:not(.wit)').html(svgicon('money'));

		if(is_portable == false) {
			$('.balance').show();
			$('.savings').show();
			$(this).removeClass('width-sum-extended').addClass('width-sum-original');
		}
	});

	$('body').on('input paste', '[name="field-sum-income"]', function() {
		let sum = $(this).val();
		let lastchar = sum.substr(sum.length - 1);

		if(sum.length == 0) {
			$('.item.income > .field > .math').hide();

		} else {
			$('.item.income > .field > .math').show();

			if(!arr_chars.includes(lastchar)) {
				$('.item.income > .field > .math > .sum').text(format_number(math.evaluate(sum.replace(',', '.')), 2));
			}
		}
	});







	$('body').on('focus', '[name="field-sum-balance"]', function() {
		let sum = ($(this).val().length == 0 ? '0' : $(this).val());

		$('.item.balance > .field.sum > div > .icon:not(.wit)').html(svgicon('calculator'));

		if(sum != '0') {
			$('.item.balance > .field > .math').show();
			$('.item.balance > .field > .math > .sum').text(format_number(math.evaluate(sum.replace(',', '.')), 2));
		}

		if(is_portable == false) {
			$('.income').hide();
			$('.savings').hide();
			$('.item.balance').removeClass('marginleft-10').addClass('margin-0');
			$(this).removeClass('width-sum-original').addClass('width-sum-extended');
		}
	});

	$('body').on('blur', '[name="field-sum-balance"]', function() {
		$('.item.balance > .field > .math').hide();
		$('.sum > div > .icon:not(.wit)').html(svgicon('money'));

		if(is_portable == false) {
			$('.income').show();
			$('.savings').show();
			$('.item.balance').removeClass('margin-0').addClass('marginleft-10');
			$(this).removeClass('width-sum-extended').addClass('width-sum-original');
		}
	});

	$('body').on('input paste', '[name="field-sum-balance"]', function() {
		let sum = $(this).val().replace(',', '.');
		let lastchar = sum.substr(sum.length - 1);

		if(sum.length == 0) {
			$('.item.balance > .field > .math').hide();

		} else {
			$('.item.balance > .field > .math').show();

			if(!arr_chars.includes(lastchar)) {
				$('.item.balance > .field > .math > .sum').text(format_number(math.evaluate(sum.replace(',', '.')), 2));
			}
		}
	});







	$('body').on('focus', '[name="field-sum-savings"]', function() {
		let sum = ($(this).val().length == 0 ? '0' : $(this).val());

		$('.item.savings > .field.sum > div > .icon:not(.wit)').html(svgicon('calculator'));

		if(sum != '0') {
			$('.item.savings > .field > .math').show();
			$('.item.savings > .field > .math > .sum').text(format_number(math.evaluate(sum.replace(',', '.')), 2));
		}

		if(is_portable == false) {
			$('.income').hide();
			$('.balance').hide();
			$('.item.savings').removeClass('marginleft-10').addClass('margin-0');
			$(this).removeClass('width-sum-original').addClass('width-sum-extended');
		}
	});

	$('body').on('blur', '[name="field-sum-savings"]', function() {
		$('.item.savings > .field > .math').hide();
		$('.sum > div > .icon:not(.wit)').html(svgicon('money'));

		if(is_portable == false) {
			$('.income').show();
			$('.balance').show();
			$('.item.savings').removeClass('margin-0').addClass('marginleft-10');
			$(this).removeClass('width-sum-extended').addClass('width-sum-original');
		}
	});

	$('body').on('input paste', '[name="field-sum-savings"]', function() {
		let sum = $(this).val().replace(',', '.');
		let lastchar = sum.substr(sum.length - 1);

		if(sum.length == 0) {
			$('.math').hide();

		} else {
			$('.item.savings > .field > .math').show();

			if(!arr_chars.includes(lastchar)) {
				$('.item.savings > .field > .math > .sum').text(format_number(math.evaluate(sum.replace(',', '.')), 2));
			}
		}
	});







	$('body').on('input paste', '[name="field-email"]', function() {
		if($(this).val().length == 0) {
			$('[name="check-expenses-expires"]').prop({ 'disabled': true, 'checked': false });
		} else {
			$('[name="check-expenses-expires"]').prop({ 'disabled': false });
		}
	});







	$('body').on('input paste', '[name="field-incomeday"]', function() {
		$('.update-expiration-dates > .update').hide();

		if($(this).val().length == 0) {
			$('[name="field-period-start"]').prop({ 'disabled': false });
			$('[name="field-period-end"]').prop({ 'disabled': false });
			$('.update-expiration-dates > .fill-in-first').show();
			$('.update-expiration-dates > .reload-page-first').hide();

		} else {
			$('[name="field-period-start"]').prop({ 'disabled': true });
			$('[name="field-period-end"]').prop({ 'disabled': true });
			$('.update-expiration-dates > .fill-in-first').hide();
			$('.update-expiration-dates > .reload-page-first').show();
		}
	});

	$('body').on('input', '[name="field-period-start"],[name="field-period-end"]', function() {
		if($('[name="field-period-start"]').val().length == 0 && $('[name="field-period-end"]').val().length != 0 || $('[name="field-period-start"]').val().length != 0 && $('[name="field-period-end"]').val().length == 0) {
			$('[name="field-incomeday"]').prop({ 'disabled': true });
			$('.update-expiration-dates > .update').hide();
			$('.update-expiration-dates > .fill-in-first').show();
			$('.update-expiration-dates > .reload-page-first').hide();

		} else if($('[name="field-period-start"]').val().length == 0 && $('[name="field-period-end"]').val().length == 0) {
			$('[name="field-incomeday"]').prop({ 'disabled': false });
			$('.update-expiration-dates > .update').hide();
			$('.update-expiration-dates > .fill-in-first').show();
			$('.update-expiration-dates > .reload-page-first').hide();

		} else {
			$('[name="field-incomeday"]').prop({ 'disabled': true });
			$('.update-expiration-dates > .update').hide();
			$('.update-expiration-dates > .fill-in-first').hide();
			$('.update-expiration-dates > .reload-page-first').show();
		}
	});







	$('body').on('click', '#option-recurrent-autoupdate', function() {
		if($(this).is(':checked')) {
			$('#option-recurrent-autoupdate-all').prop({ 'disabled': false });
		} else {
			$('#option-recurrent-autoupdate-all').prop({ 'disabled': true, 'checked': false });
		}
	});







	$('body').on('click', '.clean-sessions > #clean', function() {
		if(confirm(lang_modal_deletesessions)) {
			$.ajax({
				url: folder_name + 'ajax/account-delete-sessions.php',
				method: 'GET',
				beforeSend: function() {
					$('.clean-sessions.working').removeClass('hidden');
					$('.clean-sessions.link').addClass('hidden');
				},
				success: function(s) {
					$('.sessions > .items').html(s);
					$('.clean-sessions.inactive').removeClass('hidden');
					$('.clean-sessions.working').addClass('hidden');
				},
				error: function(e) {
					msg(lang_msg_error_othererror, 'error', 'data');
					console.log(e);
				}
			});
		}
	});



	$('body').on('click', '.options > .delete:not(.inactive)', function() {
		if(confirm(lang_modal_deletesession)) {
			let idsession = $(this).data('idsession');

			$.ajax({
				url: folder_name + 'ajax/account-delete-session.php?ids=' + idsession,
				method: 'GET',
				beforeSend: function() {
					$('.options > .wait[data-idsession="' + idsession + '"]').show();
					$('.options > .delete[data-idsession="' + idsession + '"]').hide();
				},
				success: function(s) {
					if(debugging == true) {
						msg(lang_msg_debugging, 'info', 'data');
						console.log(s);

					} else {
						$('.options > .wait[data-idsession="' + idsession + '"]').hide();
						$('.options > .delete[data-idsession="' + idsession + '"]').show();
						$('.itemset[data-idsession="' + idsession + '"]').remove();
						$('.moreinfo[data-idsession="' + idsession + '"]').hide();

						if(s == '1') {
							$('.clean-sessions.inactive').removeClass('hidden');
							$('.clean-sessions.working').addClass('hidden');
							$('.clean-sessions.link').addClass('hidden');
						}
					}
				},
				error: function(e) {
					//
				}
			});
		}
	});


});