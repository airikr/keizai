$(document).ready(function() {


	$('body').on('click', '[name="button-save"]', function() {
		let form = new FormData($('form')[0]);

		$.ajax({
			url: folder_name + 'ajax/save-notes.php',
			method: 'POST',
			beforeSend: function() {
				msg(lang_msg_working, 'wait');
			},

			success: function(s) {
				if(debugging == true) {
					msg(lang_msg_debugging, 'info');
					console.log(s);


				} else {
					msg(lang_msg_done_saved, 'done');
				}
			},

			error: function(e) {
				msg(lang_msg_error_othererror, 'error');
				console.log(e);
			},
			data: form,
			cache: false,
			contentType: false,
			processData: false
		});
	});


});