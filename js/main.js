url = window.location.href;
folder_name = $('body').data('folder');
debugging = ($('body').data('debugging') == 'n' ? false : true);
minified = ($('body').data('minified') == 'n' ? false : true);
signedin = ($('body').data('signedin') == 'n' ? false : true);
arr_chars = [
	'+', '-', '*', '/', ")", "("
];

if('serviceWorker' in navigator) {
	navigator.serviceWorker.register(folder_name + 'sw.js');
} else {
	console.log('Service worker is not supported');
}



$(document).ready(function () {


	let current_time = new Date();
	year = current_time.getFullYear();
	month = ((current_time.getMonth() + 1) < 10 ? '0' : '') + (current_time.getMonth() + 1);
	day = (current_time.getDate() < 10 ? '0' : '') + current_time.getDate();
	is_portable = $('.portable-menu').is(':visible');
	scrollto = (is_portable == true ? 90 : (debugging == false ? 20 : 75));
	timer_start = $('body').data('signout-timer');

	$('.timer').text(timer_start);



	if(signedin == true) {
		$('body').on('click', '.download', function() {
			let classname = $(this).data('classname');
			let fname = $(this).data('filename');
			let nopadding = $(this).data('nopadding');
	
			$('.screenshot > .wait').show();
			$('.screenshot > .link').hide();
	
			setTimeout(function() {
				$('a').addClass('screenshot-nounderline');
				$('.currency-website').hide();
				$('.currency-screenshot').show();

				if($('.payed').is(':visible')) {
					$('.payed').addClass('scr');
				}

				if($('#overview').is(':visible')) {
					$('.new').hide();
				}

				if(nopadding == 0) {
					$(classname).addClass('screenshot-padding');
				}

				html2canvas($(classname)[0], {
					backgroundColor: get_bgcolor($('body')),
					logging: false
				}).then(function(canvas) {
					save_as(canvas.toDataURL(), fname + '.png');
					$('.screenshot > .wait').hide();
					$('.screenshot > .link').show();
					$('.currency-website').show();
					$('.currency-screenshot').hide();
					$('a').removeClass('screenshot-nounderline');

					if($('.payed').is(':visible')) {
						$('.payed').removeClass('scr');
					}

					if($('#overview').is(':visible')) {
						$('.new').show();
					}

					if(nopadding == 0) {
						$(classname).removeClass('screenshot-padding');
					}
				});
			}, 100);
		});


		$('body').on('click', '.signout', function() {
			if(confirm($(this).data('signout'))) {
				$.ajax({
					url: folder_name + 'ajax/account-signout.php',
					method: 'GET',
					beforeSend: function() {
						$('nav > div > div > .signed-in').hide();
						$('nav > div > div > .signing-out').show();
					},
					success: function(s) {
						window.location = folder_name;
					},
					error: function(e) {
						//
					}
				});
			}
		});


		var timer_interval = setInterval(function() {
			var timer = timer_start.split(':');
			var minutes = parseInt(timer[0], 10);
			var seconds = parseInt(timer[1], 10);

			--seconds;
			minutes = (seconds < 0 ? --minutes : minutes);

			if(minutes == 1 && seconds == 0) {
				$('.modal').show();

			} else if(minutes == 0 && seconds == 0) {
				clearInterval(timer_interval);
				$.ajax({
					url: folder_name + 'ajax/account-signout.php',
					method: 'GET',
					beforeSend: function() {
						$('nav > div > div > .signed-in').hide();
						$('nav > div > div > .signing-out').show();
					},
					success: function(s) {
						window.location = folder_name;
					},
					error: function(e) {
						//
					}
				});
			}

			minutes = (minutes < 10 ? '0' : '') + minutes;
			seconds = (seconds < 0 ? 59 : seconds);
			seconds = (seconds < 10 ? '0' : '') + seconds;

			$('.timer').text(minutes + ':' + seconds);
			timer_start = minutes + ':' + seconds;
		}, 1000);
	}



	$('body').on('click', '.wit', function() {
		let element = $(this).data('element');

		$.ajax({
			url: folder_name + 'ajax/what-is-this.php?ele=' + element,
			method: 'GET',
			beforeSend: function() {
				//
			},
			success: function(s) {
				$('.what-is-this-modal').fadeIn(125);
				$('.what-is-this-modal > div > .content').html(s);
			},
			error: function(e) {
				console.log(e);
			}
		});
	});

	$('body').on('click', '.what-is-this-modal > div > .close', function() {
		$('.what-is-this-modal').fadeOut(125);
	});

	$('body').on('keyup', function(ku) {
		var code = ku.keyCode || ku.which;
		if(code == 27) {
			$('.what-is-this-modal').fadeOut(125);
		}
	});

	/*tippy('.what-is-this > a', {
		html(reference) {
			const content = reference.parentElement.firstChild.nextSibling.innerHTML;
			return content
		},
		interactive: true,
		placement: 'auto',
		animation: false,
		trigger: 'click'
	});

	tippy('[title]', {
		content(reference) {
			const title = reference.getAttribute('title');
			reference.removeAttribute('title');
			return title;
		},
		boundry: 'window',
		followCursor: 'horizontal',
		theme: 'forest',
		placement: 'top',
		touch: false,
		animation: false
	});*/



	if(localStorage.getItem('theme') !== null && $('body').data('signedin') == 'n') {
		let theme = localStorage.getItem('theme');

		$('link.theme').prop({
			'href': folder_name + 'css/theme-' + (theme == 'dark' ? 'dark-everforest' : 'light-everforest') + '.css'
		});

		if(theme == 'light') {
			$('.change.theme > .theme.dark').removeClass('hidden').addClass('visible');
			$('.change.theme > .theme.light').removeClass('visible').addClass('hidden');

		} else {
			$('.change.theme > .theme.dark').removeClass('visible').addClass('hidden');
			$('.change.theme > .theme.light').removeClass('hidden').addClass('visible');
		}

	} else {
		if($('body').data('signedin') == 'n') {
			$('.change.theme > .theme.dark').removeClass('visible').addClass('hidden');
			$('.change.theme > .theme.light').removeClass('hidden').addClass('visible');
		}
	}







	$('body').on('click', 'a.theme', function() {
		let theme = $(this).attr('class').split(' ');
		console.log(theme);

		if($('body').data('signedin') == 'y') {
			$.ajax({
				url: folder_name + 'ajax/change-theme.php?the=' + theme[1],
				method: 'GET',
				beforeSend: function() {
					$('.change.theme > .theme').removeClass('visible').addClass('hidden');
					$('.change.theme > .wait').removeClass('hidden').addClass('visible');
				},
				success: function(s) {
					$('.change.theme > .wait').removeClass('visible').addClass('hidden');

					$('link.theme').prop({
						'href': folder_name + 'css/themes/' + (minified == false ? '' : 'minified/') + (theme[1] == 'dark' ? 'dark-everforest' : 'light-everforest') + (minified == false ? '.css?' + Math.floor(Date.now() / 1000) : '.min.css')
					});

					if(theme[1] == 'light') {
						$('.change.theme > .theme.dark').removeClass('hidden').addClass('visible');
						$('.change.theme > .theme.light').removeClass('visible').addClass('hidden');

						if($('[name="list-theme"]').is(':visible')) {
							$('[name="list-theme"]').val(0);
						}

					} else {
						$('.change.theme > .theme.dark').removeClass('visible').addClass('hidden');
						$('.change.theme > .theme.light').removeClass('hidden').addClass('visible');

						if($('[name="list-theme"]').is(':visible')) {
							$('[name="list-theme"]').val(1);
						}
					}
				},
				error: function(e) {
					console.log(e);

					$('.change.theme > .wait').removeClass('visible').addClass('hidden');
					$('.change.theme > .error').removeClass('hidden').addClass('visible color-red');

					setTimeout(function() {
						$('.change.theme > .theme.' + theme[1]).removeClass('hidden').addClass('visible');
						$('.change.theme > .error').removeClass('visible').addClass('hidden');
					}, 1000);
				}
			});


		} else {
			localStorage.setItem('theme', theme[1]);

			$('link.theme').prop({
				'href': folder_name + 'css/themes/' + (minified == false ? '' : 'minified/') + (theme[1] == 'dark' ? 'dark-everforest' : 'light-everforest') + (minified == false ? '.css?' + Math.floor(Date.now() / 1000) : '.min.css')
			});

			if(theme[1] == 'light') {
				$('.change.theme > .theme.dark').removeClass('hidden').addClass('visible');
				$('.change.theme > .theme.light').removeClass('visible').addClass('hidden');

			} else {
				$('.change.theme > .theme.dark').removeClass('visible').addClass('hidden');
				$('.change.theme > .theme.light').removeClass('hidden').addClass('visible');
			}
		}
	});







	// Only activate the function when a textarea are visible
	if($('textarea').is(':visible')) {
		autosize($('textarea'));
	}

	// Only allow digits for fields that are meant for only digits
	$('[inputmode="numeric"]').on('input', function(e) {
		$(this).val($(this).val().replace(/[^0-9\+\(\)\-\*\/\.\,]/g, ''));
	});

	// Apply tabindex to {element} for a better tabbing experience
	if($('input,select,textarea').is(':visible')) {
		$('input:not([type="hidden"]),select,textarea').each(function(i) {
			$(this).prop({ 'tabindex': (i + 1) });
		});

		$('body').on('input', '[inputmode="numeric"]', function() {
			let val = $(this).val().replace(',', '.');
			$(this).val(val);
		});
	}

	// Select the associated text field
	$('body').on('click', '.field > .icon:not(.date), .field > div > .icon:not(.date)', function() {
		$(this).next('input').select();
	});


});













function readFile(input) {
	let file = input.files[0];
	let reader = new FileReader();

	reader.readAsText(file);

	reader.onload = function() {
		$('.filelabel > .browse > .text').text(file.name);
	};

	reader.onerror = function() {
		console.log(reader.error);
	};
}



function open_newtab(n) { let o = window.open(n, "_blank"); o.focus() }
function format_number(digits,decimal=0){return $.number(digits,decimal,',',' ')}
function select_firstfield() { $("input").each(function () { if ("" === this.value) return this.select(), !1 }) }
function goto(el, offset = 0) { $('html,body').animate({ scrollTop: (offset == 0 ? $(el).offset().top : ($(el).offset().top - offset)) }, 50); }

function msg(string, type, class_el, data, scroll) {
	let arr_types = {
		'error': {
			'icon': svgicon('msg-warning'),
			'color': 'red'
		},
		'info': {
			'icon': svgicon('msg-information'),
			'color': 'blue'
		},
		'wait': {
			'icon': svgicon('msg-waiting'),
			'color': 'blue'
		},
		'done': {
			'icon': svgicon('msg-done'),
			'color': 'green'
		}
	};



	if($('.msg' + (class_el == null ? '' : '.' + class_el) + ' > div').length == 0) {
		d = document.createElement('div');
		$(d).appendTo('.msg' + (class_el == null ? '' : '.' + class_el));
	}

	$('.msg' + (class_el == null ? '' : '.' + class_el)).show();
	$('.msg' + (class_el == null ? '' : '.' + class_el) + (data == null ? '' : data) + ' > div').html(arr_types[type]['icon'] + string).removeClass('color-blue').removeClass('color-red').removeClass('color-green').addClass('color-' + arr_types[type]['color']);

	$('input').blur();

	if(!$('#welcome').is(':visible') && scroll == null) {
		goto($((class_el == null ? '.msg' : '.msg.' + class_el)), (is_portable == true ? (70 + 10) : 15));
	}

	if(type == 'wait') {
		cursor('wait');
	} else {
		cursor('default');
	}
}



function svgicon($string) {
	arr = {
		'msg-warning': '<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-exclamation-mark" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 19v.01"></path><path d="M12 15v-10"></path></svg>',
		'msg-done': '<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-check" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.25" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M5 12l5 5l10 -10"></path></svg>',
		'msg-information': '<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-info-circle-filled" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 2c5.523 0 10 4.477 10 10a10 10 0 0 1 -19.995 .324l-.005 -.324l.004 -.28c.148 -5.393 4.566 -9.72 9.996 -9.72zm0 9h-1l-.117 .007a1 1 0 0 0 0 1.986l.117 .007v3l.007 .117a1 1 0 0 0 .876 .876l.117 .007h1l.117 -.007a1 1 0 0 0 .876 -.876l.007 -.117l-.007 -.117a1 1 0 0 0 -.764 -.857l-.112 -.02l-.117 -.006v-3l-.007 -.117a1 1 0 0 0 -.876 -.876l-.117 -.007zm.01 -3l-.127 .007a1 1 0 0 0 0 1.986l.117 .007l.127 -.007a1 1 0 0 0 0 -1.986l-.117 -.007z" stroke-width="0" fill="currentColor"></path></svg>',
		'msg-waiting': '<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-loader-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.25" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 3a9 9 0 1 0 9 9"></path></svg>',
		'calculator': '<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-calculator" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M4 3m0 2a2 2 0 0 1 2 -2h12a2 2 0 0 1 2 2v14a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2z"></path><path d="M8 7m0 1a1 1 0 0 1 1 -1h6a1 1 0 0 1 1 1v1a1 1 0 0 1 -1 1h-6a1 1 0 0 1 -1 -1z"></path><path d="M8 14l0 .01"></path><path d="M12 14l0 .01"></path><path d="M16 14l0 .01"></path><path d="M8 17l0 .01"></path><path d="M12 17l0 .01"></path><path d="M16 17l0 .01"></path></svg>',
		'money': '<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-cash" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M7 9m0 2a2 2 0 0 1 2 -2h10a2 2 0 0 1 2 2v6a2 2 0 0 1 -2 2h-10a2 2 0 0 1 -2 -2z"></path><path d="M14 14m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0"></path><path d="M17 9v-2a2 2 0 0 0 -2 -2h-10a2 2 0 0 0 -2 2v6a2 2 0 0 0 2 2h2"></path></svg>'
	};

	return arr[$string];
}



function cursor(type) {
	if(type == 'wait') {
		$('html,body').css({ 'cursor': 'wait' });
	} else if(type == 'default') {
		$('html,body').css({ 'cursor': 'auto' });
	}
}



function qrcode(element, size, content, image = null) {
	let colour_fill = '#232a2e';
	let colour_bg = '#f2efdf';

	if(image != null) {
		let img_swish = new Image();
		img_swish.src = image;
		img_swish.onload = function() {
			$(element).kjua({
				render: 'canvas',
				crisp: true,
				minVersion: 1,
				ecLevel: 'M',
				ratio: null,
				size: size,
				fill: colour_fill,
				back: colour_bg,
				text: content,
				rounded: 100,
				quiet: 0,
				mSize: 32,
				mPosX: 50,
				mPosY: 50,
				mode: 'image',
				image: img_swish
			});
		}

	} else {
		$(element).kjua({
			render: 'canvas',
			crisp: true,
			minVersion: 1,
			ecLevel: 'H',
			ratio: null,
			size: size,
			fill: colour_fill,
			back: colour_bg,
			text: content,
			rounded: 100,
			quiet: 0
		});
	}
}



function save_as(uri, filename) {
	let link = document.createElement('a');

	if(typeof link.download === 'string') {
		link.href = uri;
		link.download = filename;
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	} else {
		window.open(uri);
	}
}



function get_bgcolor($dom) {
	var bgcolor = '';
	while ($dom[0].tagName.toLowerCase() != 'html') {
		bgcolor = $dom.css('background-color');
		if(bgcolor != 'rgba(0, 0, 0, 0)' && bgcolor != 'transparent') { break; }
		$dom = $dom.parent();
	}

	return bgcolor;
}