<?php

	require_once 'site-header.php';

	use PragmaRX\Google2FA\Google2FA;



	$tfa = new Google2FA();
	$tfa_secret = $tfa->setWindow(0);
	$tfa_secret = $tfa->generateSecretKey(32);

	$count_sessions =
	sql("SELECT COUNT(id)
		 FROM sess
		 WHERE id_user = :_iduser
		", Array(
			'_iduser' => (int)$user['id']
		), 'count');

	if($count_sessions != 0) {
		$get_sessions =
		sql("SELECT *
			 FROM sess
			 WHERE id_user = :_iduser
			 ORDER BY timestamp_occurred DESC
			", Array(
				'_iduser' => (int)$user['id']
			));
	}







	echo '<section id="settings">';
		echo '<h1>'.$lang['titles']['settings']['settings'].'</h1>';

		echo '<div class="msg settings"></div>';



		echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate>';
			echo '<div>';
				echo '<div class="account">';
					echo '<h2 class="first info-below">'.$lang['subtitles']['settings']['account'].'</h2>';

					echo '<div class="side-by-side">';
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['username'];
							echo '</div>';

							echo '<div class="field with-explaination">';
								echo '<div class="icon">'.svgicon('user').'</div>';
								echo '<input type="text" name="field-username" aria-label="username" value="'.endecrypt($user['data_username'], false, true).'">';
								echo '<div class="icon wit" data-element="username" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
							echo '</div>';
						echo '</div>';


						echo '<div class="item email">';
							echo '<div class="label">';
								echo $lang['words']['email'];
							echo '</div>';

							echo '<div class="field with-explaination">';
								echo '<div class="icon">'.svgicon('email').'</div>';
								echo '<input type="email" name="field-email" placeholder="'.$lang['words']['optional'].'" aria-label="email"';
								echo (empty($user['data_email']) ? '' : ' value="'.endecrypt($user['data_email'], false)).'"';
								echo '>';

								echo '<div class="icon wit" data-element="email" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
							echo '</div>';
						echo '</div>';
					echo '</div>';


					echo '<div class="side-by-side last">';
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['passwords']['new'];
							echo '</div>';

							echo '<div class="field with-explaination">';
								echo '<div class="icon">'.svgicon('password').'</div>';
								echo '<input type="password" name="field-password-new" aria-label="new password">';
								echo '<div class="icon wit" data-element="password" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
							echo '</div>';
						echo '</div>';

						echo '<div class="item password-repeat">';
							echo '<div class="label">';
								echo $lang['words']['passwords']['repeat'];
							echo '</div>';

							echo '<div class="field">';
								echo '<div class="icon">'.svgicon('password').'</div>';
								echo '<input type="password" name="field-password-repeat" aria-label="repeat password">';
							echo '</div>';
						echo '</div>';
					echo '</div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['passwords']['current'];
						echo '</div>';

						echo '<div class="field with-explaination">';
							echo '<div class="icon">'.svgicon('password').'</div>';
							echo '<input type="password" name="field-password-current" aria-label="current password">';
							echo '<div class="icon wit" data-element="settings-password-current" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
						echo '</div>';
					echo '</div>';



					echo '<h2>'.$lang['subtitles']['tfa'].'</h2>';

					echo '<div class="tfa-disabled'.(empty($user['data_tfa']) ? '' : ' hidden').'"><div>';
						echo '<div class="qr-code" data-secret="'.$tfa_secret.'" data-username="'.endecrypt($user['data_username'], false, true).'"></div>';

						echo '<div class="secret">';
							echo '<input type="hidden" name="hidden-secret" value="'.$tfa_secret.'">';
							echo $lang['words']['settings']['tfa-info'];

							echo '<div class="secret">'.$tfa_secret.'</div>';

							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['words']['settings']['tfa-code'];
								echo '</div>';

								echo '<div class="field">';
									echo '<div class="icon">'.svgicon('tfa').'</div>';
									echo '<input type="text" inputmode="numeric" pattern="[0-9]{6}" name="field-tfa" maxlength="6" aria-label="6-digit number">';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '</div></div>';


					echo '<div class="tfa-backup">';
						echo '<div class="code"></div>';
						echo '<div class="text">'.$lang['messages']['settings']['tfa-backup'].'</div>';
					echo '</div>';


					echo '<div class="tfa-enabled'.(empty($user['data_tfa']) ? ' hidden' : '').'">';
						echo '<a href="javascript:void(0)" class="remove color-red">';
							echo $lang['words']['remove'];
						echo '</a>';
					echo '</div>';


					echo '<div class="tfa-status">';
						echo '<div class="working">'.svgicon('wait').'</div>';
						#echo '<div class="msg-tfa added"><div>'.svgicon('tick') . $lang['messages']['settings']['tfa-activated'].'</div></div>';
						echo '<div class="msg-tfa retry"><div>'.svgicon('exclamation') . $lang['messages']['settings']['tfa-retry'].'</div></div>';

						echo '<div class="removed">';
							echo '<div class="msg-tfa removed"><div>'.svgicon('tick') . $lang['messages']['settings']['tfa-removed'].'</div></div>';
							echo '<div class="information">'.$lang['messages']['settings']['tfa-removed-info'].'</div>';
						echo '</div>';
					echo '</div>';
				echo '</div>';



				echo '<div class="economy">';
					echo '<h2 class="first side-by-side">';
						whatisthis('settings-period');
						echo $lang['subtitles']['settings']['period'];
					echo '</h2>';

					echo '<div class="side-by-side">';
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['settings']['period-starts'];
							echo '</div>';

							echo '<div class="field">';
								echo '<div class="icon date">'.svgicon('calendar').'</div>';
								echo '<input type="date" name="field-period-start"';
								echo (empty($user['timestamp_period_start']) ? '' : ' value="'.date_(endecrypt($user['timestamp_period_start'], false), 'date').'"');
								echo (empty($user['data_incomeday']) ? '' : ' disabled');
								echo '>';
							echo '</div>';
						echo '</div>';


						echo '<div class="item period">';
							echo '<div class="label">';
								echo $lang['words']['settings']['period-ends'];
							echo '</div>';

							echo '<div class="field">';
								echo '<div class="icon date">'.svgicon('calendar').'</div>';
								echo '<input type="date" name="field-period-end"';
								echo (empty($user['timestamp_period_end']) ? '' : ' value="'.date_(endecrypt($user['timestamp_period_end'], false), 'date').'"');
								echo (empty($user['data_incomeday']) ? '' : ' disabled');
								echo '>';
							echo '</div>';
						echo '</div>';


						echo '<div class="item incomeday">';
							echo '<div class="label">';
								echo $lang['words']['settings']['day-income'];
							echo '</div>';

							echo '<div class="field">';
								echo '<div class="icon date">'.svgicon('calendar').'</div>';
								echo '<input type="text" inputmode="numeric" pattern="[0-9]{1,2}" name="field-incomeday" maxlength="2"';
								echo (empty($user['data_incomeday']) ? '' : ' value="'.endecrypt($user['data_incomeday'], false).'"');
								echo ((empty($user['timestamp_period_start']) AND empty($user['timestamp_period_end'])) ? '' : ' disabled');
								echo '>';
							echo '</div>';
						echo '</div>';
					echo '</div>';



					echo '<div class="update-expiration-dates">';
						echo '<a href="javascript:void(0)" class="update'.(($period_start_empty == true AND $period_end_empty == true AND $period_incomeday_empty == true) ? ' ishidden' : '').'" data-modal="'.$lang['modals']['update-expirationdates'].'">';
							echo svgicon('calendar-repeat') . $lang['forms']['move-recurrent-expenses'];
						echo '</a>';

						echo '<div class="updating color-blue wait">';
							echo svgicon('wait') . $lang['messages']['move-recurrent-expenses']['updating'];
						echo '</div>';

						echo '<div class="updated color-green">';
							echo svgicon('tick') . $lang['messages']['move-recurrent-expenses']['updated'];
						echo '</div>';

						echo '<div class="update-not-needed color-blue">';
							echo svgicon('info') . $lang['messages']['move-recurrent-expenses']['noupdate'];
						echo '</div>';

						echo '<div class="debugging color-blue">';
							echo svgicon('info') . $lang['messages']['debugging'];
						echo '</div>';

						echo '<div class="reload-page-first color-grey">';
							echo svgicon('calendar-repeat') . $lang['messages']['update-expenses-reload-page'];
						echo '</div>';

						echo '<div class="fill-in-first color-grey'.(($period_start_empty == true AND $period_end_empty == true AND $period_incomeday_empty == true) ? '' : ' ishidden').'">';
							echo svgicon('calendar-repeat') . $lang['messages']['update-expenses-fill-in'];
						echo '</div>';
					echo '</div>';



					echo '<h2>'.$lang['subtitles']['settings']['economy'].'</h2>';

					echo '<div class="side-by-side">';
						echo '<div class="item income">';
							echo '<div class="label">';
								echo $lang['words']['settings']['sum-income'];
							echo '</div>';

							echo '<div class="field sum with-explaination">';
								echo '<div>';
									echo '<div class="icon">'.svgicon('money').'</div>';
									echo '<input type="text" inputmode="numeric" pattern="[0-9]*" name="field-sum-income" aria-label="income sum"';
									echo (empty($user['data_sum_income']) ? '' : ' value="'.str_replace('.00', '', format_number(endecrypt($user['data_sum_income'], false), 2, '.', '')).'"');
									echo '>';

									echo '<div class="icon wit" data-element="settings-sum-income" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
								echo '</div>';

								echo '<div class="math">';
									echo '<span class="equals">=</span>';
									echo '<span class="sum"></span>';
								echo '</div>';
							echo '</div>';
						echo '</div>';


						echo '<div class="item balance">';
							echo '<div class="label">';
								echo $lang['words']['settings']['sum-current'];
							echo '</div>';

							echo '<div class="field sum with-explaination">';
								echo '<div>';
									echo '<div class="icon">'.svgicon('money').'</div>';
									echo '<input type="text" inputmode="numeric" pattern="[0-9]*" name="field-sum-balance" aria-label="current sum"';
									echo (empty($user['data_sum_balance']) ? '' : ' value="'.str_replace('.00', '', format_number(endecrypt($user['data_sum_balance'], false), 2, '.', '')).'"');
									echo '>';

									echo '<div class="icon wit" data-element="settings-sum-current" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
								echo '</div>';

								echo '<div class="math">';
									echo '<span class="equals">=</span>';
									echo '<span class="sum"></span>';
								echo '</div>';
							echo '</div>';
						echo '</div>';


						echo '<div class="item savings">';
							echo '<div class="label">';
								echo $lang['words']['settings']['sum-savings'];
							echo '</div>';

							echo '<div class="field sum with-explaination">';
								echo '<div>';
									echo '<div class="icon">'.svgicon('money').'</div>';
									echo '<input type="text" inputmode="numeric" pattern="[0-9]+" name="field-sum-savings" aria-label="savings sum"';
									echo (empty($user['data_sum_savings']) ? '' : ' value="'.str_replace('.00', '', format_number(endecrypt($user['data_sum_savings'], false), 2, '.', '')).'"');
									echo '>';

									echo '<div class="icon wit" data-element="settings-sum-savings" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
								echo '</div>';

								echo '<div class="math">';
									echo '<span class="equals">=</span>';
									echo '<span class="sum"></span>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '</div>';



					echo '<h2>'.$lang['subtitles']['settings']['adaptation'].'</h2>';

					echo '<div class="side-by-side">';
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['settings']['language'];
							echo '</div>';

							echo '<div class="field">';
								echo '<select name="list-language">';
									foreach(glob($dir_languages.'/*') AS $langfile) {
										$fileinfo = pathinfo($langfile);
										echo '<option value="'.$fileinfo['filename'].'"';
										echo (endecrypt($user['data_language'], false) == $fileinfo['filename'] ? ' selected' : ((empty($user['data_language']) AND $fileinfo['filename'] == 'se') ? ' selected' : ''));
										echo '>'.mb_strtoupper($fileinfo['filename']).'</option>';
									}
								echo '</select>';
							echo '</div>';
						echo '</div>';


						echo '<div class="item currency">';
							echo '<div class="label">';
								echo $lang['words']['settings']['currency'];
							echo '</div>';

							echo '<div class="field">';
								echo '<select name="list-currency">';
									foreach($arr_currencies AS $currency) {
										echo '<option value="'.mb_strtolower($currency).'"';
										echo (endecrypt($user['data_currency'], false) == mb_strtolower($currency) ? ' selected' : '');
										echo '>'.$currency.'</option>';
									}
								echo '</select>';
							echo '</div>';
						echo '</div>';


						echo '<div class="item theme">';
							echo '<div class="label">';
								echo $lang['words']['settings']['theme'];
							echo '</div>';

							echo '<div class="field">';
								echo '<select name="list-theme">';
									echo '<option value="0"'.((empty($user['data_theme']) OR $user['data_theme'] == 0) ? ' selected' : '').'>'.$lang['words']['light'].' (Everforest)</option>';
									echo '<option value="1"'.($user['data_theme'] == 1 ? ' selected' : '').'>'.$lang['words']['dark'].' (Everforest)</option>';
									echo '<option value="2"'.($user['data_theme'] == 2 ? ' selected' : '').'>'.$lang['words']['dark'].' (Dracula)</option>';
									echo '<option value="3"'.($user['data_theme'] == 3 ? ' selected' : '').'>'.$lang['words']['dark'].' (Tokyo Night)</option>';
								echo '</select>';
							echo '</div>';
						echo '</div>';
					echo '</div>';



					echo '<h2>'.$lang['subtitles']['settings']['options'].'</h2>';

					echo '<div class="checkboxes">';
						echo '<h3 class="first">'.$lang['subtitles']['notifications'].'</h3>';

						echo checkbox(
							$lang['forms']['checkboxes']['expenses-expires'],
							'expenses-expires',
							null,
							(empty($user['check_email_expensesexpires']) ? null : true),
							(empty($user['data_email']) ? true : false)
						);


						echo '<div class="space"></div>';


						echo '<h3>'.$lang['subtitles']['privacy'].'</h3>';

						echo checkbox(
							$lang['forms']['checkboxes']['share-allow'],
							'option-share-allow',
							null,
							(empty($user['check_option_share_allow']) ? null : true)
						);


						echo '<div class="space"></div>';


						echo '<h3>'.$lang['subtitles']['appearance'].'</h3>';

						echo checkbox(
							$lang['forms']['checkboxes']['align-center'],
							'option-align-center',
							null,
							(empty($user['check_option_align_center']) ? null : true)
						);


						echo '<div class="space"></div>';


						echo '<h3>'.$lang['subtitles']['others'].'</h3>';

						echo checkbox(
							$lang['forms']['checkboxes']['hidedate-expenses'],
							'option-hidedate-expenses',
							null,
							(empty($user['check_option_hidedate_expenses']) ? null : true)
						);

						echo checkbox(
							$lang['forms']['checkboxes']['hidedate-debts'],
							'option-hidedate-debts',
							null,
							(empty($user['check_option_hidedate_debts']) ? null : true)
						);

						echo checkbox(
							$lang['forms']['checkboxes']['hidedate-loans'],
							'option-hidedate-loans',
							null,
							(empty($user['check_option_hidedate_loans']) ? null : true)
						);

						echo checkbox(
							$lang['forms']['checkboxes']['allow-weekends'],
							'option-allow-weekends',
							null,
							(empty($user['check_option_allow_weekends']) ? null : true)
						);
					echo '</div>';
				echo '</div>';
			echo '</div>';



			echo '<div class="button">';
				echo '<input type="submit" name="button-save" value="'.$lang['words']['buttons']['save'].'">';
			echo '</div>';
		echo '</form>';







		echo '<section id="extra">';
			echo '<div class="sessions">';
				echo '<h1>'.$lang['titles']['settings']['sessions'].'</h1>';

				echo '<div class="information">';
					foreach($lang['settings-info']['sessions'] AS $content) {
						echo $Parsedown->text($content);
					}
				echo '</div>';


				echo '<div class="clean-sessions inactive'.($count_sessions == 1 ? '' : ' hidden').'">';
					echo svgicon('trash');
					echo $lang['words']['settings']['delete-sessions'];
				echo '</div>';

				echo '<div class="clean-sessions working hidden">';
					echo '<span class="wait">'.svgicon('wait').'</span>';
					echo $lang['words']['settings']['delete-sessions'];
				echo '</div>';

				echo '<div class="clean-sessions link'.($count_sessions == 1 ? ' hidden' : '').'">';
					echo '<a href="javascript:void(0)" id="clean" class="color-red">';
						echo svgicon('trash');
						echo $lang['words']['settings']['delete-sessions'];
					echo '</a>';
				echo '</div>';


				echo '<div class="items">';
					echo '<div class="itemset head">';
						echo '<div class="options"></div>';
						echo '<div class="last-action">'.$lang['words']['dates']['last-active'].'</div>';
						echo '<div class="ipaddress">'.$lang['words']['ip-address'].'</div>';
						echo '<div class="browser">'.$lang['words']['browser'].'</div>';
						echo '<div class="os">'.$lang['words']['os-short'].'</div>';
					echo '</div>';

					foreach($get_sessions AS $session) {
						$useragentinfo = new WhichBrowser\Parser(endecrypt($session['data_useragent'], false));

						$lastaction =
						sql("SELECT timestamp_occurred
							 FROM sess_actions
							 WHERE id_user = :_iduser
							 AND id_session = :_idsession
							 ORDER BY timestamp_occurred DESC
							 LIMIT 1
							", Array(
								'_iduser' => (int)$user['id'],
								'_idsession' => (int)$session['id']
							), 'fetch');

						$get_sessioninfo =
						sql("SELECT *
							 FROM sess_actions
							 WHERE id_user = :_iduser
							 AND id_session = :_idsession
							 ORDER BY timestamp_occurred DESC
							", Array(
								'_iduser' => (int)$user['id'],
								'_idsession' => (int)$session['id']
							));


						echo '<div class="itemset" data-idsession="'.(int)$session['id'].'">';
							echo '<div class="options side-by-side">';
								echo '<div class="wait" data-idsession="'.(int)$session['id'].'">';
									echo svgicon('wait');
								echo '</div>';

								if($session['data_ipaddress'] == endecrypt(getip()) AND $session['data_useragent'] == endecrypt($useragent)) {
									echo '<div class="delete inactive" title="'.$lang['tooltips']['session-delete-own'].'">';
										echo svgicon('trash-disabled');
									echo '</div>';

								} else {
									echo '<a href="javascript:void(0)" class="delete color-red" data-idsession="'.(int)$session['id'].'" title="'.$lang['tooltips']['session-delete'].'">';
										echo svgicon('trash');
									echo '</a>';
								}

								echo '<a href="javascript:void(0)" class="show more" data-idsession="'.(int)$session['id'].'" title="'.$lang['tooltips']['session-info-show'].'">';
									echo svgicon('eye-opened');
								echo '</a>';

								echo '<a href="javascript:void(0)" class="show less" data-idsession="'.(int)$session['id'].'" title="'.$lang['tooltips']['session-info-hide'].'">';
									echo svgicon('eye-closed');
								echo '</a>';
							echo '</div>';


							echo '<div class="last-action datetime">';
								if(empty($lastaction['timestamp_occurred'])) {
									echo '-';
								} else {
									echo date_($lastaction['timestamp_occurred'], 'datetime');
								}
							echo '</div>';

							echo '<div class="ipaddress ip">';
								echo (empty($user['is_demo']) ? endecrypt($session['data_ipaddress'], false) : '0.0.0.0');
							echo '</div>';

							echo '<div class="browser">';
								echo $useragentinfo->browser->toString();
							echo '</div>';

							echo '<div class="os">';
								echo $useragentinfo->os->toString();
							echo '</div>';
						echo '</div>';



						echo '<div class="moreinfo" data-idsession="'.(int)$session['id'].'">';
							echo '<div class="details">';
								echo '<b>'.$lang['words']['ip-address'].':</b> <span class="ip">'.(empty($user['is_demo']) ? endecrypt($session['data_ipaddress'], false) : '0.0.0.0').'</span><br>';
								echo '<b>'.$lang['words']['browser'].':</b> '.$useragentinfo->browser->toString().'<br>';
								echo '<b>'.$lang['words']['os-full'].':</b> '.$useragentinfo->os->toString();
							echo '</div>';


							if(!empty($lastaction['timestamp_occurred'])) {
								echo '<div class="events">';
									echo '<b>'.$lang['words']['events'].':</b>';
									foreach($get_sessioninfo AS $sessioninfo) {
										echo '<div class="itemset">';
											echo '<div class="datetime">';
												echo date_($sessioninfo['timestamp_occurred'], 'datetime');
											echo '</div>';

											echo '<div class="action">';
												echo $arr_sessioncodes[$sessioninfo['data_code']]['icon'];
												echo $arr_sessioncodes[$sessioninfo['data_code']]['text'];
											echo '</div>';
										echo '</div>';
									}
								echo '</div>';
							}
						echo '</div>';
					}
				echo '</div>';
			echo '</div>';



			echo '<div class="manage-data">';
				echo '<h1>'.$lang['titles']['settings']['manage-data'].'</h1>';

				echo '<div class="msg data"></div>';


				echo '<div class="item">';
					echo '<a href="'.url('export-data-as-xlsx').'" title="'.$lang['tooltips']['export-data-xlsx'].'">';
						echo svgicon('file-xls') . $lang['words']['export-as-xlsx'];
					echo '</a>';
				echo '</div>';

				echo '<div class="item">';
					echo '<a href="'.url('export-data-as-pdf').'" title="'.$lang['tooltips']['export-data-pdf'].'">';
						echo svgicon('file-pdf') . $lang['words']['export-as-pdf'];
					echo '</a>';
				echo '</div>';

				echo '<div class="item">';
					echo '<div class="export">'.svgicon('database-export') . $lang['words']['export'].'</div>';

					echo '<a href="'.url('export-data-as-plaintext').'" id="export" title="'.$lang['tooltips']['export-data-plaintext'].'">';
						echo mb_strtolower($lang['words']['in-plaintext']);
					echo '</a>';
					echo '/';
					echo '<a href="'.url('export-data-as-encrypted').'" id="export" title="'.$lang['tooltips']['export-data-encrypted'].'">';
						echo mb_strtolower($lang['words']['encrypted']);
					echo '</a>';
				echo '</div>';

				echo '<div class="item">';
					echo '<a href="javascript:void(0)" id="delete-data" class="color-red" title="'.$lang['tooltips']['delete-data'].'">';
						echo svgicon('database-delete') . $lang['words']['delete-data'];
					echo '</a>';
				echo '</div>';

				echo '<div class="item">';
					echo '<a href="javascript:void(0)" id="delete-account" data-iduser="'.(int)$user['id'].'" class="color-red" title="'.$lang['tooltips']['delete-account'].'">';
						echo svgicon('user-delete') . $lang['words']['delete-account'];
					echo '</a>';
				echo '</div>';


				echo '<div class="import">';
					echo '<h2>'.$lang['subtitles']['settings']['import'].'</h2>';

					echo '<div class="msg import"></div>';

					echo '<div class="information">';
						foreach($lang['forms']['informations']['import'] AS $content) {
							echo $Parsedown->text($content);
						}
					echo '</div>';


					echo '<form action="javascript:void(0)" method="POST" enctype="multipart/form-data" id="import">';
						echo '<div class="msg"></div>';

						echo '<label class="filelabel">';
							echo '<input type="file" name="file" accept="text/json" onchange="readFile(this)">';
							echo '<div class="browse side-by-side">';
								echo '<div class="icon">'.svgicon('json').'</div>';
								echo '<div class="text">'.$lang['words']['browse'].'...</div>';
							echo '</div>';
						echo '</label>';

						echo '<div class="button">';
							echo '<input type="submit" name="button-import" value="'.$lang['words']['buttons']['import'].'">';
						echo '</div>';
					echo '</form>';
				echo '</div>';
			echo '</div>';
		echo '</section>';
	echo '</section>';







	require_once 'site-footer.php';

?>