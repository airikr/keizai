<?php

	# Include the required file
	require_once 'site-settings.php';



	# Log the action (if the user exists) so the user can review it afterwards
	# You can find `log_action()` in `functions/log-action.php`
	if($check_user != 0 AND $check_session != 0) {
		log_action('signed out');
	}

	session_unset();
	session_destroy();

	# Redirect the user
	# You can find `url()` in `functions/url.php`
	header("Location: ".url(''));
	exit;

?>