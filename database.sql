-- Adminer 4.8.1 MySQL 11.2.2-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP DATABASE IF EXISTS `keizai`;
CREATE DATABASE `keizai` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `keizai`;

DROP TABLE IF EXISTS `budget`;
CREATE TABLE `budget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `data_name` text NOT NULL,
  `data_sum_current` text DEFAULT NULL,
  `data_sum_goal` text NOT NULL,
  `data_sum_permonth` text DEFAULT NULL,
  `data_notes` longtext DEFAULT NULL,
  `data_order` int(11) DEFAULT NULL,
  `timestamp_created` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `budget_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_currency_from` varchar(3) NOT NULL,
  `data_currency_to` varchar(3) NOT NULL,
  `data_rate` varchar(30) NOT NULL,
  `timestamp_updated` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_account` int(11) DEFAULT NULL,
  `data_name` text NOT NULL,
  `data_sum` text NOT NULL,
  `data_sum_permonth` text DEFAULT NULL,
  `data_contact_phone` text DEFAULT NULL,
  `data_contact_email` text DEFAULT NULL,
  `data_type` text DEFAULT NULL,
  `data_category` text DEFAULT NULL,
  `data_currency` text DEFAULT NULL,
  `data_number_payment` text DEFAULT NULL,
  `data_number_ocr` text DEFAULT NULL,
  `data_number_phone` text DEFAULT NULL,
  `data_recurrence` text DEFAULT NULL,
  `data_recurrence_type` text DEFAULT NULL,
  `data_belongsto_debt` int(11) DEFAULT NULL,
  `data_notes` longtext DEFAULT NULL,
  `check_expense_payed` tinyint(4) DEFAULT NULL,
  `check_expense_extra` tinyint(4) DEFAULT NULL,
  `check_expense_recurrent` tinyint(4) DEFAULT NULL,
  `check_expense_correctinfo` tinyint(4) DEFAULT NULL,
  `check_debt_company` tinyint(4) DEFAULT NULL,
  `check_debt_paying` tinyint(4) DEFAULT NULL,
  `check_loan_out` tinyint(4) DEFAULT NULL,
  `is_expense` tinyint(4) DEFAULT NULL,
  `is_debt` tinyint(4) DEFAULT NULL,
  `is_loan` tinyint(4) DEFAULT NULL,
  `timestamp_date` int(11) DEFAULT NULL,
  `timestamp_date_end` text DEFAULT NULL,
  `timestamp_payed` text DEFAULT NULL,
  `timestamp_added` text DEFAULT NULL,
  `timestamp_edited` text DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `items_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `item_accounts`;
CREATE TABLE `item_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `data_name` text NOT NULL,
  `is_default` tinyint(4) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `item_accounts_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `loginattempts`;
CREATE TABLE `loginattempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `timestamp_attempted` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `loginattempts_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sess`;
CREATE TABLE `sess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `data_ipaddress` text NOT NULL,
  `data_useragent` text NOT NULL,
  `timestamp_occurred` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `sess_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sess_actions`;
CREATE TABLE `sess_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_session` int(11) NOT NULL,
  `id_item` int(11) DEFAULT NULL,
  `data_code` varchar(50) NOT NULL,
  `is_expense` tinyint(4) DEFAULT NULL,
  `is_debt` tinyint(4) DEFAULT NULL,
  `is_loan` tinyint(4) DEFAULT NULL,
  `is_notes` tinyint(4) DEFAULT NULL,
  `is_tfa_enabled` tinyint(4) DEFAULT NULL,
  `is_tfa_disabled` tinyint(4) DEFAULT NULL,
  `is_exporting` tinyint(4) DEFAULT NULL,
  `is_importing` tinyint(4) DEFAULT NULL,
  `is_delete_data` tinyint(4) DEFAULT NULL,
  `timestamp_occurred` text NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `id_session` (`id_session`),
  CONSTRAINT `sess_actions_ibfk_1` FOREIGN KEY (`id_session`) REFERENCES `sess` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `shares`;
CREATE TABLE `shares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_user_with` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `data_sum` text DEFAULT NULL,
  `data_payment` text DEFAULT NULL,
  `allow_deletion` tinyint(4) DEFAULT NULL,
  `allow_markas_payed` tinyint(4) DEFAULT NULL,
  `share_number_payment` tinyint(4) DEFAULT NULL,
  `share_number_ocr` tinyint(4) DEFAULT NULL,
  `share_number_phone` tinyint(4) DEFAULT NULL,
  `share_qrcodes` tinyint(4) DEFAULT NULL,
  `share_notes` tinyint(4) DEFAULT NULL,
  `is_read` tinyint(4) DEFAULT NULL,
  `timestamp_shared` int(11) NOT NULL,
  `timestamp_edited` int(11) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_item` (`id_item`),
  CONSTRAINT `shares_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `shares_ibfk_4` FOREIGN KEY (`id_item`) REFERENCES `items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_username` text NOT NULL,
  `data_password` text NOT NULL,
  `data_email` text DEFAULT NULL,
  `data_tfa` tinytext DEFAULT NULL,
  `data_tfa_backup` text DEFAULT NULL,
  `data_saltedstring_1` varchar(10) NOT NULL,
  `data_saltedstring_2` varchar(10) NOT NULL,
  `data_incomeday` text DEFAULT NULL,
  `data_sum_income` text DEFAULT NULL,
  `data_sum_balance` text DEFAULT NULL,
  `data_sum_savings` text DEFAULT NULL,
  `data_currency` text DEFAULT NULL,
  `data_language` text DEFAULT NULL,
  `data_notes` text DEFAULT NULL,
  `data_theme` tinyint(4) DEFAULT NULL,
  `check_email_expensesexpires` tinyint(4) DEFAULT NULL,
  `check_option_hidedate_expenses` tinyint(4) DEFAULT NULL,
  `check_option_hidedate_debts` tinyint(4) DEFAULT NULL,
  `check_option_hidedate_loans` tinyint(4) DEFAULT NULL,
  `check_option_align_center` tinyint(4) DEFAULT NULL,
  `check_option_share_allow` tinyint(4) DEFAULT NULL,
  `check_option_allow_weekends` tinyint(4) DEFAULT NULL,
  `is_admin` tinyint(4) DEFAULT NULL,
  `is_demo` tinyint(4) DEFAULT NULL,
  `timestamp_registered` text DEFAULT NULL,
  `timestamp_lastactive` int(11) DEFAULT NULL,
  `timestamp_period_start` text DEFAULT NULL,
  `timestamp_period_end` text DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2023-11-27 14:29:38
