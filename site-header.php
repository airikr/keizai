<?php

	require_once 'site-settings.php';



	echo '<!DOCTYPE html>';
	echo '<html lang="'.$lang['metadata']['language'].'">';

		echo '<head>';
			echo '<title>';
				echo (empty($site_title) ? 'Economy' : $site_title);
			echo '</title>';

			echo '<meta charset="UTF-8">';
			echo '<meta name="theme-color" content="#272e33">';

			echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, maximum-scale=5">';
			echo (count($robots) == 0 ? null : '<meta name="robots" content="'.implode(',', $robots).'">');
			echo (count($google) == 0 ? null : '<meta name="robots" content="'.implode(',', $google).'">');
			echo '<meta name="description" content="'.$lang['metadata']['description'].'">';

			echo '<meta http-equiv="cache-control" content="cache">';
			echo '<meta http-equiv="expires" content="'.(60*60*24).'">';

			echo '<meta property="og:locale" content="'.$lang['metadata']['locale'].'">';
			echo '<meta property="og:locale:alternative" content="'.$lang['metadata']['locale'].'">';
			echo '<meta property="og:title" content="'.$site_title.'">';
			echo '<meta property="og:site_name" content="'.$site_title.'">';
			echo '<meta property="og:type" content="website">';
			echo '<meta property="og:url" content="'.$site_url.'">';
			echo '<meta property="og:description" content="'.$lang['metadata']['description'].'">';

			echo '<meta property="og:image" content="'.$site_url.'/images/keizai-512.webp">';
			echo '<meta property="og:image:secure_url" content="'.$site_url.'/images/keizai-512.webp">';
			echo '<meta property="og:image:height" content="512">';
			echo '<meta property="og:image:width" content="512">';
			echo '<meta property="og:image:type" content="image/png">';

			echo '<meta property="twitter:card" content="summary_large_image"></meta>';
			echo '<meta property="twitter:url" content="'.$site_url.'">';
			echo '<meta property="twitter:title" content="'.$site_title.'">';
			echo '<meta property="twitter:description" content="'.$lang['metadata']['description'].'">';
			echo '<meta property="twitter:image" content="'.$site_url.'/images/keizai-512.webp">';

			echo '<link rel="canonical" href="'.$site_url.'">';
			echo '<link href="'.$site_url.'" rel="me">';
			echo (file_exists('manifest.json') ? '<link rel="manifest" href="'.url('manifest.json').'" crossorigin="use-credentials">' : null);

			echo '<link rel="apple-touch-icon" sizes="57x57" href="'.url($dir_favicons.'/apple-icon-57x57.png').'">';
			echo '<link rel="apple-touch-icon" sizes="60x60" href="'.url($dir_favicons.'/apple-icon-60x60.png').'">';
			echo '<link rel="apple-touch-icon" sizes="72x72" href="'.url($dir_favicons.'/apple-icon-72x72.png').'">';
			echo '<link rel="apple-touch-icon" sizes="76x76" href="'.url($dir_favicons.'/apple-icon-76x76.png').'">';
			echo '<link rel="apple-touch-icon" sizes="114x114" href="'.url($dir_favicons.'/apple-icon-114x114.png').'">';
			echo '<link rel="apple-touch-icon" sizes="120x120" href="'.url($dir_favicons.'/apple-icon-120x120.png').'">';
			echo '<link rel="apple-touch-icon" sizes="144x144" href="'.url($dir_favicons.'/apple-icon-144x144.png').'">';
			echo '<link rel="apple-touch-icon" sizes="152x152" href="'.url($dir_favicons.'/apple-icon-152x152.png').'">';
			echo '<link rel="apple-touch-icon" sizes="180x180" href="'.url($dir_favicons.'/apple-icon-180x180.png').'">';
			echo '<link rel="icon" type="image/png" sizes="192x192" href="'.url($dir_favicons.'/android-icon-192x192.png').'">';
			echo '<link rel="icon" type="image/png" sizes="32x32" href="'.url($dir_favicons.'/favicon-32x32.png').'">';
			echo '<link rel="icon" type="image/png" sizes="96x96" href="'.url($dir_favicons.'/favicon-96x96.png').'">';
			echo '<link rel="icon" type="image/png" sizes="16x16" href="'.url($dir_favicons.'/favicon-16x16.png').'">';
			echo '<link rel="shortcut icon" href="'.url($dir_images.'/favicon.ico').'">';
			echo '<link rel="manifest" href="'.url($dir_favicons.'/manifest.json').'">';

			echo '<meta name="msapplication-TileColor" content="#ffffff">';
			echo '<meta name="msapplication-TileImage" content="'.url($dir_favicons.'/ms-icon-144x144.png').'">';
			echo '<meta name="theme-color" content="#ffffff">';

			echo '<link type="text/css" rel="stylesheet preload" as="style" href="'.url($dir_css.'/desktop'.($minified == true ? '.min.css' : '.css?'.time())).'">';
			echo (!file_exists($dir_css_pages.'/'.str_replace('.php', '', $filename).'.css') ? null : '<link type="text/css" rel="stylesheet preload" as="style" href="'.url($dir_css_pages . ($minified == true ? '/minified' : '').'/'.str_replace('.php', '', $filename) . ($minified == true ? '.min.css' : '.css?'.time())).'">');
			echo '<link type="text/css" rel="stylesheet preload" as="style" href="'.url($dir_css.'/portable'.($minified == true ? '.min.css' : '.css?'.time())).'">';

			echo '<link type="text/css" class="theme" rel="stylesheet" as="style" href="';
			echo url($dir_css_themes . ($minified == true ? '/minified' : '').'/'.($is_loggedin == false ? 'dark-everforest' : (empty($user['data_theme']) ? 'light-everforest' : $arr_themes[$user['data_theme']])) . ($minified == true ? '.min.css' : '.css?'.time()));
			echo '">';


			if($filename == 'index.php' OR $filename == 'page-account-forgotpw.php' OR $filename == 'page-list.php' OR $filename == 'page-account-settings.php' OR $filename == 'page-item-manage.php' OR $filename == 'page-notes.php' OR $filename == 'page-budget-manage.php' OR $filename == 'page-budget.php') {
				?><script type="text/javascript">
					var lang_modal_deletesession = "<?php echo $lang['modals']['delete-session']; ?>";
					var lang_modal_deletesessions = "<?php echo $lang['modals']['delete-sessions']; ?>";
					var lang_modal_deleteaccount = "<?php echo $lang['modals']['delete-account']; ?>";
					var lang_modal_deletedata = "<?php echo $lang['modals']['delete-data']; ?>";
					var lang_modal_deletetfa = "<?php echo $lang['modals']['delete-tfa']; ?>";
					var lang_modal_deleteitemaccount = "<?php echo $lang['modals']['delete-item-account']; ?>";
					var lang_modal_import = "<?php echo $lang['modals']['import']; ?>";

					var lang_msg_debugging = "<?php echo $lang['messages']['debugging']; ?>";
					var lang_msg_working = "<?php echo $lang['messages']['forms']['working']['working']; ?>";
					var lang_msg_working_checkingdeletion = "<?php echo $lang['messages']['forms']['working']['checking-deletion']; ?>";
					var lang_msg_working_deletingdata = "<?php echo $lang['messages']['forms']['working']['deleting-data']; ?>";
					var lang_msg_working_deletingyourdata = "<?php echo $lang['messages']['forms']['working']['deleting-yourdata']; ?>";

					var lang_msg_info_tfa_enabled = "<?php echo $lang['messages']['forms']['info']['tfa-enabled']; ?>";
					var lang_msg_info_signingout = "<?php echo $lang['messages']['signing-out']; ?>";

					var lang_msg_error_othererror = "<?php echo $lang['messages']['forms']['errors']['other-error']; ?>";
					var lang_msg_error_serversize_ini = "<?php echo $lang['messages']['forms']['errors']['file-upload']['serversize-ini']; ?>";
					var lang_msg_error_serversize_form = "<?php echo $lang['messages']['forms']['errors']['file-upload']['serversize-form']; ?>";
					var lang_msg_error_partial = "<?php echo $lang['messages']['forms']['errors']['file-upload']['partial']; ?>";
					var lang_msg_error_nofile = "<?php echo $lang['messages']['forms']['errors']['file-upload']['nofile']; ?>";
					var lang_msg_error_notmpdir = "<?php echo $lang['messages']['forms']['errors']['file-upload']['notmpdir']; ?>";
					var lang_msg_error_cantwrite = "<?php echo $lang['messages']['forms']['errors']['file-upload']['cantwrite']; ?>";
					var lang_msg_error_extension = "<?php echo $lang['messages']['forms']['errors']['file-upload']['extension']; ?>";
					var lang_msg_error_unknown = "<?php echo $lang['messages']['forms']['errors']['file-upload']['unknown']; ?>";
					var lang_msg_error_fieldsempty = "<?php echo $lang['messages']['forms']['errors']['fields-empty']; ?>";
					var lang_msg_error_itemexists = "<?php echo $lang['messages']['forms']['errors']['item-exists']; ?>";
					var lang_msg_error_itemadded = "<?php echo $lang['messages']['forms']['errors']['item-added']; ?>";
					var lang_msg_error_cantdeletedata = "<?php echo $lang['messages']['forms']['errors']['cant-delete-data']; ?>";
					var lang_msg_error_username_notexists = "<?php echo $lang['messages']['forms']['errors']['username-notexists']; ?>";
					var lang_msg_error_username_taken = "<?php echo $lang['messages']['forms']['errors']['username-taken']; ?>";
					var lang_msg_error_email_notexists = "<?php echo $lang['messages']['forms']['errors']['email-notexists']; ?>";
					var lang_msg_error_email_taken = "<?php echo $lang['messages']['forms']['errors']['email-taken']; ?>";
					var lang_msg_error_email_notvalid = "<?php echo $lang['messages']['forms']['errors']['email-notvalid']; ?>";
					var lang_msg_error_toomanyattempts = "<?php echo $lang['messages']['forms']['errors']['too-many-attempts']; ?>";
					var lang_msg_error_pw_incorrect = "<?php echo $lang['messages']['forms']['errors']['password-incorrect']; ?>";
					var lang_msg_error_pw_incorrect_current = "<?php echo $lang['messages']['forms']['errors']['password-incorrect-current']; ?>";
					var lang_msg_error_pw_notsame = "<?php echo $lang['messages']['forms']['errors']['passwords-notsame']; ?>";
					var lang_msg_error_pw_tooshort = "<?php echo $lang['messages']['forms']['errors']['passwords-tooshort']; ?>";
					var lang_msg_error_tfa_incorrect = "<?php echo $lang['messages']['forms']['errors']['tfa-incorrect']; ?>";
					var lang_msg_error_tfa_onlydigits = "<?php echo $lang['messages']['forms']['errors']['tfa-onlydigits']; ?>";
					var lang_msg_error_file_serversize_ini = "<?php echo $lang['messages']['forms']['errors']['file-upload']['serversize-ini']; ?>";
					var lang_msg_error_file_serversize_form = "<?php echo $lang['messages']['forms']['errors']['file-upload']['serversize-form']; ?>";
					var lang_msg_error_file_partial = "<?php echo $lang['messages']['forms']['errors']['file-upload']['partial']; ?>";
					var lang_msg_error_file_nofile = "<?php echo $lang['messages']['forms']['errors']['file-upload']['nofile']; ?>";
					var lang_msg_error_file_notmpdir = "<?php echo $lang['messages']['forms']['errors']['file-upload']['notmpdir']; ?>";
					var lang_msg_error_file_cantwrite = "<?php echo $lang['messages']['forms']['errors']['file-upload']['cantwrite']; ?>";
					var lang_msg_error_file_extension = "<?php echo $lang['messages']['forms']['errors']['file-upload']['extension']; ?>";
					var lang_msg_error_file_unknown = "<?php echo $lang['messages']['forms']['errors']['file-upload']['unknown']; ?>";
					var lang_msg_error_resetcode_incorrect = "<?php echo $lang['messages']['forms']['errors']['resetcode-incorrect']; ?>";
					var lang_msg_error_resetcode_length = "<?php echo $lang['messages']['forms']['errors']['resetcode-length']; ?>";
					var lang_msg_error_permission_denied = "<?php echo $lang['messages']['forms']['errors']['permission-denied']; ?>";
					var lang_msg_error_accountdefault = "<?php echo $lang['messages']['forms']['errors']['account-default']; ?>";
					var lang_msg_error_accountexists = "<?php echo $lang['messages']['forms']['errors']['account-exists']; ?>";
					var lang_msg_error_sum_onlydigits_goal = "<?php echo $lang['messages']['forms']['errors']['sum-onlydigits-goal']; ?>";
					var lang_msg_error_sum_onlydigits_permonth = "<?php echo $lang['messages']['forms']['errors']['sum-onlydigits-permonth']; ?>";
					var lang_msg_error_sum_onlydigits_budget = "<?php echo $lang['messages']['forms']['errors']['sum-onlydigits-budget']; ?>";
					var lang_msg_error_sum_belowzero = "<?php echo $lang['messages']['forms']['errors']['sum-belowzero']; ?>";
					var lang_msg_error_sum_aboveonehundred = "<?php echo $lang['messages']['forms']['errors']['sum-aboveonehundred']; ?>";
					var lang_msg_error_period_wrongorder = "<?php echo $lang['messages']['forms']['errors']['period-wrongorder']; ?>";
					var lang_msg_error_period_missingdate_start = "<?php echo $lang['messages']['forms']['errors']['period-missingdate-start']; ?>";
					var lang_msg_error_period_missingdate_end = "<?php echo $lang['messages']['forms']['errors']['period-missingdate-end']; ?>";
					var lang_msg_error_onlydigits_recurrence = "<?php echo $lang['messages']['forms']['errors']['onlydigits-recurrence']; ?>";
					var lang_msg_error_onlybetween_1_12_recurrence = "<?php echo $lang['messages']['forms']['errors']['onlybetween-1-12-recurrence']; ?>";
					var lang_msg_error_cantfetchinfo = "<?php echo $lang['messages']['forms']['errors']['cant-fetch-info']; ?>";
					var lang_msg_error_alreadyshared = "<?php echo $lang['messages']['forms']['errors']['already-shared']; ?>";
					var lang_msg_error_cantsharetoself = "<?php echo $lang['messages']['forms']['errors']['cant-share-to-self']; ?>";
					var lang_msg_error_user_notexists = "<?php echo $lang['messages']['forms']['errors']['user-notexists']; ?>";
					var lang_msg_error_privacy_sharing_notallowed = "<?php echo $lang['messages']['forms']['errors']['privacy-sharing-notallowed']; ?>";
					var lang_msg_error_sum_invalid_nonegative = "<?php echo $lang['messages']['forms']['errors']['sum-invalid-nonegative']; ?>";
					var lang_msg_error_sum_aboveactual = "<?php echo $lang['messages']['forms']['errors']['sum-aboveactual']; ?>";
					var lang_msg_error_sum_below5 = "<?php echo $lang['messages']['forms']['errors']['sum-below5']; ?>";
					var lang_msg_error_sum_percent_below1 = "<?php echo $lang['messages']['forms']['errors']['sum-percent-below1']; ?>";
					var lang_msg_error_sum_percent_above100 = "<?php echo $lang['messages']['forms']['errors']['sum-percent-above100']; ?>";

					var lang_msg_done_saved = "<?php echo $lang['messages']['forms']['done']['saved']; ?>";
					var lang_msg_done_saved_pw = "<?php echo $lang['messages']['forms']['done']['saved-pw']; ?>";
					var lang_msg_done_deletedaccount = "<?php echo $lang['messages']['forms']['done']['deleted-account']; ?>";
					var lang_msg_done_itemadded = "<?php echo $lang['messages']['forms']['done']['item-added']; ?>";
					var lang_msg_done_itemshared = "<?php echo $lang['messages']['forms']['done']['item-shared']; ?>";
					var lang_msg_done_updatedpw = "<?php echo $lang['messages']['forms']['done']['password-updated']; ?>";
					var lang_msg_done_datadeleted = "<?php echo $lang['messages']['forms']['done']['data-deleted']; ?>";
					var lang_msg_done_imported = "<?php echo $lang['messages']['forms']['done']['data-imported']; ?>";
					var lang_msg_done_signedin = "<?php echo $lang['messages']['forms']['done']['signed-in']; ?>";
					var lang_msg_done_tfa_disabled = "<?php echo $lang['messages']['forms']['done']['tfa-disabled']; ?>";
					var lang_msg_done_account_created = "<?php echo $lang['messages']['forms']['done']['account-created']; ?>";
					var lang_msg_done_emailsent = "<?php echo $lang['messages']['forms']['done']['email-sent']; ?>";
					var lang_msg_done_accountcreated = "<?php echo $lang['messages']['forms']['done']['account-created']; ?>";
					var lang_msg_done_accountsaved = "<?php echo $lang['messages']['forms']['done']['account-saved']; ?>";
					var lang_msg_done_accountdeleted = "<?php echo $lang['messages']['forms']['done']['account-deleted']; ?>";
					var lang_msg_done_budgetupdated = "<?php echo $lang['messages']['forms']['done']['budget-updated']; ?>";

					var lang_button_save = "<?php echo $lang['words']['buttons']['save']; ?>";
					var lang_button_create = "<?php echo $lang['words']['buttons']['create']; ?>";
				</script><?php
			}
		echo '</head>';

		echo '<body';
		echo ' data-folder="'.(empty($config_folder) ? '/' : '/'.$config_folder.'/').'"';
		echo ' data-debugging="'.($debugging == false ? 'n' : 'y').'"';
		echo ' data-minified="'.($minified == false ? 'n' : 'y').'"';
		echo ' data-signedin="'.($is_loggedin == false ? 'n' : 'y').'"';
		echo ' data-signout-timer="'.$config_signout_timer.'"';
		echo '>';



			echo '<button id="install" hidden>Install</button>';

			if($filename != 'page-privacypolicy.php') {
				echo '<noscript>';
					echo '<link type="text/css" rel="stylesheet" href="'.url($dir_css.'/noscript'.($minified == true ? '.min.css' : '.css?'.time())).'">';

					echo '<section id="noscript">';
						echo svgicon('js');

						echo '<div>';
							foreach($lang['noscript'] AS $content) {
								echo $Parsedown->text($content);
							}
						echo '</div>';
					echo '</section>';
				echo '</noscript>';
			}



			if($debugging == true) {
				echo '<div class="debugmode-enabled">';
					echo $lang['messages']['debugmode-enabled'];
				echo '</div>';
			}

			if(!empty($is_loggedin) AND !empty($user['is_demo'])) {
				echo '<div class="demo">';
					echo $lang['messages']['demo'].' <a href="'.url('demo').'">'.$lang['words']['read-more'].'</a>';
				echo '</div>';
			}



			echo '<div class="change theme'.(($debugging == true OR !empty($user['is_demo'])) ? ' message-ontop' : '').'">';
				echo '<a href="javascript:void(0)" class="theme light'.($is_loggedin == true ? ' '.($user['data_theme'] != 0 ? 'visible' : 'hidden') : ' hidden').'"';
				echo ' aria-label="'.$lang['aria-labels']['light-theme'].'"';
				echo '>';
					echo svgicon('theme-light');
				echo '</a>';

				echo '<a href="javascript:void(0)" class="theme dark'.($is_loggedin == true ? ' '.($user['data_theme'] != 0 ? 'hidden' : 'visible') : ' visible').'"';
				echo ' aria-label="'.$lang['aria-labels']['dark-theme'].'"';
				echo '>';
					echo svgicon('theme-dark');
				echo '</a>';

				echo '<div class="error hidden">';
					echo svgicon('error');
				echo '</div>';

				echo '<div class="wait hidden">';
					echo svgicon('wait');
				echo '</div>';
			echo '</div>';

			if(!empty($is_loggedin)) {
				echo '<div class="change language'.(($debugging == true OR !empty($user['is_demo'])) ? ' message-ontop' : '').'">';
					echo '<a href="'.url('change-language').'" class="language">';
						echo svgicon('flag-'.(endecrypt($user['data_language'], false) == 'se' ? 'gb' : 'se'));
					echo '</a>';
				echo '</div>';
			}



			echo '<div class="modal"><div>';
				echo '<div class="icon">'.svgicon('warning').'</div>';
				echo '<div class="content">';
					foreach($lang['modals']['signingout-soon'] AS $content) {
						echo $Parsedown->text($content);
					}
				echo '</div>';
			echo '</div></div>';



			echo '<div class="what-is-this-modal"><div>';
				echo '<div class="close" title="'.$lang['tooltips']['close'].'">'.svgicon('x').'</div>';
				echo '<div class="icon">'.svgicon('info').'</div>';
				echo '<div class="content"></div>';
			echo '</div></div>';

			echo '<details class="portable-menu signed-'.($is_loggedin == false ? 'out' : 'in').'">';
				#echo '<a href="javascript:void(0)" class="toggle" aria-label="'.$lang['aria-labels']['menu']['show-menu'].'">'.svgicon('menu') . $lang['words']['menu'].'</a>';
				#echo '<a href="javascript:void(0)" class="close" aria-label="'.$lang['aria-labels']['menu']['hide-menu'].'">'.svgicon('x') . $lang['words']['close'].'</a>';
				echo '<summary>';
					echo '<div class="show side-by-side">'.svgicon('menu') . $lang['words']['menu'].'</div>';
					echo '<div class="hide side-by-side">'.svgicon('x') . $lang['words']['close'].'</div>';
				echo '</summary>';

				require_once 'site-menu.php';
			echo '</details>';



			echo '<div id="logo" class="signed-'.($is_loggedin == false ? 'out' : 'in').'">';
				echo '<a href="'.url((empty($is_loggedin) ? '' : 'overview')).'">';
					echo '<img src="'.url('images/logo.svg').'">';

					echo '<div class="sitename">';
						echo '<div class="title">';
							echo $site_title;
						echo '</div>';

						echo '<div class="meaning side-by-side">';
							echo '<span>'.$lang['words']['sitename-meaning'].'</span>';
						echo '</div>';
					echo '</div>';
				echo '</a>';
			echo '</div>';



			echo '<section id="website" class="signed-'.($is_loggedin == false ? 'out' : 'in').'">';
				if($filename != 'page-upgrade.php') {
					include 'site-menu.php';
				}

				echo '<section>';
					echo '<main>';

?>