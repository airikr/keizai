<?php

	# Include the required file
	require_once 'site-header.php';



	$overview = '<img src="'.url($dir_showcase_sections.'/'.$lang['metadata']['language'].'/overview.webp').'">';
	$expenses = '<img src="'.url($dir_showcase_sections.'/'.$lang['metadata']['language'].'/expenses.webp').'">';
	$debts = '<img src="'.url($dir_showcase_sections.'/'.$lang['metadata']['language'].'/debts.webp').'">';
	$loans = '<img src="'.url($dir_showcase_sections.'/'.$lang['metadata']['language'].'/loans.webp').'">';
	$share = '<img src="'.url($dir_showcase_sections.'/'.$lang['metadata']['language'].'/share.webp').'">';
	$exportimport = '<img src="'.url($dir_showcase_sections.'/'.$lang['metadata']['language'].'/export-import.webp').'">';
	$sourcecode = '<img src="'.url($dir_showcase_sections.'/source-code.webp').'">';







	echo '<section id="welcome">';

		# The panel to the right of the page
		echo '<div class="form">';

			# Navigation
			echo '<nav>';
				echo '<a href="javascript:void(0)" data-tab="login" class="active">'.$lang['nav']['signed-out']['sign-in'].'</a>';

				# The variable `$config_solomember` in `site-settings.php` has been set to `false`
				# This means that the website are not for a single user only
				if($config_solomember == false) {
					echo '<a href="javascript:void(0)" data-tab="create">'.$lang['nav']['signed-out']['sign-up'].'</a>';
				}

				echo '<a href="javascript:void(0)" data-tab="forgot-pw">'.$lang['nav']['signed-out']['forgot-pw'].'</a>';
			echo '</nav>';

			# Show messages when the visitor uses the form
			echo '<div class="msg"></div>';



			# Login
			echo '<div class="login">';

				# Form
				# To preserve visitor's privacy, autocomplete has been disabled
				# The `novalidate` option has been added, too, since the validation will be done in PHP
				echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate>';

					# Username
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['username'];
						echo '</div>';

						echo '<div class="field">';
							echo '<div class="icon">'.svgicon('user').'</div>';
							echo '<input type="text" name="field-username" placeholder="demo" aria-label="username">';
						echo '</div>';
					echo '</div>';


					# Password
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['passwords']['password'];
						echo '</div>';

						echo '<div class="field">';
							echo '<div class="icon">'.svgicon('password').'</div>';
							echo '<input type="password" name="field-password" placeholder="demo" aria-label="password">';
						echo '</div>';
					echo '</div>';


					# Two-factor verification code (one-time password)
					echo '<div class="item tfa">';
						echo '<div class="label">';
							echo $lang['words']['tfa-code'];
						echo '</div>';

						echo '<div class="field">';
							echo '<div class="icon">'.svgicon('tfa').'</div>';
							echo '<input type="text" inputmode="numeric" pattern="[0-9]{6}" name="field-tfa" maxlength="6" aria-label="6-digit number">';
						echo '</div>';
					echo '</div>';


					# Button
					echo '<div class="button">';
						echo '<input type="submit" name="button-login" value="'.$lang['words']['buttons']['sign-in'].'">';
					echo '</div>';

				echo '</form>';



				# Option for removing two-factor verification from the account
				echo '<div class="remove-tfa link">';
					echo '<a href="javascript:void(0)">';
						echo svgicon('shield-x') . $lang['words']['remove-tfa'];
					echo '</a>';
				echo '</div>';



				echo '<div class="demo-account color-blue">';
					echo $lang['messages']['try-first'];
					echo '<p><a href="'.url('demo').'" class="color-blue">'.$lang['words']['read-more'].'</a></p>';
				echo '</div>';

			echo '</div>';







			# Create an account
			echo '<div class="create">';

				# Form
				# To preserve visitor's privacy, autocomplete has been disabled
				# The `novalidate` option has been added, too, since the validation will be done in PHP
				echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate>';

					# Get visitor's choice of language
					echo '<input type="hidden" name="hidden-language" value="'.$lang['metadata']['language'].'">';


					# Username
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['username'];
						echo '</div>';

						echo '<div class="field with-explaination">';
							echo '<div class="icon">'.svgicon('user').'</div>';
							echo '<input type="text" name="field-username" aria-label="username">';

							echo '<div class="icon wit" data-element="username" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
						echo '</div>';
					echo '</div>';


					# Password
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['passwords']['password'];
						echo '</div>';

						echo '<div class="field with-explaination">';
							echo '<div class="icon">'.svgicon('password').'</div>';
							echo '<input type="password" name="field-password" aria-label="password">';

							echo '<div class="icon wit" data-element="password" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
						echo '</div>';
					echo '</div>';


					# Repeat the password
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['passwords']['repeat'];
						echo '</div>';

						echo '<div class="field">';
							echo '<div class="icon">'.svgicon('password').'</div>';
							echo '<input type="password" name="field-password-repeat" aria-label="repeat password">';
						echo '</div>';
					echo '</div>';


					# Information
					echo '<div class="information email">';
						foreach($lang['forms']['informations']['registration_email'] AS $email) {
							echo $Parsedown->text($email);
						}
					echo '</div>';


					# Email
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['email'];
						echo '</div>';

						echo '<div class="field with-explaination">';
							echo '<div class="icon">'.svgicon('email').'</div>';
							echo '<input type="email" name="field-email" aria-label="email">';

							echo '<div class="icon wit" data-element="email" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
						echo '</div>';
					echo '</div>';


					# Button
					echo '<div class="button">';
						echo '<input type="submit" name="button-create" value="'.$lang['words']['buttons']['sign-up'].'">';
					echo '</div>';

				echo '</form>';

			echo '</div>';







			# Reset the password
			echo '<div class="forgot-pw">';

				# SMTP has been enabled
				# You can set this in `site-config.php`
				if($smtp_enabled == true) {

					# Form
					# To preserve visitor's privacy, autocomplete has been disabled
					# The `novalidate` option has been added, too, since the validation will be done in PHP
					echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate>';
	
						# Information
						echo '<div class="information">';
							foreach($lang['forms']['informations']['forgotten-pw'] AS $forgotpw) {
								echo $Parsedown->text($forgotpw);
							}
						echo '</div>';


						# Username
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['username'];
							echo '</div>';

							echo '<div class="field">';
								echo '<div class="icon">'.svgicon('user').'</div>';
								echo '<input type="text" name="field-username" aria-label="username">';
							echo '</div>';
						echo '</div>';


						# Email
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['email'];
							echo '</div>';

							echo '<div class="field with-explaination">';
								echo '<div class="icon">'.svgicon('email').'</div>';
								echo '<input type="email" name="field-email" aria-label="email">';

								echo '<div class="icon wit" data-element="loggedout-email-forgotpw" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
							echo '</div>';
						echo '</div>';


						# Button
						echo '<div class="button">';
							echo '<input type="submit" name="button-send" value="'.$lang['words']['buttons']['send'].'">';
						echo '</div>';

					echo '</form>';



				# SMTP has been disabled
				# You can set this in `site-config.php`
				} else {
					echo '<div class="message">';
						echo $lang['messages']['smtp-disabled'];
					echo '</div>';
				}

			echo '</div>';







			# Remove two-factor verification
			echo '<div class="remove-tfa">';

				# Form
				# To preserve visitor's privacy, autocomplete has been disabled
				# The `novalidate` option has been added, too, since the validation will be done in PHP
				echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate>';

					# Username
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['username'];
						echo '</div>';

						echo '<div class="field">';
							echo '<div class="icon">'.svgicon('user').'</div>';
							echo '<input type="text" name="field-username" aria-label="username">';
						echo '</div>';
					echo '</div>';


					# Reset code
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['reset-code'];
						echo '</div>';

						echo '<div class="field with-explaination">';
							echo '<div class="icon">'.svgicon('user').'</div>';
							echo '<input type="text" name="field-resetcode" maxlength="20" aria-label="reset code for tfa">';

							echo '<div class="icon wit" data-element="loggedout-resetcode" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
						echo '</div>';
					echo '</div>';


					# Button
					echo '<div class="button">';
						echo '<input type="submit" name="button-remove-tfa" value="'.$lang['words']['buttons']['remove'].'">';
						echo '<a href="javascript:void(0)" class="cancel">';
							echo $lang['words']['cancel'];
						echo '</a>';
					echo '</div>';

				echo '</form>';

			echo '</div>';

		echo '</div>';







		# The panel to the left of the page
		echo '<div class="intro">';

			# Loop the content from the language file
			foreach($lang['welcome'] AS $content) {
				echo str_replace([
					'overview.webp',
					'expenses.webp',
					'debts.webp',
					'loans.webp',
					'share.webp',
					'export-import.webp',
					'source-code.webp'
				], [
					$overview,
					$expenses,
					$debts,
					$loans,
					$share,
					$exportimport,
					$sourcecode
				], $Parsedown->text($content));
			}

		echo '</div>';

	echo '</section>';







	# Include the required file
	require_once 'site-footer.php';

?>