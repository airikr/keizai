<?php

	require_once 'site-header.php';



	$count_users =
	sql("SELECT COUNT(id)
		 FROM users
		", Array(), 'count');

	$count_usersinactive =
	sql("SELECT COUNT(id)
		 FROM users
		 WHERE timestamp_lastactive < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 2 WEEK))
		", Array(), 'count');

	$count_usersonline =
	sql("SELECT COUNT(id)
		 FROM users
		 WHERE timestamp_lastactive > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 10 MINUTE))
		", Array(), 'count');

	$count_usersneverloggedin =
	sql("SELECT COUNT(id)
		 FROM users
		 WHERE timestamp_lastactive IS NULL
		 AND is_demo IS NULL
		", Array(), 'count');

	$currencies =
	sql("SELECT (
			 SELECT COUNT(DISTINCT data_currency_from)
			 FROM currencies
		 ) AS _count,
		 (
			 SELECT DISTINCT timestamp_updated
			 FROM currencies
			 LIMIT 1
		 ) AS _updated
		", Array(), 'fetch');

	if($currencies['_count'] != 0) {
		$get_currencies =
		sql("SELECT data_currency_from, data_rate
			 FROM currencies
			 GROUP BY data_currency_from
			 ORDER BY data_currency_from
			", Array());
	}


	$current_version = file_get_contents('VERSION.md');

	$size = 0;
	$dbsize = sql("SHOW TABLE STATUS", Array());
	foreach($dbsize AS $dbsize) {
		$size += $dbsize['Data_length'] + $dbsize['Index_length'];
	}



	$get_users =
	sql("SELECT data_username, data_tfa, data_email, is_demo, timestamp_lastactive
		 FROM users
		 ORDER BY timestamp_lastactive DESC
		", Array());



	$adminlog = json_decode(file_get_contents($dir_files.'/admin.json'), true);

	if($adminlog['users'] != $count_users OR
	$adminlog['never_loggedin'] != $count_usersneverloggedin OR
	$adminlog['inactive'] != $count_usersinactive OR
	$adminlog['dbsize'] != (float)format_number($size / (1024*1024), 2, '.', '')) {
		$listall_currencies =
		sql("SELECT *
			 FROM currencies
			", Array());

		$arr_currencies = [];
		foreach($listall_currencies AS $currency) {
			$arr_currencies[] = [
				'currency_from' => $currency['data_currency_from'],
				'currency_to' => $currency['data_currency_to'],
				'rate' => (float)$currency['data_rate'],
				'updated' => $currency['timestamp_updated']
			];
		}

		$arr = [
			'users' => $count_users,
			'never_loggedin' => $count_usersneverloggedin,
			'inactive' => $count_usersinactive,
			'dbsize' => (float)format_number($size / (1024*1024), 2, '.', ''),
			'currencies' => $arr_currencies
		];

		file_put_contents($dir_files.'/admin.json', json_encode($arr));
	}







	echo '<section id="admin">';
		echo '<h1>'.$lang['titles']['admin'].'</h1>';


		echo '<div class="info">';
			echo '<div class="left">';

				echo '<div class="statistics">';
					echo '<h2>'.$lang['subtitles']['statistics'].'</h2>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['users'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value side-by-side">';
							echo svgicon(($adminlog['users'] == $count_users ? 'dash' : 'arrow-'.($adminlog['users'] < $count_users ? 'up' : 'down')));
							echo format_number($count_users, 0);
						echo '</div>';
					echo '</div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['users-neverloggedin'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value side-by-side">';
							echo svgicon(($adminlog['never_loggedin'] == $count_usersneverloggedin ? 'dash' : 'arrow-'.($adminlog['never_loggedin'] < $count_usersneverloggedin ? 'up' : 'down')));
							echo format_number($count_usersneverloggedin, 0);
						echo '</div>';
					echo '</div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['users-inactive'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value side-by-side">';
							echo svgicon(($adminlog['inactive'] == $count_usersinactive ? 'dash' : 'arrow-'.($adminlog['inactive'] < $count_usersinactive ? 'up' : 'down')));
							echo format_number($count_usersinactive, 0);
						echo '</div>';
					echo '</div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['users-online'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value">';
							echo format_number($count_usersonline, 0);
						echo '</div>';
					echo '</div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['currencies'];
							echo ($currencies['_count'] == 0 ? '' : ' (<span class="date" title="'.$lang['tooltips']['currencies-lastupdated'].'">'.date_($currencies['_updated'], 'date').'</span>)');
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value">';
							echo format_number($currencies['_count'], 0);
						echo '</div>';
					echo '</div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['database-size'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value side-by-side database">';
							echo svgicon((format_number($adminlog['dbsize']) == format_number(($size / (1024*1024))) ? 'dash' : 'arrow-'.(format_number($adminlog['dbsize']) < format_number(($size / (1024*1024))) ? 'up' : 'down')));
							echo format_number(($size / (1024*1024))).' MB';
						echo '</div>';
					echo '</div>';
				echo '</div>';



				echo '<div class="users">';
					echo '<h2>'.$lang['subtitles']['users'].'</h2>';

					echo '<div>';
						echo '<div class="item side-by-side">';
							echo '<div class="head user"></div>';

							echo '<div class="head email">';
								echo $lang['words']['email-short'];
							echo '</div>';

							echo '<div class="head tfa">';
								echo $lang['words']['tfa-short'];
							echo '</div>';

							echo '<div class="head lastactive">';
								echo $lang['words']['dates']['last-active'];
							echo '</div>';
						echo '</div>';

						$count = 0;
						foreach($get_users AS $userlist) {
							$count++;

							echo '<div class="item side-by-side">';
								echo '<div class="user">';
									echo $lang['words']['user'].' '.$count;
									echo ($userlist['data_username'] == $user['data_username'] ? ' ('.mb_strtolower($lang['words']['you']).')' : '');
									echo (!empty($userlist['is_demo']) ? ' (demo)' : '');
								echo '</div>';

								echo '<div class="email">';
									echo (empty($userlist['data_email']) ? '<span class="nearly-invisible no-select">X</span>' : 'X');
								echo '</div>';

								echo '<div class="tfa">';
									echo (empty($userlist['data_tfa']) ? '<span class="nearly-invisible no-select">X</span>' : 'X');
								echo '</div>';

								echo '<div class="lastactive date">';
									echo (empty($userlist['timestamp_lastactive']) ? '-' : date_($userlist['timestamp_lastactive'], 'datetime'));
								echo '</div>';
							echo '</div>';
						}
					echo '</div>';
				echo '</div>';



				if($currencies['_count'] != 0) {
					echo '<div class="currencies">';
						echo '<h2>'.$lang['subtitles']['currencies'].'</h2>';

						foreach($get_currencies AS $curr) {
							$rate =
							sql("SELECT data_rate
								 FROM currencies
								 WHERE data_currency_from = :_currency_from
								 AND data_currency_to = :_currency_to
								", Array(
									'_currency_from' => $curr['data_currency_from'],
									'_currency_to' => endecrypt($user['data_currency'], false)
								), 'fetch');

							echo '<div class="item">';
								echo '1 '.$curr['data_currency_from'];
								echo ' = '.format_number((empty($rate['data_rate']) ? '1' : $rate['data_rate']));
								echo ' '.mb_strtoupper(endecrypt($user['data_currency'], false));

								#echo svgicon(($adminlog['inactive'] == $curr ? 'dash' : 'arrow-'.($adminlog['inactive'] > $count_usersinactive ? 'up' : 'down')));
							echo '</div>';
						}
					echo '</div>';
				}

			echo '</div>';



			echo '<div class="right">';

				echo '<div class="version">';
					echo '<div class="version">';
						echo 'Version '.$current_version;
					echo '</div>';

					echo '<div class="check-for-updates">';
						echo '<div class="check">';
							echo '<a href="javascript:void(0)">Kontrollera version</a>';
						echo '</div>';

						echo '<div class="working">';
							echo '<div class="wait">'.svgicon('wait').'</div>';
							echo $lang['words']['checking'];
						echo '</div>';
					echo '</div>';
				echo '</div>';



				echo '<div class="configs">';
					echo '<h2>'.$lang['subtitles']['configurations'].'</h2>';

					echo '<div>minified = <b>'.($minified == true ? 'true' : 'false').'</b></div>';
					echo '<div>debugging = <b>'.($debugging == true ? 'true' : 'false').'</b></div>';
					echo '<div>solo = <b>'.($config_solomember == true ? 'true' : 'false').'</b></div>';
					echo '<div>encryption = <b>'.$config_encryptionmethod.'</b></div>';
					echo '<div>encoding = <b>'.$config_encoding.'</b></div>';
					echo '<div>timezone = <b>'.$config_timezone.'</b></div>';
					echo '<div>email_from = <b>'.$config_email_noreply.'</b></div>';
					echo '<div>email_contact = <b>'.$config_email_contact.'</b></div>';
				echo '</div>';

			echo '</div>';

		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>