<?php session_start();

	require_once 'site-config.php';

	ini_set('display_errors', ($debugging == false ? 0 : 1));
	ini_set('display_startup_errors', ($debugging == false ? 0 : 1));
	error_reporting(E_ALL);

	use MathParser\StdMathParser;
	#use ParagonIE\Halite\KeyFactory;

	date_default_timezone_set($config_timezone);



	# General information
	$host = $_SERVER['HTTP_HOST'];
	$uri = $_SERVER['REQUEST_URI'];
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	$is_local = (($host == 'localhost' OR strpos($host, '192.168') !== false) ? true : false);
	$is_loggedin = (isset($_SESSION['loggedin']) ? true : null);

	# Get current file's filename
	$filename = basename($_SERVER['PHP_SELF']);

	# Get current GET
	$filename_get = (!empty($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : null);

	# Privacy settings
	$google = ['notranslate','nositelinkssearchb'];
	$robots = ['nofollow','nosnippet','noarchive','noimageindex'];

	# Directories
	$dir_files = $config_root.'/files/keizai';
	$dir_userfiles = $dir_files.'/users';
	$dir_functions = 'functions';
	$dir_images = 'images';
	$dir_favicons = $dir_images.'/favicons';
	$dir_showcase = $dir_images.'/showcase';
	$dir_showcase_sections = $dir_images.'/showcase/sections';
	$dir_screenshots = $dir_images.'/screenshots';
	$dir_css = 'css';
	$dir_css_pages = 'css/pages';
	$dir_css_themes = 'css/themes';
	$dir_js = 'js';
	$dir_js_pages = 'js/pages';
	$dir_languages = 'languages';

	$arr_beginswith = [
		'<h', '<u', '<l', '<div', '</'
	];

	$smtp_enabled = ((empty($smtp_host) OR empty($smtp_email) OR empty($smtp_password) OR empty($smtp_port)) ? false : true);


	# Include required files
	require_once 'vendor/autoload.php';
	require_once 'site-functions.php';

	$get_language = (isset($_GET['lang']) ? safetag($_GET['lang']) : null);



	if(empty($config_encryptionmethod)) {
		die(simplepage('The encryption method must be entered.', false, true));

	} elseif(empty($encryption_key_1) OR empty($encryption_key_2)) {
		die(simplepage('One or both encryption key strings are missing.', false, true));

	} elseif(strlen($encryption_key_1) < 64 OR strlen($encryption_key_2) < 64) {
		die(simplepage('The encryption keys have less than 64 characters. Recommendation: 64 random characters.', false, true));

	} elseif(empty($encryption_salt_1) OR empty($encryption_salt_2) OR empty($encryption_salt_3) OR empty($encryption_salt_4)) {
		die(simplepage('One or more salt string are missing for encryption key.', false, true));

	} elseif(strlen($encryption_salt_1) < 10 OR strlen($encryption_salt_2) < 10 OR strlen($encryption_salt_3) < 10 OR strlen($encryption_salt_4) < 10) {
		die(simplepage('The salt strings have less than 10 characters. Recommendation: 10 random characters.', false, true));

	} elseif(empty($database_host) OR empty($database_name) OR empty($database_user) OR empty($database_pass)) {
		die(simplepage('Database info not set.', false, true));

	} elseif(empty($config_folder) AND $is_local == true) {
		die(simplepage('Folder name not set.', false, true));

	} elseif(!file_exists($dir_userfiles)) {
		die(simplepage('<code>'.$dir_userfiles.'</code> does not exists.', false, true));

	} elseif(file_exists($dir_userfiles) AND is_dir($dir_userfiles) AND !is_writeable($dir_userfiles) OR file_exists($dir_userfiles) AND is_dir($dir_userfiles) AND !is_readable($dir_userfiles)) {
		die(simplepage('Not enough permissions for <code>'.$dir_userfiles.'</code>.', false, true));

	} else {
		create_folder($dir_userfiles);
	}



	# Database connection
	if(!empty($database_host) AND !empty($database_name) AND !empty($database_user) AND !empty($database_pass)) {
		try {
			$sql = new PDO('mysql:host='.$database_host.';dbname='.$database_name, $database_user, $database_pass);
			$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		catch(PDOException $e) {
			echo $e;
			exit;
		}
	}

	/*$db_file = 'db.sqlite';
	$db_dir = $config_root.'/html'.(empty($config_folder) ? '' : '/'.$config_folder).'/'.$db_file;
	if(!file_exists($db_file)) { $database = new SQLite3($db_file); }
	$sql = new PDO('sqlite:/'.$db_dir) or die("cannot open the database");*/

	$Parsedown = new Parsedown();

	/*if(!file_exists($dir_files.'/'.hash('sha256', $site_title).'.key')) {
		$enckey = KeyFactory::generateEncryptionKey();
		KeyFactory::save($enckey, $dir_files.'/'.hash('sha256', $site_title).'.key');
	}*/






	if($is_loggedin == true) {
		if($filename == 'index.php' OR $filename == 'page-account-forgotpw.php') {
			header("Location: ".url('overview'));
			exit;
		}


		$check_user =
		sql("SELECT COUNT(id)
			 FROM users
			 WHERE id = :_iduser
			", Array(
				'_iduser' => (int)$_SESSION['loggedin']
			), 'count');

		$currencies_exists =
		sql("SELECT COUNT(*)
			 FROM currencies
			", Array(), 'count');


		if($check_user == 0 AND $filename != 'account-signout.php' AND $filename != 'account-delete-account.php') {
			header("Location: ".url('sign-out'));
			exit;
		}


		if($check_user != 0) {
			$user =
			sql("SELECT *
				 FROM users
				 WHERE id = :_iduser
				", Array(
					'_iduser' => (int)$_SESSION['loggedin']
				), 'fetch');

			if(file_exists($dir_userfiles.'/'.md5($user['data_saltedstring_1'] . (int)$user['id'] . $user['data_saltedstring_2']).'.json')) {
				$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($user['data_saltedstring_1'] . (int)$user['id'] . $user['data_saltedstring_2']).'.json'), true);
			}

			$arr_themes = [
				0 => 'light-everforest',
				1 => 'dark-everforest',
				2 => 'dark-dracula',
				3 => 'dark-tokyonight'
			];



			$period_start_empty = (empty($user['timestamp_period_start']) ? true : false);
			$period_end_empty = (empty($user['timestamp_period_end']) ? true : false);
			$period_incomeday_empty = (empty($user['data_incomeday']) ? true : false);

			$period_start = ($period_start_empty == true ? null : period(date_(endecrypt($user['timestamp_period_start'], false), 'date')));
			$period_start_nextmonth = ($period_start_empty == true ? null : period(date_(endecrypt($user['timestamp_period_start'], false), 'date'), true));
			$period_end = ($period_end_empty == true ? null : period(date_(endecrypt($user['timestamp_period_end'], false), 'date')));
			$period_end_nextmonth = ($period_end_empty == true ? null : period(date_(endecrypt($user['timestamp_period_end'], false), 'date'), true));
			$period_incomeday = period(endecrypt($user['data_incomeday'], false));
			$period_incomeday_nextmonth_1 = period(endecrypt($user['data_incomeday'], false), true);
			$period_incomeday_nextmonth_2 = period(endecrypt($user['data_incomeday'], false), true, 2);

			if($period_start_empty == false AND $period_end_empty == false) {
				$period = "AND timestamp_date >= '".$period_start."' AND timestamp_date < '".$period_end."'";

			} elseif($period_incomeday_empty == false) {
				$period = "AND timestamp_date >= '".$period_incomeday."' AND timestamp_date < '".$period_incomeday_nextmonth_1."'";

			} else {
				$period = "";
			}



			sql("UPDATE users
				 SET timestamp_lastactive = :_lastactive
				 WHERE id = :_userid
				", Array(
					'_userid' => (int)$user['id'],
					'_lastactive' => time()
				));

			$check_session =
			sql("SELECT COUNT(data_ipaddress)
				 FROM sess
				 WHERE data_ipaddress = :_ipaddress
				 AND data_useragent = :_useragent
				", Array(
					'_ipaddress' => endecrypt(getip()),
					'_useragent' => endecrypt($useragent)
				), 'count');

			if($check_session != 0) {
				$session =
				sql("SELECT *
					 FROM sess
					 WHERE data_ipaddress = :_ipaddress
					 AND data_useragent = :_useragent
					", Array(
						'_ipaddress' => endecrypt(getip()),
						'_useragent' => endecrypt($useragent)
					), 'fetch');
			}

			$count_accounts =
			sql("SELECT COUNT(id_user)
				 FROM item_accounts
				 WHERE id_user = :_iduser
				", Array(
					'_iduser' => (int)$user['id']
				), 'count');

			if($count_accounts != 0) {
				$default_account =
				sql("SELECT id
					 FROM item_accounts
					 WHERE id_user = :_iduser
					 AND is_default IS NOT NULL
					", Array(
						'_iduser' => (int)$user['id']
					), 'fetch');
			}

			$count_m =
			sql("SELECT (
					 SELECT COUNT(id_user)
					 FROM items
					 WHERE id_user = :_iduser
					 AND is_expense IS NOT NULL
					 ".$period."
				 ) AS expenses,
				 (
					 SELECT COUNT(id_user)
					 FROM items
					 WHERE id_user = :_iduser
					 AND is_debt IS NOT NULL
				 ) AS debts,
				 (
					 SELECT COUNT(id_user)
					 FROM items
					 WHERE id_user = :_iduser
					 AND is_loan IS NOT NULL
				 ) AS loans
				", Array(
					'_iduser' => (int)$user['id']
				), 'fetch');

			$count_newshares =
			sql("SELECT COUNT(id_user_with)
				 FROM shares
				 WHERE id_user_with = :_iduser
				", Array(
					'_iduser' => (int)$user['id']
				), 'count');



			if($check_session == 0 AND $filename != 'account-signout.php' AND $filename != 'account-delete-account.php') {
				header("Location: ".url('sign-out'));
				exit;
			}

			if($filename == 'page-list.php') {
				if(isset($_GET['fil']) AND !isset($_GET['ida']) AND $_GET['fil'] == 'period' AND empty($user['timestamp_period_start']) AND empty($user['timestamp_period_end']) AND empty($user['data_incomeday']) OR
				   isset($_GET['fil']) AND isset($_GET['ida']) AND $_GET['fil'] == 'period' AND empty($user['timestamp_period_start']) AND empty($user['timestamp_period_end']) AND empty($user['data_incomeday'])) {
					header("Location: ".url('expenses'.((safetag($_GET['ite']) == 'expenses' AND !empty($_GET['ida'])) ? '/account:'.safetag($_GET['ida']) : '')));
					exit;

				} elseif(!isset($_GET['fil']) AND isset($_GET['ida']) AND $count_accounts == 0) {
					header("Location: ".url('expenses'));
					exit;
				}
			}



			/*if(!empty($user['check_option_recurrent_autoupdate'])) {
				$get_recurrent =
				sql("SELECT *
					 FROM items
					 WHERE id_user = :_iduser
					 AND check_expense_recurrent IS NOT NULL
					 AND is_expense IS NOT NULL
					".(empty($user['check_option_recurrent_include_notpayed']) ? "" : "AND timestamp_payed IS NOT NULL")."
					", Array(
						'_iduser' => (int)$user['id']
					));
			
				if(!empty($user['data_incomeday']) AND endecrypt($user['data_incomeday'], false) == date('d') OR
				   !empty($user['timestamp_period_start']) AND date('d', $period_start) == date('d')) {
					foreach($get_recurrent AS $recurrent) {
						sql("UPDATE items
							 SET timestamp_date = :_date,
								 check_expense_payed = NULL,
								 timestamp_payed = NULL

							 WHERE id = :_iditem
							 AND id_user = :_iduser
							", Array(
								'_iditem' => (int)$recurrent['id'],
								'_iduser' => (int)$user['id'],
								'_date' => strtotime('+'.endecrypt($recurrent['data_recurrence'], false).' '.endecrypt($recurrent['data_recurrence_type'], false), $recurrent['timestamp_date'])
							));
					}
				}
			}*/



			if(empty($user['is_admin']) AND ($filename == 'page-admin.php')) {
				header("Location: ".url('overview'));
				exit;
			}

			/*if(empty($user['is_upgraded']) AND $filename != 'page-upgrade.php' AND $filename != 'account-upgrade.php' AND $filename != 'sign-out.php') {
				header("Location: ".url('upgrade'));
				exit;
			}*/
		}



		if($filename != 'account-signout.php' AND $filename != 'account-delete-account.php') {
			require_once $dir_languages.'/'.(empty($user['data_language']) ? 'se' : endecrypt($user['data_language'], false)).'.php';

			$arr_sessioncodes = [
				'exported settings (plaintext)' => [
					'icon' => svgicon('database-export'),
					'text' => $lang['sessioncodes']['export-plaintext']
				],
				'exported settings (encrypted)' => [
					'icon' => svgicon('database-export'),
					'text' => $lang['sessioncodes']['export-encrypted']
				],
				'exported data (plaintext)' => [
					'icon' => svgicon('database-export'),
					'text' => $lang['sessioncodes']['export-plaintext']
				],
				'exported data (encrypted)' => [
					'icon' => svgicon('database-export'),
					'text' => $lang['sessioncodes']['export-encrypted']
				],
				'exported data (xlsx)' => [
					'icon' => svgicon('database-export'),
					'text' => $lang['sessioncodes']['export-xlsx']
				],
				'imported settings' => [
					'icon' => svgicon('database-import'),
					'text' => $lang['sessioncodes']['imported']
				],
				'updated item' => [
					'icon' => svgicon('edit'),
					'text' => $lang['sessioncodes']['updated-item']
				],
				'created item' => [
					'icon' => svgicon('add'),
					'text' => $lang['sessioncodes']['created-item']
				],
				'added tfa' => [
					'icon' => svgicon('tfa'),
					'text' => $lang['sessioncodes']['added-tfa']
				],
				'removed tfa' => [
					'icon' => svgicon('tfa'),
					'text' => $lang['sessioncodes']['removed-tfa']
				],
				'updated settings' => [
					'icon' => svgicon('user-settings'),
					'text' => $lang['sessioncodes']['updated-settings']
				],
				'updated password' => [
					'icon' => svgicon('user-settings'),
					'text' => $lang['sessioncodes']['updated-password']
				],
				'updated notes' => [
					'icon' => svgicon('notes'),
					'text' => $lang['sessioncodes']['updated-notes']
				],
				'deleted session' => [
					'icon' => svgicon('x'),
					'text' => $lang['sessioncodes']['deleted-session']
				],
				'deleted sessions' => [
					'icon' => svgicon('x'),
					'text' => $lang['sessioncodes']['deleted-sessions']
				],
				'deleted data' => [
					'icon' => svgicon('x'),
					'text' => $lang['sessioncodes']['deleted-data']
				],
				'failed to delete data' => [
					'icon' => svgicon('exclamation'),
					'text' => $lang['sessioncodes']['delete-data-failed']
				],
				'failed to remove tfa' => [
					'icon' => svgicon('exclamation'),
					'text' => $lang['sessioncodes']['remove-tfa-failed']
				],
				'signed in' => [
					'icon' => svgicon('login'),
					'text' => $lang['sessioncodes']['signed-in']
				],
				'signed out' => [
					'icon' => svgicon('logout'),
					'text' => $lang['sessioncodes']['signed-out']
				],
				'auto signed out' => [
					'icon' => svgicon('logout'),
					'text' => $lang['sessioncodes']['auto-signed-out']
				],
				'created an account for expenses' => [
					'icon' => svgicon('account-add'),
					'text' => $lang['sessioncodes']['created-account-expenses']
				],
				'deleted an account for expenses' => [
					'icon' => svgicon('account-x'),
					'text' => $lang['sessioncodes']['deleted-account-expenses']
				],
				'made some changes to an account for expenses' => [
					'icon' => svgicon('account-cog'),
					'text' => $lang['sessioncodes']['updated-account-expenses']
				],
				'saved changes for a budget' => [
					'icon' => svgicon('edit'),
					'text' => $lang['sessioncodes']['saved-changes-budget']
				],
				'created budget' => [
					'icon' => svgicon('add'),
					'text' => $lang['sessioncodes']['created-budget']
				],
				'updated budget' => [
					'icon' => svgicon('edit'),
					'text' => $lang['sessioncodes']['updated-budget']
				]
			];
		}



	} else {
		if(isset($_GET['lang']) AND !file_exists($dir_languages.'/'.safetag($_GET['lang']).'.php')) {
			require_once $dir_languages.'/gb.php';
		} else {
			require_once $dir_languages.'/'.(isset($_GET['lang']) ? (safetag($_GET['lang']) == 'en' ? 'gb' : safetag($_GET['lang'])) : 'se').'.php';
		}

		if($filename == 'page-account-settings.php' OR
		   $filename == 'page-item-manage.php' OR
		   $filename == 'page-list.php' OR
		   $filename == 'page-notes.php' OR
		   $filename == 'page-admin.php' OR
		   $filename == 'page-overview.php' OR
		   ($filename != 'index.php' AND $filename != 'minify.php' AND $filename != 'change-language.php' AND $filename != 'account-login.php' AND $filename != 'account-remove-tfa-resetcode.php' AND $filename != 'account-create.php' AND $filename != 'account-forgotpw.php' AND $filename != 'page-privacypolicy.php' AND $filename != 'page-errors.php' AND $filename != 'page-donation.php' AND $filename != 'page-account-forgotpw.php' AND $filename != 'show-image.php' AND $filename != 'what-is-this.php' AND $filename != 'install-db.php' AND $filename != 'page-demo.php')) {

			header("Location: ".url(''));
			exit;

		}
	}

?>