<?php

	require_once 'site-settings.php';

	$get_image = safetag($_GET['img']);
	$get_screenshot = (isset($_GET['scr']) ? safetag($_GET['scr']) : null);
	$get_lang = (isset($_GET['lang']) ? safetag($_GET['lang']) : null);



	if($get_screenshot == false) {
		list($folder, $file, $la) = explode('-', $get_image);
		$file_name = $dir_images.'/'.$folder.'/'.$file.'-'.$la.'.webp';

	} else {
		$file_name = $dir_screenshots.'/'.$get_image;
	}


	if(file_exists($file_name)) {
		header('Content-Type: image/webp');
		echo file_get_contents($file_name);

	} else {
		die(simplepage('Image do not exists.', false, true));
	}

?>