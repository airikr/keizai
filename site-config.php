<?php

	# Information about the website
	$site_title = 'Keizai';
	$site_protocol = 'https';
	$site_domain = 'keizai.se';
	$site_subdomain = null;
	$site_url = $site_protocol.'://'.(empty($site_subdomain) ? null : $site_subdomain.'.') . $site_domain;
	$site_url_cdn = null;
	$site_url_current = sprintf(
		'%s://%s/%s',
		isset($_SERVER['HTTPS']) ? 'https' : 'http',
		$_SERVER['HTTP_HOST'],
		trim($_SERVER['REQUEST_URI'],'/\\')
	);
	$site_favicon = 'data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQEAYAAABPYyMiAAAABmJLR0T///////8JWPfcAAAACXBIWXMAAABIAAAASABGyWs+AAAAF0lEQVRIx2NgGAWjYBSMglEwCkbBSAcACBAAAeaR9cIAAAAASUVORK5CYII=';

	$metadata_image = null;

	$arr_currencies = [
		'SEK',
		'EUR'
	];

	asort($arr_currencies);



	# Configurations
	$minified = false;
	$debugging = false;
	$config_solomember = false;    # If true and if there's 1 user registered, no one can create a new account
	$config_encryptionmethod = 'AES-256-CBC';
	$config_folder = 'keizai';
	$config_root = '/'.(($host == 'localhost' OR strpos($host, '192.168') !== false) ? 'srv/http' : 'var/www').'/html/'.$config_folder;
	$config_email_noreply = 'no-reply@'.$site_domain;
	$config_email_contact = 'hi@'.$site_domain;
	$config_encoding = 'ISO-8859-1';
	$config_timezone = 'Europe/Stockholm';
	$config_signout_timer = '10:00';



	# Database settings
	$database_host = 'localhost';
	$database_name = 'keizai';
	$database_user = null;
	$database_pass = null;



	# Website's own encryption keys and salt strings
	# These are used only for the usernames
	$encryption_key_1 = null;    # Recommended length: 64
	$encryption_key_2 = null;    # Recommended length: 64
	$encryption_salt_1 = null;    # Recommended length: 10
	$encryption_salt_2 = null;    # Recommended length: 10
	$encryption_salt_3 = null;    # Recommended length: 10
	$encryption_salt_4 = null;    # Recommended length: 10



	# SMTP settings
	$smtp_host = null;
	$smtp_email = null;
	$smtp_password = null;
	$smtp_port = null;

?>