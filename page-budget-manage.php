<?php

	if(isset($_GET['pag']) AND $_GET['pag'] == 'delete') {

		require_once 'site-settings.php';

		$get_idbudget = safetag($_GET['idb']);

		sql("DELETE FROM budget
			WHERE id = :_idbudget
			AND id_user = :_iduser
			", Array(
				'_idbudget' => (int)$get_idbudget,
				'_iduser' => (int)$user['id']
			));

		header("Location: ".url('budget'));
		exit;



	} else {

		require_once 'site-header.php';



		$get_page = safetag($_GET['pag']);
		$get_idbudget = (!isset($_GET['idb']) ? null : safetag($_GET['idb']));

		$editing = false;


		if($get_page == 'edit') {
			$editing = true;

			$budget =
			sql("SELECT *
				FROM budget
				WHERE id = :_idbudget
				AND id_user = :_iduser
				", Array(
					'_idbudget' => (int)$get_idbudget,
					'_iduser' => (int)$user['id']
				), 'fetch');
		}







		echo '<section id="manage-budget">';
			echo '<h1>';
				if($editing == false) {
					echo $lang['titles']['add-budget'];
				} else {
					echo $lang['titles']['edit-budget'];
				}
			echo '</h1>';

			echo '<div class="msg"></div>';



			echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate>';
				echo ($get_page == 'edit' ? '<input type="hidden" name="hidden-idbudget" value="'.(int)$get_idbudget.'">' : null);

				echo '<div class="side-by-side">';
					echo '<div class="required">';

						echo '<h2>'.$lang['subtitles']['required'].'</h2>';


						echo '<div class="side-by-side">';
							echo '<div class="item name">';
								echo '<div class="label">';
									echo $lang['words']['name'];
								echo '</div>';

								echo '<div class="field">';
									echo '<div class="icon">'.svgicon('name').'</div>';
									echo '<input type="text" name="field-name" maxlength="35"';
									echo ($editing == false ? '' : ' value="'.endecrypt($budget['data_name'], false).'"');
									echo '>';
								echo '</div>';
							echo '</div>';
						echo '</div>';


						echo '<div class="side-by-side sums">';
							echo '<div class="item sum">';
								echo '<div class="label">';
									echo $lang['words']['sums']['goal'];
								echo '</div>';

								echo '<div class="field sum">';
									echo '<div>';
										echo '<div class="icon">'.svgicon('money').'</div>';
										echo '<input type="text" inputmode="numeric" pattern="[0-9]{6}" name="field-sum-goal"';
										echo ($editing == false ? '' : ' value="'.str_replace('.00', '', format_number(endecrypt($budget['data_sum_goal'], false), 2, '.', '')).'"');
										echo '>';
									echo '</div>';

									echo '<div class="math">';
										echo '<span class="equals">=</span>';
										echo '<span class="sum"></span>';
									echo '</div>';
								echo '</div>';
							echo '</div>';


							echo '<div class="item sum permonth">';
								echo '<div class="label">';
									echo $lang['words']['sums']['monthly'];
								echo '</div>';

								echo '<div class="field sum">';
									echo '<div>';
										echo '<div class="icon">'.svgicon('money').'</div>';
										echo '<input type="text" inputmode="numeric" pattern="[0-9]{6}" name="field-sum-permonth"';
										echo (($editing == false OR ($editing == true AND empty($budget['data_sum_permonth']))) ? '' : ' value="'.str_replace('.00', '', format_number(endecrypt($budget['data_sum_permonth'], false), 2, '.', '')).'"');
										echo '>';
									echo '</div>';

									echo '<div class="math">';
										echo '<span class="equals">=</span>';
										echo '<span class="sum"></span>';
									echo '</div>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '</div>';



					echo '<div class="optionally">';

						echo '<h2>'.$lang['subtitles']['optional'].'</h2>';


						echo '<div class="item notes">';
							echo '<div class="label">';
								echo $lang['words']['notes'];
							echo '</div>';

							echo '<div class="field">';
								echo '<textarea name="field-notes">';
									echo (($editing == false OR $editing == true AND empty($budget['data_notes'])) ? '' : endecrypt($budget['data_notes'], false));
								echo '</textarea>';
							echo '</div>';
						echo '</div>';
					echo '</div>';
				echo '</div>';



				echo '<div class="button">';
					if($editing == false) {
						echo '<input type="submit" name="button-add" value="'.$lang['words']['buttons']['add'].'">';

					} else {
						echo '<input type="submit" name="button-save" value="'.$lang['words']['buttons']['save'].'">';
						echo '<a href="'.url('budget').'">'.$lang['words']['cancel'].'</a>';
					}
				echo '</div>';
			echo '</form>';
		echo '</section>';







		require_once 'site-footer.php';

	}

?>