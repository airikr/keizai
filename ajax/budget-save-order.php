<?php

	require_once '../site-settings.php';



	$get_iditem = safetag($_GET['idi']);
	$get_order = safetag($_GET['ord']);

	sql("UPDATE budget
		 SET data_order = :_order
		 WHERE id = :_iditem
		", Array(
			'_iditem' => (int)$get_iditem,
			'_order' => (int)$get_order
		));

?>