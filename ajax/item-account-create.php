<?php

	require_once '../site-settings.php';



	$post_account_name = (empty($_POST['field-account-name']) ? null : safetag($_POST['field-account-name']));
	$check_default = (isset($_POST['check-default']) ? true : null);



	if(empty($post_account_name)) {
		echo 'fields-empty';


	} else {

		$count_accounts =
		sql("SELECT COUNT(id_user)
			 FROM item_accounts
			 WHERE id_user = :_iduser
			 AND data_name = :_name
			", Array(
				'_iduser' => (int)$user['id'],
				'_name' => endecrypt(trim($post_account_name))
			), 'count');

		$accountinfo =
		sql("SELECT data_name
			 FROM item_accounts
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'fetch');



		if($count_accounts != 0 AND $accountinfo['data_name'] != endecrypt(trim($post_account_name))) {
			echo 'account-exists';

		} else {
			if(!empty($check_default)) {
				sql("UPDATE item_accounts
					 SET is_default = NULL
					 WHERE id_user = :_iduser
					", Array(
						'_iduser' => (int)$user['id']
					));
			}

			sql("INSERT INTO item_accounts(
					 id_user,
					 data_name,
					 is_default
				 )

				 VALUES(
					 :_iduser,
					 :_name,
					 :_default
				 )
				", Array(
					'_iduser' => (int)$user['id'],
					'_name' => endecrypt(trim($post_account_name)),
					'_default' => ($count_accounts == 0 ? 1 : $check_default)
				), 'insert');



			$get_accounts =
			sql("SELECT *
				 FROM item_accounts
				 WHERE id_user = :_iduser
				", Array(
					'_iduser' => (int)$user['id']
				));

			echo '<a href="'.url('expenses/filter:period').'" class="active">'.$lang['words']['all'].'</a>';
			foreach($get_accounts AS $accountlist) {
				echo '<a href="'.url('expenses/account:'.(int)$accountlist['id']).'">'.endecrypt($accountlist['data_name'], false).'</a>';
			}



			log_action(
				'created an account for expenses'
			);
		}

	}

?>