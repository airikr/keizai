<?php

	require_once '../site-settings.php';

	use NXP\MathExecutor;

	$math = new MathExecutor();



	$hidden_object = $_POST['hidden-object'];
	$hidden_iditem = $_POST['hidden-iditem'];
	$hidden_iduser = $_POST['hidden-iduser'];

	$post_account = (isset($_POST['list-accounts']) ? safetag($_POST['list-accounts']) : null);
	$post_type = (isset($_POST['list-types']) ? safetag($_POST['list-types']) : null);
	$post_category = (isset($_POST['list-categories']) ? safetag($_POST['list-categories']) : null);
	$post_currency = (isset($_POST['list-currencies']) ? safetag($_POST['list-currencies']) : null);
	$post_name = safetag($_POST['field-name']);
	$post_sum = safetag($_POST['field-sum']);
	$post_sum_permonth = (isset($_POST['field-sum-permonth']) ? safetag($_POST['field-sum-permonth']) : null);
	$post_contact_phone = (isset($_POST['field-contact-phone']) ? safetag($_POST['field-contact-phone']) : null);
	$post_contact_email = (isset($_POST['field-contact-email']) ? safetag($_POST['field-contact-email']) : null);
	$post_date = safetag($_POST['field-date']);
	$post_date_expires = (isset($_POST['field-date-expires']) ? safetag($_POST['field-date-expires']) : null);
	$post_number_payment = (empty($_POST['field-number-payment']) ? null : safetag($_POST['field-number-payment']));
	$post_number_ocr = (empty($_POST['field-number-ocr']) ? null : safetag($_POST['field-number-ocr']));
	$post_number_phone = (empty($_POST['field-number-phone']) ? null : safetag($_POST['field-number-phone']));
	$post_payed = (empty($_POST['field-date-payed']) ? null : safetag($_POST['field-date-payed']));
	$post_notes = (empty($_POST['field-notes']) ? null : safetag($_POST['field-notes']));
	$post_recurrence = (empty($_POST['field-recurrence']) ? null : safetag($_POST['field-recurrence']));
	$post_recurrence_type = (isset($_POST['list-recurrence-types']) ? safetag($_POST['list-recurrence-types']) : null);
	$post_belongsto_debt = (isset($_POST['list-debts']) ? safetag($_POST['list-debts']) : null);

	$check_expense_payed = (isset($_POST['check-expense-payed']) ? true : null);
	$check_expense_extra = (isset($_POST['check-expense-extra']) ? true : null);
	$check_expense_subscription = (isset($_POST['check-expense-subscription']) ? true : null);
	$check_expense_recurrent = (isset($_POST['check-expense-recurrent']) ? true : null);
	$check_expense_correctinfo = (isset($_POST['check-expense-correctinfo']) ? true : null);
	$check_debt_company = (isset($_POST['check-debt-company']) ? true : null);
	$check_debt_paying = (isset($_POST['check-debt-currently-paying']) ? true : null);
	$check_loan_out = (isset($_POST['check-loan-out']) ? true : null);

	$is_shared = false;
	$has_permissions = false;



	$check_sharestatus =
	sql("SELECT COUNT(i.id_user)
		 FROM items i
		 JOIN shares s
		 ON i.id = s.id_item
		 WHERE s.id_item = :_iditem
		 AND s.id_user_with = :_iduser
		", Array(
			'_iditem' => (int)$hidden_iditem,
			'_iduser' => (int)$user['id']
		), 'count');

	if($check_sharestatus != 0) {
		$is_shared = true;

		$check_editprm =
		sql("SELECT COUNT(allow_edits)
			 FROM shares
			 WHERE id_item = :_iditem
			 AND id_user_with = :_iduser
			", Array(
				'_iditem' => (int)$hidden_iditem,
				'_iduser' => (int)$user['id']
			), 'count');


		if($check_editprm != 0) {
			$has_permissions = true;

			$usershare =
			sql("SELECT u.id AS id_user, u.data_saltedstring_1, u.data_saltedstring_2, u.data_currency, i.id AS id_item
				 FROM users u
				 JOIN items i
				 ON u.id = i.id_user
				 WHERE i.id = :_iditem
				", Array(
					'_iditem' => (int)$hidden_iditem
				), 'fetch');

			$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($usershare['data_saltedstring_1'] . (int)$usershare['id_user'] . $usershare['data_saltedstring_2']).'.json'), true);
		}
	}



	if($hidden_object == 'expense' AND empty($post_type) OR
	   $hidden_object == 'expense' AND empty($post_category) OR
	   $hidden_object == 'expense' AND empty($post_currency) OR
	   empty($post_name) OR empty($post_sum) OR empty($post_date)) {
		echo 'fields-empty';


	} else {

		$count_items =
		sql("SELECT COUNT(id_user)
			 FROM items
			 WHERE id != :_iditem
			 AND id_user = :_iduser
			 AND data_name = :_name
			 AND data_sum = :_sum
			 AND timestamp_date = :_date
			 ".($hidden_object == 'expense' ? "AND is_expense IS NOT NULL" : ($hidden_object == 'debt' ? "AND is_debt IS NOT NULL" : ($hidden_object == 'loan' ? "AND is_loan IS NOT NULL" : "")))."
			", Array(
				'_iditem' => (int)$hidden_iditem,
				'_iduser' => (int)$hidden_iduser,
				'_name' => endecrypt(trim($post_name)),
				'_sum' => endecrypt(trim($post_sum)),
				'_date' => trim(strtotime($post_date))
			), 'count');

		if($count_items >= 1) {
			$iteminfo =
			sql("SELECT data_name, data_sum, timestamp_date
				 FROM items
				 WHERE id != :_iditem
				 AND id_user != :_iduser
				 AND data_name != :_name
				 AND data_sum != :_sum
				 AND timestamp_date = :_date
				 ".($hidden_object == 'expense' ? "AND is_expense IS NOT NULL" : ($hidden_object == 'debt' ? "AND is_debt IS NOT NULL" : ($hidden_object == 'loan' ? "AND is_loan IS NOT NULL" : "")))."
				", Array(
					'_iditem' => (int)$hidden_iditem,
					'_iduser' => (int)$hidden_iduser,
					'_name' => endecrypt(trim($post_name)),
					'_sum' => endecrypt(trim($post_sum)),
					'_date' => trim(strtotime($post_date))
				), 'fetch');
		}



		if($count_items >= 1) {
			echo 'item-exists';

		} elseif(!empty($post_sum_permonth) AND !is_numeric($post_sum_permonth)) {
			echo 'onlydigits-permonth';

		} elseif(!empty($post_sum_interest) AND !is_numeric($post_sum_interest)) {
			echo 'onlydigits-interest';

		} elseif(!empty($post_sum_adminfee) AND !is_numeric($post_sum_adminfee)) {
			echo 'onlydigits-adminfee';

		} elseif(!empty($post_recurrence) AND !is_numeric($post_recurrence)) {
			echo 'onlydigits-recurrence';

		} elseif(!empty($post_recurrence) AND $post_recurrence > 12) {
			echo 'onlybetween-1-12-recurrence';

		} elseif(!empty($post_sum_permonth) AND $post_sum_permonth[0] == '-' OR
				 !empty($post_sum_interest) AND $post_sum_interest[0] == '-' OR
				 !empty($post_sum_adminfee) AND $post_sum_adminfee[0] == '-' OR
				 !empty($post_sum) AND $post_sum[0] == '-') {
			echo 'sum-invalid-nonegative';


		} else {
			sql("UPDATE items
				 SET id_account = :_idaccount,
					 data_name = :_name,
					 data_sum = :_sum,
					 data_sum_permonth = :_sum_permonth,
					 data_contact_phone = :_contact_phone,
					 data_contact_email = :_contact_email,
					 data_type = :_type,
					 data_category = :_category,
					 data_currency = :_currency,
					 data_number_payment = :_number_payment,
					 data_number_ocr = :_number_ocr,
					 data_number_phone = :_number_phone,
					 data_recurrence = :_recurrence,
					 data_recurrence_type = :_recurrence_type,
					 data_belongsto_debt = :_belongsto_debt,
					 data_notes = :_notes,
					 check_expense_payed = :_expense_payed,
					 check_expense_extra = :_expense_extra,
					 check_expense_subscription = :_expense_subscription,
					 check_expense_recurrent = :_expense_recurrent,
					 check_expense_correctinfo = :_expense_correctinfo,
					 check_debt_company = :_debt_company,
					 check_debt_paying = :_debt_paying,
					 check_loan_out = :_loan_out,
					 timestamp_date = :_date,
					 timestamp_date_end = :_date_end,
					 timestamp_payed = :_payed,
					 timestamp_edited = :_edited

				 WHERE id = :_iditem
				 AND id_user = :_iduser
				", Array(
					'_iditem' => (int)$hidden_iditem,
					'_iduser' => (int)$hidden_iduser,
					'_idaccount' => (empty($post_account) ? null : (int)$post_account),
					'_name' => endecrypt(trim($post_name)),
					'_sum' => endecrypt((float)trim($math->execute(str_replace(',', '.', $post_sum)))),
					'_sum_permonth' => (empty($post_sum_permonth) ? null : endecrypt((float)trim($math->execute(str_replace(',', '.', $post_sum_permonth))))),
					'_contact_phone' => (empty($post_contact_phone) ? null : endecrypt(trim($post_contact_phone))),
					'_contact_email' => (empty($post_contact_email) ? null : endecrypt(trim($post_contact_email))),
					'_type' => ((!isset($post_type) OR empty($post_type)) ? null : endecrypt($post_type)),
					'_category' => ((!isset($post_category) OR empty($post_category)) ? null : endecrypt($post_category)),
					'_currency' => ((!isset($post_currency) OR empty($post_currency)) ? null : endecrypt($post_currency)),
					'_number_payment' => (empty($post_number_payment) ? null : endecrypt(trim($post_number_payment))),
					'_number_ocr' => (empty($post_number_ocr) ? null : endecrypt(trim($post_number_ocr))),
					'_number_phone' => (empty($post_number_phone) ? null : endecrypt(str_replace(' ', '', str_replace('-', '', trim($post_number_phone))))),
					'_notes' => (empty($post_notes) ? null : endecrypt(trim($post_notes))),
					'_recurrence' => (empty($post_recurrence) ? null : endecrypt(trim($post_recurrence))),
					'_recurrence_type' => (empty($post_recurrence_type) ? null : endecrypt($post_recurrence_type)),
					'_belongsto_debt' => (empty($post_belongsto_debt) ? null : $post_belongsto_debt),
					'_expense_payed' => (empty($check_expense_payed) ? null : $check_expense_payed),
					'_expense_extra' => (empty($check_expense_extra) ? null : $check_expense_extra),
					'_expense_subscription' => (empty($check_expense_subscription) ? null : $check_expense_subscription),
					'_expense_recurrent' => (empty($check_expense_recurrent) ? null : $check_expense_recurrent),
					'_expense_correctinfo' => (empty($check_expense_correctinfo) ? null : $check_expense_correctinfo),
					'_debt_company' => (empty($check_debt_company) ? null : $check_debt_company),
					'_debt_paying' => (empty($check_debt_paying) ? null : $check_debt_paying),
					'_loan_out' => (empty($check_loan_out) ? null : $check_loan_out),
					'_date' => trim(strtotime($post_date)),
					'_date_end' => (empty($post_date_expires) ? null : endecrypt(strtotime($post_date_expires))),
					'_payed' => (empty($post_payed) ? null : endecrypt(strtotime($post_payed))),
					'_edited' => endecrypt(time())
				));



			/*if(!empty($check_expense_payed)) {
				sql("UPDATE users
					 SET data_sum_balance = :_sum_balance
					 WHERE id = :_iduser
					", Array(
						'_iduser' => (int)$hidden_iduser,
						'_sum_balance' => endecrypt((float)(endecrypt($user['data_sum_balance'], false) - $post_sum))
					));

			} elseif(empty($check_expense_payed)) {
				sql("UPDATE users
					 SET data_sum_balance = :_sum_balance
					 WHERE id = :_iduser
					", Array(
						'_iduser' => (int)$hidden_iduser,
						'_sum_balance' => endecrypt((float)(endecrypt($user['data_sum_balance'], false) + $post_sum))
					));
			}*/


			log_action(
				'updated item',
				null,
				null,
				(int)$hidden_iditem,
				($hidden_object == 'expense' ? 1 : null),
				($hidden_object == 'debt' ? 1 : null),
				($hidden_object == 'loan' ? 1 : null)
			);

			echo $hidden_object;
			echo '|';
			echo (empty($post_account) ? null : (int)$post_account);
			echo ($is_shared == false ? null : 'shared');
		}

	}

?>