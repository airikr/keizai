<?php

	require_once '../site-settings.php';

	use NXP\MathExecutor;



	$get_item = safetag($_GET['ite']);
	$get_iditem = safetag($_GET['idi']);
	$is_shared = false;



	$check_sharestatus =
	sql("SELECT COUNT(i.id_user)
		 FROM items i
		 JOIN shares s
		 ON i.id = s.id_item
		 WHERE s.id_item = :_iditem
		 AND s.id_user_with = :_iduser
		", Array(
			'_iditem' => (int)$get_iditem,
			'_iduser' => (int)$user['id']
		), 'count');

	if($check_sharestatus != 0) {
		$is_shared = true;

		$usershare =
		sql("SELECT u.id AS id_user, u.data_username, u.data_saltedstring_1, u.data_saltedstring_2, u.data_currency, i.id AS id_item
			 FROM users u
			 JOIN items i
			 ON u.id = i.id_user
			 WHERE i.id = :_iditem
			", Array(
				'_iditem' => (int)$get_iditem
			), 'fetch');

		$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($usershare['data_saltedstring_1'] . (int)$usershare['id_user'] . $usershare['data_saltedstring_2']).'.json'), true);


		$share =
		sql("SELECT s.*, s.data_sum AS sum_share, i.data_name, i.data_number_payment, i.data_number_ocr, i.timestamp_date
			 FROM shares s
			 JOIN items i
			 ON s.id_item = i.id
			 WHERE s.id_item = :_iditem
			 AND s.id_user_with = :_iduser
			", Array(
				'_iditem' => (int)$get_iditem,
				'_iduser' => (int)$user['id']
			), 'fetch');
	}

	$check_existence =
	sql("SELECT COUNT(id)
		 FROM items
		 WHERE id = :_iditem
		 AND id_user = :_iduser
		", Array(
			'_iditem' => (int)$get_iditem,
			'_iduser' => (int)($check_sharestatus == 0 ? $user['id'] : $usershare['id_user'])
		), 'count');



	if($check_existence == 0) {
		echo 'not-found';


	} else {

		$count_usershares =
		sql("SELECT COUNT(s.id_user_with)
			 FROM shares s
			 JOIN users u
			 ON s.id_user_with = u.id
			 WHERE s.id_item = :_iditem
			 AND s.id_user = :_iduser
			", Array(
				'_iditem' => (int)$get_iditem,
				'_iduser' => (int)$user['id']
			), 'count');

		if($count_usershares != 0) {
			$get_sharedusers =
			sql("SELECT s.id AS id_share,
						s.id_item AS id_item,
						s.data_sum AS sum_share,
						s.data_payment,
						s.allow_deletion,
						s.allow_markas_payed,
						s.share_number_payment,
						s.share_number_ocr,
						s.share_number_phone,
						s.share_qrcodes,
						s.share_notes,
						s.is_read,
						s.timestamp_shared,
						i.data_sum AS sum_item,
						u.data_username

				 FROM shares s
				 JOIN items i
				 ON s.id_item = i.id
				 JOIN users u
				 ON s.id_user_with = u.id
				 WHERE s.id_item = :_iditem
				 AND s.id_user = :_iduser
				", Array(
					'_iditem' => (int)$get_iditem,
					'_iduser' => (int)$user['id']
				));
		}



		$item =
		sql("SELECT *
			 FROM items
			 WHERE id = :_iditem
			 AND id_user = :_iduser
			", Array(
				'_iditem' => (int)$get_iditem,
				'_iduser' => (int)($check_sharestatus == 0 ? $user['id'] : $usershare['id_user'])
			), 'fetch');


		if(!empty($item['data_number_payment']) AND !empty($item['data_number_ocr'])) {
			if(preg_match('/-([0-9]{2})/i', endecrypt($item['data_number_payment'], false))) {
				$pt = 'BG';
			} else {
				$pt = 'PG';
			}
		}

		if($get_item == 'debt') {
			$monthsleft = format_number((endecrypt($item['data_sum'], false) / (empty($item['data_sum_permonth']) ? 50 : endecrypt($item['data_sum_permonth'], false))), 0);
		}

		if(!empty($item['is_expense'])) {
			$object = 'expenses';
			$scr = 'expense';

		} elseif(!empty($item['is_debt'])) {
			$object = 'debts';
			$scr = 'debt';

		} elseif(!empty($item['is_loan'])) {
			$object = 'loans';
			$scr = 'loan';
		}



		if(!empty($item['id_account'])) {
			$account =
			sql("SELECT *
				FROM item_accounts
				WHERE id = :_idaccount
				AND id_user = :_iduser
				", Array(
					'_idaccount' => (int)$item['id_account'],
					'_iduser' => (int)$user['id']
				), 'fetch');
		}

		if(!empty($item['data_belongsto_debt'])) {
			$debtinfo =
			sql("SELECT id, data_name, data_sum, timestamp_date
				FROM items
				WHERE id = :_iditem
				AND id_user = :_iduser
				AND is_debt IS NOT NULL
				", Array(
					'_iditem' => (int)$item['data_belongsto_debt'],
					'_iduser' => (int)$user['id']
				), 'fetch');
		}



		if($is_shared == true) {
			$currency = endecrypt($usershare['data_currency'], false);

		} elseif($is_shared == false AND $object == 'expenses' AND empty($item['data_currency'])) {
			$currency = endecrypt($user['data_currency'], false);

		} elseif($is_shared == false AND $object == 'expenses' AND !empty($item['data_currency'])) {
			$currency = endecrypt($item['data_currency'], false);

		} else {
			$currency = endecrypt($user['data_currency'], false);
		}



		if($object != 'expenses' OR empty($item['data_currency']) OR ($item['data_currency'] == $user['data_currency']) OR ($is_shared == true AND $item['data_currency'] == $usershare['data_currency'])) {
			if(empty($share['sum_share'])) {
				$item_sum = format_number(endecrypt($item['data_sum'], false));
				$item_sum_qrcode = endecrypt($item['data_sum'], false);

			} else {
				if(endecrypt($share['data_payment'], false) == 'sum') {
					$item_sum = format_number(endecrypt($share['sum_share'], false));
					$item_sum_qrcode = endecrypt($share['sum_share'], false);

				} else {
					$item_sum = format_number(endecrypt($item['data_sum'], false) * (endecrypt($share['sum_share'], false) / 100));
					$item_sum_qrcode = endecrypt($item['data_sum'], false) * (endecrypt($share['sum_share'], false) / 100);
				}
			}

			if(!empty($currency)) {
				$item_sum .= '<span class="currency-website">'.svgicon('currency-'.mb_strtolower($currency)).'</span>';
				$item_sum .= '<span class="currency-screenshot currency">'.mb_strtoupper($currency).'</span>';
			}

		} else {
			$item_sum = currency(endecrypt($item['data_sum'], false), endecrypt((empty($item['data_currency']) ? $user['data_currency'] : $item['data_currency']), false));
		}







		echo '<div class="payed no-select'.(!empty($item['check_expense_payed']) ? '' : ' hidden').'" data-iditem="'.(int)$item['id'].'">';
			echo $lang['words']['payed-status'];
		echo '</div>';

		echo '<h2>';
			echo '<div>';
				echo endecrypt($item['data_name'], false);

				echo '<div class="sum">';
					echo $item_sum;
				echo '</div>';
			echo '</div>';


			echo '<div data-html2canvas-ignore>';
				echo '<a href="javascript:void(0)" class="close" title="'.$lang['tooltips']['object-close'].'">';
					echo svgicon('x');
				echo '</a>';
			echo '</div>';
		echo '</h2>';



		echo '<nav class="details" data-html2canvas-ignore>';
			echo '<div class="side-by-side">';
				if($config_solomember == false AND $is_shared == false) {
					echo '<a href="javascript:void(0)" class="nav info active" title="'.$lang['tooltips']['object-info'].'">';
						echo svgicon('info-notfilled');
					echo '</a>';

					echo '<a href="javascript:void(0)" class="nav share" title="'.$lang['tooltips']['object-share'].'">';
						echo svgicon('share');
					echo '</a>';

					echo '<a href="javascript:void(0)" class="nav users" title="'.$lang['tooltips']['object-users'].'">';
						echo svgicon('users');
					echo '</a>';


				} elseif($config_solomember == true) {
					echo '<div class="inactive" title="'.$lang['tooltips']['shared-navigation-disabled-solo'].'">';
						echo svgicon('info-notfilled');
					echo '</div>';

					echo '<div class="inactive" title="'.$lang['tooltips']['shared-navigation-disabled-solo'].'">';
						echo svgicon('share');
					echo '</div>';

					echo '<div class="inactive" title="'.$lang['tooltips']['shared-navigation-disabled-solo'].'">';
						echo svgicon('users');
					echo '</div>';


				} else {
					if($usershare['id_user'] != $user['id']) {
						echo '<div class="inactive" title="'.$lang['tooltips']['shared-navigation-disabled'].'">';
							echo svgicon('info-notfilled');
						echo '</div>';

						echo '<div class="inactive" title="'.$lang['tooltips']['shared-navigation-disabled'].'">';
							echo svgicon('share');
						echo '</div>';

						echo '<div class="inactive" title="'.$lang['tooltips']['shared-navigation-disabled'].'">';
							echo svgicon('users');
						echo '</div>';


					} else {
						echo '<a href="javascript:void(0)" class="nav info active" title="'.$lang['tooltips']['object-info'].'">';
							echo svgicon('info-notfilled');
						echo '</a>';

						echo '<a href="javascript:void(0)" class="nav share" title="'.$lang['tooltips']['object-share'].'">';
							echo svgicon('share');
						echo '</a>';

						echo '<a href="javascript:void(0)" class="nav users" title="'.$lang['tooltips']['object-users'].'">';
							echo svgicon('users');
						echo '</a>';
					}
				}

				echo screenshot('.details > .content', $lang['words'][$scr].' - '.endecrypt($item['data_name'], false), 'nav');
			echo '</div>';



			echo '<div class="side-by-side">';
				if($get_item == 'expense') {
					if($is_shared == false OR $is_shared == true AND !empty($share['allow_markas_payed'])) {
						echo '<div class="wait hidden" data-iditem="'.(int)($is_shared == true ? $item['id_item'] : $item['id']).'">'.svgicon('wait').'</div>';

						echo '<div class="status not-payed'.(((!empty($get_filter) AND $get_filter == 'payed') OR empty($item['check_expense_payed'])) ? '' : ' hidden').'" data-iditem="'.(int)($is_shared == true ? $item['id_item'] : $item['id']).'">';
							echo '<a href="javascript:void(0)" class="markas-payed" data-iditem="'.(int)($is_shared == true ? $share['id_item'] : $item['id']).'" title="'.$lang['tooltips']['markas-payed'].'">';
								echo svgicon('tick');
							echo '</a>';
						echo '</div>';

						echo '<div class="status payed'.(((!empty($get_filter) AND $get_filter == 'payed') OR empty($item['check_expense_payed'])) ? ' hidden' : null).'" data-iditem="'.(int)($is_shared == true ? $item['id_item'] : $item['id']).'">';
							echo '<a href="javascript:void(0)" class="markas-notpayed" data-iditem="'.(int)($is_shared == true ? $share['id_item'] : $item['id']).'" title="'.$lang['tooltips']['markas-notpayed'].'">';
								echo svgicon('tick');
							echo '</a>';
						echo '</div>';
					}
				}


				if($is_shared == false) {
					echo '<a href="'.url('edit-'.$get_item.':'.(int)$item['id']).'" title="'.$lang['tooltips']['object-edit'].'">';
						echo svgicon('edit');
					echo '</a>';

				} else {
					echo '<div class="inactive" title="'.$lang['tooltips']['notallowed-edits'].'">';
						echo svgicon('edit');
					echo '</div>';
				}


				if($is_shared == false) {
					echo '<a href="'.url('delete-'.$get_item.':'.(int)$item['id'] . (($get_item == 'expense' AND !empty($item['id_account'])) ? '/account:'.(int)$item['id_account'] : '')).'" class="color-red" title="'.$lang['tooltips']['object-remove'].'" onClick="return confirm(\''.$lang['modals']['delete-item'].'\')">';
						echo svgicon('trash');
					echo '</a>';

				} else {
					if(empty($share['allow_deletion'])) {
						echo '<div class="inactive color-red" title="'.$lang['tooltips']['notallowed-deletion'].'">';
							echo svgicon('trash');
						echo '</div>';

					} else {
						echo '<a href="'.url('delete-'.$get_item.':'.(int)$item['id'] . (($get_item == 'expense' AND !empty($item['id_account']) AND $is_shared == false) ? '/account:'.(int)$item['id_account'] : '')).'" class="color-red" title="'.$lang['tooltips']['object-remove'].'" onClick="return confirm(\''.$lang['modals']['delete-item'].'\')">';
							echo svgicon('trash');
						echo '</a>';
					}
				}
			echo '</div>';
		echo '</nav>';







		echo '<div class="info" data-iditem="'.(int)$item['id'].'">';
			if(!empty($item['is_expense'])) {
				if($item['data_currency'] != ($check_sharestatus == 0 ? $user['data_currency'] : $usershare['data_currency'])) {
					echo '<div class="column" data-html2canvas-ignore>';
						echo '<div class="side-by-side">';
							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['words']['sums']['specified'];
								echo '</div>';

								echo '<div class="value">';
									echo format_number(endecrypt($item['data_sum'], false));
								echo '</div>';
							echo '</div>';

							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['words']['currency'];
								echo '</div>';

								echo '<div class="value">';
									echo mb_strtoupper(endecrypt($item['data_currency'], false));
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '</div>';
				}



				if($check_sharestatus == 0) {
					echo '<div class="column">';
						echo '<div class="side-by-side">';
							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['words']['account'];
								echo '</div>';

								echo '<div class="value">';
									if(empty($item['id_account'])) {
										echo '-';
									} else {
										echo '<a href="'.url('expenses'.(empty($item['id_account']) ? '' : '/account:'.(int)$item['id_account'])).'" title="'.$lang['tooltips']['filters']['by-type'].'">';
											echo endecrypt($account['data_name'], false);
										echo '</a>';
									}
								echo '</div>';
							echo '</div>';


							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['words']['type'];
								echo '</div>';

								echo '<div class="value">';
									echo '<a href="'.url('expenses'.(empty($item['id_account']) ? '' : '/account:'.(int)$item['id_account']).'/filter-by-type:'.endecrypt($item['data_type'], false)).'" title="'.$lang['tooltips']['filters']['by-type'].'">';
										echo $lang['types'][endecrypt($item['data_type'], false)];
									echo '</a>';
								echo '</div>';
							echo '</div>';


							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['words']['category'];
								echo '</div>';

								echo '<div class="value">';
									echo '<a href="'.url('expenses'.(empty($item['id_account']) ? '' : '/account:'.(int)$item['id_account']).'/filter-by-category:'.endecrypt($item['data_category'], false)).'" title="'.$lang['tooltips']['filters']['by-category'].'">';
										echo $lang['categories'][endecrypt($item['data_category'], false)];
									echo '</a>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '</div>';
				}



				echo '<div class="column">';
					echo '<div class="side-by-side">';
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['dates']['expires'];
							echo '</div>';

							echo '<div class="value date">';
								echo '<a href="'.url($object.'/filter-by-date:'.date_($item['timestamp_date'], 'date')).'" title="'.$lang['tooltips']['filter-by-date'].'">';
									echo date_($item['timestamp_date'], 'date');
								echo '</a>';
							echo '</div>';
						echo '</div>';


						echo '<div class="item payedstatus" data-iditem="'.(int)$item['id'].'">';
							echo '<div class="label">';
								echo $lang['words']['sums']['payed'];
							echo '</div>';

							echo '<div class="value">';
								echo '<div class="not-payed'.(empty($item['timestamp_payed']) ? '' : ' hidden').'">';
									echo '-';
								echo '</div>';

								echo '<div class="payed date'.(empty($item['timestamp_payed']) ? ' hidden' : '').'">';
									if(!empty($item['timestamp_payed'])) {
										echo '<a href="'.url($object.'/filter-by-date:'.date_(endecrypt($item['timestamp_payed'], false), 'date')).'" title="'.$lang['tooltips']['filter-by-date'].'">';
											echo date_(endecrypt($item['timestamp_payed'], false), 'date');
										echo '</a>';
									}
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '</div>';
				echo '</div>';



				if($is_shared == false OR
				   $is_shared == true AND !empty($share['share_number_payment']) OR
				   $is_shared == true AND !empty($share['share_number_ocr']) OR
				   $is_shared == true AND !empty($share['share_number_phone'])) {
					echo '<div class="column">';
						echo '<div class="side-by-side">';
							if($is_shared == false OR $is_shared == true AND !empty($share['share_number_payment'])) {
								echo '<div class="item payment">';
									echo '<div class="label">';
										echo $lang['words']['number-payment'];
									echo '</div>';

									echo '<div class="value">';
										if(empty($item['data_number_payment'])) {
											echo '-';
										} else {
											echo endecrypt($item['data_number_payment'], false);
										}
									echo '</div>';
								echo '</div>';
							}


							if($is_shared == false OR $is_shared == true AND !empty($share['share_number_ocr'])) {
								echo '<div class="item ocr">';
									echo '<div class="label">';
										echo $lang['words']['number-ocr'];
									echo '</div>';

									echo '<div class="value">';
										if(empty($item['data_number_ocr'])) {
											echo '-';
										} else {
											echo endecrypt($item['data_number_ocr'], false);
										}
									echo '</div>';
								echo '</div>';
							}


							if($is_shared == false OR $is_shared == true AND !empty($share['share_number_phone'])) {
								echo '<div class="item phone">';
									echo '<div class="label">';
										echo $lang['words']['number-phone'];
									echo '</div>';

									echo '<div class="value">';
										echo (empty($item['data_number_phone']) ? '-' : endecrypt($item['data_number_phone'], false));
									echo '</div>';
								echo '</div>';
							}
						echo '</div>';
					echo '</div>';


					if(!empty($item['data_belongsto_debt'])) {
						echo '<div class="column">';
							echo '<div class="item phone">';
								echo '<div class="label">';
									echo $lang['words']['belongs-to-debt'];
								echo '</div>';

								echo '<div class="value date">';
									echo '<a href="'.url('debts#debt-'.(int)$debtinfo['id']).'">';
										echo date_($debtinfo['timestamp_date'], 'date').': ';
										echo endecrypt($debtinfo['data_name'], false);
									echo '</a>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					}
				}





				if($is_shared == true OR !empty($item['check_expense_extra']) OR !empty($item['check_expense_recurrent']) OR !empty($item['check_expense_subscription'])) {
					echo '<div class="info">';
						if($is_shared == false) {
							if(!empty($item['check_expense_extra'])) {
								echo '<div class="is-extra'.((empty($item['data_number_payment']) AND empty($item['data_number_ocr'])) ? ' no-qr' : '').'">';
									echo '<div>'.svgicon('info').'</div>';
									echo $lang['information']['expense']['extra'];
								echo '</div>';
							}

							if(!empty($item['check_expense_recurrent'])) {
								echo '<div class="is-recurrent'.((empty($item['data_number_payment']) AND empty($item['data_number_ocr'])) ? ' no-qr' : '').'">';
									echo '<div>'.svgicon('recurrent').'</div>';
									echo $lang['information']['expense']['recurrent'];

									if(!empty($item['data_recurrence']) AND !empty($item['data_recurrence_type'])) {
										echo ' ('.mb_strtolower($lang['words']['recurrences'][endecrypt($item['data_recurrence'], false).'-'.endecrypt($item['data_recurrence_type'], false)]).')';
									}
								echo '</div>';
							}

							if(!empty($item['check_expense_subscription'])) {
								echo '<div class="is-subscsription">';
									echo '<div>'.svgicon('subscription').'</div>';
									echo $lang['information']['expense']['subscsription'];
								echo '</div>';
							}


						} else {
							echo '<div class="is-shared">';
								echo '<div>'.svgicon('share').'</div>';

								echo ($check_sharestatus == 0 ? '' : endecrypt($usershare['data_username'], false, true));
								echo ($usershare['id_user'] == $user['id'] ? $lang['information']['item-is-shared-tosomeone'] : ' '.mb_strtolower($lang['information']['item-is-shared-toyou']));
							echo '</div>';
						}
					echo '</div>';
				}



				if(!empty($item['data_number_payment']) AND !empty($item['data_number_ocr'])) {
					if($is_shared == false OR $is_shared == true AND !empty($share['share_number_payment']) AND !empty($share['share_number_ocr'])) {
						echo '<div class="info-status'.(empty($item['check_expense_extra']) ? ' no-extra' : '').'">';
							if(empty($item['check_expense_correctinfo'])) {
								echo '<div class="not-sure">';
									echo '<div>'.svgicon('exclamation').'</div>';
									echo $lang['information']['expense']['info-can-be-wrong'];
								echo '</div>';

							} else {
								echo '<div class="sure">';
									echo '<div>'.svgicon('tick').'</div>';
									echo $lang['information']['expense']['info-is-correct'];
								echo '</div>';
							}
						echo '</div>';
					}


					if($is_shared == false OR $is_shared == true AND !empty($share['share_qrcodes'])) {
						echo '<div class="qr-code">';
							echo '<div class="not-payed'.(empty($item['check_expense_payed']) ? '' : ' hidden').'" data-iditem="'.(int)$item['id'].'">';
								echo '<div class="info color-blue">'.$lang['messages']['scan-qrcode-bank'].'</div>';

								echo '<div class="code"';
								echo ' data-nme="'.endecrypt(($is_shared == false ? $item['data_name'] : $share['data_name']), false).'"';
								echo ' data-ocr="'.endecrypt(($is_shared == false ? $item['data_number_ocr'] : $share['data_number_ocr']), false).'"';
								echo ' data-ddt="'.date('Ymd', ($is_shared == false ? $item['timestamp_date'] : $share['timestamp_date'])).'"';
								echo ' data-acc="'.endecrypt(($is_shared == false ? $item['data_number_payment'] : $share['data_number_payment']), false).'"';
								echo ' data-pt="'.$pt.'"';
								echo ' data-due="'.format_number($item_sum_qrcode, 2, '.', '').'"';
								echo '></div>';
							echo '</div>';

							echo '<div class="payed'.(empty($item['check_expense_payed']) ? ' hidden' : '').'" data-iditem="'.(int)$item['id'].'">';
								echo '<div class="info color-blue">'.$lang['messages']['scan-qrcode-bank-payed'].'</div>';
							echo '</div>';
						echo '</div>';
					}
				}



				if($is_shared == false AND !empty($item['data_number_phone']) AND empty($item['check_expense_payed']) OR
				   $is_shared == true AND !empty($item['data_number_phone']) AND !empty($share['share_qrcodes']) AND empty($item['check_expense_payed'])) {
					echo '<div class="qr-code">';
						echo '<div class="not-payed'.(empty($item['check_expense_payed']) ? '' : ' hidden').'" data-iditem="'.(int)$item['id'].'">';
							echo '<div class="info color-blue">'.$lang['messages']['scan-qrcode-swish'].'</div>';
							echo '<div class="swish" data-sw="'.endecrypt($item['data_number_phone'], false).'" data-amt="'.endecrypt($item['data_sum'], false).'"></div>';
						echo '</div>';

						echo '<div class="payed'.(empty($item['check_expense_payed']) ? ' hidden' : '').'" data-iditem="'.(int)$item['id'].'">';
							echo '<div class="info color-blue">'.$lang['messages']['scan-qrcode-swish-payed'].'</div>';
						echo '</div>';
					echo '</div>';
				}















			} elseif(!empty($item['is_debt'])) {
				echo '<div class="column">';
					echo '<div class="side-by-side">';
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['sums']['monthly'];
							echo '</div>';

							echo '<div class="value">';
								if(empty($item['data_sum_permonth'])) {
									echo $lang['words']['at-least'].' 5';
									echo (empty($currency) ? null : svgicon('currency-'.mb_strtolower($currency)));

								} else {
									echo format_number(endecrypt($item['data_sum_permonth'], false), 2);
									echo (empty($currency) ? null : svgicon('currency-'.mb_strtolower($currency)));
								}
							echo '</div>';
						echo '</div>';


						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['dates']['started'];
							echo '</div>';

							echo '<div class="value date">';
								echo '<a href="'.url($object.'/filter-by-date:'.date_($item['timestamp_date'], 'date')).'" title="'.$lang['tooltips']['filter-by-date'].'">';
									echo date_($item['timestamp_date'], 'date');
								echo '</a>';
							echo '</div>';
						echo '</div>';


						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['dates']['estimated-payed'];
							echo '</div>';

							echo '<div class="value">';
								echo date_(strtotime('+'.$monthsleft.' month'), 'month-year');
							echo '</div>';
						echo '</div>';
					echo '</div>';
				echo '</div>';



				echo '<div class="column">';
					echo '<div class="side-by-side">';
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['contact-phone'];
							echo '</div>';

							echo '<div class="value">';
								if(empty($item['data_contact_phone'])) {
									echo '-';

								} else {
									echo '<div class="desktop">'.endecrypt($item['data_contact_phone'], false).'</div>';
									echo '<div class="portable">';
										echo '<a href="tel:'.endecrypt($item['data_contact_phone'], false).'">';
											echo endecrypt($item['data_contact_phone'], false);
										echo '</a>';
									echo '</div>';
								}
							echo '</div>';
						echo '</div>';


						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['contact-email'];
							echo '</div>';

							echo '<div class="value">';
								if(empty($item['data_contact_email'])) {
									echo '-';

								} else {
									echo '<a href="mailto:'.endecrypt($item['data_contact_email'], false).'">';
										echo endecrypt($item['data_contact_email'], false);
									echo '</a>';
								}
							echo '</div>';
						echo '</div>';
					echo '</div>';
				echo '</div>';



				echo '<div class="info">';
					if($is_shared == false) {
						echo '<div class="is-debt-paying">';
							if(!empty($item['check_debt_paying'])) {
								echo '<div>'.svgicon('info').'</div>';
								echo $lang['information']['debt']['amortization-started'];

							} else {
								echo '<div>'.svgicon('info').'</div>';
								echo $lang['information']['debt']['amortization-notstarted'];
							}
						echo '</div>';


					} else {
						echo '<div class="is-shared">';
							echo '<div>'.svgicon('share').'</div>';

							echo ($check_sharestatus == 0 ? '' : endecrypt($usershare['data_username'], false, true));
							echo ($usershare['id_user'] == $user['id'] ? $lang['information']['item-is-shared-tosomeone'] : ' '.mb_strtolower($lang['information']['item-is-shared-toyou']));
						echo '</div>';
					}
				echo '</div>';















			} elseif(!empty($item['is_loan'])) {
				echo '<div class="column">';
					echo '<div class="side-by-side">';
						if($check_sharestatus != 0) {
							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['words']['shared-from'];
								echo '</div>';

								echo '<div class="value">';
									echo endecrypt($usershare['data_username'], false, true);
								echo '</div>';
							echo '</div>';
						}


						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['dates']['loan-from'];
							echo '</div>';

							echo '<div class="value date">';
								echo '<a href="'.url($object.'/filter-by-date:'.date_($item['timestamp_date'], 'date')).'" title="'.$lang['tooltips']['filter-by-date'].'">';
									echo date_($item['timestamp_date'], 'date');
								echo '</a>';
							echo '</div>';
						echo '</div>';


						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['words']['dates']['loan-to'];
							echo '</div>';

							echo '<div class="value date">';
								if(empty($item['timestamp_date_end'])) {
									echo '-';
								} else {
									echo '<a href="'.url($object.'/filter-by-date:'.date_(endecrypt($item['timestamp_date_end'], false), 'date')).'" title="'.$lang['tooltips']['filter-by-date'].'">';
										echo date_(endecrypt($item['timestamp_date_end'], false), 'date');
									echo '</a>';
								}
							echo '</div>';
						echo '</div>';
					echo '</div>';
				echo '</div>';


				echo '<div class="column">';
					echo '<div class="item phone">';
						echo '<div class="label">';
							echo $lang['words']['number-phone'];
						echo '</div>';

						echo '<div class="value">';
							echo (empty($item['data_number_phone']) ? '-' : endecrypt($item['data_number_phone'], false));
						echo '</div>';
					echo '</div>';
				echo '</div>';



				echo '<div class="info">';
					if($is_shared == false) {
						echo '<div class="is-loan-status">';
							echo '<div>'.svgicon('info').'</div>';
							echo (!empty($item['check_loan_out']) ? $lang['information']['loan']['lend'] : $lang['information']['loan']['borrow']);
						echo '</div>';


					} else {
						echo '<div class="is-shared">';
							echo '<div>'.svgicon('share').'</div>';

							echo ($check_sharestatus == 0 ? '' : endecrypt($usershare['data_username'], false, true));
							echo ($usershare['id_user'] == $user['id'] ? $lang['information']['item-is-shared-tosomeone'] : ' '.mb_strtolower($lang['information']['item-is-shared-toyou']));
						echo '</div>';
					}
				echo '</div>';



				if($is_shared == false AND !empty($item['data_number_phone']) OR
				$is_shared == true AND !empty($item['data_number_phone']) AND !empty($share['share_qrcodes'])) {
					echo '<div class="qr-code">';
						echo '<div class="info color-blue">'.$lang['messages']['scan-qrcode-swish'].'</div>';
						echo '<div class="swish" data-sw="'.endecrypt($item['data_number_phone'], false).'" data-amt="'.endecrypt($item['data_sum'], false).'"></div>';
					echo '</div>';
				}
			}



			if($is_shared == false OR $is_shared == true AND !empty($share['share_notes'])) {
				echo '<div class="notes">';
					echo '<h3>'.$lang['subtitles']['notes'].'</h3>';

					if(empty($item['data_notes'])) {
						echo '<p class="color-blue">'.$lang['information']['no-notes'].'</p>';
					} else {
						echo $Parsedown->text(endecrypt($item['data_notes'], false));
					}
				echo '</div>';
			}
		echo '</div>';









		echo '<div class="share">';
			echo '<div class="content">';
				foreach($lang['shortinfo']['share-item'] AS $content) {
					echo $Parsedown->text($content);
				}
			echo '</div>';

			echo '<div class="msg share"></div>';

			echo '<form action="javascript:void(0)" method="POST" id="share" autocomplete="off" novalidate>';
				echo '<input type="hidden" name="hidden-iditem" value="'.(int)$item['id'].'">';
				echo '<input type="hidden" name="hidden-editing" value="n">';

				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['words']['username'];
					echo '</div>';

					echo '<div class="field">';
						echo '<div class="icon">'.svgicon('user').'</div>';
						echo '<input type="text" name="field-username" aria-label="username">';
					echo '</div>';
				echo '</div>';

				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['words']['recipient-pays'];
					echo '</div>';

					echo '<div class="field">';
						echo '<div class="icon">'.svgicon('money').'</div>';
						echo '<input type="text" inputmode="numeric" pattern="[0-9]{6}" name="field-sum" aria-label="sum">';

						echo '<select name="list-payment-type">';
							echo '<option value="sum">'.$lang['words']['sums']['amount'].'</option>';
							echo '<option value="percent">'.$lang['words']['sums']['percentage'].'</option>';
						echo '</select>';
					echo '</div>';
				echo '</div>';


				echo '<div class="checkboxes '.$get_item.'">';
					echo checkbox($lang['forms']['checkboxes']['share']['allow-deletion'], 'allow-deletion', null);
					echo checkbox($lang['forms']['checkboxes']['share']['allow-markas-payed'], 'allow-markas-payed', null, false, ($get_item == 'expense' ? false : true));

					echo '<div class="space"></div>';

					echo checkbox($lang['forms']['checkboxes']['share']['number-payment'], 'share-number-payment', null, false, ($get_item == 'expense' ? false : true));
					echo checkbox($lang['forms']['checkboxes']['share']['number-ocr'], 'share-number-ocr', null, false, ($get_item == 'expense' ? false : true));
					echo checkbox($lang['forms']['checkboxes']['share']['number-phone'], 'share-number-phone', null, false, false);
					echo checkbox($lang['forms']['checkboxes']['share']['qrcodes'], 'share-qrcodes', $lang['forms']['checkboxes']['descriptions']['share-qrcodes'], false, true);
					echo checkbox($lang['forms']['checkboxes']['share']['notes'], 'share-notes', null);
				echo '</div>';


				echo '<div class="button">';
					echo '<input type="submit" name="button-share" value="'.$lang['words']['buttons']['share'].'">';
				echo '</div>';
			echo '</form>';
		echo '</div>';



		echo '<div class="share-edit"></div>';









		echo '<div class="users">';
			echo '<div class="content">';
				foreach($lang['shortinfo']['shared-to-users'] AS $content) {
					echo $Parsedown->text($content);
				}
			echo '</div>';


			if($count_usershares == 0) {
				echo '<div class="message">';
					echo $lang['messages']['no-items'];
				echo '</div>';

			} else {
				echo '<div class="userlist">';
					echo '<div class="item head side-by-side">';
						echo '<div class="options"></div>';

						echo '<div class="username">';
							echo $lang['words']['username'];
						echo '</div>';

						echo '<div class="sum">';
							echo $lang['words']['sums']['amount'];
						echo '</div>';
					echo '</div>';


					foreach($get_sharedusers AS $usershare) {
						echo '<div class="item side-by-side">';
							echo '<div class="options">';
								echo '<a href="javascript:void(0)" class="edit-share" data-idshare="'.(int)$usershare['id_share'].'" data-iditem="'.(int)$usershare['id_item'].'" title="'.$lang['tooltips']['edit-share'].'">';
									echo svgicon('edit');
								echo '</a>';

								echo '<a href="'.url('shares/delete-id:'.(int)$usershare['id_share']).'" class="color-red" title="'.$lang['tooltips']['delete-share'].'" onClick="return confirm(\''.$lang['modals']['delete-share'].'\')">';
									echo svgicon('trash');
								echo '</a>';
							echo '</div>';

							echo '<div class="username">';
								echo endecrypt($usershare['data_username'], false, true);
							echo '</div>';

							echo '<div class="sum">';
								if(endecrypt($usershare['data_payment'], false) == 'sum') {
									echo format_number(endecrypt($usershare['sum_share'], false));
									echo svgicon('currency-'.mb_strtolower(endecrypt(($is_shared == true ? $usershare['data_currency'] : $user['data_currency']), false)));

								} elseif(endecrypt($usershare['data_payment'], false) == 'percent') {
									echo format_number(endecrypt($usershare['sum_item'], false) * (endecrypt($usershare['sum_share'], false) / 100));
									echo svgicon('currency-'.mb_strtolower(endecrypt(($is_shared == true ? $usershare['data_currency'] : $user['data_currency']), false)));
									echo '('.endecrypt($usershare['sum_share'], false).'%)';

								} else {
									echo format_number(endecrypt($usershare['sum_item'], false));
									echo svgicon('currency-'.mb_strtolower(endecrypt(($is_shared == true ? $usershare['data_currency'] : $user['data_currency']), false)));
								}
							echo '</div>';
						echo '</div>';
					}
				echo '</div>';
			}
		echo '</div>';

	}

?>