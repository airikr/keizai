<?php

	require_once '../site-settings.php';



	$post_file = $_FILES['file'];



	if($post_file['error'] == 1) {
		echo 'server-size-ini';

	} elseif($post_file['error'] == 2) {
		echo 'server-size-form';

	} elseif($post_file['error'] == 3) {
		echo 'partial';

	} elseif($post_file['error'] == 4) {
		echo 'no-file';

	} elseif($post_file['error'] == 6) {
		echo 'no-tmpdir';

	} elseif($post_file['error'] == 7) {
		echo 'cant-write';

	} elseif($post_file['error'] == 8) {
		echo 'extension';


	} elseif($post_file['error'] == 0) {
		$file = json_decode(file_get_contents($post_file['tmp_name']), true);
		$file_enc = json_decode($file['file']['content'], true);
		$encrypted = $file['export']['encrypted'];

		sql("DELETE FROM items WHERE id_user = :_iduser;
			 DELETE FROM item_accounts WHERE id_user = :_iduser;
			 DELETE FROM budget WHERE id_user = :_iduser
			", Array('_iduser' => (int)$user['id']));


		if($encrypted == true AND $enc['keys'][0] != $file_enc['keys'][0] OR $encrypted == true AND $enc['keys'][1] != $file_enc['keys'][1]) {
			function decrypt($string) {
				global $file_enc, $config_encryptionmethod;

				if(empty($string)) {
					return 'dencr.err';

				} else {
					return openssl_decrypt(
						base64_decode($string),
						$config_encryptionmethod,
						hash('sha256', $file_enc['keys'][0]),
						0,
						substr(hash('sha256', $file_enc['keys'][1]), 0, 16)
					);
				}
			}

			function encrypt($string) {
				global $enc, $config_encryptionmethod;

				if(empty($string)) {
					return 'encr.err';

				} else {
					return base64_encode(openssl_encrypt(
						$string,
						$config_encryptionmethod,
						hash('sha256', $enc['keys'][0]),
						0,
						substr(hash('sha256', $enc['keys'][1]), 0, 16)
					));
				}
			}



			sql("UPDATE users
				 SET data_email = :_email,
					 data_tfa = :_tfa,
					 data_tfa_backup = :_tfa_backup,
					 data_incomeday = :_incomeday,
					 data_sum_income = :_sum_income,
					 data_sum_balance = :_sum_balance,
					 data_sum_savings = :_sum_savings,
					 data_currency = :_currency,
					 data_language = :_language,
					 data_notes = :_notes,
					 data_theme = :_theme,
					 check_email_expensesexpires = :_email_expenses_expires,
					 check_option_hidedate_expenses = :_option_hidedate_expenses,
					 check_option_hidedate_debts = :_option_hidedate_debts,
					 check_option_hidedate_loans = :_option_hidedate_loans,
					 timestamp_period_start = :_period_start,
					 timestamp_period_end = :_period_end

				 WHERE id = :_iduser
				", Array(
					'_iduser' => (int)$user['id'],
					'_email' => (empty($file['user']['email']) ? null : encrypt(decrypt($file['user']['email']))),
					'_tfa' => (empty($file['user']['two-factor']) ? null : $file['user']['two-factor']),
					'_tfa_backup' => (empty($file['user']['two-factor-backup']) ? null : $file['user']['two-factor-backup']),
					'_incomeday' => (empty($file['user']['income-day']) ? null : encrypt(decrypt($file['user']['income-day']))),
					'_sum_income' => (empty($file['user']['sums']['income']) ? null : encrypt(decrypt($file['user']['sums']['income']))),
					'_sum_balance' => (empty($file['user']['sums']['balance']) ? null : encrypt(decrypt($file['user']['sums']['balance']))),
					'_sum_savings' => (empty($file['user']['sums']['savings']) ? null : encrypt(decrypt($file['user']['sums']['savings']))),
					'_currency' => (empty($file['user']['currency']) ? null : encrypt(decrypt($file['user']['currency']))),
					'_language' => (empty($file['user']['language']) ? null : encrypt(decrypt($file['user']['language']))),
					'_notes' => (empty($file['user']['notes']) ? null : encrypt(decrypt($file['user']['notes']))),
					'_theme' => (empty($file['user']['theme']) ? null : $file['user']['theme']),
					'_email_expenses_expires' => (empty($file['user']['checkboxes']['expenses-expires']) ? null : $file['user']['checkboxes']['expenses-expires']),
					'_option_hidedate_expenses' => (empty($file['user']['checkboxes']['hide-date-expenses']) ? null : $file['user']['checkboxes']['hide-date-expenses']),
					'_option_hidedate_debts' => (empty($file['user']['checkboxes']['hide-date-debts']) ? null : $file['user']['checkboxes']['hide-date-debts']),
					'_option_hidedate_loans' => (empty($file['user']['checkboxes']['hide-date-loans']) ? null : $file['user']['checkboxes']['hide-date-loans']),
					'_period_start' => (empty($file['user']['timestamps']['period-start']) ? null : encrypt(decrypt($file['user']['timestamps']['period-start']))),
					'_period_end' => (empty($file['user']['timestamps']['period-end']) ? null : encrypt(decrypt($file['user']['timestamps']['period-end'])))
				));


			if(!empty($file['accounts'])) {
				foreach($file['accounts'] AS $account) {
					sql("INSERT INTO item_accounts(
							 id_user,
							 data_name,
							 is_default
						 )

						 VALUES(
							 :_iduser,
							 :_name,
							 :_isdefault
						 )
						", Array(
							'_iduser' => (int)$user['id'],
							'_name' => encrypt(decrypt($account['name'])),
							'_isdefault' => ($account['is_default'] == false ? null : 1)
						), 'insert');
				}
			}


			foreach($file['items'] AS $item) {
				if(!empty($item['account'])) {
					$account =
					sql("SELECT id
						FROM item_accounts
						WHERE data_name = :_name
						AND id_user = :_iduser
						", Array(
							'_name' => encrypt(decrypt($item['account'])),
							'_iduser' => (int)$user['id']
						), 'fetch');
				}

				sql("INSERT INTO items(
						 id_user,
						 id_account,
						 data_name,
						 data_sum,
						 data_sum_permonth,
						 data_type,
						 data_category,
						 data_currency,
						 data_number_payment,
						 data_number_ocr,
						 data_number_phone,
						 data_recurrence,
						 data_recurrence_type,
						 data_notes,
						 check_expense_payed,
						 check_expense_extra,
						 check_expense_subscription,
						 check_expense_recurrent,
						 check_expense_correctinfo,
						 check_debt_company,
						 check_debt_paying,
						 check_loan_out,
						 is_expense,
						 is_debt,
						 is_loan,
						 timestamp_date,
						 timestamp_date_end,
						 timestamp_payed,
						 timestamp_added
					 )

					 VALUES(
						 :_iduser,
						 :_idaccount,
						 :_name,
						 :_sum,
						 :_sum_permonth,
						 :_type,
						 :_category,
						 :_currency,
						 :_number_payment,
						 :_number_ocr,
						 :_number_phone,
						 :_recurrence,
						 :_recurrence_type,
						 :_notes,
						 :_expense_payed,
						 :_expense_extra,
						 :_expense_subscription,
						 :_expense_recurrent,
						 :_expense_correctinfo,
						 :_debt_company,
						 :_debt_paying,
						 :_loan_out,
						 :_isexpense,
						 :_isdebt,
						 :_isloan,
						 :_date,
						 :_date_end,
						 :_payed,
						 :_added
					 )
					", Array(
						'_iduser' => (int)$user['id'],
						'_idaccount' => (empty($item['account']) ? null : (int)$account['id']),
						'_name' => encrypt(decrypt($item['name'])),
						'_sum' => encrypt(decrypt($item['sums']['sum'])),
						'_sum_permonth' => (empty($item['sums']['permonth']) ? null : encrypt(decrypt($item['sums']['permonth']))),
						'_type' => (empty($item['type']) ? null : encrypt(decrypt($item['type']))),
						'_category' => (empty($item['category']) ? null : encrypt(decrypt($item['category']))),
						'_currency' => (empty($item['currency']) ? null : encrypt(decrypt($item['currency']))),
						'_number_payment' => (empty($item['number-payment']) ? null : encrypt(decrypt($item['number-payment']))),
						'_number_ocr' => (empty($item['number-ocr']) ? null : encrypt(decrypt($item['number-ocr']))),
						'_number_phone' => (empty($item['number-phone']) ? null : encrypt(decrypt($item['number-phone']))),
						'_recurrence' => (empty($item['recurrence']) ? null : encrypt(decrypt($item['recurrence']))),
						'_recurrence_type' => (empty($item['recurrence-type']) ? null : encrypt(decrypt($item['recurrence-type']))),
						'_notes' => (empty($item['notes']) ? null : encrypt(decrypt($item['notes']))),
						'_expense_payed' => (empty($item['checkboxes']['expense-payed']) ? null : $item['checkboxes']['expense-payed']),
						'_expense_extra' => (empty($item['checkboxes']['expense-extra']) ? null : $item['checkboxes']['expense-extra']),
						'_expense_subscription' => (empty($item['checkboxes']['expense-subscription']) ? null : $item['checkboxes']['expense-subscription']),
						'_expense_recurrent' => (empty($item['checkboxes']['expense-recurrent']) ? null : $item['checkboxes']['expense-recurrent']),
						'_expense_correctinfo' => (empty($item['checkboxes']['expense-correctinfo']) ? null : $item['checkboxes']['expense-correctinfo']),
						'_debt_company' => (empty($item['checkboxes']['debt-company']) ? null : $item['checkboxes']['debt-company']),
						'_debt_paying' => (empty($item['checkboxes']['debt-paying']) ? null : $item['checkboxes']['debt-paying']),
						'_loan_out' => (empty($item['checkboxes']['loan-out']) ? null : $item['checkboxes']['loan-out']),
						'_isexpense' => (empty($item['is-expense']) ? null : $item['is-expense']),
						'_isdebt' => (empty($item['is-debt']) ? null : $item['is-debt']),
						'_isloan' => (empty($item['is-loan']) ? null : $item['is-loan']),
						'_date' => (empty($item['timestamps']['date']) ? null : $item['timestamps']['date']),
						'_date_end' => (empty($item['timestamps']['date-end']) ? null : encrypt(decrypt($item['timestamps']['date-end']))),
						'_payed' => (empty($item['timestamps']['payed']) ? null : encrypt(decrypt($item['timestamps']['payed']))),
						'_added' => encrypt(time())
					), 'insert');
			}


			if(!empty($file['budget'])) {
				foreach($file['budget'] AS $budget) {
					sql("INSERT INTO budget(
							 id_user,
							 data_name,
							 data_sum_current,
							 data_sum_goal,
							 data_notes,
							 timestamp_created
						 )

						 VALUES(
							 :_iduser,
							 :_name,
							 :_sum_current,
							 :_sum_goal,
							 :_notes,
							 :_created
						 )
						", Array(
							'_iduser' => (int)$user['id'],
							'_name' => encrypt(decrypt($budget['name'])),
							'_sum_current' => encrypt(decrypt($budget['sums']['current'])),
							'_sum_goal' => encrypt(decrypt($budget['sums']['goal'])),
							'_notes' => encrypt(decrypt($budget['notes'])),
							'_created' => $budget['created']
						), 'insert');
				}
			}







		} else {
			sql("UPDATE users
				 SET data_email = :_email,
					 data_tfa = :_tfa,
					 data_tfa_backup = :_tfa_backup,
					 data_incomeday = :_incomeday,
					 data_sum_income = :_sum_income,
					 data_sum_balance = :_sum_balance,
					 data_sum_savings = :_sum_savings,
					 data_currency = :_currency,
					 data_language = :_language,
					 data_notes = :_notes,
					 data_theme = :_theme,
					 check_email_expensesexpires = :_email_expenses_expires,
					 check_option_hidedate_expenses = :_option_hidedate_expenses,
					 check_option_hidedate_debts = :_option_hidedate_debts,
					 check_option_hidedate_loans = :_option_hidedate_loans,
					 timestamp_period_start = :_period_start,
					 timestamp_period_end = :_period_end

				 WHERE id = :_iduser
				", Array(
					'_iduser' => (int)$user['id'],
					'_email' => (empty($file['user']['email']) ? null : ($encrypted == true ? $file['user']['email'] : endecrypt($file['user']['email']))),
					'_tfa' => (empty($file['user']['two-factor']) ? null : $file['user']['two-factor']),
					'_tfa_backup' => (empty($file['user']['two-factor-backup']) ? null : $file['user']['two-factor-backup']),
					'_incomeday' => (empty($file['user']['income-day']) ? null : ($encrypted == true ? $file['user']['income-day'] : endecrypt($file['user']['income-day']))),
					'_sum_income' => (empty($file['user']['sums']['income']) ? null : ($encrypted == true ? $file['user']['sums']['income'] : endecrypt($file['user']['sums']['income']))),
					'_sum_balance' => (empty($file['user']['sums']['balance']) ? null : ($encrypted == true ? $file['user']['sums']['balance'] : endecrypt($file['user']['sums']['balance']))),
					'_sum_savings' => (empty($file['user']['sums']['savings']) ? null : ($encrypted == true ? $file['user']['sums']['savings'] : endecrypt($file['user']['sums']['savings']))),
					'_currency' => ($encrypted == true ? $file['user']['currency'] : endecrypt($file['user']['currency'])),
					'_language' => ($encrypted == true ? $file['user']['language'] : endecrypt($file['user']['language'])),
					'_notes' => (empty($file['user']['notes']) ? null : ($encrypted == true ? $file['user']['notes'] : endecrypt($file['user']['notes']))),
					'_theme' => (empty($file['user']['theme']) ? null : $file['user']['theme']),
					'_email_expenses_expires' => (empty($file['user']['checkboxes']['expenses-expires']) ? null : $file['user']['checkboxes']['expenses-expires']),
					'_option_hidedate_expenses' => (empty($file['user']['checkboxes']['hide-date-expenses']) ? null : $file['user']['checkboxes']['hide-date-expenses']),
					'_option_hidedate_debts' => (empty($file['user']['checkboxes']['hide-date-debts']) ? null : $file['user']['checkboxes']['hide-date-debts']),
					'_option_hidedate_loans' => (empty($file['user']['checkboxes']['hide-date-loans']) ? null : $file['user']['checkboxes']['hide-date-loans']),
					'_period_start' => (empty($file['user']['timestamps']['period-start']) ? null : ($encrypted == true ? $file['user']['timestamps']['period-start'] : endecrypt($file['user']['timestamps']['period-start']))),
					'_period_end' => (empty($file['user']['timestamps']['period-end']) ? null : ($encrypted == true ? $file['user']['timestamps']['period-end'] : endecrypt($file['user']['timestamps']['period-end'])))
				));


			if(!empty($file['accounts'])) {
				foreach($file['accounts'] AS $account) {
					sql("INSERT INTO item_accounts(
							 id_user,
							 data_name,
							 is_default
						 )

						 VALUES(
							 :_iduser,
							 :_name,
							 :_isdefault
						 )
						", Array(
							'_iduser' => (int)$user['id'],
							'_name' => ($encrypted == true ? $account['name'] : endecrypt($account['name'])),
							'_isdefault' => ($account['is_default'] == false ? null : 1),
						), 'insert');
				}
			}


			if(!empty($file['items'])) {
				foreach($file['items'] AS $item) {
					if(!empty($item['account'])) {
						$account =
						sql("SELECT id
							 FROM item_accounts
							 WHERE data_name = :_name
							 AND id_user = :_iduser
							", Array(
								'_name' => ($encrypted == true ? $item['account'] : endecrypt($item['account'])),
								'_iduser' => (int)$user['id']
							), 'fetch');
					}
	
					sql("INSERT INTO items(
							 id_user,
							 id_account,
							 data_name,
							 data_sum,
							 data_sum_permonth,
							 data_type,
							 data_category,
							 data_currency,
							 data_number_payment,
							 data_number_ocr,
							 data_number_phone,
							 data_recurrence,
							 data_recurrence_type,
							 data_notes,
							 check_expense_payed,
							 check_expense_extra,
							 check_expense_subscription,
							 check_expense_recurrent,
							 check_expense_correctinfo,
							 check_debt_company,
							 check_debt_paying,
							 check_loan_out,
							 is_expense,
							 is_debt,
							 is_loan,
							 timestamp_date,
							 timestamp_date_end,
							 timestamp_payed,
							 timestamp_added
						 )

						 VALUES(
							 :_iduser,
							 :_idaccount,
							 :_name,
							 :_sum,
							 :_sum_permonth,
							 :_type,
							 :_category,
							 :_currency,
							 :_number_payment,
							 :_number_ocr,
							 :_number_phone,
							 :_recurrence,
							 :_recurrence_type,
							 :_notes,
							 :_expense_payed,
							 :_expense_extra,
							 :_expense_subscription,
							 :_expense_recurrent,
							 :_expense_correctinfo,
							 :_debt_company,
							 :_debt_paying,
							 :_loan_out,
							 :_isexpense,
							 :_isdebt,
							 :_isloan,
							 :_date,
							 :_date_end,
							 :_payed,
							 :_added
						 )
						", Array(
							'_iduser' => (int)$user['id'],
							'_idaccount' => (empty($item['account']) ? null : (int)$account['id']),
							'_name' => ($encrypted == true ? $item['name'] : endecrypt($item['name'])),
							'_sum' => ($encrypted == true ? $item['sums']['sum'] : endecrypt($item['sums']['sum'])),
							'_sum_permonth' => (empty($item['sums']['permonth']) ? null : ($encrypted == true ? $item['sums']['permonth'] : endecrypt($item['sums']['permonth']))),
							'_type' => (empty($item['type']) ? null : ($encrypted == true ? $item['type'] : endecrypt($item['type']))),
							'_category' => (empty($item['category']) ? null : ($encrypted == true ? $item['category'] : endecrypt($item['category']))),
							'_currency' => (empty($item['currency']) ? null : ($encrypted == true ? $item['currency'] : endecrypt($item['currency']))),
							'_number_payment' => (empty($item['number-payment']) ? null : ($encrypted == true ? $item['number-payment'] : endecrypt($item['number-payment']))),
							'_number_ocr' => (empty($item['number-ocr']) ? null : ($encrypted == true ? $item['number-ocr'] : endecrypt($item['number-ocr']))),
							'_number_phone' => (empty($item['number-phone']) ? null : ($encrypted == true ? $item['number-phone'] : endecrypt($item['number-phone']))),
							'_recurrence' => (empty($item['recurrence']) ? null : ($encrypted == true ? $item['recurrence'] : endecrypt($item['recurrence']))),
							'_recurrence_type' => (empty($item['recurrence-type']) ? null : ($encrypted == true ? $item['recurrence-type'] : endecrypt($item['recurrence-type']))),
							'_notes' => (empty($item['notes']) ? null : ($encrypted == true ? $item['notes'] : endecrypt($item['notes']))),
							'_expense_payed' => (empty($item['checkboxes']['expense-payed']) ? null : $item['checkboxes']['expense-payed']),
							'_expense_extra' => (empty($item['checkboxes']['expense-extra']) ? null : $item['checkboxes']['expense-extra']),
							'_expense_subscription' => (empty($item['checkboxes']['expense-subscription']) ? null : $item['checkboxes']['expense-subscription']),
							'_expense_recurrent' => (empty($item['checkboxes']['expense-recurrent']) ? null : $item['checkboxes']['expense-recurrent']),
							'_expense_correctinfo' => (empty($item['checkboxes']['expense-correctinfo']) ? null : $item['checkboxes']['expense-correctinfo']),
							'_debt_company' => (empty($item['checkboxes']['debt-company']) ? null : $item['checkboxes']['debt-company']),
							'_debt_paying' => (empty($item['checkboxes']['debt-paying']) ? null : $item['checkboxes']['debt-paying']),
							'_loan_out' => (empty($item['checkboxes']['loan-out']) ? null : $item['checkboxes']['loan-out']),
							'_isexpense' => (empty($item['is-expense']) ? null : $item['is-expense']),
							'_isdebt' => (empty($item['is-debt']) ? null : $item['is-debt']),
							'_isloan' => (empty($item['is-loan']) ? null : $item['is-loan']),
							'_date' => (empty($item['timestamps']['date']) ? null : $item['timestamps']['date']),
							'_date_end' => (empty($item['timestamps']['date-end']) ? null : ($encrypted == true ? $item['timestamps']['date-end'] : endecrypt($item['timestamps']['date-end']))),
							'_payed' => (empty($item['timestamps']['payed']) ? null : ($encrypted == true ? $item['timestamps']['payed'] : endecrypt($item['timestamps']['payed']))),
							'_added' => endecrypt(time())
						), 'insert');
				}
			}


			if(!empty($file['budget'])) {
				foreach($file['budget'] AS $budget) {
					sql("INSERT INTO budget(
							 id_user,
							 data_name,
							 data_sum_current,
							 data_sum_goal,
							 data_notes,
							 timestamp_created
						 )

						 VALUES(
							 :_iduser,
							 :_name,
							 :_sum_current,
							 :_sum_goal,
							 :_notes,
							 :_created
						 )
						", Array(
							'_iduser' => (int)$user['id'],
							'_name' => ($encrypted == true ? $budget['name'] : endecrypt($budget['name'])),
							'_sum_current' => ($encrypted == true ? $budget['sums']['current'] : endecrypt($budget['sums']['current'])),
							'_sum_goal' => ($encrypted == true ? $budget['sums']['goal'] : endecrypt($budget['sums']['goal'])),
							'_notes' => ($encrypted == true ? $budget['notes'] : endecrypt($budget['notes'])),
							'_created' => $budget['created']
						), 'insert');
				}
			}
		}



		echo 'success';

		log_action(
			'imported settings',
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			1
		);







	} else {
		echo 'unknown';
	}

?>