<?php

	require_once '../site-settings.php';



	$get_iditem = safetag($_GET['idi']);
	$get_idshare = safetag($_GET['ids']);

	$share =
	sql("SELECT *, s.id AS id_share, s.data_sum AS sum_share, i.id AS id_item
		 FROM shares s
		 JOIN items i
		 ON i.id = s.id_item
		 WHERE s.id = :_idshare
		 AND s.id_user = :_iduser
		", Array(
			'_idshare' => (int)$get_idshare,
			'_iduser' => (int)$user['id']
		), 'fetch');

	$usershare =
	sql("SELECT data_username
		 FROM users
		 WHERE id = :_iduser
		", Array(
			'_iduser' => (int)$share['id_user_with']
		), 'fetch');



	if(!empty($share['is_expense'])) {
		$itemtype = 'expense';
	} elseif(!empty($share['is_debt'])) {
		$itemtype = 'debt';
	} elseif(!empty($share['is_loan'])) {
		$itemtype = 'loan';
	}



	echo '<div class="content">';
		foreach($lang['shortinfo']['edit-shared-item'] AS $content) {
			echo $Parsedown->text($content);
		}
	echo '</div>';

	echo '<div class="msg share-edit"></div>';

	echo '<form action="javascript:void(0)" method="POST" id="share-edit" autocomplete="off" novalidate>';
		echo '<input type="hidden" name="hidden-idshare" value="'.(int)$share['id_share'].'">';
		echo '<input type="hidden" name="hidden-iditem" value="'.(int)$share['id_item'].'">';
		echo '<input type="hidden" name="hidden-editing" value="y">';

		echo '<div class="item">';
			echo '<div class="label">';
				echo $lang['words']['username'];
			echo '</div>';

			echo '<div class="field">';
				echo '<div class="icon">'.svgicon('user').'</div>';
				echo '<input type="text" name="field-username" aria-label="username" value="';
				echo endecrypt($usershare['data_username'], false, true);
				echo '" readonly>';
			echo '</div>';
		echo '</div>';

		echo '<div class="item">';
			echo '<div class="label">';
				echo $lang['words']['recipient-pays'];
			echo '</div>';

			echo '<div class="field">';
				echo '<div class="icon">'.svgicon('money').'</div>';
				echo '<input type="text" inputmode="numeric" pattern="[0-9]{6}" name="field-sum" aria-label="sum"';
				echo (empty($share['sum_share']) ? '' : ' value="'.endecrypt($share['sum_share'], false).'"');
				echo '>';

				echo '<select name="list-payment-type">';
					echo '<option value="sum"'.(endecrypt($share['data_payment'], false) == 'sum' ? ' selected' : '').'>'.$lang['words']['sums']['amount'].'</option>';
					echo '<option value="percent"'.(endecrypt($share['data_payment'], false) == 'percent' ? ' selected' : '').'>'.$lang['words']['sums']['percentage'].'</option>';
				echo '</select>';
			echo '</div>';
		echo '</div>';


		echo '<div class="checkboxes">';
			echo checkbox($lang['forms']['checkboxes']['share']['allow-deletion'], 'allow-deletion', null, (empty($share['allow_deletion']) ? false : true));
			echo checkbox($lang['forms']['checkboxes']['share']['allow-markas-payed'], 'allow-markas-payed', null, (empty($share['allow_markas_payed']) ? false : true), ($itemtype == 'expense' ? false : true));

			echo '<div class="space"></div>';

			echo checkbox($lang['forms']['checkboxes']['share']['number-payment'], 'share-number-payment', null, (empty($share['share_number_payment']) ? false : true), ($itemtype == 'expense' ? false : true));
			echo checkbox($lang['forms']['checkboxes']['share']['number-ocr'], 'share-number-ocr', null, (empty($share['share_number_ocr']) ? false : true), ($itemtype == 'expense' ? false : true));
			echo checkbox($lang['forms']['checkboxes']['share']['number-phone'], 'share-number-phone', null, (empty($share['share_number_phone']) ? false : true), false);
			echo checkbox($lang['forms']['checkboxes']['share']['qrcodes'], 'share-qrcodes', $lang['forms']['checkboxes']['descriptions']['share-qrcodes'], (empty($share['share_qrcodes']) ? false : true), (($itemtype == 'expense' AND !empty($share['share_number_payment']) AND !empty($share['share_number_ocr'])) ? false : true));
			echo checkbox($lang['forms']['checkboxes']['share']['notes'], 'share-notes', null, (empty($share['share_notes']) ? false : true));
		echo '</div>';


		echo '<div class="button">';
			echo '<input type="submit" name="button-save-share" value="'.$lang['words']['buttons']['save'].'">';

			echo '<a href="javascript:void(0)" class="cancel-editshare">';
				echo $lang['words']['cancel'];
			echo '</a>';
		echo '</div>';
	echo '</form>';

?>