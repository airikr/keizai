<?php

	require_once '../site-settings.php';



	$hidden_idaccount = safetag($_POST['hidden-idaccount']);
	$post_account_name = (empty($_POST['field-account-name']) ? null : safetag($_POST['field-account-name']));
	$check_default = (isset($_POST['check-default']) ? true : null);



	if(empty($post_account_name)) {
		echo 'fields-empty';


	} else {

		$count_accounts =
		sql("SELECT COUNT(id_user)
			 FROM item_accounts
			 WHERE id != :_idaccount
			 AND id_user = :_iduser
			 AND data_name = :_name
			", Array(
				'_idaccount' => (int)$hidden_idaccount,
				'_iduser' => (int)$user['id'],
				'_name' => endecrypt(trim($post_account_name))
			), 'count');

		$accountinfo =
		sql("SELECT data_name
			 FROM item_accounts
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'fetch');



		if($count_accounts != 0) {
			echo 'account-exists';

		} else {
			echo 'success';

			sql("UPDATE item_accounts
				 SET is_default = NULL
				 WHERE id_user = :_iduser
				", Array(
					'_iduser' => (int)$user['id']
				));

			sql("UPDATE item_accounts
				 SET data_name = :_name,
					 is_default = :_isdefault

				 WHERE id = :_idaccount
				 AND id_user = :_iduser
				", Array(
					'_idaccount' => (int)$hidden_idaccount,
					'_iduser' => (int)$user['id'],
					'_name' => endecrypt(trim($post_account_name)),
					'_isdefault' => $check_default
				));

			log_action(
				'made some changes to an account for expenses'
			);
		}

	}

?>