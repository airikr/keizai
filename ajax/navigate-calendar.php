<?php

	require_once '../site-settings.php';

	$factory = new CalendR\Calendar;

	$get_date = (isset($_GET['dat']) ? safetag($_GET['dat']) : null);
	$get_year = (isset($_GET['yea']) ? safetag($_GET['yea']) : null);
	$get_month = (isset($_GET['mon']) ? safetag($_GET['mon']) : null);
	$get_viewmonth = (isset($_GET['vie']) ? safetag($_GET['vie']) : null);

	$month = ((empty($get_month) AND empty($get_viewmonth)) ? $factory->getMonth(date('Y', strtotime($get_date)), date('n', strtotime($get_date))) : null);
	$year = (((empty($get_date) AND !empty($get_month)) OR (empty($get_date) AND !empty($get_viewmonth))) ? $factory->getYear(date('Y')) : null);

	$c_expenses = 0;
	$c_debts = 0;
	$c_loans = 0;



	if(empty($get_date) AND !empty($get_month) OR empty($get_date) AND !empty($get_viewmonth)) {
		echo '<div class="dim"></div>';

		echo '<div class="icons side-by-side">';
			whatisthis('overview-calendar');
			echo screenshot('.calendar', $lang['screenshots']['overview-calendar']);
		echo '</div>';

		echo '<div class="top">';
			echo '<div class="left">';
				/*echo '<a href="javascript:void(0)" class="nav left" data-year="'.($year->format('Y') - 1).'">';
					echo svgicon('arrow-left');
				echo '</a>';

				echo '<div class="nav left wait">';
					echo svgicon('wait');
				echo '</div>';*/
			echo '</div>';

			echo '<div class="middle">';
				echo '<a href="'.url('filter/year:'.$year->format('Y')).'" data-html2canvas-ignore>'.svgicon('eye-opened').'</a>';
				echo $year->format('Y');
			echo '</div>';

			echo '<div class="right">';
				/*echo '<a href="javascript:void(0)" class="nav right" data-year="'.($year->format('Y') + 1).'">';
					echo svgicon('arrow-right');
				echo '</a>';

				echo '<div class="nav right wait">';
					echo svgicon('wait');
				echo '</div>';*/
			echo '</div>';
		echo '</div>';

		echo '<div class="months">';
			for($months = 1; $months < 13; $months++) {
				$date_month_minus_1week = $year->format('Y').'-'.($months < 10 ? '0' : '') . $months.'-'.date('d', strtotime('-1 week'));

				$has_expenses =
				sql("SELECT COUNT(timestamp_date)
					 FROM items
					 WHERE id_user = :_iduser
					 AND YEAR(FROM_UNIXTIME(timestamp_date)) = :_year
					 AND MONTH(FROM_UNIXTIME(timestamp_date)) = :_month
					 AND is_expense IS NOT NULL
					", Array(
						'_iduser' => (int)$user['id'],
						'_year' => $year->format('Y'),
						'_month' => ($months < 10 ? '0' : '') . $months
					), 'count');

				$has_debts =
				sql("SELECT COUNT(timestamp_date)
					 FROM items
					 WHERE id_user = :_iduser
					 AND YEAR(FROM_UNIXTIME(timestamp_date)) = :_year
					 AND MONTH(FROM_UNIXTIME(timestamp_date)) = :_month
					 AND is_debt IS NOT NULL
					", Array(
						'_iduser' => (int)$user['id'],
						'_year' => $year->format('Y'),
						'_month' => ($months < 10 ? '0' : '') . $months
					), 'count');

				$has_loans =
				sql("SELECT COUNT(timestamp_date)
					 FROM items
					 WHERE id_user = :_iduser
					 AND YEAR(FROM_UNIXTIME(timestamp_date)) = :_year
					 AND MONTH(FROM_UNIXTIME(timestamp_date)) = :_month
					 AND is_loan IS NOT NULL
					", Array(
						'_iduser' => (int)$user['id'],
						'_year' => $year->format('Y'),
						'_month' => ($months < 10 ? '0' : '') . $months
					), 'count');

				$c_expenses += $has_expenses;
				$c_debts += $has_debts;
				$c_loans += $has_loans;



				echo '<div>';
					echo '<div>';
						echo '<a href="'.url('filter/year:'.$year->format('Y').'/month:'.$months).'" data-html2canvas-ignore>'.svgicon('eye-opened').'</a>';
						echo '<a href="javascript:void(0)" class="month '.$date_month_minus_1week;
						echo '" data-date="'.$date_month_minus_1week.'">';
							echo $lang['months'][strtolower(date('F', strtotime($date_month_minus_1week)))];
						echo '</a>';

						echo '<div class="month '.$date_month_minus_1week.' inactive">';
							echo $lang['months'][strtolower(date('F', strtotime($date_month_minus_1week)))];
						echo '</div>';

						echo '<div class="month '.$date_month_minus_1week.' wait">';
							echo svgicon('wait');
						echo '</div>';
					echo '</div>';

					echo '<div class="dots">';
						echo ($has_expenses == 0 ? '<div class="dot expenses inactive"></div>' : '<div class="dot expenses"></div>');
						echo ($has_debts == 0 ? '<div class="dot debts inactive"></div>' : '<div class="dot debts"></div>');
						echo ($has_loans == 0 ? '<div class="dot loans inactive"></div>' : '<div class="dot loans"></div>');
					echo '</div>';
				echo '</div>';
			}
		echo '</div>';





	} else {
		echo '<div class="dim"></div>';

		echo '<div class="icons side-by-side">';
			whatisthis('overview-calendar');
			echo screenshot('.calendar', $lang['screenshots']['overview-calendar']);
		echo '</div>';

		echo '<div class="top">';
			echo '<div class="left">';
				echo '<a href="javascript:void(0)" class="nav left" data-date="'.date('Y-m-d', strtotime('-1 month '.$month->format('Y-m-d'))).'">';
					echo svgicon('arrow-left');
				echo '</a>';

				echo '<div class="nav left wait">';
					echo svgicon('wait');
				echo '</div>';
			echo '</div>';

			echo '<div class="middle">';
				echo '<a href="javascript:void(0)" class="view-month '.$month->format('n').'" data-month="'.$month->format('n').'">';
					echo $lang['months'][mb_strtolower($month->format('F'))].' '.$month->format('Y');
				echo '</a>';

				echo '<div class="wait '.$month->format('n').'">';
					echo svgicon('wait');
				echo '</div>';
			echo '</div>';

			echo '<div class="right">';
				echo '<a href="javascript:void(0)" class="nav right" data-date="'.date('Y-m-d', strtotime('+1 month '.$month->format('Y-m-d'))).'">';
					echo svgicon('arrow-right');
				echo '</a>';

				echo '<div class="nav right wait">';
					echo svgicon('wait');
				echo '</div>';
			echo '</div>';
		echo '</div>';

		echo '<table cellspacing="0">';
			echo '<thead>';
				echo '<tr>';
					echo '<th>'.$lang['words']['week-short'].'</th>';
					foreach($lang['days-short'] AS $day) {
						echo '<th>'.$day.'</th>';
					}
				echo '</tr>';
			echo '</thead>';


			foreach($month AS $week) {
				echo '<tbody>';
					echo '<tr>';
						echo '<td class="week">'.$week->format('W').'</td>';

						foreach($week AS $day) {
							$has_expenses =
							sql("SELECT COUNT(timestamp_date)
								 FROM items
								 WHERE id_user = :_iduser
								 AND timestamp_date = :_date
								 AND is_expense IS NOT NULL
								", Array(
									'_iduser' => (int)$user['id'],
									'_date' => strtotime($day->format('Y-m-d'))
								), 'count');

							$has_debts =
							sql("SELECT COUNT(timestamp_date)
								 FROM items
								 WHERE id_user = :_iduser
								 AND timestamp_date = :_date
								 AND is_debt IS NOT NULL
								", Array(
									'_iduser' => (int)$user['id'],
									'_date' => strtotime($day->format('Y-m-d'))
								), 'count');

							$has_loans =
							sql("SELECT COUNT(timestamp_date)
								 FROM items
								 WHERE id_user = :_iduser
								 AND timestamp_date = :_date
								 AND is_loan IS NOT NULL
								", Array(
									'_iduser' => (int)$user['id'],
									'_date' => strtotime($day->format('Y-m-d'))
								), 'count');

							if($has_expenses != 0 AND $day->format('Y-m') == date('Y-m')) { $c_expenses += $has_expenses; }
							if($has_debts != 0 AND $day->format('Y-m') == date('Y-m')) { $c_debts += $has_debts; }
							if($has_loans != 0 AND $day->format('Y-m') == date('Y-m')) { $c_loans += $has_loans; }


							echo '<td'.(!$month->includes($day) ? ' class="out-of-month"' : '').'>';
								echo '<div class="digit';

								if($period_start_empty == false AND $period_end_empty == false AND empty($user['data_incomeday'])) {
									foreach(listalldates(date('Y-m-d', $period_start), date('Y-m-d', $period_end)) AS $date) {
										echo ($date->format('Y-m-d') == $day->format('Y-m-d') ? ' active' : '');
									}

								} elseif(empty($user['timestamp_period_start']) AND empty($user['timestamp_period_end']) AND $period_incomeday_empty == false) {
									foreach(listalldates(date('Y-m-d', $period_incomeday), date('Y-m-d', $period_incomeday_nextmonth_1)) AS $date) {
										echo ($date->format('Y-m-d') == $day->format('Y-m-d') ? ' active' : '');
									}
								}

								echo '">';
									echo '<a href="'.url('filter/year:'.$day->format('Y').'/month:'.$day->format('n').'/day:'.$day->format('j')).'">';
										echo $day->format('j');
									echo '</a>';
								echo '</div>';

								echo ($day->format('Y-m-d') == date('Y-m-d') ? '<div class="today"></div>' : '');

								echo '<div class="dots">';
									echo '<div class="dot expenses'.($has_expenses == 0 ? ' inactive' : '').'"></div>';
									echo '<div class="dot debts'.($has_debts == 0 ? ' inactive' : '').'"></div>';
									echo '<div class="dot loans'.($has_loans == 0 ? ' inactive' : '').'"></div>';
								echo '</div>';
							echo '</td>';
						}
					echo '</tr>';
				echo '</tbody>';
			}
		echo '</table>';
	}



	echo '<div class="statistics">';
		echo '<div><div><div class="dot expenses"></div>'.$lang['words']['expenses'].'</div><div'.(empty($c_expenses) ? ' class="empty"' : '').'>'.(empty($c_expenses) ? 0 : $c_expenses).'</div></div>';
		echo '<div><div><div class="dot debts"></div>'.$lang['words']['debts'].'</div><div'.(empty($c_debts) ? ' class="empty"' : '').'>'.(empty($c_debts) ? 0 : $c_debts).'</div></div>';
		echo '<div><div><div class="dot loans"></div>'.$lang['words']['loans'].'</div><div'.(empty($c_loans) ? ' class="empty"' : '').'>'.(empty($c_loans) ? 0 : $c_loans).'</div></div>';
		echo '<div><div class="total">'.$lang['words']['total'].'</div><div'.((empty($c_expenses + $c_debts + $c_loans)) ? ' class="empty"' : '').'>'.($c_expenses + $c_debts + $c_loans).'</div></div>';
	echo '</div>';

?>