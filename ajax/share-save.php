<?php

	require_once '../site-settings.php';



	$hidden_iditem = safetag($_POST['hidden-iditem']);
	$hidden_idshare = safetag($_POST['hidden-idshare']);
	$hidden_editing = safetag($_POST['hidden-editing']);

	$post_username = safetag($_POST['field-username']);
	$post_sum = safetag($_POST['field-sum']);
	$post_payment_type = safetag($_POST['list-payment-type']);

	$check_allow_deletion = (isset($_POST['check-allow-deletion']) ? true : null);
	$check_allow_markas_payed = (isset($_POST['check-allow-markas-payed']) ? true : null);
	$check_number_payment = (isset($_POST['check-share-number-payment']) ? true : null);
	$check_number_ocr = (isset($_POST['check-share-number-ocr']) ? true : null);
	$check_number_phone = (isset($_POST['check-share-number-phone']) ? true : null);
	$check_qrcodes = (isset($_POST['check-share-qrcodes']) ? true : null);
	$check_notes = (isset($_POST['check-share-notes']) ? true : null);



	if($hidden_editing == 'n') {
		$check_existence =
		sql("SELECT COUNT(id)
			 FROM users
			 WHERE data_username = :_username
			", Array(
				'_username' => endecrypt($post_username, true, true)
			), 'count');
	
		$check_with =
		sql("SELECT COUNT(s.id_user_with)
			 FROM shares s
			 JOIN users u
			 ON s.id_user_with = u.id
			 WHERE s.id_item = :_iditem
			 AND u.data_username = :_username
			", Array(
				'_iditem' => (int)$hidden_iditem,
				'_username' => endecrypt($post_username, true, true)
			), 'count');

		$userinfo =
		sql("SELECT check_option_share_allow
			 FROM users
			 WHERE data_username = :_username
			", Array(
				'_username' => endecrypt($post_username, true, true)
			), 'fetch');
	}

	$iteminfo =
	sql("SELECT id, data_sum
		 FROM items
		 WHERE id = :_iditem
		 AND id_user = :_iduser
		", Array(
			'_iditem' => (int)$hidden_iditem,
			'_iduser' => (int)$user['id']
		), 'fetch');



	if($hidden_editing == 'n' AND $check_existence == 0) {
		echo 'user-not-exists';

	} elseif($hidden_editing == 'n' AND $check_with != 0) {
		echo 'already-shared';

	} elseif($hidden_editing == 'n' AND $post_username == endecrypt($user['data_username'], false, true)) {
		echo 'cant-share-to-self';

	} elseif($hidden_editing == 'n' AND empty($userinfo['check_option_share_allow'])) {
		echo 'privacy-sharing-notallowed';

	} elseif(!empty($post_sum) AND $post_payment_type == 'sum' AND $post_sum > endecrypt($iteminfo['data_sum'], false)) {
		echo 'sum-aboveactual';

	} elseif(!empty($post_sum) AND $post_payment_type == 'sum' AND $post_sum < 5) {
		echo 'sum-below5';

	} elseif(!empty($post_sum) AND $post_payment_type == 'percent' AND is_numeric($post_sum) > 100) {
		echo 'sum-percent-above100';

	} elseif(!empty($post_sum) AND $post_payment_type == 'percent' AND is_numeric($post_sum) < 1) {
		echo 'sum-percent-below1';


	} else {
		sql("UPDATE shares
			 SET data_sum = :_sum,
				 data_payment = :_payment,
				 allow_deletion = :_deletion,
				 allow_markas_payed = :_markas_payed,
				 share_number_payment = :_number_payment,
				 share_number_ocr = :_number_ocr,
				 share_number_phone = :_number_phone,
				 share_qrcodes = :_qrcodes,
				 share_notes = :_notes,
				 timestamp_edited = :_edited

			 WHERE id = :_idshare
			 AND id_user = :_iduser
			", Array(
				'_idshare' => (int)$hidden_idshare,
				'_iduser' => (int)$user['id'],
				'_sum' => (empty($post_sum) ? null : endecrypt($post_sum)),
				'_payment' => (empty($post_sum) ? null : endecrypt($post_payment_type)),
				'_deletion' => $check_allow_deletion,
				'_markas_payed' => $check_allow_markas_payed,
				'_number_payment' => $check_number_payment,
				'_number_ocr' => $check_number_ocr,
				'_number_phone' => $check_number_phone,
				'_qrcodes' => $check_qrcodes,
				'_notes' => $check_notes,
				'_edited' => time()
			));
	}

?>