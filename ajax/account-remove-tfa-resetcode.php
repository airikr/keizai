<?php

	require_once '../site-settings.php';



	$post_username = safetag($_POST['field-username']);
	$post_resetcode = safetag($_POST['field-resetcode']);


	if(empty($post_resetcode)) {
		echo 'fields-empty';


	} else {

		$check_user =
		sql("SELECT COUNT(data_username)
			 FROM users
			 WHERE data_username = :_username
			", Array(
				'_username' => endecrypt($post_username, true, true)
			), 'count');


		if($check_user == 0) {
			echo 'user-notexists';

		} elseif(strlen($post_resetcode) != 20) {
			echo 'resetcode-length';

		} else {
			$userinfo =
			sql("SELECT id, data_tfa_backup
				 FROM users
				 WHERE data_username = :_username
				", Array(
					'_username' => endecrypt($post_username, true, true)
				), 'fetch');


			if(!password_verify($post_resetcode, $userinfo['data_tfa_backup'])) {
				echo 'resetcode-incorrect';

			} else {
				echo 'success';

				sql("UPDATE users
					 SET data_tfa = NULL,
						 data_tfa_backup = NULL

					 WHERE id = :_iduser
					", Array(
						'_iduser' => (int)$userinfo['id']
					));
			}
		}

	}

?>