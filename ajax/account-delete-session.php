<?php

	require_once '../site-settings.php';



	$get_idsession = safetag($_GET['ids']);

	sql("DELETE FROM sess
		 WHERE id = :_idsession
		 AND id_user = :_iduser
		", Array(
			'_idsession' => (int)$get_idsession,
			'_iduser' => (int)$user['id']
		));

	log_action('deleted session');



	$count_sessions =
	sql("SELECT COUNT(id_user)
		 FROM sess
		 WHERE id_user = :_iduser
		", Array(
			'_iduser' => (int)$user['id']
		), 'count');

	echo $count_sessions;

?>