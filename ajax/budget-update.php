<?php

	require_once '../site-settings.php';

	use NXP\MathExecutor;

	$math = new MathExecutor();



	$hidden_idbudget = safetag($_POST['hidden-idbudget']);

	$post_sum = safetag($_POST['field-sum']);
	$post_option = explode('-', $_POST['option']);



	if(empty($post_sum)) {
		echo 'fields-empty';


	} else {
		$current_sum =
		sql("SELECT data_sum_current, data_sum_goal
			 FROM budget
			 WHERE id = :_idbudget
			 AND id_user = :_iduser
			", Array(
				'_idbudget' => (int)$hidden_idbudget,
				'_iduser' => (int)$user['id']
			), 'fetch');

		$current = (float)endecrypt($current_sum['data_sum_current'], false);

		if($post_option[0] == 'in') {
			$sum = $current + str_replace(',', '.', $post_sum);
		} elseif($post_option[0] == 'out') {
			$sum = $current - str_replace(',', '.', $post_sum);
		}


		if(!is_numeric((float)$post_sum)) {
			echo 'onlydigits-sum';

		} elseif($post_option[0] == 'out' AND (float)trim($math->execute($current - str_replace(',', '.', $post_sum))) < 0) {
			echo 'sum-belowzero';

		} elseif($post_option[0] == 'in' AND (float)trim($math->execute($current + str_replace(',', '.', $post_sum))) > endecrypt($current_sum['data_sum_goal'], false)) {
			echo 'sum-aboveonehundred';


		} else {
			sql("UPDATE budget
				 SET data_sum_current = :_sum_current
				 WHERE id = :_idbudget
				 AND id_user = :_iduser
				", Array(
					'_idbudget' => (int)$hidden_idbudget,
					'_iduser' => (int)$user['id'],
					'_sum_current' => endecrypt((float)trim($math->execute($sum)))
				));



			$budgetinfo =
			sql("SELECT *
				 FROM budget
				 WHERE id = :_idbudget
				 AND id_user = :_iduser
				", Array(
					'_idbudget' => (int)$hidden_idbudget,
					'_iduser' => (int)$user['id']
				), 'fetch');

			$percentage = number_format((endecrypt($budgetinfo['data_sum_current'], false) * 100) / endecrypt($budgetinfo['data_sum_goal'], false), 0, '', '');

			echo format_number(endecrypt($budgetinfo['data_sum_current'], false));
			echo '|';
			echo $percentage;
			echo '|';
			echo (endecrypt($budgetinfo['data_sum_current'], false) == endecrypt($budgetinfo['data_sum_goal'], false) ? 'done' : '');

			log_action('updated budget');
		}

	}

?>