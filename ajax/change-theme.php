<?php

	require_once '../site-settings.php';

	$get_theme = safetag($_GET['the']);



	sql("UPDATE users
		 SET data_theme = :_theme
		 WHERE id = :_iduser
		", Array(
			'_iduser' => (int)$user['id'],
			'_theme' => ($get_theme == 'light' ? 0 : 1)
		));

?>