<?php

	require_once '../site-settings.php';



	$get_element = safetag($_GET['ele']);

	if(!isset($lang['what-is-this'][$get_element]) OR empty($lang['what-is-this'][$get_element])) {
		echo '<div class="message">';
			echo $lang['messages']['no-explaination'];
		echo '</div>';

	} else {
		foreach($lang['what-is-this'][$get_element] AS $content) {
			echo $Parsedown->text($content);
		}
	}

?>