<?php

	require_once '../site-settings.php';



	$get_markas = safetag($_GET['mar']);
	$get_iditem = safetag($_GET['idi']);

	$check_sharestatus =
	sql("SELECT COUNT(i.id_user)
		 FROM items i
		 JOIN shares s
		 ON i.id = s.id_item
		 WHERE s.id_user = :_iduser
		 AND i.is_expense IS NOT NULL
		 OR s.id_user_with = :_iduser
		 AND i.is_expense IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		), 'count');

	if($check_sharestatus != 0) {
		$usershared =
		sql("SELECT u.id, u.data_saltedstring_1, u.data_saltedstring_2, u.data_currency
			 FROM users u
			 JOIN items i
			 ON u.id = i.id_user
			 WHERE i.id = :_iditem
			", Array(
				'_iditem' => (int)$get_iditem
			), 'fetch');

		$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($usershared['data_saltedstring_1'] . (int)$usershared['id'] . $usershared['data_saltedstring_2']).'.json'), true);
	}



	sql("UPDATE items
		 SET timestamp_payed = :_payed,
			 check_expense_payed = :_markas_payed

		 WHERE id = :_iditem
		", Array(
			'_iditem' => (int)$get_iditem,
			'_payed' => ($get_markas == 'payed' ? endecrypt(strtotime(date('Y-m-d'))) : null),
			'_markas_payed' => ($get_markas == 'payed' ? 1 : null)
		));

?>