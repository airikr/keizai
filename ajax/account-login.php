<?php

	require_once '../site-settings.php';

	use PragmaRX\Google2FA\Google2FA;



	$post_username = safetag($_POST['field-username']);
	$post_password = safetag($_POST['field-password']);
	$post_tfa = safetag($_POST['field-tfa']);

	$tfa = new Google2FA();



	$check_user =
	sql("SELECT COUNT(data_username)
		 FROM users
		 WHERE data_username = :_username
		", Array(
			'_username' => endecrypt($post_username, true, true)
		), 'count');

	if($check_user != 0) {
		$userinfo =
		sql("SELECT id, data_password, data_tfa, data_saltedstring_1, data_saltedstring_2, is_upgraded
			 FROM users
			 WHERE data_username = :_username
			", Array(
				'_username' => endecrypt($post_username, true, true)
			), 'fetch');

		$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($userinfo['data_saltedstring_1'] . (int)$userinfo['id'] . $userinfo['data_saltedstring_2']).'.json'), true);
	}



	if(empty($post_username) OR empty($post_password)) {
		echo 'fields-empty';


	} else {
		if($check_user == 0) {
			echo 'user-notfound';

		} else {
			$count_attempts =
			sql("SELECT COUNT(id_user)
				 FROM loginattempts
				 WHERE id_user = :_iduser
				", Array(
					'_iduser' => (int)$userinfo['id']
				), 'count');


			if($count_attempts == 3) {
				echo 'too-many-attempts';

			} elseif(!password_verify($post_password, $userinfo['data_password'])) {
				echo 'password-incorrect';

				sql("INSERT INTO loginattempts(id_user, timestamp_attempted) VALUES(:_iduser, :_attempted)", Array(
					'_iduser' => (int)$userinfo['id'],
					'_attempted' => time()
				), 'insert');

			} elseif(!empty($userinfo['data_tfa']) AND empty($post_tfa)) {
				echo 'tfa-enabled';

			} elseif(!empty($userinfo['data_tfa']) AND !is_numeric($post_tfa)) {
				echo 'tfa-nodigits';

			} elseif(!empty($userinfo['data_tfa']) AND !empty($post_tfa) AND !$tfa->verifyKey($userinfo['data_tfa'], $post_tfa)) {
				echo 'tfa-incorrect';

				sql("INSERT INTO loginattempts(id_user, timestamp_attempted) VALUES(:_iduser, :_attempted)", Array(
					'_iduser' => (int)$userinfo['id'],
					'_attempted' => time()
				), 'insert');



			} else {
				if(empty($userinfo['is_upgraded'])) {
					echo 'need-to-upgrade';
				} else {
					echo 'success';
				}

				#session_set_cookie_params((10 * 60), '/', $site_domain, true, false);
				#session_regenerate_id(true);

				$_SESSION['loggedin'] = (int)$userinfo['id'];

				sql("UPDATE users
					 SET timestamp_lastactive = :_lastactive
					 WHERE id = :_userid
					", Array(
						'_userid' => (int)$userinfo['id'],
						'_lastactive' => time()
					));


				$check_session =
				sql("SELECT COUNT(data_ipaddress)
					 FROM sess
					 WHERE data_ipaddress = :_ipaddress
					 AND data_useragent = :_useragent
					", Array(
						'_ipaddress' => endecrypt(getip()),
						'_useragent' => endecrypt($useragent)
					), 'count');


				if($check_session == 0) {
					sql("INSERT INTO sess(
							 id_user,
							 data_ipaddress,
							 data_useragent,
							 timestamp_occurred
						 )

						 VALUES(
							 :_iduser,
							 :_ipaddress,
							 :_useragent,
							 :_occurred
						 )
						", Array(
							'_iduser' => (int)$userinfo['id'],
							'_ipaddress' => endecrypt(getip()),
							'_useragent' => endecrypt($useragent),
							'_occurred' => time()
						), 'insert');
				}

				$session =
				sql("SELECT id
					 FROM sess
					 WHERE data_ipaddress = :_ipaddress
					 AND data_useragent = :_useragent
					", Array(
						'_ipaddress' => endecrypt(getip()),
						'_useragent' => endecrypt($useragent)
					), 'fetch');

				sql("DELETE FROM loginattempts
					 WHERE id_user = :_iduser
					", Array(
						'_iduser' => (int)$userinfo['id']
					));


				log_action('signed in', (int)$userinfo['id'], (int)$session['id']);
			}
		}

	}

?>