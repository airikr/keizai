<?php

	require_once '../site-settings.php';

	use NXP\MathExecutor;
	use PragmaRX\Google2FA\Google2FA;

	$math = new MathExecutor();



	$hidden_secret = safetag($_POST['hidden-secret']);
	$post_username = safetag($_POST['field-username']);
	$post_email = (empty($_POST['field-email']) ? null : safetag($_POST['field-email']));
	$post_password_new = (empty($_POST['field-password-new']) ? null : safetag($_POST['field-password-new']));
	$post_password_repeat = (empty($_POST['field-password-repeat']) ? null : safetag($_POST['field-password-repeat']));
	$post_password_current = (empty($_POST['field-password-current']) ? null : safetag($_POST['field-password-current']));
	$post_tfa = ((!isset($_POST['field-tfa']) OR empty($_POST['field-tfa'])) ? null : safetag($_POST['field-tfa']));
	$post_incomeday = (empty($_POST['field-incomeday']) ? null : safetag($_POST['field-incomeday']));
	$post_sum_income = (empty($_POST['field-sum-income']) ? null : safetag($_POST['field-sum-income']));
	$post_sum_balance = (empty($_POST['field-sum-balance']) ? null : safetag($_POST['field-sum-balance']));
	$post_sum_savings = (empty($_POST['field-sum-savings']) ? null : safetag($_POST['field-sum-savings']));
	$post_language = (empty($_POST['list-language']) ? null : safetag($_POST['list-language']));
	$post_currency = (empty($_POST['list-currency']) ? null : safetag($_POST['list-currency']));
	$post_theme = (empty($_POST['list-theme']) ? null : safetag($_POST['list-theme']));
	$post_period_start = (empty($_POST['field-period-start']) ? null : safetag($_POST['field-period-start']));
	$post_period_end = (empty($_POST['field-period-end']) ? null : safetag($_POST['field-period-end']));

	$check_expenses_expires = (isset($_POST['check-expenses-expires']) ? true : null);
	$check_option_hidedate_expenses = (isset($_POST['check-option-hidedate-expenses']) ? true : null);
	$check_option_hidedate_debts = (isset($_POST['check-option-hidedate-debts']) ? true : null);
	$check_option_hidedate_loans = (isset($_POST['check-option-hidedate-loans']) ? true : null);
	$check_option_align_center = (isset($_POST['check-option-align-center']) ? true : null);
	$check_option_share_allow = (isset($_POST['check-option-share-allow']) ? true : null);
	$check_option_allow_weekends = (isset($_POST['check-option-allow-weekends']) ? true : null);

	$tfa = new Google2FA();
	$tfa_backup = bin2hex(random_bytes(10));



	# Commented due to no fix (see line 63)
	/*if(!empty($post_email)) {
		$check_email =
		sql("SELECT COUNT(data_email)
			 FROM users
			 WHERE data_email = :_email
			", Array(
				'_email' => endecrypt($post_email)
			), 'count');
	}*/



	if(empty($post_username) OR
	   (empty($post_password_new) AND !empty($post_password_repeat) AND empty($post_password_current)) OR
	   (empty($post_password_new) AND !empty($post_password_repeat) AND !empty($post_password_current)) OR
	   (!empty($post_password_new) AND empty($post_password_repeat) AND empty($post_password_current)) OR
	   (!empty($post_password_new) AND empty($post_password_repeat) AND !empty($post_password_current)) OR
	   (!empty($post_password_new) AND !empty($post_password_repeat) AND empty($post_password_current)) OR
	   (!empty($post_email) AND endecrypt($post_email) != $user['data_email'] AND empty($post_password_current))) {
		echo 'fields-empty';

	} elseif(!empty($post_email) AND validate('email', $post_email, 'email') == false) {
		echo 'email-notvalid';

	# Commented because of no fix for checking the email against other user's email addresses without decrypt their email address
	#} elseif(!empty($post_email) AND $check_email != 0 AND endecrypt($post_email) != $user['data_email']) {
		#echo 'email-exists';

	} elseif(!empty($post_password_new) AND strlen($post_password_new) < 10 OR !empty($post_password_repeat) AND strlen($post_password_repeat) < 10) {
		echo 'password-tooshort';

	} elseif(!empty($post_password_new) AND $post_password_new != $post_password_repeat) {
		echo 'password-notsame';

	} elseif(!empty($post_email) AND endecrypt($post_email) != $user['data_email'] AND !password_verify($post_password_current, $user['data_password']) OR
			 !empty($post_password_new) AND !empty($post_password_repeat) AND !password_verify($post_password_current, $user['data_password'])) {
		echo 'password-incorrect';

	} elseif(!empty($post_tfa) AND !is_numeric($post_tfa)) {
		echo 'tfa-nodigits';

	} elseif(!empty($post_tfa) AND !$tfa->verifyKey($hidden_secret, $post_tfa)) {
		echo 'tfa-incorrect';

	} elseif(!empty($post_period_start) AND !empty($post_period_end) AND strtotime($post_period_start) > strtotime($post_period_end)) {
		echo 'period-wrongorder';

	} elseif(empty($post_period_start) AND !empty($post_period_end)) {
		echo 'period-missingdate-start';

	} elseif(!empty($post_period_start) AND empty($post_period_end)) {
		echo 'period-missingdate-end';

	} elseif(!empty($post_sum_income) AND $post_sum_income[0] == '-' OR !empty($post_sum_savings) AND $post_sum_savings[0] == '-') {
		echo 'sum-invalid-nonegative';


	} else {
		sql("UPDATE users
			 SET data_username = :_username,
				 data_email = :_email,
				 data_incomeday = :_incomeday,
				 data_sum_income = :_income,
				 data_sum_balance = :_balance,
				 data_sum_savings = :_savings,
				 data_language = :_language,
				 data_currency = :_currency,
				 data_theme = :_theme,
				 check_email_expensesexpires = :_expensesexpires,
				 check_option_hidedate_expenses = :_option_hidedate_expenses,
				 check_option_hidedate_debts = :_option_hidedate_debts,
				 check_option_hidedate_loans = :_option_hidedate_loans,
				 check_option_align_center = :_option_align_center,
				 check_option_share_allow = :_option_share_allow,
				 check_option_allow_weekends = :_option_allow_weekends,
				 timestamp_period_start = :_period_start,
				 timestamp_period_end = :_period_end

			 WHERE id = :_iduser
			", Array(
				'_iduser' => (int)$user['id'],
				'_username' => endecrypt($post_username, true, true),
				'_email' => (empty($post_email) ? null : endecrypt($post_email)),
				'_incomeday' => (empty($post_incomeday) ? null : endecrypt($post_incomeday)),
				'_income' => (empty($post_sum_income) ? null : endecrypt(trim((float)str_replace(',', '.', $math->execute($post_sum_income))))),
				'_balance' => (empty($post_sum_balance) ? null : endecrypt(trim((float)str_replace(',', '.', $math->execute($post_sum_balance))))),
				'_savings' => (empty($post_sum_savings) ? null : endecrypt(trim((float)str_replace(',', '.', $math->execute($post_sum_savings))))),
				'_language' => (empty($post_language) ? null : endecrypt($post_language)),
				'_currency' => (empty($post_currency) ? null : endecrypt($post_currency)),
				'_theme' => (empty($post_theme) ? null : $post_theme),
				'_expensesexpires' => (empty($check_expenses_expires) ? null : $check_expenses_expires),
				'_option_hidedate_expenses' => (empty($check_option_hidedate_expenses) ? null : $check_option_hidedate_expenses),
				'_option_hidedate_debts' => (empty($check_option_hidedate_debts) ? null : $check_option_hidedate_debts),
				'_option_hidedate_loans' => (empty($check_option_hidedate_loans) ? null : $check_option_hidedate_loans),
				'_option_align_center' => (empty($check_option_align_center) ? null : $check_option_align_center),
				'_option_share_allow' => (empty($check_option_share_allow) ? null : $check_option_share_allow),
				'_option_allow_weekends' => (empty($check_option_allow_weekends) ? null : $check_option_allow_weekends),
				'_period_start' => (empty($post_period_start) ? null : endecrypt(strtotime($post_period_start))),
				'_period_end' => (empty($post_period_end) ? null : endecrypt(strtotime($post_period_end)))
			));



		echo 'success';

		log_action('updated settings');



		if(!empty($post_password_new)) {
			sql("UPDATE users
				 SET data_password = :_password
				 WHERE id = :_iduser
				", Array(
					'_iduser' => (int)$user['id'],
					'_password' => password_hash($post_password_new, PASSWORD_BCRYPT)
				));

			log_action('updated password');
		}


		if(!empty($post_tfa)) {
			echo '|tfa-'.$tfa_backup;

			sql("UPDATE users
				 SET data_tfa = :_tfa,
					 data_tfa_backup = :_tfa_backup

				 WHERE id = :_iduser
				", Array(
					'_iduser' => (int)$user['id'],
					'_tfa' => $hidden_secret,
					'_tfa_backup' => password_hash($tfa_backup, PASSWORD_BCRYPT)
				));

			log_action('added tfa');
		}



		/*if(!empty($user['timestamp_period_start']) AND strtotime($post_period_start) > $user['timestamp_period_start'] AND
		   !empty($user['timestamp_period_end']) AND strtotime($post_period_end) > $user['timestamp_period_end']) {

		}*/

	}

?>