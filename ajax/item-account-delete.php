<?php

	require_once '../site-settings.php';



	$get_idaccount = safetag($_GET['ida']);

	$accountinfo =
	sql("SELECT *, COUNT(data_name) AS count_accounts, COUNT(is_default) AS count_defaults
		 FROM item_accounts
		 WHERE id_user = :_iduser
		", Array(
			'_iduser' => (int)$user['id']
		), 'fetch');

	$is_default =
	sql("SELECT COUNT(is_default)
		 FROM item_accounts
		 WHERE id = :_idaccount
		 AND id_user = :_iduser
		", Array(
			'_idaccount' => (int)$get_idaccount,
			'_iduser' => (int)$user['id']
		), 'count');




	if($accountinfo['count_accounts'] > 1 AND $is_default != 0) {
		echo 'account-default';

	} else {
		sql("DELETE FROM item_accounts
			 WHERE id = :_idaccount
			 AND id_user = :_iduser
			", Array(
				'_idaccount' => (int)$get_idaccount,
				'_iduser' => (int)$user['id']
			));

		sql("UPDATE items
			 SET id_account = NULL
			 WHERE id_user = :_iduser
			 AND id_account = :_idaccount
			", Array(
				'_iduser' => (int)$user['id'],
				'_idaccount' => (int)$get_idaccount
			));

		log_action(
			'deleted an account for expenses'
		);
	}

?>