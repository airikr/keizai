<?php

	require_once '../site-settings.php';



	$post_notes = safetag($_POST['field-notes']);

	sql("UPDATE users
		 SET data_notes = :_notes
		 WHERE id = :_iduser
		", Array(
			'_iduser' => (int)$user['id'],
			'_notes' => (empty($post_notes) ? null : endecrypt($post_notes))
		));

	log_action(
		'updated notes',
		null,
		null,
		null,
		null,
		null,
		null,
		1
	);

?>