<?php

	require_once '../site-settings.php';

	$check_network = @fopen($site_url.':443', 'r');



	if($check_network) {
		$current_version = file_get_contents('../VERSION.md');
		$latest_version = @file_get_contents('https://codeberg.org/airikr/Keizai/raw/branch/main/VERSION.md');

		if($current_version < $latest_version) {
			echo link_(svgicon('info').'<b>v'.$latest_version.'</b> '.mb_strtolower($lang['messages']['version-are-available']), 'https://codeberg.org/airikr/Keizai');

		} elseif($current_version > $latest_version) {
			echo '<div class="icon color-yellow">'.svgicon('question') . $lang['messages']['version-above-current'].'</div>';

		} else {
			echo '<div class="icon color-green">'.svgicon('tick') . $lang['messages']['version-uptodate'].'</div>';
		}



	} else {
		echo '<div class="icon color-yellow">'.svgicon('no-network') . $lang['messages']['no-internet'].'</div>';
	}

	fclose($check_network);

?>