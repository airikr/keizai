<?php

	require_once '../site-settings.php';

	use NXP\MathExecutor;

	$math = new MathExecutor();



	$hidden_object = $_POST['hidden-object'];

	$post_account = (isset($_POST['list-accounts']) ? safetag($_POST['list-accounts']) : null);
	$post_type = (isset($_POST['list-types']) ? safetag($_POST['list-types']) : null);
	$post_category = (isset($_POST['list-categories']) ? safetag($_POST['list-categories']) : null);
	$post_currency = (isset($_POST['list-currencies']) ? safetag($_POST['list-currencies']) : null);
	$post_name = safetag($_POST['field-name']);
	$post_sum = safetag($_POST['field-sum']);
	$post_sum_permonth = (isset($_POST['field-sum-permonth']) ? safetag($_POST['field-sum-permonth']) : null);
	$post_contact_phone = (isset($_POST['field-contact-phone']) ? safetag($_POST['field-contact-phone']) : null);
	$post_contact_email = (isset($_POST['field-contact-email']) ? safetag($_POST['field-contact-email']) : null);
	$post_date = safetag($_POST['field-date']);
	$post_date_expires = (isset($_POST['field-date-expires']) ? safetag($_POST['field-date-expires']) : null);
	$post_number_payment = (empty($_POST['field-number-payment']) ? null : safetag($_POST['field-number-payment']));
	$post_number_ocr = (empty($_POST['field-number-ocr']) ? null : safetag($_POST['field-number-ocr']));
	$post_number_phone = (empty($_POST['field-number-phone']) ? null : safetag($_POST['field-number-phone']));
	$post_payed = (empty($_POST['field-date-payed']) ? null : safetag($_POST['field-date-payed']));
	$post_notes = (empty($_POST['field-notes']) ? null : safetag($_POST['field-notes']));
	$post_recurrence = (empty($_POST['field-recurrence']) ? null : safetag($_POST['field-recurrence']));
	$post_recurrence_type = (isset($_POST['list-recurrence-types']) ? safetag($_POST['list-recurrence-types']) : null);
	$post_belongsto_debt = (isset($_POST['list-debts']) ? safetag($_POST['list-debts']) : null);

	$check_expense_payed = (isset($_POST['check-expense-payed']) ? true : null);
	$check_expense_extra = (isset($_POST['check-expense-extra']) ? true : null);
	$check_expense_subscription = (isset($_POST['check-expense-subscription']) ? true : null);
	$check_expense_recurrent = (isset($_POST['check-expense-recurrent']) ? true : null);
	$check_expense_correctinfo = (isset($_POST['check-expense-correctinfo']) ? true : null);
	$check_debt_company = (isset($_POST['check-debt-company']) ? true : null);
	$check_debt_paying = (isset($_POST['check-debt-currently-paying']) ? true : null);
	$check_loan_out = (isset($_POST['check-loan-out']) ? true : null);



	if($hidden_object == 'expense' AND empty($post_type) OR
	   $hidden_object == 'expense' AND empty($post_category) OR
	   $hidden_object == 'expense' AND empty($post_currency) OR
	   empty($post_name) OR empty($post_sum) OR empty($post_date)) {
		echo 'fields-empty';


	} else {

		$count_items =
		sql("SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 AND data_name = :_name
			 AND data_sum = :_sum
			 ".((empty($post_number_payment) AND empty($post_number_payment)) ? '' : "AND data_number_payment = '".$post_number_payment."' AND data_number_ocr = '".$post_number_ocr."'")."
			 AND timestamp_date = :_date
			 ".($hidden_object == 'expense' ? "AND is_expense IS NOT NULL" : ($hidden_object == 'debt' ? "AND is_debt IS NOT NULL" : ($hidden_object == 'loan' ? "AND is_loan IS NOT NULL" : "")))."
			", Array(
				'_iduser' => (int)$user['id'],
				'_name' => endecrypt(trim($post_name)),
				'_sum' => endecrypt(trim($post_sum)),
				'_date' => trim(strtotime($post_date))
			), 'count');

		$iteminfo =
		sql("SELECT data_name, data_sum, timestamp_date
			 FROM items
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'fetch');



		if($count_items != 0) {
			echo 'item-exists';

		} elseif(!empty($post_sum_permonth) AND !is_numeric($post_sum_permonth)) {
			echo 'onlydigits-permonth';

		} elseif(!empty($post_sum_interest) AND !is_numeric($post_sum_interest)) {
			echo 'onlydigits-interest';

		} elseif(!empty($post_sum_adminfee) AND !is_numeric($post_sum_adminfee)) {
			echo 'onlydigits-adminfee';

		} elseif(!empty($post_sum_permonth) AND $post_sum_permonth[0] == '-' OR
				 !empty($post_sum_interest) AND $post_sum_interest[0] == '-' OR
				 !empty($post_sum_adminfee) AND $post_sum_adminfee[0] == '-' OR
				 !empty($post_sum) AND $post_sum[0] == '-') {
			echo 'sum-invalid-nonegative';


		} else {
			sql("INSERT INTO items(
					 id_user,
					 id_account,
					 data_name,
					 data_sum,
					 data_sum_permonth,
					 data_contact_phone,
					 data_contact_email,
					 data_type,
					 data_category,
					 data_currency,
					 data_number_payment,
					 data_number_ocr,
					 data_number_phone,
					 data_recurrence,
					 data_recurrence_type,
					 data_belongsto_debt,
					 data_notes,
					 check_expense_payed,
					 check_expense_extra,
					 check_expense_subscription,
					 check_expense_recurrent,
					 check_expense_correctinfo,
					 check_debt_company,
					 check_debt_paying,
					 check_loan_out,
					 is_expense,
					 is_debt,
					 is_loan,
					 timestamp_date,
					 timestamp_date_end,
					 timestamp_payed,
					 timestamp_added
				 )

				 VALUES(
					 :_iduser,
					 :_idaccount,
					 :_name,
					 :_sum,
					 :_sum_permonth,
					 :_contact_phone,
					 :_contact_email,
					 :_type,
					 :_category,
					 :_currency,
					 :_number_payment,
					 :_number_ocr,
					 :_number_phone,
					 :_recurrence,
					 :_recurrence_type,
					 :_belongsto_debt,
					 :_notes,
					 :_expense_payed,
					 :_expense_extra,
					 :_expense_subscription,
					 :_expense_recurrent,
					 :_expense_correctinfo,
					 :_debt_company,
					 :_debt_paying,
					 :_loan_out,
					 :_isexpense,
					 :_isdebt,
					 :_isloan,
					 :_date,
					 :_date_end,
					 :_payed,
					 :_added
				 )
				", Array(
					'_iduser' => (int)$user['id'],
					'_idaccount' => (empty($post_account) ? null : (int)$post_account),
					'_name' => endecrypt(trim($post_name)),
					'_sum' => endecrypt((float)trim($math->execute(str_replace(',', '.', $post_sum)))),
					'_sum_permonth' => (empty($post_sum_permonth) ? null : endecrypt((float)trim($math->execute(str_replace(',', '.', $post_sum_permonth))))),
					'_contact_phone' => (empty($post_contact_phone) ? null : endecrypt(trim($post_contact_phone))),
					'_contact_email' => (empty($post_contact_email) ? null : endecrypt(trim($post_contact_email))),
					'_type' => (empty($post_category) ? null : endecrypt($post_type)),
					'_category' => (empty($post_category) ? null : endecrypt($post_category)),
					'_currency' => (empty($post_currency) ? null : endecrypt($post_currency)),
					'_number_payment' => (empty($post_number_payment) ? null : endecrypt(trim($post_number_payment))),
					'_number_ocr' => (empty($post_number_ocr) ? null : endecrypt(trim($post_number_ocr))),
					'_number_phone' => (empty($post_number_phone) ? null : endecrypt(str_replace(' ', '', trim($post_number_phone)))),
					'_notes' => (empty($post_notes) ? null : endecrypt(trim($post_notes))),
					'_recurrence' => (empty($post_recurrence) ? null : endecrypt(trim($post_recurrence))),
					'_recurrence_type' => (empty($post_recurrence_type) ? null : endecrypt($post_recurrence_type)),
					'_belongsto_debt' => (empty($post_belongsto_debt) ? null : $post_belongsto_debt),
					'_expense_payed' => (empty($check_expense_payed) ? null : $check_expense_payed),
					'_expense_extra' => (empty($check_expense_extra) ? null : $check_expense_extra),
					'_expense_subscription' => (empty($check_expense_subscription) ? null : $check_expense_subscription),
					'_expense_recurrent' => (empty($check_expense_recurrent) ? null : $check_expense_recurrent),
					'_expense_correctinfo' => (empty($check_expense_correctinfo) ? null : $check_expense_correctinfo),
					'_debt_company' => (empty($check_debt_company) ? null : $check_debt_company),
					'_debt_paying' => (empty($check_debt_paying) ? null : $check_debt_paying),
					'_loan_out' => (empty($check_loan_out) ? null : $check_loan_out),
					'_isexpense' => ($hidden_object == 'expense' ? 1 : null),
					'_isdebt' => ($hidden_object == 'debt' ? 1 : null),
					'_isloan' => ($hidden_object == 'loan' ? 1 : null),
					'_date' => trim(strtotime($post_date)),
					'_date_end' => (empty($post_date_expires) ? null : endecrypt(strtotime($post_date_expires))),
					'_payed' => (empty($post_payed) ? null : endecrypt(strtotime($post_payed))),
					'_added' => endecrypt(time())
				), 'insert');



			$iteminfo =
			sql("SELECT id, COUNT(id) AS check_
				 FROM items
				 WHERE id_user = :_iduser
				 ORDER BY timestamp_added DESC
				 LIMIT 1
				", Array(
					'_iduser' => (int)$user['id']
				), 'fetch');

			if($iteminfo['check_'] != 0) {
				log_action(
					'created item',
					null,
					null,
					(int)$iteminfo['id'],
					($hidden_object == 'expense' ? 1 : null),
					($hidden_object == 'debt' ? 1 : null),
					($hidden_object == 'loan' ? 1 : null)
				);

				echo $hidden_object.'|'.(empty($post_account) ? null : (int)$post_account);


			} else {
				echo 'not-added';
			}
		}

	}

?>