<?php

	require_once '../site-settings.php';



	if(isset($_GET['del'])) {
		sql("DELETE FROM items WHERE id_user = :_iduser;
			 DELETE FROM item_accounts WHERE id_user = :_iduser;
			 DELETE FROM budget WHERE id_user = :_iduser;
			 DELETE FROM shares WHERE id_user = :_iduser;
			 DELETE FROM sess WHERE id_user = :_iduser AND data_ipaddress != :_ipaddress AND data_useragent != :_useragent;
			", Array(
				'_iduser' => (int)$user['id'],
				'_ipaddress' => endecrypt(getip()),
				'_useragent' => endecrypt($useragent)
			));

		sql("UPDATE users
			 SET data_sum_income = NULL,
				 data_sum_balance = NULL,
				 data_sum_savings = NULL,
				 data_notes = NULL,
				 timestamp_period_start = NULL,
				 timestamp_period_end = NULL

			 WHERE id = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			));




	} elseif(isset($_GET['con'])) {
		$confirm_i =
		sql("SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'count');

		$confirm_b =
		sql("SELECT COUNT(id_user)
			 FROM budget
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'count');

		$confirm_u =
		sql("SELECT data_sum_income,
					data_sum_balance,
					data_sum_savings,
					data_notes,
					timestamp_period_start,
					timestamp_period_end

			 FROM users
			 WHERE id = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'fetch');


		if(empty($confirm_u['data_sum_income']) AND
		   empty($confirm_u['data_sum_balance']) AND
		   empty($confirm_u['data_sum_savings']) AND
		   empty($confirm_u['data_notes']) AND
		   empty($confirm_u['timestamp_period_start']) AND
		   empty($confirm_u['timestamp_period_end']) AND
		   $confirm_i == 0 AND
		   $confirm_b == 0) {
			echo 'success';

			log_action('deleted data');


		} else {
			echo 'retry';

			log_action('failed to delete data');
		}

	}

?>