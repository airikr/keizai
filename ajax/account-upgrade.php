<?php

	require_once '../site-settings.php';

	use ParagonIE\Halite\KeyFactory;
	use ParagonIE\Halite\Symmetric\Crypto as Symmetric;
	use ParagonIE\HiddenString\HiddenString;

	if(!file_exists($dir_userfiles.'/'.hash('sha256', $user['data_username']).'.key')) {
		KeyFactory::save(KeyFactory::generateEncryptionKey(), $dir_userfiles.'/'.hash('sha256', $user['data_username']).'.key');
	}

	$get_step = (!isset($_GET['ste']) ? null : safetag((int)$_GET['ste']));



	function encrypt($string, $websiteskey = false) {
		global $dir_files, $site_title, $dir_userfiles, $user;

		if($websiteskey == false) {
			$encryptionkey = KeyFactory::loadEncryptionKey($dir_userfiles.'/'.hash('sha256', $user['data_username']).'.key');
		} else {
			$encryptionkey = KeyFactory::loadEncryptionKey($dir_files.'/'.hash('sha256', $site_title).'.key');
		}

		$ciphertext = Symmetric::encrypt(new HiddenString($string), $encryptionkey);

		return $ciphertext;
	}

	/*function decrypt($string) {
		global $dir_userfiles, $user;

		$encryptionkey = KeyFactory::loadEncryptionKey($dir_userfiles.'/'.hash('sha256', $user['data_username']).'.key');
		$decrypted = Symmetric::decrypt($string, $encryptionkey);

		return $decrypted->getString();
	}*/

	function decrypt_old($string) {
		global $dir_userfiles, $user, $config_encryptionmethod;

		$file_enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($user['data_saltedstring_1'] . (int)$user['id'] . $user['data_saltedstring_2']).'.json'), true);

		if(empty($string)) {
			return 'dencr.err';

		} else {
			return openssl_decrypt(
				base64_decode($string),
				$config_encryptionmethod,
				hash('sha256', $file_enc['keys'][0]),
				0,
				substr(hash('sha256', $file_enc['keys'][1]), 0, 16)
			);
		}
	}







	if($get_step == 1) {

		$count_accounts =
		sql("SELECT COUNT(id_user)
			 FROM item_accounts
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'count');

		if($count_accounts != 0) {
			$get_accounts =
			sql("SELECT *
				 FROM item_accounts
				 WHERE id_user = :_iduser
				", Array(
					'_iduser' => (int)$user['id']
				));
		}

		if($count_accounts != 0) {
			foreach($get_accounts AS $account) {
				sql("UPDATE item_accounts
					 SET data_name => :_name
					 WHERE id_user = :_iduser
					", Array(
						'_iduser' => (int)$user['id'],
						'_name' => encrypt(decrypt_old($account['name']))
					));
			}
		}

		echo 'success';



	} elseif($get_step == 2) {

		$count_items =
		sql("SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'count');

		if($count_items != 0) {
			$get_items =
			sql("SELECT *
				 FROM items
				 WHERE id_user = :_iduser
				", Array(
					'_iduser' => (int)$user['id']
				));
		}

		foreach($get_items AS $item) {
			if($count_accounts != 0) {
				$account =
				sql("SELECT id
					 FROM item_accounts
					 WHERE id_user = :_iduser
					 AND data_name = :_name
					", Array(
						'_iduser' => (int)$user['id'],
						'_name' => encrypt(decrypt_old($item['data_name']))
					), 'fetch');
			}

			sql("UPDATE items
				 SET id_account = :_idaccount,
					 data_name = :_name,
					 data_sum = :_sum,
					 data_sum_permonth = :_sum_permonth,
					 data_type = :_type,
					 data_category = :_category,
					 data_currency = :_currency,
					 data_number_payment = :_number_payment,
					 data_number_ocr = :_number_ocr,
					 data_number_phone = :_number_phone,
					 data_recurrence = :_recurrence,
					 data_recurrence_type = :_recurrence_type,
					 data_notes = :_notes,
					 timestamp_date_end = :_date_end,
					 timestamp_payed = :_payed,
					 timestamp_added = :_added

				 WHERE id_user = :_iduser
				", Array(
					'_iduser' => (int)$user['id'],
					'_idaccount' => (empty($item['id_account']) ? null : (int)$account['id']),
					'_name' => encrypt(decrypt_old($item['data_name'])),
					'_sum' => encrypt(decrypt_old($item['data_sum'])),
					'_sum_permonth' => (empty($item['data_sum_permonth']) ? null : encrypt(decrypt_old($item['data_sum_permonth']))),
					'_type' => (empty($item['data_type']) ? null : encrypt(decrypt_old($item['data_type']))),
					'_category' => (empty($item['data_category']) ? null : encrypt(decrypt_old($item['data_category']))),
					'_currency' => (empty($item['data_currency']) ? null : encrypt(decrypt_old($item['data_currency']))),
					'_number_payment' => (empty($item['data_number_payment']) ? null : encrypt(decrypt_old($item['data_number_payment']))),
					'_number_ocr' => (empty($item['data_number_ocr']) ? null : encrypt(decrypt_old($item['data_number_ocr']))),
					'_number_phone' => (empty($item['data_number_phone']) ? null : encrypt(decrypt_old($item['data_number_phone']))),
					'_recurrence' => (empty($item['data_recurrence']) ? null : encrypt(decrypt_old($item['data_recurrence']))),
					'_recurrence_type' => (empty($item['data_recurrence_type']) ? null : encrypt(decrypt_old($item['data_recurrence_type']))),
					'_notes' => (empty($item['data_notes']) ? null : encrypt(decrypt_old($item['data_notes']))),
					'_date_end' => (empty($item['timestamp_date-end']) ? null : encrypt(decrypt_old($item['timestamp_date_end']))),
					'_payed' => (empty($item['timestamp_payed']) ? null : encrypt(decrypt_old($item['timestamp_payed']))),
					'_added' => encrypt(decrypt_old($item['timestamp_added']))
				));
		}

		echo 'success';



	} elseif($get_step == 3) {

		$count_budgets =
		sql("SELECT COUNT(id_user)
			 FROM budget
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'count');

		if($count_budgets != 0) {
			$get_budgets =
			sql("SELECT *
				 FROM budget
				 WHERE id_user = :_iduser
				", Array(
					'_iduser' => (int)$user['id']
				));
		}

		if($count_budgets != 0) {
			foreach($get_budgets AS $budget) {
				sql("UPDATE budget
					 SET data_name = :_name,
						 data_sum_current = :_sum_current,
						 data_sum_goal = :_sum_goal,
						 data_notes = :_notes

					 WHERE id_user = :_iduser
					", Array(
						'_iduser' => (int)$user['id'],
						'_name' => encrypt(decrypt_old($budget['data_name'])),
						'_sum_current' => encrypt(decrypt_old($budget['data_sum_current'])),
						'_sum_goal' => encrypt(decrypt_old($budget['data_sum_goal'])),
						'_notes' => encrypt(decrypt_old($budget['data_notes']))
					));
			}
		}

		echo 'success';



	} elseif($get_step == 4) {

		sql("UPDATE users
			 SET data_email = :_email,
				 data_tfa = :_tfa,
				 data_tfa_backup = :_tfa_backup,
				 data_incomeday = :_incomeday,
				 data_sum_income = :_sum_income,
				 data_sum_balance = :_sum_balance,
				 data_sum_savings = :_sum_savings,
				 data_currency = :_currency,
				 data_language = :_language,
				 data_notes = :_notes,
				 data_theme = :_theme,
				 check_email_expensesexpires = :_email_expenses_expires,
				 check_option_hidedate_expenses = :_option_hidedate_expenses,
				 check_option_hidedate_debts = :_option_hidedate_debts,
				 check_option_hidedate_loans = :_option_hidedate_loans,
				 is_upgraded = :_isupgraded,
				 timestamp_registered = :_registered,
				 timestamp_period_start = :_period_start,
				 timestamp_period_end = :_period_end

			 WHERE id = :_iduser
			", Array(
				'_iduser' => (int)$user['id'],
				'_email' => (empty($user['data_email']) ? null : encrypt(decrypt_old($user['data_email']))),
				'_tfa' => (empty($user['data_tfa']) ? null : $user['data_tfa']),
				'_tfa_backup' => (empty($user['data_tfa_backup']) ? null : $user['data_tfa_backup']),
				'_incomeday' => (empty($user['data_incomeday']) ? null : encrypt(decrypt_old($user['data_incomeday'], false))),
				'_sum_income' => (empty($user['data_sum_income']) ? null : encrypt(decrypt_old($user['data_sum_income']))),
				'_sum_balance' => (empty($user['data_sum_balance']) ? null : encrypt(decrypt_old($user['data_sum_balance']))),
				'_sum_savings' => (empty($user['data_sum_savings']) ? null : encrypt(decrypt_old($user['data_sum_savings']))),
				'_currency' => (empty($user['data_currency']) ? null : encrypt(decrypt_old($user['data_currency']))),
				'_language' => (empty($user['data_language']) ? null : encrypt(decrypt_old($user['data_language']))),
				'_notes' => (empty($user['data_notes']) ? null : encrypt(decrypt_old($user['data_notes']))),
				'_theme' => (empty($user['data_theme']) ? null : $user['data_theme']),
				'_email_expenses_expires' => (empty($user['check_email_expensesexpires']) ? null : $user['check_email_expensesexpires']),
				'_option_hidedate_expenses' => (empty($user['check_option_hidedate_expenses']) ? null : $user['check_option_hidedate_expenses']),
				'_option_hidedate_debts' => (empty($user['check_option_hidedate_debts']) ? null : $user['check_option_hidedate_debts']),
				'_option_hidedate_loans' => (empty($user['check_option_hidedate_loans']) ? null : $user['check_option_hidedate_loans']),
				'_isupgraded' => 1,
				'_registered' => decrypt_old($user['timestamp_registered']),
				'_period_start' => (empty($user['timestamp_period_start']) ? null : encrypt(decrypt_old($user['timestamp_period_start']))),
				'_period_end' => (empty($user['timestamp_period_end']) ? null : encrypt(decrypt_old($user['timestamp_period_end'])))
			));

		echo 'success';

	}

?>