<?php

	require_once '../site-settings.php';



	$get_iduser = safetag((int)$_GET['idu']);

	if(isset($_GET['del'])) {
		unlink($dir_userfiles.'/'.md5($user['data_saltedstring_1'] . $get_iduser . $user['data_saltedstring_2']).'.json');

		sql("DELETE FROM users
			 WHERE id = :_iduser
			", Array(
				'_iduser' => $get_iduser
			));



	} elseif(isset($_GET['con'])) {
		$confirm_i =
		sql("SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => $get_iduser
			), 'count');

		$confirm_b =
		sql("SELECT COUNT(id_user)
			 FROM budget
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'count');

		$confirm_sa =
		sql("SELECT COUNT(id_user)
			 FROM sess_actions
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => $get_iduser
			), 'count');

		$confirm_u =
		sql("SELECT COUNT(id)
			 FROM users
			 WHERE id = :_iduser
			", Array(
				'_iduser' => $get_iduser
			), 'count');

		$confirm_s =
		sql("SELECT COUNT(id)
			 FROM sess
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => $get_iduser
			), 'count');


		if($confirm_i == 0 AND
		   $confirm_b == 0 AND
		   $confirm_sa == 0 AND
		   $confirm_u == 0 AND
		   $confirm_s == 0) {
			echo 'success';


		} else {
			echo 'retry';
		}

	}

?>