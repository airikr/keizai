<?php

	require_once '../site-settings.php';



	$hidden_language = safetag($_POST['hidden-language']);

	$post_username = safetag($_POST['field-username']);
	$post_password = safetag($_POST['field-password']);
	$post_password_repeat = safetag($_POST['field-password-repeat']);
	$post_email = (empty($_POST['field-email']) ? null : safetag($_POST['field-email']));

	$random_string_1 = pw_random(64);
	$random_string_2 = pw_random(64);
	$salt_1 = pw_random(10);
	$salt_2 = pw_random(10);
	$salt_3 = pw_random(10);
	$salt_4 = pw_random(10);
	$salt_5 = pw_random(10);
	$salt_6 = pw_random(10);



	if(empty($post_username) OR empty($post_password)) {
		echo 'fields-empty';


	} else {

		$check_user =
		sql("SELECT COUNT(data_username)
			 FROM users
			 WHERE data_username = :_username
			", Array(
				'_username' => endecrypt($post_username, true, true)
			), 'count');

		if(!empty($post_email)) {
			$check_email =
			sql("SELECT COUNT(data_email)
				 FROM users
				 WHERE data_email = :_email
				", Array(
					'_email' => endecrypt($post_email)
				), 'count');
		}



		if($check_user != 0) {
			echo 'user-exists';

		} elseif(strlen($post_password) < 10 OR strlen($post_password_repeat) < 10) {
			echo 'password-tooshort';

		} elseif($post_password != $post_password_repeat) {
			echo 'passwords-notmatching';

		} elseif(!empty($post_email) AND validate('email', $post_email, 'email') == false) {
			echo 'email-notvalid';

		} elseif(!empty($post_email) AND $check_email != 0) {
			echo 'email-exists';

		} elseif(!is_writeable($dir_userfiles)) {
			echo 'permissions-denied';

		} else {
			echo 'success';

			sql("INSERT INTO users(
					 data_username,
					 data_password,
					 data_saltedstring_1,
					 data_saltedstring_2,
					 data_theme
				 )

				 VALUES(
					:_username,
					:_password,
					:_saltedstring_1,
					:_saltedstring_2,
					:_theme
				 )
				", Array(
					'_username' => endecrypt($post_username, true, true),
					'_password' => password_hash($post_password, PASSWORD_BCRYPT),
					'_saltedstring_1' => $salt_1,
					'_saltedstring_2' => $salt_2,
					'_theme' => 1
				), 'insert');



			$userinfo =
			sql("SELECT id
				 FROM users
				 WHERE data_username = :_username
				", Array(
					'_username' => endecrypt($post_username, true, true)
				), 'fetch');

			$content = [
				'hash' => md5($salt_1 . (int)$userinfo['id'] . $salt_2),
				'keys' => [
					$salt_3 . $random_string_1 . $salt_4,
					$salt_5 . $random_string_2 . $salt_6
				]
			];

			$fp = fopen($dir_userfiles.'/'.md5($salt_1 . (int)$userinfo['id'] . $salt_2).'.json', 'w');
			fwrite($fp, json_encode($content));
			fclose($fp);

			function encrypt($string) {
				global $config_encryptionmethod, $content;

				return base64_encode(openssl_encrypt(
					$string,
					$config_encryptionmethod,
					hash('sha256', $content['keys'][0]),
					0,
					substr(hash('sha256', $content['keys'][1]), 0, 16)
				));
			}



			$count_users =
			sql("SELECT COUNT(id)
				 FROM users
				", Array(), 'count');

			sql("UPDATE users
				 SET data_email = :_email,
					 data_currency = :_currency,
					 data_language = :_language,
					 is_admin = :_isadmin,
					 timestamp_registered = :_registered

				 WHERE id = :_iduser
				", Array(
					'_iduser' => (int)$userinfo['id'],
					'_email' => (empty($post_email) ? null : encrypt($post_email)),
					'_currency' => encrypt('sek'),
					'_language' => encrypt(($hidden_language == 'en' ? 'gb' : $hidden_language)),
					'_isadmin' => ($count_users == 1 ? 1 : null),
					'_registered' => encrypt(time())
				));
		}

	}

?>