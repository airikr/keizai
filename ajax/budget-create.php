<?php

	require_once '../site-settings.php';

	use NXP\MathExecutor;

	$math = new MathExecutor();



	$post_name = safetag($_POST['field-name']);
	$post_sum_goal = safetag($_POST['field-sum-goal']);
	$post_sum_permonth = safetag($_POST['field-sum-permonth']);
	$post_notes = (empty($_POST['field-notes']) ? null : safetag($_POST['field-notes']));



	if(empty($post_name) OR empty($post_sum_goal)) {
		echo 'fields-empty';


	} else {

		$count_items =
		sql("SELECT COUNT(id_user)
			 FROM budget
			 WHERE id_user = :_iduser
			 AND data_name = :_name
			 AND data_sum_goal = :_sum_goal
			", Array(
				'_iduser' => (int)$user['id'],
				'_name' => endecrypt(trim($post_name)),
				'_sum_goal' => endecrypt(trim($post_sum_goal))
			), 'count');

		$iteminfo =
		sql("SELECT data_name, data_sum, timestamp_date
			 FROM items
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'fetch');



		if($count_items != 0) {
			echo 'item-exists';

		} elseif(!is_numeric($post_sum_goal)) {
			echo 'onlydigits-sum-goal';

		} elseif(!is_numeric($post_sum_permonth)) {
			echo 'onlydigits-sum-permonth';


		} else {
			sql("INSERT INTO budget(
					 id_user,
					 data_name,
					 data_sum_goal,
					 data_sum_permonth,
					 data_notes,
					 timestamp_created
				 )

				 VALUES(
					 :_iduser,
					 :_name,
					 :_sum_goal,
					 :_sum_permonth,
					 :_notes,
					 :_created
				 )
				", Array(
					'_iduser' => (int)$user['id'],
					'_name' => endecrypt(trim($post_name)),
					'_sum_goal' => endecrypt((float)trim($math->execute(str_replace(',', '.', $post_sum_goal)))),
					'_sum_permonth' => endecrypt((float)trim($math->execute(str_replace(',', '.', $post_sum_permonth)))),
					'_notes' => (empty($post_notes) ? null : endecrypt(trim($post_notes))),
					'_created' => time()
				), 'insert');



			$iteminfo =
			sql("SELECT id, COUNT(id) AS check_
				 FROM budget
				 WHERE id_user = :_iduser
				 ORDER BY timestamp_created DESC
				 LIMIT 1
				", Array(
					'_iduser' => (int)$user['id']
				), 'fetch');

			if($iteminfo['check_'] != 0) {
				log_action('created budget');

			} else {
				echo 'not-added';
			}
		}

	}

?>