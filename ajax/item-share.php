<?php

	require_once '../site-settings.php';



	$hidden_iditem = safetag($_POST['hidden-iditem']);

	$post_username = safetag($_POST['field-username']);
	$post_sum = safetag($_POST['field-sum']);
	$post_payment_type = safetag($_POST['list-payment-type']);

	$check_allow_deletion = (isset($_POST['check-allow-deletion']) ? true : null);
	$check_allow_markas_payed = (isset($_POST['check-allow-markas-payed']) ? true : null);
	$check_number_payment = (isset($_POST['check-share-number-payment']) ? true : null);
	$check_number_ocr = (isset($_POST['check-share-number-ocr']) ? true : null);
	$check_number_phone = (isset($_POST['check-share-number-phone']) ? true : null);
	$check_qrcodes = (isset($_POST['check-share-qrcodes']) ? true : null);
	$check_notes = (isset($_POST['check-share-notes']) ? true : null);



	if(empty($post_username)) {
		echo 'fields-empty';


	} else {

		$check_existence =
		sql("SELECT COUNT(id)
			 FROM users
			 WHERE data_username = :_username
			", Array(
				'_username' => endecrypt($post_username, true, true)
			), 'count');

		$check_with =
		sql("SELECT COUNT(s.id_user_with)
			 FROM shares s
			 JOIN users u
			 ON s.id_user_with = u.id
			 WHERE s.id_item = :_iditem
			 AND u.data_username = :_username
			", Array(
				'_iditem' => (int)$hidden_iditem,
				'_username' => endecrypt($post_username, true, true)
			), 'count');

		$userinfo =
		sql("SELECT check_option_share_allow
			 FROM users
			 WHERE data_username = :_username
			", Array(
				'_username' => endecrypt($post_username, true, true)
			), 'fetch');

		$iteminfo =
		sql("SELECT data_sum
			 FROM items
			 WHERE id = :_iditem
			", Array(
				'_iditem' => (int)$hidden_iditem
			), 'fetch');



		if($check_existence == 0) {
			echo 'user-not-exists';

		} elseif($check_with != 0) {
			echo 'already-shared';

		} elseif($post_username == endecrypt($user['data_username'], false, true)) {
			echo 'cant-share-to-self';

		} elseif(empty($userinfo['check_option_share_allow'])) {
			echo 'privacy-sharing-notallowed';

		} elseif($post_payment_type == 'sum' AND !empty($post_sum) AND $post_sum > endecrypt($iteminfo['data_sum'], false)) {
			echo 'sum-aboveactual';

		} elseif($post_payment_type == 'sum' AND !empty($post_sum) AND $post_sum < 5) {
			echo 'sum-below5';

		} elseif($post_payment_type == 'percent' AND !empty($post_sum) AND $post_sum > 100) {
			echo 'sum-above-100-percent';


		} else {

			$with =
			sql("SELECT id
				 FROM users
				 WHERE data_username = :_username
				", Array(
					'_username' => endecrypt($post_username, true, true)
				), 'fetch');


			sql("INSERT INTO shares(
					 id_user,
					 id_user_with,
					 id_item,
					 data_sum,
					 data_payment,
					 allow_deletion,
					 allow_markas_payed,
					 share_number_payment,
					 share_number_ocr,
					 share_number_phone,
					 share_qrcodes,
					 share_notes,
					 timestamp_shared
				 )

				 VALUES(
					 :_iduser,
					 :_iduser_with,
					 :_iditem,
					 :_sum,
					 :_payment,
					 :_deletion,
					 :_markas_payed,
					 :_number_payment,
					 :_number_ocr,
					 :_number_phone,
					 :_qrcodes,
					 :_notes,
					 :_shared
				 )
				", Array(
					'_iduser' => (int)$user['id'],
					'_iduser_with' => (int)$with['id'],
					'_iditem' => (int)$hidden_iditem,
					'_sum' => ((empty($post_sum) OR $post_sum == 0) ? null : endecrypt($post_sum)),
					'_payment' => ((empty($post_sum) OR $post_sum == 0) ? null : endecrypt($post_payment_type)),
					'_deletion' => $check_allow_deletion,
					'_markas_payed' => $check_allow_markas_payed,
					'_number_payment' => $check_number_payment,
					'_number_ocr' => $check_number_ocr,
					'_number_phone' => $check_number_phone,
					'_qrcodes' => $check_qrcodes,
					'_notes' => $check_notes,
					'_shared' => time()
				), 'insert');

		}

	}

?>