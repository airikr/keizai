<?php

	require_once '../site-settings.php';



	sql("UPDATE users
		 SET data_tfa = NULL,
			 data_tfa_backup = NULL

		 WHERE id = :_iduser
		", Array(
			'_iduser' => (int)$user['id']
		));

	$check_remove =
	sql("SELECT data_tfa
		 FROM users
		 WHERE id = :_iduser
		", Array(
			'_iduser' => (int)$user['id']
		), 'fetch');



	if(!empty($check_remove['data_tfa'])) {
		echo 'retry';

		log_action('failed to remove tfa');


	} else {
		echo 'success';

		log_action('removed tfa');
	}

?>