<?php

	require_once '../site-settings.php';



	$get_action = safetag($_GET['act']);

	if($get_action == 'email') {
		$post_username = safetag($_POST['field-username']);
		$post_email = safetag($_POST['field-email']);


		if(empty($post_username) OR empty($post_email)) {
			echo 'fields-empty';


		} else {

			$check_user =
			sql("SELECT COUNT(data_username)
				 FROM users
				 WHERE data_username = :_username
				", Array(
					'_username' => endecrypt($post_username, true, true)
				), 'count');
	
			if($check_user != 0) {
				$userinfo =
				sql("SELECT id, data_password, data_tfa, data_saltedstring_1, data_saltedstring_2
					 FROM users
					 WHERE data_username = :_username
					", Array(
						'_username' => endecrypt($post_username, true, true)
					), 'fetch');
	
				$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($userinfo['data_saltedstring_1'] . (int)$userinfo['id'] . $userinfo['data_saltedstring_2']).'.json'), true);
	
				$ncrypt = new mukto90\Ncrypt;
				$ncrypt->set_secret_key($enc['keys'][0]);
				$ncrypt->set_secret_iv($enc['keys'][1]);
				$ncrypt->set_cipher('AES-256-CBC');

				$check_email =
				sql("SELECT COUNT(data_email)
					 FROM users
					 WHERE data_email = :_email
					", Array(
						'_email' => endecrypt($post_email)
					), 'count');
			}


			if($check_user == 0) {
				echo 'user-notexists';

			} elseif(validate('email', $post_email, 'email') == false) {
				echo 'email-notvalid';

			} elseif($check_email == 0) {
				echo 'email-notexists';

			} else {
				echo 'success';

				$content_html = '<h1>Återställ ditt lösenord</h1>';
				$content_html .= '<p>Gå till följande adress för och välja ett nytt lösenord för ditt konto:</p>';
				$content_html .= '<p><a href="'.$site_url.'/forgot-pw/'.md5($post_email).'">Återställ lösenordet</a> ('.$site_url.'/forgot-pw/'.md5($post_email).')</p>';

				$content_nohtml = 'Gå till följande adress för och välja ett nytt lösenord för ditt konto: '.$site_url.'/forgot-pw/'.md5($post_email);

				send_email($post_email, 'Återställning av lösenord', $content_html, $content_nohtml);
			}

		}







	} elseif($get_action == 'update') {
		$hidden_email = safetag($_POST['hidden-email']);
		$post_username = safetag($_POST['field-username']);
		$post_password_new = safetag($_POST['field-password-new']);
		$post_password_repeat = safetag($_POST['field-password-repeat']);


		if(empty($post_username) OR empty($post_password_new) OR empty($post_password_repeat)) {
			echo 'fields-empty';


		} else {

			$check_user =
			sql("SELECT COUNT(data_username)
				 FROM users
				 WHERE data_username = :_username
				", Array(
					'_username' => endecrypt($post_username, true, true)
				), 'count');

			if($check_user != 0) {
				$userinfo =
				sql("SELECT id, data_email, data_password, data_tfa, data_saltedstring_1, data_saltedstring_2
					 FROM users
					 WHERE data_username = :_username
					", Array(
						'_username' => endecrypt($post_username, true, true)
					), 'fetch');

				$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($userinfo['data_saltedstring_1'] . (int)$userinfo['id'] . $userinfo['data_saltedstring_2']).'.json'), true);
			}


			if($check_user == 0) {
				echo 'user-notexists';

			} elseif(empty($userinfo['data_email']) OR md5(endecrypt($userinfo['data_email'], false)) != $hidden_email) {
				echo 'email-notexists';

			} elseif($post_password_new != $post_password_repeat) {
				echo 'password-notsame';


			} else {
				echo 'success';

				sql("UPDATE users
					 SET data_password = :_password
					 WHERE id = :_iduser
					", Array(
						'_iduser' => (int)$userinfo['id'],
						'_password' => password_hash($post_password_new, PASSWORD_BCRYPT)
					));
			}

		}

	}

?>