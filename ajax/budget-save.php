<?php

	require_once '../site-settings.php';

	use NXP\MathExecutor;

	$math = new MathExecutor();



	$hidden_idbudget = safetag($_POST['hidden-idbudget']);

	$post_name = safetag($_POST['field-name']);
	$post_sum_goal = safetag($_POST['field-sum-goal']);
	$post_sum_permonth = safetag($_POST['field-sum-permonth']);
	$post_notes = (empty($_POST['field-notes']) ? null : safetag($_POST['field-notes']));



	if(empty($post_name) OR empty($post_sum_goal)) {
		echo 'fields-empty';


	} else {

		$count_budget =
		sql("SELECT COUNT(id_user)
			 FROM budget
			 WHERE id != :_idbudget
			 AND id_user = :_iduser
			 AND data_name = :_name
			 AND data_sum_goal = :_sum_goal
			", Array(
				'_idbudget' => (int)$hidden_idbudget,
				'_iduser' => (int)$user['id'],
				'_name' => endecrypt($post_name, false),
				'_sum_goal' => endecrypt($post_sum_goal, false)
			), 'count');

		$iteminfo =
		sql("SELECT data_name, data_sum, timestamp_date
			 FROM items
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			), 'fetch');



		if($count_budget != 0) {
			echo 'budget-exists';

		} elseif(!is_numeric($post_sum_goal)) {
			echo 'onlydigits-sum-goal';

		} elseif(!empty($post_sum_permonth) AND !is_numeric($post_sum_permonth)) {
			echo 'onlydigits-sum-permonth';


		} else {
			sql("UPDATE budget
				 SET data_name = :_name,
					 data_sum_goal = :_sum_goal,
					 data_sum_permonth = :_sum_permonth,
					 data_notes = :_notes

				 WHERE id = :_idbudget
				 AND id_user = :_iduser
				", Array(
					'_idbudget' => (int)$hidden_idbudget,
					'_iduser' => (int)$user['id'],
					'_name' => endecrypt($post_name),
					'_sum_goal' => endecrypt((float)trim($math->execute(str_replace(',', '.', $post_sum_goal)))),
					'_sum_permonth' => (empty($post_sum_permonth) ? null : endecrypt((float)trim($math->execute(str_replace(',', '.', $post_sum_permonth))))),
					'_notes' => endecrypt($post_notes)
				));


			log_action(
				'saved changes for a budget',
				null, null, (int)$hidden_idbudget
			);
		}

	}

?>