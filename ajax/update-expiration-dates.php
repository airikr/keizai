<?php

	require_once '../site-settings.php';



	if(!empty($user['timestamp_period_start']) AND !empty($user['timestamp_period_end']) AND empty($user['data_incomeday']) OR
	   empty($user['timestamp_period_start']) AND empty($user['timestamp_period_end']) AND !empty($user['data_incomeday'])) {
		$get_recurrent =
		sql("SELECT id, data_recurrence, data_recurrence_type, timestamp_date
			 FROM items
			 WHERE id_user = :_iduser
			 AND check_expense_recurrent IS NOT NULL
			 AND is_expense IS NOT NULL
			 AND timestamp_date < :_period_start
			", Array(
				'_iduser' => (int)$user['id'],
				'_period_start' => ($period_start_empty == false ? $period_start : $period_incomeday)
			));

	/*if(!empty($user['data_incomeday']) AND endecrypt($user['data_incomeday'], false) == date('d') OR
	   !empty($user['timestamp_period_start']) AND date('d', endecrypt($user['timestamp_period_start'], false)) == date('d') AND
	   !empty($user['timestamp_period_end']) AND date('d', endecrypt($user['timestamp_period_end'], false)) == date('d')) {*/

		$count = 0;
		foreach($get_recurrent AS $recurrent) {
			$count++;
			sql("UPDATE items
				 SET timestamp_date = :_date,
					 check_expense_payed = NULL,
					 check_expense_correctinfo = NULL,
					 timestamp_payed = NULL

				 WHERE id = :_iditem
				 AND id_user = :_iduser
				", Array(
					'_iditem' => (int)$recurrent['id'],
					'_iduser' => (int)$user['id'],
					'_date' => strtotime('+'.endecrypt($recurrent['data_recurrence'], false).' '.endecrypt($recurrent['data_recurrence_type'], false), $recurrent['timestamp_date'])
				));
		}

		if($count == 0) {
			echo 'no-update';
		} else {
			echo 'updated';
		}
	}

?>