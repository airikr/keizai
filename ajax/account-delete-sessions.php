<?php

	require_once '../site-settings.php';



	$get_othersessions =
	sql("SELECT id
		 FROM sess
		 WHERE id != :_idsession
		 AND id_user = :_iduser
		", Array(
			'_idsession' => (int)$session['id'],
			'_iduser' => (int)$user['id']
		));


	foreach($get_othersessions AS $othersession) {
		sql("DELETE FROM sess
			 WHERE id = :_idsession
			 AND id_user = :_iduser
			", Array(
				'_idsession' => (int)$othersession['id'],
				'_iduser' => (int)$user['id']
			));

		log_action('deleted sessions');
	}


	$count_sessions =
	sql("SELECT COUNT(id)
		 FROM sess
		 WHERE id_user = :_iduser
		 ORDER BY timestamp_occurred DESC
		", Array(
			'_iduser' => (int)$user['id']
		), 'count');

	if($count_sessions != 0) {
		$get_sessions =
		sql("SELECT *
			 FROM sess
			 WHERE id_user = :_iduser
			 ORDER BY timestamp_occurred DESC
			", Array(
				'_iduser' => (int)$user['id']
			));
	}



	echo '<div class="itemset head">';
		echo '<div class="options"></div>';
		echo '<div class="last-action">'.$lang['words']['dates']['last-active'].'</div>';
		echo '<div class="ipaddress">'.$lang['words']['ip-address'].'</div>';
		echo '<div class="browser">'.$lang['words']['browser'].'</div>';
		echo '<div class="os">'.$lang['words']['os-short'].'</div>';
	echo '</div>';

	foreach($get_sessions AS $session) {
		$useragentinfo = new WhichBrowser\Parser(endecrypt($session['data_useragent'], false));

		$lastaction =
		sql("SELECT timestamp_occurred
			FROM sess_actions
			WHERE id_user = :_iduser
			AND id_session = :_idsession
			ORDER BY timestamp_occurred DESC
			LIMIT 1
			", Array(
				'_iduser' => (int)$user['id'],
				'_idsession' => (int)$session['id']
			), 'fetch');

		$get_sessioninfo =
		sql("SELECT *
			FROM sess_actions
			WHERE id_user = :_iduser
			AND id_session = :_idsession
			ORDER BY timestamp_occurred DESC
			", Array(
				'_iduser' => (int)$user['id'],
				'_idsession' => (int)$session['id']
			));


		echo '<div class="itemset" data-idsession="'.(int)$session['id'].'">';
			echo '<div class="options">';
				echo '<span class="wait" data-idsession="'.(int)$session['id'].'">';
					echo svgicon('wait');
				echo '</span>';

				if($session['data_ipaddress'] == endecrypt(getip()) AND $session['data_useragent'] == endecrypt($useragent)) {
					echo '<span class="delete inactive" title="'.$lang['tooltips']['session-delete-own'].'">';
						echo svgicon('trash-disabled');
					echo '</span>';

				} else {
					echo '<a href="javascript:void(0)" class="delete color-red" data-idsession="'.(int)$session['id'].'" title="'.$lang['tooltips']['session-delete'].'">';
						echo svgicon('trash');
					echo '</a>';
				}

				echo '<a href="javascript:void(0)" class="show more" data-idsession="'.(int)$session['id'].'" title="'.$lang['tooltips']['session-info-show'].'">';
					echo svgicon('eye-opened');
				echo '</a>';

				echo '<a href="javascript:void(0)" class="show less" data-idsession="'.(int)$session['id'].'" title="'.$lang['tooltips']['session-info-hide'].'">';
					echo svgicon('eye-closed');
				echo '</a>';
			echo '</div>';


			echo '<div class="last-action">';
				if(empty($lastaction['timestamp_occurred'])) {
					echo '-';
				} else {
					echo date_($lastaction['timestamp_occurred'], 'datetime');
				}
			echo '</div>';

			echo '<div class="ipaddress">';
				echo endecrypt($session['data_ipaddress'], false);
			echo '</div>';

			echo '<div class="browser">';
				echo $useragentinfo->browser->toString();
			echo '</div>';

			echo '<div class="os">';
				echo $useragentinfo->os->toString();
			echo '</div>';
		echo '</div>';



		echo '<div class="moreinfo" data-idsession="'.(int)$session['id'].'">';
			echo '<div class="details">';
				echo '<b>'.$lang['words']['ip-address'].':</b> '.endecrypt($session['data_ipaddress'], false).'<br>';
				echo '<b>'.$lang['words']['browser'].':</b> '.$useragentinfo->browser->toString().'<br>';
				echo '<b>'.$lang['words']['os-full'].':</b> '.$useragentinfo->os->toString();
			echo '</div>';


			if(!empty($lastaction['timestamp_occurred'])) {
				echo '<div class="events">';
					echo '<b>'.$lang['words']['events'].':</b>';
					foreach($get_sessioninfo AS $sessioninfo) {
						echo '<div class="itemset">';
							echo '<div class="datetime">';
								echo date_($sessioninfo['timestamp_occurred'], 'datetime');
							echo '</div>';

							echo '<div class="action">';
								echo $arr_sessioncodes[$sessioninfo['data_code']]['icon'];
								echo $arr_sessioncodes[$sessioninfo['data_code']]['text'];
							echo '</div>';
						echo '</div>';
					}
				echo '</div>';
			}
		echo '</div>';
	}
	echo '</div>';

?>