<?php

	# Recommended use for crontab: every 10 minutes

	$server_root = '/path/to/html/keizai';



	if(!file_exists($server_root)) {
		echo 'Path in <code>$server_root</code> does not exists.';

	} else {
		require_once $server_root.'/site-config.php';
		require_once $server_root.'/functions/sql.php';

		try {
			$sql = null;
			if($sql === null) {
				$sql = new PDO('mysql:host='.$database_host.';dbname='.$database_name, $database_user, $database_pass);
				$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
		}

		catch(PDOException $e) {
			echo $e;
			exit;
		}



		sql("DELETE FROM loginattempts
			 WHERE timestamp_attempted < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 10 MINUTE))
			", Array());
	}

?>