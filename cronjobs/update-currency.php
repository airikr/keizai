<?php

	# Recommended use for crontab: every day

	$server_root = '/path/to/html/keizai';



	if(!file_exists($server_root)) {
		echo 'Path in <code>$server_root</code> does not exists.';

	} else {
		require_once $server_root.'/vendor/autoload.php';
		require_once $server_root.'/site-config.php';
		require_once $server_root.'/functions/sql.php';

		try {
			$sql = null;
			if($sql === null) {
				$sql = new PDO('mysql:host='.$database_host.';dbname='.$database_name, $database_user, $database_pass);
				$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
		}

		catch(PDOException $e) {
			echo $e;
			exit;
		}



		sql("TRUNCATE currencies", Array());

		foreach($arr_currencies AS $curr_f) {
			foreach($arr_currencies AS $curr_t) {
				if($curr_f != $curr_t) {
					$currency = json_decode(file_get_contents('https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/'.mb_strtolower($curr_f).'.json'), true);
					sql("INSERT INTO currencies(
							 data_currency_from,
							 data_currency_to,
							 data_rate,
							 timestamp_updated
						 )

						 VALUES(
							 :_currency_from,
							 :_currency_to,
							 :_rate,
							 :_updated
						 )
						", Array(
							'_currency_from' => $curr_f,
							'_currency_to' => $curr_t,
							'_rate' => $currency[mb_strtolower($curr_f)][mb_strtolower($curr_t)],
							'_updated' => strtotime($currency['date'])
						), 'insert');
				}
			}
		}
	}

?>