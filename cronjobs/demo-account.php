<?php

	# Recommended use for crontab: every hour

	$server_root = '/path/to/html/keizai';



	if(!file_exists($server_root)) {
		echo 'Path in <code>$server_root</code> does not exists.';

	} else {
		require_once $server_root.'/site-config.php';
		require_once $server_root.'/functions/endecrypt.php';
		require_once $server_root.'/functions/sql.php';
		require_once $server_root.'/functions/pw-random.php';

		$username = 'demo';
		$password = 'demo';
		$salt_1 = pw_random(10);
		$salt_2 = pw_random(10);



		try {
			$sql = null;
			if($sql === null) {
				$sql = new PDO('mysql:host='.$database_host.';dbname='.$database_name, $database_user, $database_pass);
				$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
		}

		catch(PDOException $e) {
			echo $e;
			exit;
		}



		$demo =
		sql("SELECT id, COUNT(is_demo) AS _exists
			 FROM users
			 WHERE is_demo IS NOT NULL
			", Array(), 'fetch');

		if($demo['_exists'] != 0) {
			sql("DELETE FROM users
				 WHERE id = :_iduser
				", Array(
					'_iduser' => (int)$demo['id']
				));
		}

		sql("INSERT INTO users(
				 data_username,
				 data_password,
				 data_saltedstring_1,
				 data_saltedstring_2,
				 data_theme,
				 is_demo
			 )

			 VALUES(
				 :_username,
				 :_password,
				 :_saltedstring_1,
				 :_saltedstring_2,
				 :_theme,
				 :_demo
			 )
			", Array(
				'_username' => endecrypt($username, true, true),
				'_password' => password_hash($password, PASSWORD_BCRYPT),
				'_saltedstring_1' => $salt_1,
				'_saltedstring_2' => $salt_2,
				'_theme' => 1,
				'_demo' => 1
			), 'insert');
	}

?>