<?php

	# Recommended use for crontab: each week

	$server_root = '/path/to/html/keizai';
	$dir_userfiles = '/path/to/files/keizai/users';



	if(!file_exists($server_root)) {
		echo 'Path in <code>$server_root</code> does not exists.';

	} elseif(!file_exists($dir_userfiles)) {
		echo 'Path in <code>$dir_userfiles</code> does not exists.';

	} else {
		$dir_languages = $server_root.'/languages';

		require_once $server_root.'/site-config.php';
		require_once $server_root.'/functions/sql.php';

		$smtp_enabled = ((empty($smtp_host) OR empty($smtp_email) OR empty($smtp_password) OR empty($smtp_port)) ? false : true);



		try {
			$sql = null;
			if($sql === null) {
				$sql = new PDO('mysql:host='.$database_host.';dbname='.$database_name, $database_user, $database_pass);
				$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
		}

		catch(PDOException $e) {
			echo $e;
			exit;
		}



		if($smtp_enabled == true) {
			require_once $server_root.'/vendor/autoload.php';
			require_once $server_root.'/functions/endecrypt.php';
			require_once $server_root.'/functions/sql.php';
			require_once $server_root.'/functions/simple-page.php';
			require_once $server_root.'/functions/send-email.php';
			require_once $server_root.'/functions/svg-icon.php';
			require_once $server_root.'/functions/date.php';
			require_once $server_root.'/functions/external-link.php';


			$get_inactiveusers =
			sql("SELECT id, data_email, data_saltedstring_1, data_saltedstring_2, data_language
				 FROM users
				 WHERE data_email IS NOT NULL
				 AND timestamp_lastactive < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 2 MONTH))
				", Array());

			foreach($get_inactiveusers AS $inactiveuser) {
				$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($inactiveuser['data_saltedstring_1'] . (int)$inactiveuser['id'] . $inactiveuser['data_saltedstring_2']).'.json'), true);
				require_once $dir_languages.'/'.(empty($inactiveuser['data_language']) ? 'se' : endecrypt($inactiveuser['data_language'], false)).'.php';

				send_email(endecrypt($inactiveuser['data_email'], false), $lang['emails']['account-deleted']['subject'], $lang['emails']['account-deleted']['html'], $lang['emails']['account-deleted']['nohtml']);
			}
		}



		$get_inusers =
		sql("SELECT id
			 FROM users
			 WHERE timestamp_lastactive IS NOT NULL
			 AND timestamp_lastactive < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 2 MONTH))
			 OR timestamp_lastactive IS NULL
			", Array());

		foreach($get_inusers AS $inuser) {
			sql("DELETE FROM users WHERE id = :_iduser", Array('_iduser' => (int)$inuser['id']));
		}
	}

?>