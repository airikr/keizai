<?php

	# Recommended use for crontab: each 3 days

	$server_root = '/path/to/html/keizai';
	$dir_userfiles = '/path/to/files/keizai/users';



	if(!file_exists($server_root)) {
		echo 'Path in <code>$server_root</code> does not exists.';

	} elseif(!file_exists($dir_userfiles)) {
		echo 'Path in <code>$dir_userfiles</code> does not exists.';

	} else {
		$dir_languages = $server_root.'/languages';

		require_once $server_root.'/site-config.php';

		$smtp_enabled = ((empty($smtp_host) OR empty($smtp_email) OR empty($smtp_password) OR empty($smtp_port)) ? false : true);



		if($smtp_enabled == true) {
			require_once $server_root.'/vendor/autoload.php';
			require_once $server_root.'/functions/endecrypt.php';
			require_once $server_root.'/functions/sql.php';
			require_once $server_root.'/functions/simple-page.php';
			require_once $server_root.'/functions/send-email.php';
			require_once $server_root.'/functions/svg-icon.php';
			require_once $server_root.'/functions/date.php';
			require_once $server_root.'/functions/external-link.php';

			try {
				$sql = null;
				if($sql === null) {
					$sql = new PDO('mysql:host='.$database_host.';dbname='.$database_name, $database_user, $database_pass);
					$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				}
			}

			catch(PDOException $e) {
				echo $e;
				exit;
			}



			$get_users =
			sql("SELECT id, data_email, data_saltedstring_1, data_saltedstring_2, data_language, check_email_expensesexpires
				 FROM users
				", Array());


			foreach($get_users AS $userinfo) {
				if(!empty($userinfo['check_email_expensesexpires'])) {
					$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($userinfo['data_saltedstring_1'] . (int)$userinfo['id'] . $userinfo['data_saltedstring_2']).'.json'), true);
					require_once $dir_languages.'/'.(empty($userinfo['data_language']) ? 'se' : endecrypt($userinfo['data_language'], false)).'.php';

					$get_expenses =
					sql("SELECT id_user
						 FROM items
						 WHERE id_user = :_iduser
						 AND is_expense IS NOT NULL
						 AND timestamp_date < UNIX_TIMESTAMP(CURDATE() + INTERVAL 7 DAY)
						 AND timestamp_payed IS NULL
						", Array(
							'_iduser' => (int)$userinfo['id']
						));

					foreach($get_expenses AS $expense) {
						send_email(endecrypt($userinfo['data_email'], false), $lang['emails']['expense-expires']['subject'], $lang['emails']['expense-expires']['html'], $lang['emails']['expense-expires']['nohtml']);
						exit;
					}
				}
			}



		} else {
			die('SMTP not enabled');
		}
	}

?>