<?php

	# Recommended use for crontab: every week

	$server_root = '/path/to/html/keizai';
	$dir_userfiles = '/path/to/files/keizai/users';



	if(!file_exists($server_root)) {
		echo 'Path in <code>$server_root</code> does not exists.';

	} elseif(!file_exists($dir_userfiles)) {
		echo 'Path in <code>$dir_userfiles</code> does not exists.';

	} else {
		$dir_languages = $server_root.'/languages';

		require_once $server_root.'/site-config.php';

		$smtp_enabled = ((empty($smtp_host) OR empty($smtp_email) OR empty($smtp_password) OR empty($smtp_port)) ? false : true);



		if($smtp_enabled == true) {
			require_once $server_root.'/vendor/autoload.php';
			require_once $server_root.'/functions/endecrypt.php';
			require_once $server_root.'/functions/sql.php';
			require_once $server_root.'/functions/simple-page.php';
			require_once $server_root.'/functions/send-email.php';
			require_once $server_root.'/functions/svg-icon.php';
			require_once $server_root.'/functions/date.php';
			require_once $server_root.'/functions/external-link.php';

			try {
				$sql = null;
				if($sql === null) {
					$sql = new PDO('mysql:host='.$database_host.';dbname='.$database_name, $database_user, $database_pass);
					$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				}
			}

			catch(PDOException $e) {
				echo $e;
				exit;
			}



			$count_inactiveusers =
			sql("SELECT COUNT(id)
				 FROM users
				 WHERE data_email IS NOT NULL
				 AND timestamp_lastactive < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 2 WEEK))
				 OR data_email IS NOT NULL
				 AND timestamp_lastactive < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 4 WEEK))
				 OR data_email IS NOT NULL
				 AND timestamp_lastactive < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 6 WEEK))
				", Array(), 'count');


			if($count_inactiveusers == 0) {
				die('No inactive users yet');

			} else {
				$get_inactiveusers =
				sql("SELECT id, data_email, data_saltedstring_1, data_saltedstring_2, data_language
					 FROM users
					 WHERE data_email IS NOT NULL
					", Array());

				foreach($get_inactiveusers AS $inactiveuser) {
					$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($inactiveuser['data_saltedstring_1'] . (int)$inactiveuser['id'] . $inactiveuser['data_saltedstring_2']).'.json'), true);
					require_once $dir_languages.'/'.(empty($inactiveuser['data_language']) ? 'se' : endecrypt($inactiveuser['data_language'], false)).'.php';

					send_email(endecrypt($inactiveuser['data_email'], false), $lang['emails']['inactive-user']['subject'], $lang['emails']['inactive-user']['html'], $lang['emails']['inactive-user']['nohtml']);
				}
			}



		} else {
			die('SMTP not enabled');
		}
	}

?>