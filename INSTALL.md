# Requirements
You need to have an SQL server such as MariaDB and PHP 8 or above.

# Installation
1. Download the files from Codeberg.
2. Change the following lines in `site-config.php`: [45-48](https://codeberg.org/airikr/Keizai/src/branch/main/site-config.php#L45-L48), [54-59](https://codeberg.org/airikr/Keizai/src/branch/main/site-config.php#L54-L59) and [64-67](https://codeberg.org/airikr/Keizai/src/branch/main/site-config.php#L64-L67). If you will have Keizai linked to your own domain, change the value in [line 7](https://codeberg.org/airikr/Keizai/src/branch/main/site-config.php#L7).
3. Make sure that the website can read and write the folder in the `$dir_userfiles` variable.
4. Open the terminal and go to the path where `keizai` are (example: `/var/www/keizai`). Now run the following commands to install necessary functions: `npm install` & `composer install`.
5. Visit `localhost/keizai` and everything should work without problems.
6. Optional: add the following in crontab:

```
0 0 1 * *     /usr/bin/php /var/www/html/keizai/cronjobs/delete-inactive-users.php
0 0 * * */3   /usr/bin/php /var/www/html/keizai/cronjobs/email-expensesexpires.php
0 0 * * 0     /usr/bin/php /var/www/html/keizai/cronjobs/email-inactiveusers.php
*/10 * * * *  /usr/bin/php /var/www/html/keizai/cronjobs/reset-loginattempts.php
0 0 * * *     /usr/bin/php /var/www/html/keizai/cronjobs/update-currency.php
```

7. Go to `localhost/keizai` or to the domain you have the website on and create your first account. Enjoy :)

# Updating
The updating process are identical with the installation process.

1. Download the ZIP-file from Codeberg.
2. Make a copy of `site-config.php` and the `cronjobs` folder. If you don't make a backup of the `cronjobs` folder, you need to change the variables containing the path to the files. Please be aware that we might have also changed these files.
3. Extract the newly downloaded files to the folder of Keizai and replace everything.
4. Move or copy back the backed up files and replace them with the newly extracted files.
5. Done.