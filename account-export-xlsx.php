<?php

	require_once 'vendor/autoload.php';
	require_once 'site-settings.php';

	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

	function cmp($a, $b){return strcmp($a[0], $b[0]);}
	function cmp_e($a, $b){return strcmp($a[1], $b[1]);}



	# Log the action so the user can review it afterwards
	# log_action() is a function that can be located in `functions/log-action.php`
	log_action(
		'exported data (xlsx)',
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		1
	);



	$userdata =
	sql("SELECT data_sum_income, data_sum_balance, data_sum_savings, data_currency
		 FROM users
		 WHERE id = :_iduser
		", Array(
			'_iduser' => (int)$user['id']
		), 'fetch');

	$get_expenses =
	sql("SELECT data_name, data_sum, data_currency, check_expense_payed, timestamp_date
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_expense IS NOT NULL
		 AND timestamp_date >= '".($period_start_empty == false ? $period_start : ($period_incomeday_empty == false ? $period_incomeday : null))."'
		 AND timestamp_date < '".($period_end_empty == false ? $period_end : ($period_incomeday_empty == false ? $period_incomeday_nextmonth : null))."'
		", Array(
			'_iduser' => (int)$user['id']
		));

	$get_debts =
	sql("SELECT data_name, data_sum, data_currency, timestamp_date
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_debt IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$get_loans =
	sql("SELECT data_name, data_sum, data_currency, timestamp_date
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_loan IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$get_currencies =
	sql("SELECT *
		 FROM currencies
		 WHERE data_currency_to = :_to
		 ORDER BY data_currency_from
		", Array(
			'_to' => endecrypt($user['data_currency'], false)
		));



	$arr_expenses = [];
	$arr_expenses_othercurr = [];
	foreach($get_expenses AS $expense) {
		$rate =
		sql("SELECT data_rate
			 FROM currencies
			 WHERE data_currency_from = :_from
			 AND data_currency_to = :_to
			", Array(
				'_from' => endecrypt($expense['data_currency'], false),
				'_to' => endecrypt($user['data_currency'], false)
			), 'fetch');

		$is_diffcurrency = false;
		if(endecrypt($expense['data_currency'], false) != endecrypt($user['data_currency'], false)) {
			$is_diffcurrency = true;
		}

		$arr_expenses[] = [
			(empty($expense['check_expense_payed']) ? '' : 'Bet'),
			endecrypt($expense['data_name'], false),
			format_number(($is_diffcurrency == false ? endecrypt($expense['data_sum'], false) : (endecrypt($expense['data_sum'], false) * $rate['data_rate'])), 2, '.', ''),
			mb_strtoupper(endecrypt($user['data_currency'], false)),
			($is_diffcurrency == false ? '' : format_number(endecrypt($expense['data_sum'], false), 2, '.', '')),
			($is_diffcurrency == false ? '' : mb_strtoupper(endecrypt($expense['data_currency'], false)))
		];
	}

	$arr_debts = [];
	foreach($get_debts AS $debt) {
		$arr_debts[] = [
			endecrypt($debt['data_name'], false),
			format_number(endecrypt($debt['data_sum'], false), 2, '.', '')
		];
	}

	$arr_loans = [];
	foreach($get_loans AS $loan) {
		$arr_loans[] = [
			endecrypt($loan['data_name'], false),
			format_number(endecrypt($loan['data_sum'], false), 2, '.', '')
		];
	}

	$arr_currencies = [];
	foreach($get_currencies AS $currency) {
		$arr_currencies[] = [
			$currency['data_currency_from'],
			$currency['data_rate']
		];
	}

	usort($arr_expenses, 'cmp_e');
	usort($arr_debts, 'cmp');
	usort($arr_loans, 'cmp');
	usort($arr_currencies, 'cmp');

	$formatcode = '# ##0.00';
	$column_expenses_1 = 'A';
	$column_expenses_2 = 'B';
	$column_expenses_3 = 'C';
	$column_expenses_4 = 'D';
	$column_expenses_5 = 'E';
	$column_expenses_6 = 'F';
	$column_debts_1 = 'H';
	$column_debts_2 = 'I';
	$column_loans_1 = 'K';
	$column_loans_2 = 'L';
	$column_other_1 = 'N';
	$column_other_2 = 'O';
	$column_other_3 = 'P';



	$spreadsheet = new Spreadsheet();
	$sheet_1 = $spreadsheet->getActiveSheet();
	$sheet_1->setTitle('Keizai')->getTabColor()->setRGB('FF0000');
	#$sheet_1 = $spreadsheet->setActiveSheetIndexByName('Sheet1');
	$spreadsheet->getDefaultStyle()->getFont()->setName('Arial');

	#$spreadsheet->getActiveSheet()->getStyle($column_expenses_1.':'.$column_expenses_6)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87ff87');
	#$spreadsheet->getActiveSheet()->getStyle($column_debts_1.':'.$column_debts_2)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f789ff');
	#$spreadsheet->getActiveSheet()->getStyle($column_loans_1.':'.$column_loans_2)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ffc885');

	$sheet_1->getColumnDimension($column_expenses_1)->setWidth(5);
	$sheet_1->getColumnDimension($column_expenses_2)->setWidth(40);
	$sheet_1->getColumnDimension($column_expenses_3)->setWidth(15);
	$sheet_1->getColumnDimension($column_expenses_4)->setWidth(6);
	$sheet_1->getColumnDimension($column_expenses_5)->setWidth(15);
	$sheet_1->getColumnDimension($column_expenses_6)->setWidth(6);
	$sheet_1->getColumnDimension($column_debts_1)->setWidth(30);
	$sheet_1->getColumnDimension($column_debts_2)->setWidth(15);
	$sheet_1->getColumnDimension($column_loans_1)->setWidth(30);
	$sheet_1->getColumnDimension($column_loans_2)->setWidth(15);
	$sheet_1->getColumnDimension($column_other_1)->setWidth(30);
	$sheet_1->getColumnDimension($column_other_2)->setWidth(15);
	$sheet_1->getColumnDimension($column_other_3)->setWidth(6);

	$sheet_1->getStyle($column_expenses_3.':'.$column_expenses_3)->getNumberFormat()->setFormatCode($formatcode);
	$sheet_1->getStyle($column_expenses_5.':'.$column_expenses_5)->getNumberFormat()->setFormatCode($formatcode);
	$sheet_1->getStyle($column_debts_2.':'.$column_debts_2)->getNumberFormat()->setFormatCode($formatcode);
	$sheet_1->getStyle($column_loans_2.':'.$column_loans_2)->getNumberFormat()->setFormatCode($formatcode);
	$sheet_1->getStyle($column_other_2.':'.$column_other_2)->getNumberFormat()->setFormatCode($formatcode);


	$expenses_rich = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
	$expenses = $expenses_rich->createTextRun($lang['words']['expenses'])->getFont()->setBold(true);
	$sheet_1->getCell($column_expenses_2.'1')->setValue($expenses_rich);
	$sheet_1->fromArray($arr_expenses, null, $column_expenses_1.'2');


	$debts_rich = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
	$debts = $debts_rich->createTextRun($lang['words']['debts'])->getFont()->setBold(true);
	$sheet_1->getCell($column_debts_1.'1')->setValue($debts_rich);
	$sheet_1->fromArray($arr_debts, null, $column_debts_1.'2');


	$loans_rich = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
	$loans = $loans_rich->createTextRun($lang['words']['loans'])->getFont()->setBold(true);
	$sheet_1->getCell($column_loans_1.'1')->setValue($loans_rich);
	$sheet_1->fromArray($arr_loans, null, $column_loans_1.'2');


	$exported_rich = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
	$exported = $exported_rich->createTextRun($lang['words']['exported'])->getFont()->setBold(true);
	$sheet_1->getCell($column_other_1.'4')->setValue($exported_rich);

	$sheet_1->setCellValue($column_other_1.'5', $lang['words']['date']);
	$sheet_1->setCellValue($column_other_2.'5', date('Y-m-d'));
	$sheet_1->setCellValue($column_other_1.'6', $lang['words']['time']);
	$sheet_1->setCellValue($column_other_2.'6', date('H:i:s'));


	$overview_rich = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
	$overview = $overview_rich->createTextRun($lang['words']['overview'])->getFont()->setBold(true);
	$sheet_1->getCell($column_other_1.'8')->setValue($overview_rich);

	$sheet_1->setCellValue($column_other_1.'9', $lang['words']['sums']['income']);
	$sheet_1->setCellValue($column_other_2.'9', (empty(endecrypt($userdata['data_sum_income'], false)) ? '0.00' : endecrypt($userdata['data_sum_income'], false)));
	$sheet_1->setCellValue($column_other_3.'9', mb_strtoupper(endecrypt($userdata['data_currency'], false)));

	$sheet_1->setCellValue($column_other_1.'10', $lang['words']['sums']['balance']);
	$sheet_1->setCellValue($column_other_2.'10', (empty($userdata['data_sum_balance']) ? '0.00' : endecrypt($userdata['data_sum_balance'], false)));
	$sheet_1->setCellValue($column_other_3.'10', mb_strtoupper(endecrypt($userdata['data_currency'], false)));

	$sheet_1->setCellValue($column_other_1.'11', $lang['words']['sums']['savings']);
	$sheet_1->setCellValue($column_other_2.'11', (empty($userdata['data_sum_savings']) ? '0.00' : endecrypt($userdata['data_sum_savings'], false)));
	$sheet_1->setCellValue($column_other_3.'11', mb_strtoupper(endecrypt($userdata['data_currency'], false)));

	$sheet_1->setCellValue($column_other_1.'12', $lang['words']['not-payed-expenses']);
	$sheet_1->setCellValue($column_other_2.'12', '=SUM('.$column_expenses_3.':'.$column_expenses_3.')-SUMIF('.$column_expenses_1.':'.$column_expenses_1.', "Bet", '.$column_expenses_3.':'.$column_expenses_3.')');
	$sheet_1->setCellValue($column_other_3.'12', mb_strtoupper(endecrypt($userdata['data_currency'], false)));

	$sheet_1->setCellValue($column_other_1.'13', $lang['words']['sums']['balance-afterexpenses']);
	$sheet_1->setCellValue($column_other_2.'13', '=IF('.$column_other_2.'10=0.00, SUM('.$column_other_2.'9-'.$column_other_2.'12), SUM('.$column_other_2.'10-'.$column_other_2.'12))');
	$sheet_1->setCellValue($column_other_3.'13', mb_strtoupper(endecrypt($userdata['data_currency'], false)));

	$sheet_1->setCellValue($column_other_1.'15', $lang['words']['expenses']);
	$sheet_1->setCellValue($column_other_2.'15', '=SUM('.$column_expenses_3.':'.$column_expenses_3.')');
	$sheet_1->setCellValue($column_other_3.'15', mb_strtoupper(endecrypt($userdata['data_currency'], false)));

	$sheet_1->setCellValue($column_other_1.'16', $lang['words']['debts']);
	$sheet_1->setCellValue($column_other_2.'16', '=SUM('.$column_debts_2.':'.$column_debts_2.')');
	$sheet_1->setCellValue($column_other_3.'16', mb_strtoupper(endecrypt($userdata['data_currency'], false)));

	$sheet_1->setCellValue($column_other_1.'17', $lang['words']['loans']);
	$sheet_1->setCellValue($column_other_2.'17', '=SUM('.$column_loans_2.':'.$column_loans_2.')');
	$sheet_1->setCellValue($column_other_3.'17', mb_strtoupper(endecrypt($userdata['data_currency'], false)));


	$currencies_rich = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
	$currencies = $currencies_rich->createTextRun($lang['words']['currency-exchange-from'].' '.mb_strtoupper(endecrypt($user['data_currency'], false)))->getFont()->setBold(true);
	$sheet_1->getCell($column_other_1.'19')->setValue($currencies_rich);
	$sheet_1->fromArray($arr_currencies, null, $column_other_1.'20');


	$sheet_1->setCellValue($column_other_1.'30', (endecrypt($user['data_language'], false) == 'se' ? 'Valutaomvandlaren är inte färdigutvecklad.' : 'The currency converter is not fully developed.'));



	$file = date('Y-m-d, Hi').' - '.$site_title.' export ('.endecrypt($user['data_username'], false, true).').xlsx';
	$writer = new Xlsx($spreadsheet);
	$writer->save($file);



	# Download the outputted data
	header('Content-disposition: attachment; filename="'.$file.'"');
	header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Length: '.filesize($file));
	header('Content-Transfer-Encoding: binary');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');

	ob_clean();
	flush();

	readfile($file);
	unlink($file);

?>