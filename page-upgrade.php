<?php

	require_once 'site-header.php';







	echo '<section id="upgrade">';
		if($lang['metadata']['language'] == 'se') {
			echo '<h1>Ny krypteringsmetod</h1>';
			echo '<p>Vi har bytt krypteringsmetod och för att du ska kunna fortsätta använda ditt konto här på '.$site_title.', måste du uppgradera ditt konto till den nya krypteringsmetoden.</p>';
			echo '<p>Anledningen till bytet är att få bättre funktionalitet och säkerhet.</p>';

			echo '<a href="javascript:void(0)" class="continue">';
				echo 'Uppgradera ditt konto';
			echo '</a>';

			echo '<div class="working">';
				echo '<div class="step-1">';
					echo '<span class="wait">'.svgicon('wait').'</span>';
					echo '<span class="success color-green">'.svgicon('tick').'</span>';

					echo '<span class="active">Uppgraderar dina utgiftskonton</span>';
				echo '</div>';

				echo '<div class="step-2">';
					echo '<span class="empty"></span>';
					echo '<span class="wait">'.svgicon('wait').'</span>';
					echo '<span class="success color-green">'.svgicon('tick').'</span>';

					echo '<span class="inactive">Uppgraderar dina utgifter</span>';
					echo '<span class="active">Uppgraderar dina utgifter</span>';
				echo '</div>';

				echo '<div class="step-3">';
					echo '<span class="empty"></span>';
					echo '<span class="wait">'.svgicon('wait').'</span>';
					echo '<span class="success color-green">'.svgicon('tick').'</span>';

					echo '<span class="inactive">Uppgraderar dina budgetar</span>';
					echo '<span class="active">Uppgraderar dina budgetar</span>';
				echo '</div>';

				echo '<div class="step-4">';
					echo '<span class="empty"></span>';
					echo '<span class="wait">'.svgicon('wait').'</span>';
					echo '<span class="success color-green">'.svgicon('tick').'</span>';

					echo '<span class="inactive">Uppgraderar dina kontouppgifter och anteckningar</span>';
					echo '<span class="active">Uppgraderar dina kontouppgifter och anteckningar</span>';
				echo '</div>';
			echo '</div>';



		} else {
			echo '<h1>New encryption method</h1>';
			echo '<p>dsa</p>';

			echo '<a href="javascript:void(0)" class="continue">';
				echo 'Upgrade your account';
			echo '</a>';
		}
	echo '</section>';







	require_once 'site-footer.php';

?>