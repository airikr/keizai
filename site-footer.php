<?php

					echo '</main>';
				echo '</section>';
			echo '</section>';


			if($filename != 'page-account-forgotpw.php') {
				echo '<footer>';
					echo '<div class="links">';
						echo '<a href="'.url('privacy-policy').'">'.$lang['footer']['links']['privacypolicy'].'</a>';
						# echo link_($lang['footer']['links']['donation'], 'https://donation.airikr.me');
						echo link_($lang['footer']['links']['source-code'], 'https://codeberg.org/airikr/Keizai');
					echo '</div>';


					echo '<div class="text">';
						echo $lang['footer']['text']['made-with-love'];
					echo '</div>';
				echo '</footer>';
			}



		echo '</body>';
	echo '</html>';



	echo '<script type="text/javascript" src="'.url($dir_js.'/jquery.min.js').'"></script>';
	echo '<script type="text/javascript" src="'.url($dir_js.'/main'.($minified == true ? '.min.js' : '.js?'.time())).'"></script>';
	echo (!file_exists($dir_js_pages.'/'.str_replace('.php', '', $filename).'.js') ? null : '<script type="text/javascript" src="'.url($dir_js_pages . ($minified == true ? '/minified' : '').'/'.str_replace('.php', '', $filename) . ($minified == true ? '.min.js' : '.js?'.time())).'"></script>');

	if($is_loggedin == true) {
		echo '<script type="text/javascript" src="'.url('node_modules/autosize/dist/autosize.min.js').'"></script>';
		echo '<script type="text/javascript" src="'.url('node_modules/mathjs/lib/browser/math.js').'"></script>';
		echo '<script type="text/javascript" src="'.url('node_modules/kjua/dist/kjua.min.js').'"></script>';
		echo '<script type="text/javascript" src="'.url($dir_js.'/jquery.number.min.js').'"></script>';
		echo '<script type="text/javascript" src="'.url('node_modules/sortablejs/Sortable.min.js').'"></script>';
		echo '<script type="text/javascript" src="'.url('node_modules/html2canvas/dist/html2canvas.min.js').'"></script>';
		/*echo '<script src="https://unpkg.com/@popperjs/core@2"></script>';
		echo '<script src="https://unpkg.com/tippy.js@6"></script>';*/
	}

?>