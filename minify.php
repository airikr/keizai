<?php

	# Include the required file
	require_once 'site-settings.php';

	use MatthiasMullie\Minify;



	# 
	$arr_singlefiles = [
		'desktop.css',
		'portable.css',
		'noscript.css',
		'main.js'
	];


	# Minify all the CSS files for the pages
	foreach(glob('css/pages/*.css') AS $cssp) {
		$fileinfo = pathinfo($cssp);
		$minify = new Minify\CSS($cssp);
		$minify->minify('css/pages/minified/'.$fileinfo['filename'].'.min.css');
	}

	# Minify all the CSS files for the themes
	foreach(glob('css/themes/*.css') AS $csst) {
		$fileinfo = pathinfo($csst);
		$minify = new Minify\CSS($csst);
		$minify->minify('css/themes/minified/'.$fileinfo['filename'].'.min.css');
	}

	# Minofy all the JS files for the pages
	foreach(glob('js/pages/*.js') AS $jsp) {
		$fileinfo = pathinfo($jsp);
		$minify = new Minify\JS($jsp);
		$minify->minify('js/pages/minified/'.$fileinfo['filename'].'.min.js');
	}


	# Loop through the previous mentioned array
	foreach($arr_singlefiles AS $singlefile) {
		$fileinfo = pathinfo($singlefile);

		if($fileinfo['extension'] == 'css') {
			$minify = new Minify\CSS('css/'.$fileinfo['filename'].'.css');
			$minify->minify('css/'.$fileinfo['filename'].'.min.css');

		} elseif($fileinfo['extension'] == 'js') {
			$minify = new Minify\JS('js/'.$fileinfo['filename'].'.js');
			$minify->minify('js/'.$fileinfo['filename'].'.min.js');
		}

	}


	# Redirect the visitor or user
	header("Location: ".url(''));
	exit;

?>