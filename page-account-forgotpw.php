<?php

	require_once 'site-header.php';







	echo '<section id="forgot-pw">';
		echo '<h1>'.$lang['titles']['new-password'].'</h1>';

		echo '<div class="msg"></div>';



		echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate>';
			echo '<input type="hidden" name="hidden-email" value="'.safetag($_GET['ema']).'">';

			echo '<div class="item">';
				echo '<div class="label">';
					echo $lang['forms']['username'];
				echo '</div>';

				echo '<div class="field">';
					echo '<div class="icon">'.svgicon('user').'</div>';
					echo '<input type="text" name="field-username" aria-label="username">';
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo $lang['forms']['passwords']['new'];
				echo '</div>';

				echo '<div class="field">';
					echo '<div class="icon">'.svgicon('password').'</div>';
					echo '<input type="password" name="field-password-new" aria-label="new password">';
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo $lang['forms']['passwords']['repeat'];
				echo '</div>';

				echo '<div class="field">';
					echo '<div class="icon">'.svgicon('password').'</div>';
					echo '<input type="password" name="field-password-repeat" aria-label="repeat password">';
				echo '</div>';
			echo '</div>';



			echo '<div class="button">';
				echo '<input type="submit" name="button-finish" value="'.$lang['forms']['buttons']['finish'].'">';
			echo '</div>';
		echo '</form>';
	echo '</section>';







	require_once 'site-footer.php';

?>