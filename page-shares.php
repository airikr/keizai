<?php

	if(isset($_GET['act']) AND $_GET['act'] == 'delete') {

		require_once 'site-settings.php';

		sql("DELETE FROM shares
			 WHERE id = :_iditem
			", Array(
				'_iditem' => (int)safetag($_GET['idi'])
			));

		header("Location: ".url('shares'));
		exit;



	} else {

		require_once 'site-header.php';



		if($config_solomember == false) {
			$count_shares_from =
			sql("SELECT COUNT(id_user)
				 FROM shares
				 WHERE id_user = :_iduser
				", Array(
					'_iduser' => (int)$user['id']
				), 'count');

			if($count_shares_from != 0) {
				$get_shares_from =
				sql("SELECT *
					 FROM shares
					 WHERE id_user = :_iduser
					 ORDER BY timestamp_shared DESC
					", Array(
						'_iduser' => (int)$user['id']
					));
			}


			$count_shares_to =
			sql("SELECT COUNT(id_user)
				 FROM shares
				 WHERE id_user_with = :_iduser
				", Array(
					'_iduser' => (int)$user['id']
				), 'count');

			if($count_shares_to != 0) {
				$get_shares_to =
				sql("SELECT *
					 FROM shares
					 WHERE id_user_with = :_iduser
					 ORDER BY timestamp_shared DESC
					", Array(
						'_iduser' => (int)$user['id']
					));
			}
		}







		echo '<section id="shares">';
			if($config_solomember == true) {
				echo '<div class="no-permissions">';
					echo '<h1>'.$lang['titles']['no-permissions'].'</h1>';

					foreach($lang['no-permissions'] AS $content) {
						echo $Parsedown->text($content);
					}
				echo '</div>';



			} else {
				echo '<h1>'.$lang['titles']['shares'].'</h1>';


				echo '<h2 class="first">';
					echo $lang['subtitles']['from-you'].' ('.format_number($count_shares_from, 0, '', ' ').')';
				echo '</h2>';

				if($count_shares_from == 0) {
					echo '<div class="message">';
						echo $lang['messages']['no-items'];
					echo '</div>';


				} else {
					echo '<div class="list">';

						echo '<div class="items">';
							echo '<div class="head">';
								echo '<div class="options"></div>';
								echo '<div class="item-type">'.$lang['words']['item-type'].'</div>';
								echo '<div class="item">'.$lang['words']['item'].'</div>';
								echo '<div class="shared">'.$lang['words']['shared'].'</div>';
								echo '<div class="username">'.$lang['words']['shared-to'].'</div>';
							echo '</div>';


							foreach($get_shares_from AS $share_f) {
								$item_f =
								sql("SELECT *
									FROM items
									WHERE id = :_iditem
									", Array(
										'_iditem' => (int)$share_f['id_item']
									), 'fetch');

								$usershare_f =
								sql("SELECT data_username
									FROM users
									WHERE id = :_iduser
									", Array(
										'_iduser' => (int)$share_f['id_user_with']
									), 'fetch');


								if(!empty($item_f['is_expense'])) {
									$type = $lang['words']['expense'];
									$object = 'expense';

								} elseif(!empty($item_f['is_debt'])) {
									$type = $lang['words']['debt'];
									$object = 'debt';

								} elseif(!empty($item_f['is_loan'])) {
									$type = $lang['words']['loan'];
									$object = 'loan';
								}



								echo '<div class="body">';
									echo '<div class="options">';
										echo '<a href="'.url('shares/delete-id:'.(int)$share_f['id']).'" class="color-red" onClick="return confirm(\'Du kommer inte kunna ångra dig, om du väljer att fortsätta.\')">';
											echo svgicon('trash');
										echo '</a>';
									echo '</div>';

									echo '<div class="item-type">';
										echo $type;
									echo '</div>';

									echo '<div class="item">';
										echo '<a href="'.url($object.'s#'.$object.'-'.(int)$item_f['id']).'">';
											echo endecrypt($item_f['data_name'], false);
										echo '</a>';
									echo '</div>';

									echo '<div class="shared">';
										echo date_($share_f['timestamp_shared'], 'datetime');
									echo '</div>';

									echo '<div class="username">';
										echo endecrypt($usershare_f['data_username'], false, true);
									echo '</div>';
								echo '</div>';
							}

						echo '</div>';

					echo '</div>';
				}







				echo '<h2>';
					echo $lang['subtitles']['to-you'].' ('.format_number($count_shares_to, 0, '', ' ').')';
				echo '</h2>';

				if($count_shares_to == 0) {
					echo '<div class="message">';
						echo $lang['messages']['no-items'];
					echo '</div>';


				} else {
					echo '<div class="list">';

						echo '<div class="items">';
							echo '<div class="head">';
								echo '<div class="options"></div>';
								echo '<div class="item-type">'.$lang['words']['item-type'].'</div>';
								echo '<div class="item">'.$lang['words']['item'].'</div>';
								echo '<div class="shared">'.$lang['words']['shared'].'</div>';
								echo '<div class="username">'.$lang['words']['shared-from'].'</div>';
							echo '</div>';


							foreach($get_shares_to AS $share_t) {
								$item_t =
								sql("SELECT *
									FROM items
									WHERE id = :_iditem
									", Array(
										'_iditem' => (int)$share_t['id_item']
									), 'fetch');

								$usershare_t =
								sql("SELECT id, data_username, data_saltedstring_1, data_saltedstring_2, data_currency
									FROM users
									WHERE id = :_iduser
									", Array(
										'_iduser' => (int)$share_t['id_user']
									), 'fetch');

								$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($usershare_t['data_saltedstring_1'] . (int)$usershare_t['id'] . $usershare_t['data_saltedstring_2']).'.json'), true);


								if(!empty($item_t['is_expense'])) {
									$type = $lang['words']['expense'];
									$object = 'expense';

								} elseif(!empty($item_t['is_debt'])) {
									$type = $lang['words']['debt'];
									$object = 'debt';

								} elseif(!empty($item_t['is_loan'])) {
									$type = $lang['words']['loan'];
									$object = 'loan';
								}



								echo '<div class="body">';
									echo '<div class="options">';
										echo '<a href="'.url('shares/delete-id:'.(int)$share_t['id']).'" class="color-red" onClick="return confirm(\'Om du väljer att fortsätta, måste användaren dela om objektet till dig.\')">';
											echo svgicon('trash');
										echo '</a>';
									echo '</div>';

									echo '<div class="item-type">';
										echo $type;
									echo '</div>';

									echo '<div class="item">';
										echo (empty($share_t['is_read']) ? '<span class="new">NY</span>' : '');
										echo '<a href="'.url($object.'s/filter:shared#'.$object.'-'.(int)$item_t['id']).'">';
											echo endecrypt($item_t['data_name'], false);
										echo '</a>';
									echo '</div>';

									echo '<div class="shared">';
										echo date_($share_t['timestamp_shared'], 'datetime');
									echo '</div>';

									echo '<div class="username">';
										echo endecrypt($usershare_t['data_username'], false, true);
									echo '</div>';
								echo '</div>';


								sql("UPDATE shares
									SET is_read = '1'
									WHERE id_item = :_iditem
									", Array(
										'_iditem' => (int)$share_t['id_item']
									));
							}

						echo '</div>';

					echo '</div>';
				}
			}
		echo '</section>';







		require_once 'site-footer.php';

	}

?>