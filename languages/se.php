<?php

	$lang = [
		'metadata' => [
			'language' => 'se',
			'locale' => 'sv_SE',
			'description' => $site_title.' är en ekonomisk översiktsida som hjälper dig att hålla stenkoll på din ekonomi, samtidigt som tjänsten gynnar din integritet.',
			'pdf' => [
				'subject' => 'Ekonomisk översikt',
				'keywords' => [
					'översikt',
					'ekonomi',
					'utgifter',
					'skulder',
					'lån'
				]
			]
		],

		'modals' => [
			'remove-budget' => 'Du kommer inte kunna ångra dig, om du väljer att fortsätta.',
			'sign-out' => 'Du är på väg att loggas ut.',
			'delete-item' => 'Du är på väg att ta bort objektet.',
			'delete-item-account' => 'Du är på väg att ta bort kontot.\n\nAlla objekt som är länkade till kontot, kommer inte att raderas.',
			'delete-session' => 'Du kommer inte kunna ångra dig, om du väljer att fortsätta.',
			'delete-sessions' => 'Du är på väg att ta bort alla sessioner förutom din egna.',
			'delete-account' => 'Följande kommer att tas bort:\n- Krypteringsfil\n- Användarnamn\n- E-postadress (om angiven)\n- Lösenord\n- Tvåstegsverifiering\n- Sessioner\n- Händelser för sessioner\n- Utgifter\n- Skulder\n- Lån\n- Budget\n- Delningar\n- Anteckningar\n- Inkomstbelopp\n- Nuvarande belopp\n- Sparande\n- Period (börjar och slutar)\n- Språk\n- Valuta\n\nDu kommer inte kunna ångra dig!',
			'delete-data' => 'Du måste börja om från början om du inte exporterar all din data först, och sen återställer datan.\n\nFöljande data kommer att tas bort:\n- Utgifter\n- Skulder\n- Lån\n- Budget\n- Delningar\n- Anteckningar\n- Händelser för sessioner\n\nFöljande data kommer inte att tas bort:\n- Krypteringsfil\n- Användarnamn\n- E-postadress (om angiven)\n- Lösenord\n- Tvåstegsverifiering\n- Sessioner\n\nFöljande inställningar återställs:\n- Inkomstbelopp\n- Nuvarande belopp\n- Sparande\n- Period (börjar och slutar)\n- Språk\n- Valuta',
			'delete-tfa' => 'Du måste lägga till tvåstegsverifieringen igen, om du väljer att fortsätta.',
			'delete-share' => 'Du måste dela om objektet om du väljer att fortsätta.',
			'import' => 'Vänligen observera att alla dina eventuellt redan inlagda utgifter, skulder och lån kommer att raderas och ersättas med de i säkerhetskopian.\n\nOch följande kommer att ersättas med motsvariga uppgifter:\n- Användarnamn\n- E-postadress (om angiven)\n- Tvåstegsverifiering\n- Inkomst\n- Nuvarande belopp\n- Sparande\n- Period (börjar och slutar)\n- Språk\n- Valuta\n- Utgifter\n- Skulder\n- Lån\n- Budget\n- Delningar',
			'update-expirationdates' => 'Alla återkommande utgifter som inte har flyttats fram till den sparade perioden, kommer att flyttas fram, baserat på deras återkommande värden.',
			'signingout-soon' => [
				'# Du är inaktiv!',
				'Du har inte gjort något aktivt på webbsidan under 9 minuter och 30 sekunder.',
				'Om du inte [uppdaterar sidan]() kommer du snart att bli automatiskt utloggad.'
			]
		],

		'emails' => [
			'inactive-user' => [
				'subject' => 'Ditt konto anses som inaktivt',
				'html' => '<h1>Du har inte varit inloggad på ett tag</h1><p>Vi kommer att skicka 3 e-postmeddelanden till dig, innan kontot tas bort, en gång per varannan vecka.</p><p>2 månader efter senaste inloggningen, kommer ditt konto med tillhörande data att tas bort på grund av inaktivitet.</p><p>Borttagningen är permanent och kan inte ångras. Om du vill behålla din data, gå till <a href="'.$site_url.'">keizai.se</a> och logga in så fort som möjligt!</p><p>Du kommer att få ett e-postmeddelande när ditt konto är borttaget.</p>',
				'nohtml' => 'Du har inte varit inloggad på ett tag och vi skickar därför ett e-postmeddelande till dig som en påminnelse. Vi kommer att skicka ut 2 till e-postmeddelanden till dig, och om du har varit inaktiv under 2 månader efter senaste inloggningen, kommer ditt konto att raderas permanent. Om du vill behålla ditt konto, gå till keizai.se och logga in så snart som möjligt!'
			],
			'expense-expires' => [
				'subject' => 'Förfallodatumet är inom en vecka',
				'html' => '<h1>7 eller färre dagar kvar på en eller flera utgifter</h1><p>Vi vill göra dig uppmärksam om att du har en eller flera utgifter vars förfallodatum är inom 7 dagar. '.link_('Logga in', $site_url).' för och se vilka dessa är.</p>',
				'nohtml' => 'Vi vill göra dig uppmärksam om att du har en eller flera utgifter vars förfallodatum är inom 7 dagar. Logga in på '.$site_domain.' för och se vilka dessa är.'
			],
			'account-deleted' => [
				'subject' => 'Ditt konto har tagits bort',
				'html' => '<h1>Kontot togs bort på grund av inaktivitet</h1><p>Det har gått 2 månader sen din senaste inloggning och togs därför bort på grund av inaktivitet. Borttagningen är permanent och kan inte ångras.</p><p>Om du vill använda '.$site_title.' igen, gå till '.$site_domain.' och skapa ett nytt konto.</p>',
				'nohtml' => 'Kontot togs bort på grund av att du inte hade loggat in på det sen 2 månader tillbaka. Borttagningen är permanent och kan inte ångras. Om du vill använda '.$site_title.' igen, gå till '.$site_domain.' och skapa ett nytt konto.'
			]
		],

		'months' => [
			'january' => 'Januari',
			'february' => 'Februari',
			'march' => 'Mars',
			'april' => 'April',
			'may' => 'Maj',
			'june' => 'Juni',
			'july' => 'Juli',
			'august' => 'Augusti',
			'september' => 'September',
			'november' => 'November',
			'october' => 'Oktober',
			'december' => 'December'
		],

		'days-long' => [
			'monday' => 'Måndag',
			'tuesday' => 'Tisdag',
			'wednesday' => 'Onsdag',
			'thursday' => 'Torsdag',
			'friday' => 'Fredag',
			'saturday' => 'Lördag',
			'sunday' => 'Söndag'
		],

		'days-short' => [
			'mon' => 'Mån',
			'tue' => 'Tis',
			'wed' => 'Ons',
			'thu' => 'Tors',
			'fri' => 'Fre',
			'sat' => 'Lör',
			'sun' => 'Sön'
		],

		'aria-labels' => [
			'light-theme' => 'Byt till det ljusa temat',
			'dark-theme' => 'Byt till det mörka temat',
			'align-left' => 'Placera webbsidan till vänster',
			'align-center' => 'Placera webbsidan i mitten',
			'menu' => [
				'show-menu' => 'Visa menyn',
				'hide-menu' => 'Göm menyn',
				'overview' => 'Gå till översiktsidan',
				'expenses' => 'Visa alla tillagda utgifter',
				'debts' => 'Visa alla tillagda skulder',
				'loans' => 'Visa alla tillagda lån',
				'budget' => 'Visa alla tillagda budget',
				'notes' => 'Visa dina anteckningar',
				'shares' => 'Visa alla delningar som du har skickat och som du har fått',
				'settings' => 'Hantera bland annat dina kontoinställningar',
				'admin' => 'Visa bland annat administrativa inställningar',
				'logout' => 'Logga ut'
			]
		],

		'nav' => [
			'signed-out' => [
				'sign-in' => 'Logga in',
				'sign-up' => 'Registrera',
				'forgot-pw' => 'Återställ lösenord'
			],
			'signed-in' => [
				'overview' => 'Översikt',
				'expenses' => 'Utgifter',
				'debts' => 'Skulder',
				'loans' => 'Lån',
				'budget' => 'Budget',
				'notes' => 'Anteckningar',
				'shares' => 'Delningar',
				'settings' => 'Inställningar',
				'admin' => 'Admin',
				'sign-out' => 'Logga ut'
			],
			'sub' => [
				'create-directory' => 'Skapa ett nytt konto',
				'add-expense' => 'Lägg till en ny utgift',
				'add-debt' => 'Lägg till en ny skuld',
				'add-loan' => 'Lägg till ett nytt lån',
				'add-budget' => 'Lägg till en ny budget'
			],
			'filter' => [
				'all' => 'Alla',
				'period' => 'Period',
				'not-payed' => 'Obetalade',
				'payed' => 'Betalda',
				'extra' => 'Extra',
				'future' => 'Framtida',
				'recurrent' => 'Återkommande',
				'private' => 'Privatpersoner',
				'companies' => 'Företag',
				'loans-out' => 'Utlånat',
				'loans-in' => 'Lånat',
				'shared' => 'Delade med dig',
				'subscriptions' => 'Prenumerationer'
			]
		],

		'footer' => [
			'links' => [
				'privacypolicy' => 'Integritetspolicy',
				'donation' => 'Donation',
				'source-code' => 'Källkod'
			],
			'text' => [
				'made-with-love' => 'Skapad med '.svgicon('heart').' i Sverige'
			]
		],

		'types' => [
			'e-invoice' => 'E-faktura',
			'credit-card' => 'Kreditkort',
			'direct-debit' => 'Autogiro',
			'invoice' => 'Pappersfaktura',
			'swish' => 'Swish',
			'transfer' => 'Överföring',
			'invoice-email' => 'Faktura via e-post'
		],

		'categories' => [
			'amortization' => 'Amortering',
			'broadband' => 'Bredband',
			'electricity' => 'El',
			'insurance' => 'Försäkring',
			'rent' => 'Hyra',
			'loan' => 'Lån',
			'literature' => 'Litteratur',
			'pet-food' => 'Husdjursfoder',
			'pet-toys' => 'Husdjursleksaker',
			'pet-vet' => 'Veterinär',
			'garden' => 'Trädgård',
			'subscription' => 'Prenumeration',
			'travel' => 'Resa',
			'healthcare' => 'Sjukvård',
			'tech-n-stuff' => 'Teknik & prylar',
			'health-n-beauty' => 'Hälsa & skönhet',
			'telephony' => 'Telefoni',
			'entertainment' => 'Underhållning',
			'education' => 'Utbildning',
			'other' => 'Övrigt',
			'savings' => 'Sparande',
			'webhosting' => 'Webbtjänst',
			'shippping' => 'Frakt',
			'outlet' => 'Outlet',
			'fuel' => 'Bränsle',
			'clothing' => 'Kläder',
			'shoes' => 'Skor'
		],

		'sessioncodes' => [
			'saved-changes-budget' => 'Sparade ändringarna för en budget',
			'created-budget' => 'Skapade en budget',
			'updated-budget' => 'Uppdaterade en budget',
			'export-plaintext' => 'Exporterade datan i klartext till en JSON-fil',
			'export-encrypted' => 'Exporterade datan krypterad till en JSON-fil',
			'export-xlsx' => 'Exporterade datan till en XLSX-fil',
			'imported' => 'Importerade data',
			'updated-item' => 'Uppdaterade ett objekt',
			'updated-settings' => 'Uppdaterade inställningarna',
			'updated-password' => 'Uppdaterade lösenordet',
			'updated-notes' => 'Uppdaterade anteckningarna',
			'updated-account-expenses' => 'Uppdaterade ett konto för utgifter',
			'deleted-session' => 'Tog bort en session',
			'deleted-sessions' => 'Tog bort alla sessioner förutom ens egna',
			'deleted-data' => 'Tog bort data',
			'deleted-account-expenses' => 'Tog bort ett konto för utgifter',
			'removed-tfa' => 'Tog bort tvåstegsverifiering',
			'added-tfa' => 'Aktiverade tvåstegsverifiering',
			'created-account-expenses' => 'Skapade ett konto för utgifter',
			'created-item' => 'Skapade ett nytt objekt',
			'delete-data-failed' => 'Borttagning av data misslyckades',
			'remove-tfa-failed' => 'Borttagning av tvåstegsverifiering misslyckades',
			'signed-in' => 'Loggades in',
			'signed-out' => 'Loggades ut',
			'auto-signed-out' => 'Var inaktiv och loggades ut'
		],

		'titles' => [
			'shares' => 'Delningar',
			'expense' => 'Utgift',
			'expenses' => 'Utgifter',
			'debt' => 'Skuld',
			'debts' => 'Skulder',
			'loan' => 'Lån',
			'loans' => 'Lån',
			'new-password' => 'Ange ett nytt lösenord',
			'notes' => 'Anteckningar',
			'admin' => 'Adminpanel',
			'budget' => 'Budget',
			'add-budget' => 'Lägg till en ny budget',
			'edit-budget' => 'Ändra en budget',
			'no-permissions' => 'Inga rättigheter',
			'settings' => [
				'settings' => 'Inställningar',
				'sessions' => 'Sessioner',
				'manage-data' => 'Hantera din data'
			]
		],

		'subtitles' => [
			'users' => 'Användare',
			'currencies' => 'Valutor',
			'statistics' => 'Statistik',
			'next-period' => 'Nästa period',
			'from-you' => 'Från dig',
			'to-you' => 'Till dig',
			'notifications' => 'Aviseringar',
			'others' => 'Övrigt',
			'privacy' => 'Integritet',
			'appearance' => 'Utseende',
			'notes' => 'Anteckningar',
			'configurations' => 'Konfigurationer',
			'subscriptions' => 'Prenumerationer',
			'required' => 'Krävs',
			'expenses' => 'Utgifter',
			'debts' => 'Skulder',
			'loans' => 'Lån',
			'optional' => 'Valfria',
			'current-period' => 'Aktuell period',
			'not-payed' => 'Obetalda',
			'payed' => 'Betalda',
			'others' => 'Övrigt',
			'extra' => 'Extra',
			'future' => 'Framtida',
			'shared' => 'Delade',
			'share' => 'Dela',
			'recurrent' => 'Återkommande',
			'tfa' => 'Tvåstegsverifiering',
			'default-account' => 'Standardkontot',
			'settings' => [
				'account' => 'Konto',
				'economy' => 'Ekonomi',
				'period' => 'Period',
				'tfa' => 'Tvåstegsverifiering',
				'adaptation' => 'Anpassning',
				'import' => 'Importera',
				'options' => 'Alternativ'
			]
		],

		'forms' => [
			'move-recurrent-expenses' => 'Flytta fram återkommande utgifter',
			'checkboxes' => [
				'allow-weekends' => 'Jag får min inkomst under helger',
				'loan-out' => 'Det är jag som lånar ut',
				'expenses-expires' => 'Avisera mig när minst en utgift håller på att förfalla (experimentell)',
				'default-directory' => 'Kontot är ett standardkonto',
				'hidedate-expenses' => 'Göm förfallodatumet för utgifter',
				'hidedate-debts' => 'Göm datumet när skulder påbörjades',
				'hidedate-loans' => 'Göm lånedatumet för lån',
				'align-center' => 'Centrera webbsidan',
				'period-use-incomeday' => 'Använd inkomstdagen istället för en period',
				'recurrent-autoupdate' => 'Automatiskt ändra förfallodatumet till aktuell period',
				'recurrent-autoupdate-all' => 'Ändra för alla återkommande utgifter',
				'share-allow' => 'Tillåt andra användare att dela objekt till mig',
				'share' => [
					'number-payment' => 'Dela inbetalningsnumret',
					'number-ocr' => 'Dela OCR-numret',
					'number-phone' => 'Dela mobilnumret',
					'qrcodes' => 'Dela autogenererade QR-koder',
					'notes' => 'Dela anteckningar',
					'allow-edits' => 'Tillåt att mottagaren kan ändra objektet',
					'allow-deletion' => 'Tillåt att mottagaren kan ta bort objektet',
					'allow-markas-payed' => 'Tillåt att mottagaren kan markera utgiften som betald'
				],
				'expense' => [
					'payed' => 'Utgiften har blivit betald',
					'extra' => 'Utgiften är en extrautgift',
					'subscription' => 'Utgiften är en prenumeration',
					'recurrent' => 'Utgiften är återkommande',
					'correct-info' => 'Inbetalningsnumret och OCR-numret är korrekta'
				],
				'debt' => [
					'company' => 'Skulden är till ett företag',
					'paying' => 'Jag avbetalar för närvarande på skulden'
				],
				'descriptions' => [
					'share-qrcodes' => 'Delning av QR-koder kräver att du har antingen inbetalningsnumret och OCR-numret ikryssade, eller mobilnumret ikryssat.'
				]
			],
			'informations' => [
				'registration_email' => [
					'Att ange din e-postadress är valfritt, men den krävs vid återställning av lösenordet. Du kan alltid ändra eller ta bort adressen i dina kontoinställningar.'
				],
				'forgotten-pw' => [
					$site_title.' behöver ditt användarnamn för och få tillgång till dina krypteringsnycklar. Inte förrän då kan '.$site_title.' skicka e-postmeddelandet.'
				],
				'import' => [
					'Återställ data från en tidigare säkerhetskopia.'
				]
			]
		],

		'messages' => [
			'update-expenses-reload-page' => 'Flytta fram återkommande utgifter (spara och ladda om sidan först)',
			'update-expenses-fill-in' => 'Flytta fram återkommande utgifter (period ej angiven)',
			'version-uptodate' => 'Du använder den senaste versionen',
			'version-above-current' => 'Den lokala versionen är nyare än källan',
			'version-are-available' => 'Finns tillgänglig',
			'no-internet' => 'Kan inte ansluta till internet',
			'try-first' => 'Vill du prova '.$site_title.' först, innan du skapar ett konto? Ange i sådana fall <code>demo</code> som både användarnamn och lösenord.',
			'demo' => 'Du använder demo-kontot.',
			'debugmode-enabled' => 'Felsökningsläge är aktiverad',
			'budget-order-updating' => 'Uppdaterar, var god vänta...',
			'budget-order-error' => 'Ett fel uppstod - försök igen',
			'smtp-disabled' => 'SMTP är ej konfigurerat',
			'no-accounts' => 'Du har inte lagt till några konton än',
			'no-items' => 'Kunde inte hitta något',
			'no-explaination' => 'Kunde inte hitta någon förklaring',
			'no-expenses' => 'Du hade inte lagt in några utgifter',
			'no-debts' => 'Du hade inte lagt in några skulder',
			'no-loans' => 'Du hade inte lagt in några lån',
			'no-period' => 'Du har ingen satt någon period än',
			'choose-item' => 'Välj något från listan',
			'loading-item-details' => 'Hämtar detaljer om objektet...',
			'debugging' => 'Felsökning aktiverad. Kolla konsolen för mer info',
			'scan-qrcode-bank' => 'Öppna din bankapp och skanna QR-koden nedan när du ska överföra pengar. De betalningsuppgifter som du har angett kommer då att automatiskt fyllas i. Alla bankappar stödjer dock inte QR-kodskanning.',
			'scan-qrcode-bank-payed' => 'Utgiften har redan blivit betald. QR-koden är därmed gömd för att undvika fler betalningar. Inbetalningsnumret och OCR-numret är dock kvar.',
			'scan-qrcode-swish' => 'Öppna Swish-appen i din enhet, välj "Skanna" och skanna QR-koden nedan.',
			'scan-qrcode-swish-payed' => 'Utgiften har redan blivit betald. QR-koden är därmed gömd för att undvika fler betalningar. Telefonnumret är dock kvar.',
			'signing-out' => 'Loggar ut...',

			'move-recurrent-expenses' => [
				'updating' => 'Flyttar fram, vänligen vänta...',
				'updated' => 'Flytten slutförd',
				'noupdate' => 'Inget att flyttas fram'
			],

			'settings' => [
				'tfa-backup' => 'Spara textsträngen ovan på ett säkert ställe. Om du skulle förlora åtkomsten till dina engångskoder, måste du använda återställningskoden för att ta bort tvåstegsverifieringen.',
				'tfa-removed' => 'Tvåstegsverifieringen har inaktiverats',
				'tfa-removed-info' => 'Du behöver ladda om sidan om du vill återaktivera tvåstegsverifieringen igen.',
				'tfa-retry' => 'Något gick fel vid aktiveringen. Försök igen'
			],

			'forms' => [
				'working' => [
					'working' => 'Arbetar - var god vänta',
					'checking-deletion' => 'Kontrollerar borttagningen',
					'deleting-data' => 'Tar bort all data som är kopplat till dig',
					'deleting-yourdata' => 'Tar bort din data'
				],
				'info' => [
					'tfa-enabled' => 'Tvåstegsverifiering är aktiverad'
				],
				'errors' => [
					'other-error' => 'Ett generellt fel har uppstått',
					'fields-empty' => 'Vänligen fyll i de obligatoriska fälten först',
					'item-exists' => 'Du har redan lagt till ett objekt med samma uppgifter som dessa',
					'item-added' => 'Objektet har lagts till',
					'cant-delete-data' => 'Kunde inte ta bort data - försök igen',
					'cant-fetch-info' => 'Kunde inte hämta informationen',
					'email-notexists' => 'E-postadressen finns inte',
					'email-taken' => 'E-postadressen finns redan i systemet',
					'email-notvalid' => 'E-postadressen är inte giltig',
					'username-notexists' => 'Användarnamnet finns inte',
					'username-taken' => 'Användarnamnet finns redan i systemet',
					'passwords-notsame' => 'Lösenorden är inte samma',
					'password-incorrect' => 'Lösenordet är inte korrekt',
					'password-incorrect-current' => 'Ditt nuvarande lösenord är inte korrekt',
					'passwords-tooshort' => 'Det angivna lösenordet är för kort',
					'tfa-onlydigits' => 'Engångskoden får endast innehålla siffror',
					'tfa-incorrect' => 'Engångskoden är inte korrekt',
					'too-many-attempts' => 'För många inloggningsförsök',
					'resetcode-notexists' => 'Återställningskoden finns inte',
					'resetcode-incorrect' => 'Återställningskoden är inte korrekt',
					'resetcode-length' => 'Återställningskoden måste vara 20 tecken långt',
					'permission-denied' => 'Serverfel (PRM)',
					'account-default' => 'Du kan inte ta bort ett standardkonto',
					'account-exists' => 'Det finns redan ett konto med det namnet',
					'sum-onlydigits-goal' => 'Målbeloppet får endast innehålla siffror',
					'sum-onlydigits-permonth' => 'Månadsbeloppet får endast innehålla siffror',
					'sum-onlydigits-budget' => 'Beloppet får endast innehålla siffror',
					'sum-belowzero' => 'Du kan inte gå minus i budgeten',
					'sum-aboveonehundred' => 'Du kan inte gå över målbeloppet',
					'sum-percent-below1' => 'Du kan inte gå under 1%',
					'sum-percent-above100' => 'Du kan inte gå över 100%',
					'sum-invalid-nonegative' => 'Beloppet får inte vara negativt (inte gå under 0 kr)',
					'sum-aboveactual' => 'Beloppet är över objektets belopp',
					'sum-below5' => 'Beloppet får inte vara under 5',
					'period-wrongorder' => 'Startdatumet för perioden måste vara före slutdatumet',
					'period-missingdate-start' => 'Startdatumet för perioden fattas',
					'period-missingdate-end' => 'Slutdatumet för perioden fattas',
					'onlydigits-recurrence' => 'Upprepningen får endast innehålla siffror',
					'onlybetween-1-12-recurrence' => 'Upprepningstypen får endast vara mellan 1 och 12',
					'already-shared' => 'Du har redan delat objektet till användaren',
					'cant-share-to-self' => 'Du kan inte dela objektet till dig själv',
					'user-notexists' => 'Användaren existerar inte',
					'privacy-sharing-notallowed' => 'Mottagaren har valt att inte tillåta delningar',
					'file-upload' => [
						'serversize-ini' => 'Serverfel (INI)',
						'serversize-form' => 'Serverfel (FRM)',
						'partial' => 'Kunde inte ladda upp filen helt - försök igen',
						'nofile' => 'Vänligen välj en fil först',
						'notmpdir' => 'Serverfel (TMD)',
						'cantwrite' => 'Serverfel (PRM)',
						'extension' => 'Serverfel (EXT)',
						'unknown' => 'Okänt fel - försök igen'
					]
				],
				'done' => [
					'saved' => 'Dina ändringar har sparats',
					'saved-pw' => 'Dina ändringar har sparats, inklusive det nya lösenordet. Du loggas nu ut - var god vänta',
					'deleted-account' => 'Kontot har blivit borttaget - loggar ut dig',
					'item-added' => 'Objektet har lagts till - omdirigerar dig',
					'item-shared' => 'Objektet har delats till användaren',
					'password-updated' => 'Lösenordet har återställts - omdirigerar dig',
					'data-deleted' => 'Borttagningen lyckades - laddar om sidan',
					'data-imported' => 'Importeringen lyckades - omdirigerar dig',
					'signed-in' => 'Du har blivit inloggad - omdirigerar dig',
					'tfa-disabled' => 'Tvåstegsverifieringen har inaktiverats',
					'account-created' => 'Kontot har skapats - du kan nu logga in',
					'email-sent' => 'Ett e-postmeddelande har skickats',
					'account-created' => 'Kontot har skapats - omdirigerar dig',
					'account-deleted' => 'Kontot har tagits bort - omdirigerar dig',
					'account-saved' => 'Dina ändringar har sparats - omdirigerar dig',
					'budget-updated' => 'Budgeten har uppdaterats'
				]
			]
		],

		'tooltips' => [
			'take-screenshot' => 'Ta en skärmdump',
			'what-is-this' => 'Förklaring',
			'move-item' => 'Ändra placering',
			'shared-navigation-disabled' => 'Du kan inte använda navigeringen för ett delat objekt',
			'shared-navigation-disabled-solo' => 'Du har valt att tillåta max 1 användare',
			'notallowed-edits' => 'Du har inte rättigheter till att ändra objektet',
			'notallowed-deletion' => 'Du har inte rättigheter till att ta bort objektet',
			'object-remove' => 'Ta bort',
			'object-edit' => 'Ändra',
			'object-close' => 'Stäng',
			'object-share' => 'Dela',
			'object-info' => 'Info',
			'object-users' => 'Användare',
			'no-period' => 'Du har inte angett någon period än',
			'markas-payed' => 'Markera som betald',
			'markas-notpayed' => 'Markera som ej betald',
			'filter-by-date' => 'Visa alla objekt från detta datum',
			'show-details' => 'Visa detaljer om objektet',
			'item-have-notes' => 'Objektet har en anteckning',
			'item-has-been-shared' => 'Objektet har delats till en eller flera användare',
			'item-has-qrcode' => 'Objektet har en QR-kod',
			'export-data-plaintext' => 'Exportera all din data i klartext till en JSON-fil',
			'export-data-encrypted' => 'Exportera all din data krypterat till en JSON-fil',
			'export-data-xlsx' => 'Exportera all data till en XLSX-fil',
			'export-data-pdf' => 'Exportera till en PDF-fil (endast översikt)',
			'currencies-lastupdated' => 'Valutorna var senast uppdaterad detta datum',
			'delete-data' => 'Ta bort data som du har lagt till',
			'delete-account' => 'Ta bort ditt konto',
			'delete-share' => 'Ta bort delningen',
			'delete-budget' => 'Ta bort budgeten',
			'edit-budget' => 'Ändra budgeten',
			'edit-share' => 'Ändra delningen',
			'close' => 'Stäng',
			'session-delete' => 'Ta bort sessionen och logga ut enheten',
			'session-delete-own' => 'Du kan inte ta bort din egna session - logga ut istället',
			'session-info-show' => 'Visa mer information',
			'session-info-hide' => 'Göm informationen',
			'showall-expenses' => 'Visa alla utgifter',
			'showall-expenses-period' => 'Visa alla utgifter för den aktuella perioden',
			'showall-expenses-notpayed' => 'Visa alla obetalda utgifter',
			'showall-expenses-payed' => 'Visa alla betalda utgifter',
			'showall-expenses-extra' => 'Visa alla extrautgifter',
			'showall-expenses-future' => 'Visa alla framtida utgifter',
			'showall-expenses-recurrent' => 'Visa alla återkommande utgifter',
			'showall-expenses-shared' => 'Visa alla utgifter som har delats till dig',
			'showall-debts' => 'Visa alla skulder',
			'showall-debts-individuals' => 'Visa alla skulder till privatpersoner',
			'showall-debts-companies' => 'Visa alla företagsskulder',
			'showall-debts-shared' => 'Visa alla skulder som har delats till dig',
			'showall-loans' => 'Visa alla lån',
			'showall-loans-individuals' => 'Visa alla lån till och från privatpersoner',
			'showall-loans-companies' => 'Visa alla lån till företag',
			'showall-loans-shared' => 'Visa alla lån som har delats till dig',
			'showall-expenses-subscriptions' => 'Visa alla prenumerationer',
			'update-income' => 'Uppdatera inkomstbeloppet',
			'update-current' => 'Uppdatera det nuvarande beloppet',
			'update-savings' => 'Uppdatera sparandet',

			'filters' => [
				'by-type' => 'Visa alla objekt med denna typ',
				'by-category' => 'Visa alla objekt med denna kategori'
			]
		],

		'screenshots' => [
			'overview-balance' => 'Balans',
			'overview-calendar' => 'Översikt - Kalenderöversikt',
			'overview-expenses' => 'Översikt - Statistik för utgifter',
			'overview-subscriptions' => 'Översikt - Statistik för prenumerationer',
			'overview-debts' => 'Översikt - Statistik för skulder',
			'overview-loans' => 'Översikt - Statistik för lån',
			'overview-others' => 'Översikt - Statistik för övrigt',
			'overview-nextperiod' => 'Översikt - Statistik för nästa period'
		],

		'words' => [
			'active-subscriptions' => 'Prenumerationer',
			'monthly-cost' => 'Månadskostnad',
			'quarterly-cost' => 'Kvartalskostnad',
			'halfyearly-cost' => 'Halvårskostnad',
			'yearly-cost' => 'Årskostnad',
			'monthly-cost' => 'Månadskostnad',
			'is-paying' => 'Betalar',
			'is-company' => 'Företag',
			'estimated' => 'Beräknat',
			'read-more' => 'Läs mer',
			'sitename-meaning' => 'Ekonomi på japanska',
			'period' => 'Period',
			'browse' => 'Bläddra',
			'overview' => 'Översikt',
			'dark' => 'Mörkt',
			'light' => 'Ljust',
			'belongs-to-debt' => 'Tillhörande skuld',
			'tfa-short' => 'TFA',
			'user' => 'Användare',
			'recipient-pays' => 'Mottagaren betalar',
			'shared-from' => 'Delad från',
			'item-type' => 'Objekttyp',
			'item' => 'Objekt',
			'shared' => 'Delat',
			'shared-overview' => 'Delat från andra',
			'shared-to' => 'Delades till',
			'shared-from' => 'Delades från',
			'database-size' => 'Databasstorlek',
			'week-short' => 'V',
			'expense' => 'Utgift',
			'debt' => 'Skuld',
			'loan' => 'Lån',
			'month' => 'Månad',
			'week' => 'Vecka',
			'year' => 'År',
			'yes' => 'Ja',
			'no' => 'Nej',
			'expenses' => 'Utgifter',
			'debts' => 'Skulder',
			'loans' => 'Lån',
			'recurrence' => 'Upprepning',
			'at-least' => 'Minst',
			'optional' => 'Valfritt',
			'account' => 'Konto',
			'menu' => 'Meny',
			'go-back' => 'Gå tillbaka',
			'new' => 'Ny',
			'add' => 'Lägg till',
			'add-expense' => 'Lägg till en ny utgift',
			'add-debt' => 'Lägg till en ny skuld',
			'add-loan' => 'Lägg till ett nytt lån',
			'approx' => 'Ca.',
			'total' => 'Totalt',
			'piece' => 'St',
			'pieces' => 'St',
			'folder' => 'Mapp',
			'current-period' => 'Aktuell period',
			'remove' => 'Ta bort',
			'events' => 'Händelser',
			'currency' => 'Valuta',
			'currencies' => 'Valutor',
			'you' => 'Du',
			'users' => 'Användare',
			'users-inactive' => 'Inaktiva',
			'users-online' => 'Inloggade',
			'users-neverloggedin' => 'Aldrig loggats in',
			'cash-inout' => 'Sätt in / ta ut',
			'cash-in' => 'Sätt in',
			'cash-out' => 'Ta ut',
			'version-check' => 'Versionskoll',
			'inactive' => 'Inaktiv',
			'checking' => 'Kontrollerar',
			'week' => 'Vecka',
			'weeks' => 'Veckor',
			'day' => 'Dag',
			'days' => 'Dagar',
			'lend' => 'Utlånat',
			'borrowed' => 'Lånat',
			'companies' => 'Företag',
			'individuals' => 'Privatpersoner',
			'an-expense' => 'En utgift',
			'the-expense' => 'Utgiften',
			'remove-tfa' => 'Ta bort tvåstegsverifiering',
			'a-debt' => 'En skuld',
			'the-debt' => 'Skulden',
			'a-loan' => 'En lån',
			'the-loan' => 'Lånet',
			'export' => 'Exportera',
			'exported' => 'Exporterades',
			'export-as-xlsx' => 'Exportera till en XLSX-fil',
			'export-as-pdf' => 'Exportera till en PDF-fil',
			'in-plaintext' => 'I klartext',
			'all' => 'Alla',
			'reset-code' => 'Återställningskod',
			'delete-data' => 'Ta bort din data',
			'delete-account' => 'Ta bort ditt konto',
			'encrypted' => 'Krypterat',
			'username' => 'Användarnamn',
			'number-payment' => 'Inbetalningsnummer',
			'number-ocr' => 'OCR-nummer',
			'number-phone' => 'Mobilnummer',
			'not-payed' => 'Obetalda',
			'not-payed-expenses' => 'Obetalda utgifter',
			'payed' => 'Betalda',
			'payed-status' => 'Betalad',
			'extra' => 'Extra',
			'recurrent' => 'Återkommande',
			'recurrent-expenses' => 'Återkommande utgifter',
			'future' => 'Framtida',
			'future-expenses' => 'Framtida utgifter',
			'tfa-code' => 'Engångskod',
			'contact-phone' => 'Telefon',
			'contact-email' => 'E-post',
			'email' => 'E-postadress',
			'email-short' => 'E-post',
			'type' => 'Typ',
			'category' => 'Kategori',
			'name' => 'Namn',
			'ip-address' => 'IP-adress',
			'notes' => 'Anteckningar',
			'cancel' => 'Avbryt',
			'browser' => 'Webbläsare',
			'os-short' => 'System',
			'os-full' => 'Operativsystem',
			'qr-show' => 'Visa QR-kod',
			'qr-hide' => 'Göm QR-kod',
			'close' => 'Stäng',
			'edit' => 'Ändra',
			'loan-out' => 'Utlånat',
			'loan-in' => 'Lånat',
			'currency-exchange-from' => 'Valutaomvandlare från',
			'date' => 'Datum',
			'time' => 'Tid',
			'refers-to' => 'Avser',

			'permissions' => [
				'aed' => 'Tillåt ändringar',
				'ade' => 'Tillåt borttagning',
				'map' => 'Tillåt markera som betald',
				'npa' => 'Delning av inbetalningsnumret',
				'noc' => 'Delning av OCR-numret',
				'nph' => 'Delning av mobilnumret',
				'qrc' => 'Delning av QR-kod',
				'not' => 'Delning av anteckningar'
			],
			'sums' => [
				'money-left' => 'Pengar över',
				'amount' => 'Belopp',
				'loan' => 'Lånebelopp',
				'debt' => 'Skuldbelopp',
				'specified' => 'Angivet belopp',
				'monthly' => 'Månadsbelopp',
				'payed' => 'Betalades',
				'estimated' => 'Uppskattat totalbelopp',
				'goal' => 'Målbelopp',
				'percentage' => 'Procent',
				'income' => 'Inkomst',
				'balance' => 'Nuvarande belopp',
				'balance-afterexpenses' => 'Balans efter betalda utgifter',
				'savings' => 'Sparande',
				'weekly-budget' => 'Veckobudget',
				'daily-budget' => 'Dagsbudget'
			],
			'dates' => [
				'expires' => 'Förfallodatum',
				'expires-list' => 'Förfaller',
				'expired' => 'Förföll',
				'started' => 'Påbörjades',
				'last-active' => 'Senast aktiv',
				'loan' => 'Lånedatum',
				'loan-from' => 'Lånedatum (från)',
				'loan-to' => 'Lånedatum (till)',
				'payed' => 'Betalades',
				'estimated-payed' => 'Uppskattas vara betald'
			],
			'passwords' => [
				'password' => 'Lösenord',
				'repeat' => 'Upprepa lösenord',
				'new' => 'Nytt lösenord',
				'current' => 'Nuvarande lösenord'
			],
			'settings' => [
				'tfa-info' => 'Skanna QR-koden nedan eller lägg till koden manuellt, för och komma igång.',
				'tfa-code' => 'Ange engångskoden',
				'language' => 'Språk',
				'theme' => 'Tema',
				'currency' => 'Valuta',
				'sum-income' => 'Inkomst',
				'sum-current' => 'Nuvarande belopp',
				'sum-savings' => 'Sparande',
				'period-starts' => 'Börjar',
				'period-ends' => 'Slutar',
				'day-income' => 'Inkomstdag',
				'delete-sessions' => 'Ta bort alla andra sessioner än ens egna'
			],
			'buttons' => [
				'create' => 'Skapa',
				'sign-in' => 'Logga in',
				'sign-up' => 'Registrera',
				'send' => 'Skicka',
				'finish' => 'Slutför',
				'save' => 'Spara',
				'add' => 'Lägg till',
				'import' => 'Importera',
				'remove' => 'Ta bort',
				'share' => 'Dela'
			],
			'recurrences' => [
				'1-week' => 'Varje vecka',
				'1-month' => 'Varje månad',
				'1-year' => 'Varje år',
				'2-week' => 'Varannan vecka',
				'2-month' => 'Varannan månad',
				'2-year' => 'Varannat år',
				'3-week' => 'Var tredje vecka',
				'3-month' => 'varje kvartal',
				'3-year' => 'Vart tredje år',
				'4-week' => 'Var fjärde vecka',
				'4-month' => 'Var fjärde månad',
				'4-year' => 'Vart fjärde år',
				'5-week' => 'Var femte vecka',
				'5-month' => 'Var femte månad',
				'5-year' => 'Vart femte år',
				'6-week' => 'Var sjätte vecka',
				'6-month' => 'varje halvår',
				'6-year' => 'Vart sjätte år',
				'7-week' => 'Var sjunde vecka',
				'7-month' => 'Var sjunde månad',
				'7-year' => 'Vart sjunde år',
				'8-week' => 'Var åttonde vecka',
				'8-month' => 'Var åttonde månad',
				'8-year' => 'Vart åttonde år',
				'9-week' => 'Var nionde vecka',
				'9-month' => 'Var nionde månad',
				'9-year' => 'Vart nionde år',
				'10-week' => 'Var tionde vecka',
				'10-month' => 'Var tionde månad',
				'10-year' => 'En gång per decennium',
				'11-week' => 'Var elfte vecka',
				'11-month' => 'Var elfte månad',
				'11-year' => 'Vart elfte år',
				'12-week' => 'Var tolfte vecka',
				'12-month' => 'Var tolfte månad',
				'12-year' => 'Vart tolfte år'
			]
		],

		'information' => [
			'no-monthlysum' => 'Inget månadsbelopp angivet',
			'no-notes' => 'Ingen anteckning',
			'item-is-shared-toyou' => 'Har delat detta objekt till dig',
			'item-is-shared-tosomeone' => 'Du har delat det här objektet till en eller flera användare',
			'subscriptions-onlyones' => 'Endast de prenumerationer som sker en gång per månad och/eller en gång per år, räknas med i kalkylen nedan.',

			'expense' => [
				'extra' => 'Utgiften är en extrautgift',
				'recurrent' => 'Utgiften är återkommande',
				'info-can-be-wrong' => 'Betalningsuppgifterna kan vara fel',
				'info-is-correct' => 'Betalningsuppgifterna har bekräftats som korrekta',
				'subscsription' => 'Utgiften är en prenumeration'
			],
			'debt' => [
				'amortization-started' => 'Jag avbetalar för närvarande på skulden',
				'amortization-notstarted' => 'Jag avbetalar inte på skulden'
			],
			'loan' => [
				'lend' => 'Det är jag som lånar ut',
				'borrow' => 'Det är jag som har tagit lånet'
			]
		],

		'shortinfo' => [
			'share-item' => [
				'Ange mottagarens användarnamn i textfältet nedan. Du kan lämna "Mottagaren betalar" tom för att göra så att mottagaren kan se hela beloppet.',
				'Alla delningar för det här objektet visas i "Användare"-fliken (gruppikonen ovan).'
			],
			'shared-to-users' => [
				'Nedan kan du se alla de användare (om några) som du har delat objektet till. För mer information om en delning, välj att redigera den.'
			],
			'edit-shared-item' => [
				'Nedan kan du granska inställningarna för delningen, samt ändra dom. Du kan lämna "Mottagaren betalar" tom för att göra så att mottagaren kan se hela beloppet.',
				'Du måste dela om objektet om du vill ändra användarnamnet.'
			]
		],



		'noscript' => [
			'# JavaScript är inaktiverat',
			$site_title.' är beroende av JavaScript och krävs för att formulären ska fungera korrekt.',
			'## Varför krävs JavaScript?',
			'Vi vill visa för användarna vad som händer i bakgrunden, när '.$site_title.' arbetar med något, till exempel borttagningen av ens data. Utan JavaScript är detta inte möjligt.',
			'## Men är inte JavaScript skadligt?',
			'Nej. Det beror helt på hur utvecklaren har byggt en webbsida med hjälp av JavaScript. Vi kan försäkra dig om att vi inte har använt någon kod som kan skada din säkerhet och integritet.',
			'Om du inte tror på oss, '.link_('granska JavaScript-filerna', 'https://codeberg.org/airikr/Keizai/src/branch/main/js').' som '.$site_title.' använder sig av.'
		],

		'welcome' => [
			'# Hej, och välkommen!',
			$site_title.' är en ekonomisk översiktsida som låter dig lägga till utgifter för en period åt gången (till exempel, 18 oktober till 17 november). Du kan även lägga till dina aktuella skulder, lån och dina budgetar, samt dela objekt till andra användare.',
			'Det finns inga krav på något, utan allt är enligt vad du själv vill lägga till på ditt konto.',

			'## Överskådlig översikt',
			'Översiktsidan visar en sammanfattning på allt du har lagt till (förutom budgetar). Där berättar '.$site_title.' bland annat vecko- och dagsbudgeten, om du har angett vad du har i pengaväg för den aktuella perioden.',
			'overview.webp',

			'## Utgifter',
			'Du kan lägga till detaljerad information om en utgift, så som inbetalnings- och OCR-numret, samt skapa utgiftskonton för och organisera utgifterna efter var de ska betalas. Perfekt om du har 2 bankkonton.',
			'expenses.webp',

			'## Skulder & lån',
			'Ingen vill ha skulder, men om du har några, kan du lägga till dessa för och hålla koll på dom. Du kan även ange hur mycket du betalar eller vill betala på skulden. '.$site_title.' kommer då ge dig ett datum om när du har betalat av hela skulden.',
			'Du kan även lägga till lån till och från exempelvis privatpersoner. Ange när lånet påbörjades och när du eller den andra ska betala tillbaka till dig.',
			'debts.webp',
			'loans.webp',

			'## Dela till andra användare',
			'Är du sambo och delar på till exempel hyra och el? Lägg till dom i '.$site_title.' och dela de med din sambo. Ange hur mycket sambon ska betala (till exempel 50% av beloppet), och om han eller hon ska till exempel kunna markera utgiften som betald.',
			'Du kan givetvis dela skulder och lån också.',
			'share.webp',

			'## Full kontroll över din data',
			'Med '.$site_title.' har du full kontroll över den data du ger webbsidan. Du kan när som helst exportera din data, och även permanent radera din data eller hela ditt konto. Du kan sen skapa ett nytt konto och importera datan för att sen låta det vara exakt så som du lämnade det.',
			'export-import.webp',

			'## Din data är just det, din data',
			'När du skapar ett konto på '.$site_title.', skapar webbsidan en fil med dina krypteringsnycklar i. Dessa inkluderas i dataexporteringen. Vi tar inte över något ägarskap, utan all data som du ger '.$site_title.', ägs av dig och ingen annan.',

			'## Öppen källkod och self-hosting',
			'Vill du säkerställa att det vi säger om tjänsten är sant? Gå då till [Codeberg](https://codeberg.org/airikr/Keizai) och grotta ner dig i '.$site_title.'s källkod.',
			'Du kan alltid ladda hem källkoden och installera '.$site_title.' på din egna server. Med versionskoll på adminpanelen, kan du säkerställa att du har den senaste versionen.',
			'source-code.webp'
		],

		'demo' => [
			'# Demo',
			'Demo-kontot finns för att du ska kunna prova '.$site_title.' först, innan du skapar ditt egna konto.',
			'Vem som helst kan logga in på kontot när som helst. Om något som du har ändrat helt plöstligt ändras utan att du har gjort något, så beror det inte på '.$site_title.', utan att någon annan har gjort ändringen.',

			'## Återställs varje hel timme',
			'Vi tar bort demo-kontot och skapar det på nytt igen varje timme. Så om du vill se hur det är att börja från en ny kula, logga kort efter varje hel timme.',
			'Om du möts med att kontot inte existerar när du försöker logga in på det, vänta tills nästa hel timme och försök igen.',

			'## Do you want to change to English?',
			"- When logged in, choose `Inställningar` in the menu.\n- Scroll down and change from `SE` to `GB` below `Språk`.\n- Save the change by hitting the button called `Spara` and then reload the page.",
			'Or simply click/tap on the English flag in the top left corner.'
		],

		'overview' => [
			'balance' => [
				'expenses-notpayed' => 'Du kommer att ha följande belopp, efter att alla de obetalade utgifterna har blivit betalade:',
				'expenses-payed' => 'Ditt nuvarande belopp är följande:'
			]
		],

		'settings-info' => [
			'sessions' => [
				'Nedan kan du se vilka som har eller har haft tillgång till ditt konto.'
			]
		],

		'no-permissions' => [
			'Du har inte några rättigheter till att besöka denna sida.'
		],

		'privacy-policy' => [
			'# Integritetspolicy',
			'## 1. Om den här policyn',
			'Policyn beskriver hur vi behandlar och hanterar dina uppgifter (personuppgifter inkluderade) på '.$site_domain.'. Vi kommer successivt att förbättra policyn allt eftersom. Du kan se när den blev senast uppdaterad längst ner på sidan. Vi kommer dock aldrig ändra policyn till den grad att den inkräktar på din integritet.',
			$site_title.' är en tjänst som är skapad av en person och inte ett företag.',

			'## 2. Vad för uppgifter vi lagrar',
			"- Enhetens IP-adress.\n- Användaragenten, som oftast visar vad för operativsystem och webbläsare du använder.\n- **Den data som du självmant ger ".$site_title.":**\n  - Ditt användarnamn.\n  - Din e-postadress.\n  - Dina utgifter, skulder och lån.\n  - Din inkomst, ditt nuvarande belopp, och ditt sparande.\n  - Din budget.\n  - Dina anteckningar.",

			'## 3. Hur vi hämtar dina uppgifter',
			'### 3.1. IP-adress och användaragent',
			'IP-adressen och användaragenten (där IP-adressen är en personuppgift) hämtas enbart när ni loggar in på '.$site_title.'.',
			'### 3.2. E-postadress',
			'E-postadressen hämtas via registreringsformuläret, eller via formuläret för dina kontoinställningar när du är inloggad. [Enligt IMY](https://www.imy.se/verksamhet/dataskydd/det-har-galler-enligt-gdpr/introduktion-till-gdpr/personuppgifter/) anses e-postadressen som en personuppgift om den innehåller ditt förnamn och/eller efternamn.',
			'### 3.3. Övriga uppgifter',
			'De övriga uppgifterna så som utgifter och inkomstbelopp, hämtas endast när du själv ger oss dessa uppgifter. '.$site_title.' är en tjänst som inte hämtar några andra uppgifter om dig automatiskt, utan det är du som bestämmer vad för uppgifter du vill ge oss.',

			'## 4. Vad vi använder uppgifterna till',
			'### 4.1. IP-adress och användaragent',
			'IP-adressen kan identifiera både plats och internetleverantör (ISP) och lagras endast för att låta användaren se om någon obehörig har loggats in.',
			'Användaragenten kan visa nödvändig tilläggsinfo, och då mer specifikt vad för webbläsare och enhet den inloggade använde.',
			'För att klargöra, dessa uppgifter lagras endast vid inloggningen.',
			'### 4.2. E-postadress',
			'Om användaren anger sin e-postadress, kommer den endast att användas för e-postutskick som till exempel påminnelser och återställning av lösenordet.',
			'### 4.3. Inkomst, balans och sparande',
			'Att ange sin inkomst och/eller vad man har i pengaväg just nu (balans), kan '.$site_title.' visa hur mycket pengar man har kvar att leva på, efter att alla utgifter är betalade.',
			'Sparbeloppet används inte till något annat än att bara visa hur mycket man har sparat ihop till.',
			'### 4.4. Utgifter, skulder och lån',
			'Grunden till att '.$site_title.' ska fungera som en ekonomiöversiktsida, är att ange vad man har i utgifter under den aktuella perioden (om angiven), och vad man har för skulder och lån. Utifrån uppgifterna, ger '.$site_title.' en överskådlig översikt över hur användarens ekonomiska situation ser ut.',
			'### 4.5. Budget',
			'Om användaren lägger till en eller flera budgetar, kommer '.$site_title.' hjälpa användaren med att hålla ordning på sparandet. Precis som med allt annat på '.$site_title.', sker all inmatning manuellt.',
			'### 4.6. Anteckningar',
			'Anteckningarna existerar endast för att låta användaren skriva upp saker rörande sin ekonomiska situation, till exempel vad man ska prata med kundtjänst på exempelvis Klarna om.',

			'## 5. Hur vi lagrar dina uppgifter',
			'Varje konto på '.$site_title.' har sina egna krypteringsnycklar som webbsidan använder sig av. All data som användaren ger '.$site_title.' (förutom användarnamnet), krypteras med hjälp av dessa nycklar. Och för att förbehålla säkerheten till användarnas känsliga data, använder vi <mark>'.$config_encryptionmethod.' + saltning</mark> som metod.',

			'### 5.1. Hur användarens krypteringsnycklar lagras',
			'När en person skapar sitt konto på '.$site_title.', skapas en JSON-fil som lagras utanför root-katalogen på servern. Filen innehåller krypteringsnycklarna. Det är omöjligt att veta vilken JSON-fil som tillhör användaren, utan något hjälpmedel.',
			'Vi på '.$site_title.' har noll intresse eller ork till att ta reda på vem som är vem. Det skulle även förstöra allt som vi står för.',

			'### 5.2. Hur användarnamnet krypteras',
			'För att webbsidan ska kunna identifiera den som loggar in, lagras användarnamnet med hjälp av webbsidans egna krypteringsnycklar (samma metod som den som används för användarnas data).',

			'## 6. Tredjepartstjänster och cookies',
			"- Vi har valt att <mark>inte använda någon analytiktjänst</mark>, så som [Plausible](https://plausible.io/). Vi ser ingen anledning till varför vi ska använda en sådan tjänst.\n- Den <mark>automatiserade loggningen av Apache HTTP Server har stängts av</mark> för att värna om din integritet.\n- Vi har varit noga med att använda tredjepartstjänster från GitHub som <mark>inte inkränktar på din integritet</mark>.\n- ".$site_title." <mark>lagrar en form av en kaka (cookie) vid namn localStorage i din enhet</mark>, när du ändrar tema och placeringen av webbsidan (se valen högst upp till vänster) som utloggad. localStorage rensas så fort du loggar in. ".$site_title." lagrar dock <mark>inga andra kakor (cookies), förutom sessionskakan när man loggar in</mark>.",

			'## 7. Dina rättigheter',
			'Om ni har frågor om dina rättigheter, tveka inte att kontakta oss (se punkt 10).',
			'### 7.1. Tillgång',
			'Ni kan när som helst (som inloggad) ladda hem er data (exportera) på sidan "Inställningar". Ni har då valmöjligheten att ladda hem er data i antingen klartext eller krypterad.',
			'Era krypteringsnycklar är inkluderade i exporteringen.',
			'### 7.2. Rättelse',
			'Ni kan när som helst (som inloggad) ändra de uppgifter som ni har gett '.$site_title.'.',
			'Vi kan i nuläget inte hantera er data. Men i framtiden kan ni dela er data till valfri användare, och då bestämma vad för data som ska hanteras av vem.',
			'### 7.3. Borttagning',
			'Ni kan när som helst (som inloggad) ta bort all er data eller ert konto på sidan "Inställningar". Ni kan även ta bort enskild data.',
			'Vi kan inte ta bort något åt dig.',
			'### 7.4. Begränsa behandlingen',
			'Ni kan (som inloggad) begränsa vad '.$site_title.' ska få tillgång till gällande din ekonomiska situation, genom att endast lägga in det som ni tycker '.$site_title.' ska få.',
			'### 7.5. Invändning mot behandlingen',
			'Ni kan när som helst (som inloggad) bestämma vad för personuppgifter som ni vill ge '.$site_title.', och välja vad för personuppgifter som ska tas bort från webbsidan.',
			'### 7.6. Dataportabilitet',
			'Ni kan när som helst (som inloggad) importera tidigare exporterad data från '.$site_title.'. Detta görs på sidan "Inställningar".',

			'## 8. Delning av dina personuppgifter',
			$site_title.' kommer aldrig att dela någon uppgift till varken personer eller företag, utan användarens tillåtelse. Det är användarna själva som avgör när och hur de vill dela något med någon.',

			'## 10. Hur du kontaktar oss',
			'Ni kan kontakta oss på följande e-postadress: <mark>hi [at] keizai [dot] se</mark>',

			'----',
			'Senast uppdaterad: 28 september 2023'
		],



		'what-is-this' => [
			'overview-calendar' => [
				'# Kalendern på översiktsidan',
				'Kalendern visar 2 saker: när perioden börjar och slutar (om angiven), och vilka dagar som du har utgifter, skulder, och/eller lån på.',

				'## När perioden börjar och slutar',
				'Det är inte obligatoriskt att du anger när du får in pengar, och när du får mer pengar nästa gång. Men om du anger detta på inställningssidan, kommer den angivna perioden att markeras ut på kalendern med en lite ljusare bakgrundsfärg.',

				'## Vilka dagar som du har något objekt på',
				'Oavsett om du lägger in en eller flera utgifter, skulder, och/eller lån, kommer '.$site_title.' att visa när dessa sker.',
				"- Utgifter markeras ut då de förfaller.\n- Skulder markeras ut när de påbörjades.\n- Lån markeras ut när de lånades eller lånades ut.",
				'De färgade plupparna under dagarna i kalendern är valbara, och kommer att visa dig vad för utgifter, skulder, eller lån som är aktuella för det valda datumet.',
				'Dessa har olika färgkoder: utgifter är gröna, skulder är lila och lån är oranga.',

				'## Navigeringen över kalendern',
				'Över kalendern kan du se 2 pilar på vardera sida, med nuvarande månadens namn och året i mitten. Dessa är valbara. Vänsterpilen går bakåt en månad, medan högerpilen går framåt en månad. Och om du väljer nuvarande månaden och året, får du en lista över alla månaderna för det aktuella året.',
				'Varje månad i månadsvyn är valbara och kommer att visa den månadens kalendervy.'
			],

			'settings-period' => [
				'# Period',
				'Du måste välja antingen när perioden börjar och slutar, eller vilken dag du får din inkomst. Det går inte att välja båda. Fyller du i inkomstdagen, kommer fälten för "Börjar" och "Slutar" att inaktiveras, och vise verse.',
				'Om startperioden och/eller slutperioden är en helgdag (lördag eller söndag), kommer '.$site_title.' istället att visa närmsta vardagen på översikt- och utgiftersidan.',

				'## Inkomstdagen',
				'Om du väljer att fylla i den dag som du får in pengar, kommer '.$site_title.' själv avgöra när period ska börja, baserat på valda dag. Detta är endast för de som får in pengar 1 gång per månad.',
				'**Giltigt värde:** en siffra mellan 1 och 31.',

				'## Flytta fram återkommande utgifter',
				'Om du har återkommande utgifter som måste flyttas fram till rätt period, väljer du "Flytta fram återkommande utgifter". Då kommer alla utgifter som har markerats som återkommande, att flyttas fram de veckorna, månaderna, eller åren som du har valt för varje utgift.',
				'**Exempel:** du har angett en period mellan den 18 oktober och 17 november. Du har även fler än en utgift vars förfallodatum är innan den 18 oktober, och de har satts som återkommande varje månad. Istället för att välja varje utgift, en efter en, och ändra förfallodatumet, kan du välja "Flytta fram återkommande utgifter" och flytta fram alla dessa en månad.'
			],

			'settings-sum-income' => [
				'# Inkomst',
				'Beloppet avser det belopp som du får in tack vare till exempel ett arbete. Om du får in exempelvis 10 000 kr under en dag, och sen 600 kr efter 1 vecka, så skriver du in 10 600 kr (utan mellanrum).',
				'När du ska skriva in ett belopp i textfältet, kommer det att expanderas och ikonen byts ut till en kalkylator. Det du kommer att skrivas in eller har skrivit in, kommer att synas som vanlig text under textfältet. Detta sker för att du enklare ska kunna genomföra matematiska beräkningar, likt den i Swish.',
				'**Giltigt värde:** siffror och punkt eller komma som decimaltecken. Inga mellanrum.'
			],

			'settings-sum-current' => [
				'# Nuvarande belopp',
				'Beloppet avser det belopp som du har just nu och som du använder till dina räkningar och till annat, till exempel mat och nöjen.',
				'När du ska skriva in ett belopp i textfältet, kommer det att expanderas och ikonen byts ut till en kalkylator. Det du kommer att skrivas in eller har skrivit in, kommer att synas som vanlig text under textfältet. Detta sker för att du enklare ska kunna genomföra matematiska beräkningar, likt den i Swish.',
				'**Giltigt värde:** siffror och punkt eller komma som decimaltecken. Inga mellanrum.'
			],

			'settings-sum-savings' => [
				'# Sparande',
				'Beloppet avser det belopp som du har satt in på ett sparkonto, eller som du har sparat kontant.',
				'När du ska skriva in ett belopp i textfältet, kommer det att expanderas och ikonen byts ut till en kalkylator. Det du kommer att skrivas in eller har skrivit in, kommer att synas som vanlig text under textfältet. Detta sker för att du enklare ska kunna genomföra matematiska beräkningar, likt den i Swish.',
				'**Giltigt värde:** siffror och punkt eller komma som decimaltecken. Inga mellanrum.'
			],

			'username' => [
				'# Användarnamn',
				'Användarnamnet är unikt och ska är det namn som du ska använda när du loggar in på ditt konto här på '.$site_title.'. Användarnamnet används även när du ska dela ett objekt med någon annan användare.'
			],

			'email' => [
				'# E-postadress',
				'Det är frivilligt att lägga till sin e-postadress till sitt konto, men det har sina fördelar.',
				"- Du kan återställa ditt lösenord via \"Återställ lösenord\" som utloggad.\n- Du kan få aviseringar vid förfallande utgifter (om du väljer detta i \"Inställningar\")."
			],

			'password' => [
				'# Lösenord',
				'**OBSERVERA! Använd ALDRIG några lösenord som visas här!** Lösenorden i exemplen är offentliga.',
				'Det är mycket viktigt att lösenordet till ditt konto är tillräckligt starkt för och förhindra att hackare kommer åt ditt konto på flera decennier. Vi rekommenderar att du använder dig av en så kallad lösenordsfras (passphrase) framför ett vanligt lösenord. Lösenordsfraser är enklare att komma ihåg och kan stänga ute hackare under den redan sagda perioden.',

				'## Lösenordfraser',
				'Om du väljer att använda lösenordfraser som lösenord, är det rekommenderat att använda minst 4 ord med bindestreck emellan varje ord. Och för att sätta pricken över i\'et, lägg till en siffra i slutet av orden. Orden måste vara slumpmässiga och varje ord måste börja med en stor bokstav.',
				'Exempel: `Karlstad-Stockholm3-Kiruna-Tanumshede`',

				'## Lösenord',
				'Om du väljer att använda ett vanligt lösenord, är det viktigt att det har följande kriterier:',
				"- Minst 10 tecken långt.\n- Stora och små bokstäver.\n- Siffror.\n- Helst även specialtecken, men inte viktigt.",
				'Ett 10 tecken långt lösenord (till exempel `g2Dr4gVAto`) kan vanligtvis knäckas på 12 dagar. Detsamma gäller om du har samma längd på lösenordet, men använder specialtecken (till exempel `e$fM78!z.3`).',
				'Ju fler slumpmässiga tecken, med eller utan specialtecken, desto svårare blir det för hackaren att knäcka det. Det tar till exempel flera århundranden för hackare att knäcka följande lösenord: `Dzt76XA673hm23253pWa`.',
				'Det är varmt rekommenderat att du använder dig av [en lösenordsgenerator för att skapa lösenordet](https://bitwarden.com/password-generator/). Använd en [lösenordshanteringstjänst så som Bitwarden](https://bitwarden.com) för att lagra lösenordet i det.'
			],

			'settings-password-current' => [
				'# Nuvarande lösenord',
				'För att kunna spara ändringarna för användarnamnet, e-postadressen och/eller det nya lösenordet, måste du ange ditt nuvarande lösenord. Detta krävs av säkerhetssyfte.'
			],

			'item-number-phone' => [
				'# Mobilnummer',
				'Mobilnumret ska vara ett nummer som du kan nå personen eller företaget på (beroende på typ av objekt).',

				'## Swish',
				'Om det inte gäller en skuld, kan du ange antingen mottagarens mobilnummer, eller ditt mobilnummer. Om det till exempel är du som lånar ut, kan du ange ditt mobilnummer och sen dela objektet till mottagaren. Mottagaren kan då kunna skanna en QR-kod som '.$site_title.' visar, via Swish-appen för smidigare återbetalning.'
			],

			'item-number-payment' => [
				'# Inbetalningsnummer',
				'Inbetalningsnumret är ett bankgiro- eller ett plusgironummer, det vill säga företagets bankkonto. Numret kan hittas i närheten av betalningsmottagaren på betalningsavin.'
			],

			'item-number-ocr' => [
				'# OCR-nummer',
				'OCR-numret är det referensnummer som står på fakturan. Numret är vanligtvis fakturanumret och/eller kundnumret, och står oftast längst ned på betalningsavin.'
			],

			'item-contact-phone' => [
				'# Telefon',
				'Telefonnumret ska vara till det företag som skulden är till. Om du väljer att ange ett nummer, kommer du kunna trycka på telefonnumret för och börja ringa från din telefonapp i telefonen. Detta är inte möjligt från Linux, Windows, eller macOS.'
			],

			'item-contact-email' => [
				'# E-post',
				'E-postadressen ska vara till det företag som skulden är till. Du kommer då kunna klicka/trycka på adressen när du tittar på detaljerna för en skuld, för och börja skriva till dom. Detta är möjligt från oavsett operativsystem.'
			],

			'loggedout-email-forgotpw' => [
				'# E-postadress',
				'För att kunna återställa lösenordet för ditt konto, måste du ha knutit din e-postadress till ditt konto. Om du inte har gjort det, kan du inte använda återställningen.'
			],

			'loggedout-resetcode' => [
				'# Återställningskod',
				'När man skyddar sitt konto med tvåstegsverifiering (TFA, 2FA, eller TOTP), ger '.$site_title.' dig en återställningskod. Den kan du använda när du har blivit utelåst från ditt konto. Koden kommer då att ta bort tvåstegsverifieringen så att du kan logga in utan den.',
				'Om du dessvärre inte har en återställningskod och du inte kan logga in, vänligen kontakta oss på '.$config_email_contact.'. Vi kan då försöka hjälpa dig mot en säkerhetsfråga.'
			]
		]
	];

?>