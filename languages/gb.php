<?php

	$lang = [
		'metadata' => [
			'language' => 'en',
			'locale' => 'en_GB',
			'description' => $site_title.' is a financial overview site that helps you keep track of your finances, while the service benefits your privacy.',
			'pdf' => [
				'subject' => 'Economical overview',
				'keywords' => [
					'overview',
					'economy',
					'expenses',
					'debts',
					'loans'
				]
			]
		],

		'modals' => [
			'remove-budget' => 'You will not be able to regret it, if you choose to continue.',
			'sign-out' => 'You are about to be logged out.',
			'delete-item' => 'You are about to delete the item.',
			'delete-item-account' => 'You are about to delete the account.\n\nAny items linked to the account will not be deleted.',
			'delete-session' => 'You will not be able to regret it, if you choose to continue.',
			'delete-sessions' => 'You are about to delete all sessions except your own.',
			'delete-account' => 'The following will be removed:\n- Encryption file\n- Username\n- Email address (if provided)\n- Password\n- Two-step verification\n- Sessions\n- Session events\n- Expenses\n- Liabilities \n- Loans\n- Budget\n- Notes\n- Income amount\n- Current amount\n- Savings\n- Period (start and end)\n- Language\n- Currency\n\nYou wont be able to regret it!',
			'delete-data' => 'You will have to start from scratch unless you export all your data first, and then restore the data.\n\nThe following data will be deleted:\n- Expenses\n- Debts\n- Loans\n- Budget\n- Notes\n- Events for sessions\n\nThe following data will not be deleted:\n- Encryption file\n- Username\n- Email address (if provided)\n- Password\n- Two-step verification\n- Sessions\n\nThe following settings will be reset: \n- Amount of income\n- Current amount\n- Savings\n- Period (start and end)\n- Language\n- Currency\n- Expenses\n- Debts\n- Loans\n- Budget',
			'delete-tfa' => 'You will need to add the two-step verification again, if you choose to continue.',
			'delete-share' => 'You must reshare the item if you choose to continue.',
			'import' => 'Please note that any expenses, debts and loans you may have already entered will be deleted and replaced with those in the backup.\n\nAnd the following will be replaced with the corresponding information:\n- Username\n- Email address (if provided)\ n- Two-step verification\n- Income\n- Current amount\n- Savings\n- Period (start and end)\n- Language\n- Currency',
			'update-expirationdates' => 'All recurring expenses that have not been carried forward to the saved period will be carried forward, based on their recurring values.',
			'signingout-soon' => [
				'# You are inactive!',
				'You have not done anything active on the website for 9 minutes and 30 seconds.',
				'If you do not [refresh the page](), you will soon be automatically logged out.'
			]
		],

		'emails' => [
			'inactive-user' => [
				'subject' => 'Your account is considered inactive',
				'html' => '<h1>You haven\'t been logged in for a while</h1><p>We will send 3 emails to you, before the account is deleted, once every two weeks.</p><p>2 months after the last login, your account and associated data will be deleted due to inactivity.</p><p>The deletion is permanent and cannot be cancelled. If you want to keep your data, go to <a href="'.$site_url.'">keizai.se</a> and log in as soon as possible!</p><p>You will receive an email when your account is deleted.</p>',
				'nohtml' => 'You haven\'t been logged in for a while, so we will send you an email as a reminder. We will send out 2 more emails to you, and if you have been inactive for 2 months after your last login, your account will be permanently deleted. If you want to keep your account, go to keizai.se and log in as soon as possible!'
			],
			'expense-expires' => [
				'subject' => 'The due date is within a week',
				'html' => '<h1>7 or fewer days left on one or more expenses</h1><p>We would like to draw your attention to the fact that you have one or more expenses whose due date is within 7 days. '.link_('Sign in', $site_url).' to see who these are.</p>',
				'nohtml' => 'We would like to draw your attention to the fact that you have one or more expenses whose due date is within 7 days. Sign in to '.$site_domain.' to see who these are.'
			],
			'account-deleted' => [
				'subject' => 'Your account has been deleted',
				'html' => '<h1>Account deleted due to inactivity</h1><p>It has been 2 months since your last login and therefore deleted due to inactivity. The removal is permanent and cannot be undone.</p><p>If you want to use '.$site_title.' again, go to '.$site_domain.' and create a new account.</p>',
				'nohtml' => 'The account was removed because you had not logged into it for 2 months. The removal is permanent and cannot be undone. If you want to use '.$site_title.' again, go to '.$site_domain.' and create a new account.'
			]
		],

		'months' => [
			'january' => 'January',
			'february' => 'February',
			'march' => 'March',
			'may' => 'May',
			'april' => 'April',
			'june' => 'June',
			'july' => 'July',
			'august' => 'August',
			'september' => 'September',
			'november' => 'November',
			'october' => 'October',
			'december' => 'December'
		],

		'days-long' => [
			'monday' => 'Monday',
			'tuesday' => 'Tuesday',
			'wednesday' => 'Wednesday',
			'thursday' => 'Thursday',
			'friday' => 'Friday',
			'saturday' => 'Saturday',
			'sunday' => 'Sunday'
		],

		'days-short' => [
			'mon' => 'Mon',
			'tue' => 'Tue',
			'wed' => 'Wed',
			'thu' => 'Thu',
			'fri' => 'Fri',
			'sat' => 'Sat',
			'sun' => 'Sun'
		],

		'aria-labels' => [
			'light-theme' => 'Switch to the light theme',
			'dark-theme' => 'Change to the dark theme',
			'align-left' => 'Place the webpage on the left',
			'align-center' => 'Place the web page in the centre',
			'menu' => [
				'show-menu' => 'Show the menu',
				'hide-menu' => 'Hide the menu',
				'overview' => 'Go to the overview page',
				'expenses' => 'Show all added expenses',
				'debts' => 'Show all added debts',
				'loans' => 'Show all added loans',
				'budget' => 'Show all added budget',
				'notes' => 'Show your notes',
				'shares' => 'Show all shares that you have sent and that you have received',
				'settings' => 'Manage your account settings, among other things',
				'admin' => 'View administrative settings, among other things',
				'logout' => 'Log out'
			]
		],

		'nav' => [
			'signed-out' => [
				'sign-in' => 'Sign in',
				'sign-up' => 'Sign up',
				'forgot-pw' => 'Reset password'
			],
			'signed-in' => [
				'overview' => 'Overview',
				'expenses' => 'Expenses',
				'debts' => 'Debts',
				'loans' => 'Loans',
				'budget' => 'Budget',
				'notes' => 'Notes',
				'shares' => 'Shares',
				'settings' => 'Settings',
				'admin' => 'Admin',
				'sign-out' => 'Sign out'
			],
			'sub' => [
				'create-directory' => 'Create a new account',
				'add-expense' => 'Add a new expense',
				'add-debt' => 'Add a new debt',
				'add-loan' => 'Add a new loan',
				'add-budget' => 'Add a new budget'
			],
			'filter' => [
				'all' => 'All',
				'period' => 'Period',
				'not-payed' => 'Not payed',
				'payed' => 'Payed',
				'extra' => 'Extra',
				'future' => 'Future',
				'recurrent' => 'Recurrent',
				'private' => 'Individuals',
				'companies' => 'Companies',
				'loans-out' => 'Lent out',
				'loans-in' => 'Borrowed',
				'shared' => 'Shared with you',
				'subscriptions' => 'Subscriptions'
			]
		],

		'footer' => [
			'links' => [
				'privacypolicy' => 'Privacy policy',
				'donation' => 'Donation',
				'source-code' => 'Source code'
			],
			'text' => [
				'made-with-love' => 'Created with '.svgicon('heart').' in Sweden'
			]
		],

		'types' => [
			'e-invoice' => 'E-invoice',
			'credit-card' => 'Credit card',
			'direct-debit' => 'Direct debit',
			'invoice' => 'Invoice',
			'swish' => 'Swish',
			'transfer' => 'Transfer',
			'invoice-email' => 'Invoice via e-mail'
		],

		'categories' => [
			'amortization' => 'Amortization',
			'broadband' => 'Broadband',
			'electricity' => 'Electricity',
			'insurance' => 'Insurance',
			'rent' => 'Rent',
			'loan' => 'Loan',
			'literature' => 'Literature',
			'pet-food' => 'Pet food',
			'pet-toys' => 'Pet toys',
			'pet-vet' => 'Veterinary',
			'garden' => 'Garden',
			'subscription' => 'Subscription',
			'travel' => 'Travel',
			'healthcare' => 'Healthcare',
			'tech-n-stuff' => 'Techn & stuff',
			'health-n-beauty' => 'Health & beauty',
			'telephony' => 'Telephony',
			'entertainment' => 'Entertainment',
			'education' => 'Education',
			'other' => 'Other',
			'savings' => 'Savings',
			'webservice' => 'Web service',
			'shipping' => 'Shipping',
			'outlet' => 'Outlet',
			'fuel' => 'Fuel',
			'clothing' => 'Clothing',
			'shoes' => 'Shoes'
		],

		'sessioncodes' => [
			'saved-changes-budget' => 'Saved the changes for a budget',
			'created-budget' => 'Created a budget',
			'updated-budget' => 'Updated a budget',
			'export-plaintext' => 'Exported the data in plaintext to a JSON file',
			'export-encrypted' => 'Exported data encrypted to a JSON file',
			'export-xlsx' => 'Exported the data to an XLSX file',
			'imported' => 'Imported data',
			'updated-item' => 'Updated a item',
			'updated-settings' => 'Updated the settings',
			'updated-password' => 'Updated the password',
			'updated-notes' => 'Updated the notes',
			'updated-account-expenses' => 'Updated an account for expenses',
			'deleted-session' => 'Deleted a session',
			'deleted-sessions' => 'Deleted all sessions except one\'s own',
			'deleted-data' => 'Deleted data',
			'deleted-account-expenses' => 'Removed an expense account',
			'removed-tfa' => 'Removed two-step verification',
			'added-tfa' => 'Enabled two-step verification',
			'created-account-expenses' => 'Created an account for expenses',
			'created-item' => 'Created a new item',
			'delete-data-failed' => 'Data deletion failed',
			'remove-tfa-failed' => 'Failed to remove two-step verification',
			'signed-in' => 'Signed in',
			'signed-out' => 'Signed out',
			'auto-signed-out' => 'Was inactive and signed out'
		],

		'titles' => [
			'shares' => 'Delningar',
			'expense' => 'Expense',
			'expenses' => 'Expenses',
			'debt' => 'Debt',
			'debts' => 'Debts',
			'loan' => 'Loan',
			'loans' => 'Loans',
			'new-password' => 'Enter a new password',
			'notes' => 'Notes',
			'admin' => 'Admin panel',
			'budget' => 'Budget',
			'add-budget' => 'Add a new budget',
			'edit-budget' => 'Change a budget',
			'no-permissions' => 'No permissions',
			'settings' => [
				'settings' => 'Settings',
				'sessions' => 'Sessions',
				'manage-data' => 'Manage your data'
			]
		],

		'subtitles' => [
			'users' => 'Users',
			'currencies' => 'Currencies',
			'statistics' => 'Statistics',
			'next-period' => 'Next period',
			'from-you' => 'From you',
			'to-you' => 'To you',
			'notifications' => 'Notifications',
			'others' => 'Other',
			'privacy' => 'Privacy',
			'appearance' => 'Appearance',
			'notes' => 'Notes',
			'configurations' => 'Configurations',
			'subscriptions' => 'Subscriptions',
			'required' => 'Required',
			'expenses' => 'Expenses',
			'debts' => 'Debts',
			'loans' => 'Loans',
			'optional' => 'Optional',
			'current-period' => 'Current period',
			'not-payed' => 'Not payed',
			'payed' => 'Payed',
			'others' => 'Others',
			'extra' => 'Extra',
			'future' => 'Future',
			'shared' => 'Shared',
			'share' => 'Share',
			'recurrent' => 'Recurrent',
			'tfa' => 'Two-step verification',
			'default-account' => 'Default account',
			'settings' => [
				'account' => 'Account',
				'economy' => 'Economy',
				'period' => 'Period',
				'tfa' => 'Two-step verification',
				'adaptation' => 'Adaptation',
				'import' => 'Import',
				'options' => 'Options'
			]
		],

		'forms' => [
			'move-recurrent-expenses' => 'Move recurring expenses forward',
			'checkboxes' => [
				'allow-weekends' => 'I get my income during weekends',
				'loan-out' => 'I am the one who lends',
				'expenses-expires' => 'Notify me when at least one expense is due (experimental)',
				'default-directory' => 'The account is a standard account',
				'hidedate-expenses' => 'Hide the due date of expenses',
				'hidedate-debts' => 'Hide the start date of debts',
				'hidedate-loans' => 'Hide the loan date for loans',
				'align-center' => 'Center the website',
				'period-use-incomeday' => 'Use the income day instead of a period',
				'recurrent-autoupdate' => 'Automatically change the due date to the current period',
				'recurrent-autoupdate-all' => 'Change for all recurring expenses',
				'share-allow' => 'Allow other users to share items with me',
				'share' => [
					'number-payment' => 'Share the payment number',
					'number-ocr' => 'Share the OCR number',
					'number-phone' => 'Share the mobile number',
					'qrcodes' => 'Share auto-generated QR codes',
					'notes' => 'Share notes',
					'allow-edits' => 'Allow the recipient to edit the item',
					'allow-deletion' => 'Allow recipient to delete item',
					'allow-markas-payed' => 'Allow the recipient to mark the expense as paid'
				],
				'expense' => [
					'payed' => 'The expense has been paid',
					'extra' => 'The expense is an additional expense',
					'subscription' => 'The expense is a subscription',
					'recurrent' => 'The expense is recurring',
					'correct-info' => 'The payment number and the OCR number are correct'
				],
				'debt' => [
					'company' => 'The debt is to a company',
					'paying' => 'I am currently paying off the debt'
				],
				'descriptions' => [
					'share-qrcodes' => 'Sharing QR codes requires that you have either the payment number and the OCR number crossed out, or the mobile number crossed out.'
				]
			],
			'informations' => [
				'registration_email' => [
					'Entering your email address is optional, but is required when resetting your password. You can always change or remove the address in your account settings.'
				],
				'forgotten-pw' => [
					$site_title.' needs your username to access your encryption keys. Not until then can '.$site_title.' send the email.'
				],
				'import' => [
					'Restore data from a previous backup.'
				]
			]
		],

		'messages' => [
			'update-expenses-reload-page' => 'Move forward recurrent expenditure (save and reload the page first)',
			'update-expenses-fill-in' => 'Move forward recurrent expenditure (period not specified)',
			'version-uptodate' => 'You are using the latest version',
			'version-above-current' => 'The local version is newer than the source',
			'version-are-available' => 'Are available',
			'no-internet' => 'Unable to connect to the internet',
			'try-first' => 'Do you want to try Keizai first, before creating an account? In such cases, enter <code>demo</code> as both username and password.',
			'demo' => 'You are using the demo account.',
			'debugmode-enabled' => 'Debug mode is enabled',
			'budget-order-updating' => 'Updating, please wait...',
			'budget-order-error' => 'An error occurred - please try again',
			'smtp-disabled' => 'SMTP are not configurated',
			'no-accounts' => 'You have not added any accounts yet',
			'no-items' => 'Couldn\'t find anything',
			'no-explaination' => 'Could not find any explanation',
			'no-expenses' => 'You had not added any expenses',
			'no-depbts' => 'You had not added any debts',
			'no-loans' => 'You had not added any loans',
			'no-period' => 'You have not set any period yet',
			'choose-item' => 'Choose something from the list',
			'loading-item-details' => 'Getting details about the object...',
			'debugging' => 'Debugging enabled. Check the console for more info',
			'scan-qrcode-bank' => 'Open your banking app and scan the QR code below to transfer money. The payment details you have entered will then be automatically filled in. However, not all banking apps support QR code scanning.',
			'scan-qrcode-bank-payed' => 'The expense has already been paid. The QR code is thus hidden to avoid more payments. However, the payment number and the OCR number remain.',
			'scan-qrcode-swish' => 'Open the Swish app on your device, select "Scan" and scan the QR code below.',
			'scan-qrcode-swish-payed' => 'The expense has already been paid. The QR code is thus hidden to avoid more payments. However, the phone number remains.',
			'signing-out' => 'Signing out...',

			'move-recurrent-expenses' => [
				'updating' => 'Moving forward, please wait...',
				'updated' => 'Move completed',
				'noupdate' => 'Nothing to move forward'
			],

			'settings' => [
				'tfa-backup' => 'Save the text string above in a safe place. If you should lose access to your one-time codes, you must use the recovery code to remove the two-step verification.',
				'tfa-removed' => 'Two-step verification has been disabled',
				'tfa-removed-info' => 'You need to reload the page to re-enable two-step verification again.',
				'tfa-retry' => 'Something went wrong during activation. Try again'
			],

			'forms' => [
				'working' => [
					'working' => 'Working - please wait',
					'checking-deletion' => 'Checking the removal',
					'deleting-data' => 'Deleting all data associated to you',
					'deleting-yourdata' => 'Deleting your data'
				],
				'info' => [
					'tfa-enabled' => 'Two-step verification are enabled'
				],
				'errors' => [
					'other-error' => 'A general error has occurred',
					'fields-empty' => 'Please fill in the required fields first',
					'item-exists' => 'You have already added an item with the same information as these',
					'item-added' => 'The item has been added',
					'cant-delete-data' => 'Could not delete data - try again',
					'cant-fetch-info' => 'Could not retrieve the information',
					'email-notexists' => 'The email address does not exist',
					'email-taken' => 'The email address is already in the system',
					'email-notvalid' => 'The email address is not valid',
					'username-notexists' => 'The username does not exist',
					'username-taken' => 'The username already exists in the system',
					'passwords-notsame' => 'The passwords are not the same',
					'password-incorrect' => 'The password is incorrect',
					'password-incorrect-current' => 'Your current password is incorrect',
					'passwords-tooshort' => 'The password entered is too short',
					'tfa-onlydigits' => 'The one-time code may only contain numbers',
					'tfa-incorrect' => 'The one-time code is not correct',
					'too-many-attempts' => 'Too many login attempts',
					'resetcode-notexists' => 'The recovery code does not exist',
					'resetcode-incorrect' => 'The recovery code is not correct',
					'resetcode-length' => 'The reset code must be 20 characters long',
					'permission-denied' => 'Server error (PRM)',
					'account-default' => 'You cannot delete a default account',
					'account-exists' => 'There is already an account with that name',
					'sum-onlydigits-goal' => 'The target amount may only contain numbers',
					'sum-onlydigits-permonth' => 'The monthly amount may only contain numbers',
					'sum-onlydigits-budget' => 'The amount may only contain numbers',
					'sum-belowzero' => 'You cannot go negative in the budget',
					'sum-aboveonehundred' => 'You cannot exceed the target amount',
					'sum-percent-below1' => 'You can\'t go below 1%',
					'sum-percent-above100' => 'You can\'t go over 100%',
					'sum-invalid-nonegative' => 'The amount must not be negative (do not go below SEK 0)',
					'sum-aboveactual' => 'The amount is above the amount of the object',
					'sum-below5' => 'The amount must not be less than 5',
					'period-wrongorder' => 'The start date of the period must be before the end date',
					'period-missingdate-start' => 'The start date of the period is missing',
					'period-missingdate-end' => 'The end date of the period is missing',
					'onlydigits-recurrence' => 'The recurrence can only contain numbers',
					'onlybetween-1-12-recurrence' => 'The recurrence type can only be between 1 and 12',
					'already-shared' => 'You have already shared the item to the user',
					'cant-share-to-self' => 'You cannot share the item to yourself',
					'user-notexists' => 'The user does not exist',
					'privacy-sharing-notallowed' => 'The recipient has chosen not to allow sharing',
					'file-upload' => [
						'serversize-ini' => 'Server error (INI)',
						'serversize-form' => 'Server error (FRM)',
						'partial' => 'Failed to upload file completely - please try again',
						'nofile' => 'Please select a file first',
						'notmpdir' => 'Server error (TMD)',
						'cantwrite' => 'Server error (PRM)',
						'extension' => 'Server error (EXT)',
						'unknown' => 'Unknown error - try again'
					]
				],
				'done' => [
					'saved' => 'Your changes have been saved',
					'saved-pw' => 'Your changes have been saved, including the new password. You are now logged out - please wait',
					'deleted-account' => 'Account has been removed - logging you out',
					'item-added' => 'Item has been added - redirecting you',
					'item-shared' => 'The item has been shared to the user',
					'password-updated' => 'Password reset - redirecting you',
					'data-deleted' => 'The removal was successful - reloading the page',
					'data-imported' => 'Import successful - redirecting you',
					'signed-in' => 'You have been signed in - redirecting you',
					'tfa-disabled' => 'Two-step verification have been disabled',
					'account-created' => 'The account has been created - you can now sign in',
					'email-sent' => 'An email has been sent',
					'account-created' => 'Account has been created - redirecting you',
					'account-deleted' => 'Account has been deleted - redirecting you',
					'account-saved' => 'Your changes have been saved - redirecting you',
					'budget-updated' => 'The budget has been updated'
				]
			]
		],

		'tooltips' => [
			'take-screenshot' => 'Take a screenshot',
			'what-is-this' => 'Explaination',
			'move-item' => 'Change place',
			'shared-navigation-disabled' => 'You cannot use the navigation for a shared item',
			'shared-navigation-disabled-solo' => 'You have chosen to allow a maximum of 1 user',
			'notallowed-edits' => 'You do not have rights to edit the item',
			'notallowed-deletion' => 'You do not have rights to delete the item',
			'object-remove' => 'Remove',
			'object-edit' => 'Edit',
			'object-close' => 'Close',
			'object-share' => 'Share',
			'object-info' => 'Info',
			'object-users' => 'Users',
			'no-period' => 'You have not entered a period yet',
			'markas-payed' => 'Mark as payed',
			'markas-notpayed' => 'Mark as not payed',
			'filter-by-date' => 'Show all items from this date',
			'show-details' => 'Show details about the item',
			'item-have-notes' => 'The item has a note',
			'item-has-been-shared' => 'The object has been shared with one or more users',
			'item-has-qrcode' => 'The item has a QR code',
			'export-data-plaintext' => 'Export all your data in plain text to a JSON file',
			'export-data-encrypted' => 'Export all your data encrypted to a JSON file',
			'export-data-xlsx' => 'Export all data to an XLSX file',
			'export-data-pdf' => 'Export to a PDF file (overview only)',
			'currencies-lastupdated' => 'The currencies were last updated on this date',
			'delete-data' => 'Delete the data you have added',
			'delete-account' => 'Delete your account',
			'delete-share' => 'Delete the share',
			'delete-budget' => 'Delete budget',
			'edit-budget' => 'Edit budget',
			'edit-share' => 'Edit the share',
			'close' => 'Close',
			'session-delete' => 'Delete the session and log out the logged in',
			'session-delete-own' => 'You cannot delete your own session - log out instead',
			'session-info-show' => 'Show more information',
			'session-info-hide' => 'Hide the information',
			'showall-expenses' => 'Show all expenses',
			'showall-expenses-period' => 'Show all expenses for the current period',
			'showall-expenses-notpayed' => 'View all unpaid expenses',
			'showall-expenses-payed' => 'View all paid expenses',
			'showall-expenses-extra' => 'Show all extra expenses',
			'showall-expenses-future' => 'Show all future expenses',
			'showall-expenses-recurrent' => 'Show all recurring expenses',
			'showall-expenses-shared' => 'View all expenses that have been shared with you',
			'showall-debts' => 'Show all debts',
			'showall-debts-individuals' => 'Show all debts to individuals',
			'showall-debts-companies' => 'Show all company debts',
			'showall-debts-shared' => 'View all debts that have been assigned to you',
			'showall-loans' => 'Show all loans',
			'showall-loans-individuals' => 'Show all loans to and from individuals',
			'showall-loans-companies' => 'Show all loans to companies',
			'showall-loans-shared' => 'View all loans that have been shared with you',
			'showall-expenses-subscriptions' => 'Show all subscriptions',
			'update-income' => 'Update the income amount',
			'update-current' => 'Update the current amount',
			'update-savings' => 'Update the savings',

			'filters' => [
				'by-type' => 'Show all objects with this type',
				'by-category' => 'Show all items with this category'
			]
		],

		'screenshots' => [
			'overview-balance' => 'Balance',
			'overview-calendar' => 'Overview - Calendar',
			'overview-expenses' => 'Overview - Statistics for expenses',
			'overview-subscriptions' => 'Overview - Statistics for subscriptions',
			'overview-debts' => 'Overview - Statistics for debts',
			'overview-loans' => 'Overview - Statistics for loans',
			'overview-others' => 'Overview - Statistics for others',
			'overview-nextperiod' => 'Overview - Statistics for the next period'
		],

		'words' => [
			'active-subscriptions' => 'Subscriptions',
			'yearly-cost' => 'Yearly cost',
			'monthly-cost' => 'Monthly cost',
			'quarterly-cost' => 'Quarterly cost',
			'halfyearly-cost' => 'Half-yearly cost',
			'yearly-cost' => 'Yearly',
			'is-paying' => 'Paying',
			'is-company' => 'Company',
			'estimated' => 'Beräknat',
			'read-more' => 'Read more',
			'sitename-meaning' => 'Economy in Japanese',
			'period' => 'Period',
			'browse' => 'Browse',
			'overview' => 'Overview',
			'dark' => 'Dark',
			'light' => 'Light',
			'belongs-to-debt' => 'Associated debt',
			'tfa-short' => 'TFA',
			'user' => 'User',
			'recipient-pays' => 'The recipient pays',
			'shared-from' => 'Shared from',
			'item-type' => 'Item type',
			'item' => 'Item',
			'shared' => 'Shared',
			'shared-overview' => 'Shared with you',
			'shared-to' => 'Shared to',
			'shared-from' => 'Shared from',
			'database-size' => 'Database size',
			'week-short' => 'W',
			'expense' => 'Expense',
			'debt' => 'Debt',
			'loan' => 'Loan',
			'month' => 'Month',
			'week' => 'Week',
			'year' => 'Year',
			'yes' => 'Yes',
			'no' => 'No',
			'expenses' => 'Expenses',
			'debts' => 'Debts',
			'loans' => 'Loans',
			'recurrence' => 'Recurrence',
			'at-least' => 'At least',
			'optional' => 'Optional',
			'account' => 'Account',
			'menu' => 'Menu',
			'go-back' => 'Go back',
			'new' => 'New',
			'add' => 'Add',
			'add-expense' => 'Add new expense',
			'add-debt' => 'Add new debt',
			'add-loan' => 'Add new loan',
			'approx' => 'App.',
			'total' => 'Total',
			'piece' => 'St',
			'pieces' => 'St',
			'folder' => 'Folder',
			'current-period' => 'Current period',
			'remove' => 'Remove',
			'events' => 'Events',
			'currency' => 'Currency',
			'currencies' => 'Currencies',
			'you' => 'You',
			'users' => 'Users',
			'users-inactive' => 'Inactive',
			'users-online' => 'Online',
			'users-neverloggedin' => 'Never signed in',
			'cash-inout' => 'Cash in / out',
			'cash-in' => 'Cash in',
			'cash-out' => 'Cash out',
			'version-check' => 'Version check',
			'inactive' => 'Inactive',
			'checking' => 'Checking',
			'week' => 'Week',
			'weeks' => 'Weeks',
			'day' => 'Day',
			'days' => 'Days',
			'lend' => 'Lend',
			'borrowed' => 'Borrowed',
			'companies' => 'Companies',
			'individuals' => 'Individuals',
			'an-expense' => 'An expense',
			'the-expense' => 'The expense',
			'remove-tfa' => 'Remove two-step verification',
			'a-debt' => 'A debt',
			'the-debt' => 'The debt',
			'a-loan' => 'A loan',
			'the-loan' => 'The loan',
			'export' => 'Export',
			'exported' => 'Exported',
			'export-as-xlsx' => 'Export to an XLSX file',
			'export-as-pdf' => 'Export to a PDF file',
			'in-plaintext' => 'In plain text',
			'all' => 'All',
			'reset-code' => 'Reset code',
			'delete-data' => 'Delete your data',
			'delete-account' => 'Delete your account',
			'encrypted' => 'Encrypted',
			'username' => 'Username',
			'number-payment' => 'Payment number',
			'number-ocr' => 'OCR number',
			'number-phone' => 'Phone number',
			'not-payed' => 'Not payed',
			'not-payed-expenses' => 'Not payed expenses',
			'payed' => 'Payed',
			'payed-status' => 'Payed',
			'extra' => 'Extra',
			'recurrent' => 'Recurrent',
			'recurrent-expenses' => 'Recurrent expenses',
			'future' => 'Future',
			'future-expenses' => 'Future expenses',
			'tfa-code' => 'One-time code',
			'contact-phone' => 'Phone',
			'contact-email' => 'Email',
			'email' => 'Email address',
			'email-short' => 'Email',
			'type' => 'Type',
			'category' => 'Category',
			'name' => 'Name',
			'ip-address' => 'IP address',
			'notes' => 'Notes',
			'cancel' => 'Cancel',
			'browser' => 'Browser',
			'os-short' => 'System',
			'os-full' => 'Operating system',
			'qr-show' => 'Show QR code',
			'qr-hide' => 'Hide QR code',
			'close' => 'Close',
			'edit' => 'Edit',
			'loan-out' => 'Lent out',
			'loan-in' => 'Borrowed',
			'currency-exchange-from' => 'Currency exchange from',
			'date' => 'Date',
			'time' => 'Time',
			'refers-to' => 'Refers to',

			'permissions' => [
				'aed' => 'Allow changes',
				'ade' => 'Allow deletion',
				'map' => 'Allow mark as paid',
				'npa' => 'Partition of the payment number',
				'noc' => 'Partition of the OCR number',
				'nph' => 'Sharing the mobile number',
				'qrc' => 'Sharing QR Code',
				'not' => 'Sharing notes'
			],
			'sums' => [
				'money-left' => 'Money left',
				'amount' => 'Amount',
				'loan' => 'Loan amount',
				'debt' => 'Debt amount',
				'specified' => 'Indicated amount',
				'monthly' => 'Monthly amount',
				'payed' => 'Payed',
				'estimated' => 'Estimated total amount',
				'goal' => 'Goal amount',
				'percentage' => 'Percent',
				'income' => 'Income',
				'balance' => 'Balance',
				'balance-afterexpenses' => 'Balance after payed expenses',
				'savings' => 'Savings',
				'weekly-budget' => 'Weekly budget',
				'daily-budget' => 'Daily budget'
			],
			'dates' => [
				'expires' => 'Expires',
				'expires-list' => 'Expires',
				'expired' => 'Expired',
				'started' => 'Started',
				'last-active' => 'Last active',
				'loan' => 'Loan date',
				'loan-from' => 'Loan date (from)',
				'loan-to' => 'Loan date (to)',
				'payed' => 'Was payed',
				'estimated-payed' => 'Estimated to be paid'
			],
			'passwords' => [
				'password' => 'Password',
				'repeat' => 'Repeat password',
				'new' => 'New password',
				'current' => 'Current password'
			],
			'settings' => [
				'tfa-info' => 'Scan the QR code below or add the code manually to get started.',
				'tfa-code' => 'Enter the one-time code',
				'language' => 'Language',
				'theme' => 'Theme',
				'currency' => 'Currency',
				'sum-income' => 'Income',
				'sum-current' => 'Balance',
				'sum-savings' => 'Savings',
				'period-starts' => 'Starts',
				'period-ends' => 'Ends',
				'day-income' => 'Income day',
				'delete-sessions' => 'Delete all sessions other than your own'
			],
			'buttons' => [
				'create' => 'Create',
				'sign-in' => 'Sign in',
				'sign-up' => 'Sign up',
				'send' => 'Send',
				'finish' => 'Finish',
				'save' => 'Save',
				'add' => 'Add',
				'import' => 'Import',
				'remove' => 'Remove',
				'share' => 'Share'
			],
			'recurrences' => [
				'1-week' => 'Each week',
				'1-month' => 'Each month',
				'1-year' => 'Each year',
				'2-week' => 'Biweekly',
				'2-month' => 'Every other month',
				'2-year' => 'Every other year',
				'3-week' => 'Every 3 weeks',
				'3-month' => 'Every 3 months',
				'3-year' => 'Every 3 year',
				'4-week' => 'Every 4 weeks',
				'4-month' => 'Every 4 months',
				'4-year' => 'Every 4 years',
				'5-week' => 'Every 5 weeks',
				'5-month' => 'Every 5 months',
				'5-year' => 'Every 5 years',
				'6-week' => 'Every 6 week',
				'6-month' => 'Every 6 months',
				'6-year' => 'Every 6 years',
				'7-week' => 'Every 7 weeks',
				'7-month' => 'Every 7 months',
				'7-year' => 'Every 7 years',
				'8-week' => 'Every 8 weeks',
				'8-month' => 'Every 8 months',
				'8-year' => 'Every 8 years',
				'9-week' => 'Every 9 weeks',
				'9-month' => 'Every 9 months',
				'9-year' => 'Every 9 years',
				'10-week' => 'Every 10 weeks',
				'10-month' => 'Every 10 months',
				'10-year' => 'Once per decade',
				'11-week' => 'Every 11 weeks',
				'11-month' => 'Every 11 months',
				'11-year' => 'Every 11 years',
				'12-week' => 'Every 12 weeks',
				'12-month' => 'Every 12 months',
				'12-year' => 'Every 12 years'
			]
		],

		'information' => [
			'no-monthlysum' => 'No monthly amount specified',
			'no-notes' => 'No notes',
			'item-is-shared-toyou' => 'A user has shared this item with you',
			'item-is-shared-tosomeone' => 'You have shared this item with one or more users',
			'subscriptions-onlyones' => 'Only those subscriptions that take place once a month and/or once a year are included in the calculation below.',

			'expense' => [
				'extra' => 'The expense is an additional expense',
				'recurrent' => 'The expense is recurring',
				'info-can-be-wrong' => 'The payment information may be incorrect',
				'info-is-correct' => 'The payment details have been confirmed as correct',
				'subscsription' => 'The expense is a subscription'
			],
			'debt' => [
				'amortization-started' => 'I am currently paying off the debt',
				'amortization-notstarted' => 'I am not paying off the debt'
			],
			'loan' => [
				'lend' => 'I am the one who lends',
				'borrow' => 'I\'m the one who took the loan'
			]
		],

		'shortinfo' => [
			'share-item' => [
				'Enter the recipient\'s username in the text field below. You can leave "Recipient pays" blank to allow the recipient to see the full amount.',
				'All shares for this item are displayed in the "Users" tab (group icon above).'
			],
			'shared-to-users' => [
				'Below you can see all the users (if any) to whom you have shared the item. For more information about a share, choose to edit it.'
			],
			'edit-shared-item' => [
				'Below you can review the sharing settings and change them. You can leave "Recipient pays" blank to allow the recipient to see the full amount.',
				'You must reshare the item to change the username.'
			]
		],



		'noscript' => [
			'# JavaScript is disabled',
			$site_title.' is dependent on JavaScript and is required for the forms to function correctly.',
			'## Why is JavaScript required?',
			'We want to show users what happens in the background, when '.$site_title.' working on something, such as the deletion of one\'s data. Without JavaScript disabled, this is not possible.',
			'## But isn\'t JavaScript harmful?',
			'No. It all depends on how the developer has built a web page using JavaScript. We can assure you that we have not used any code that could harm your security and privacy.',
			'If you don\'t believe us, '.link_('review the JavaScript files', 'https://codeberg.org/airikr/'.$site_title.'/src/branch/main/js').' that '.$site_title.' are using.'
		],

		'welcome' => [
			'# Hello and welcome!',
			$site_title.' is a financial overview page that allows you to add expenses for one period at a time (for example, October 18 to November 17). You can also add your current debts, loans and your budgets, as well as share items to other users.',
			'There are no requirements for anything, but everything is according to what you yourself want to add to your account.',

			'## Clear overview',
			'The overview page shows a summary of everything you\'ve added (except budgets). There, '.$site_title.' tells you, among other things, the weekly and daily budget, if you have entered what you have in the way of money for the period in question.',
			'overview.webp',

			'## Expenses',
			'You can add detailed information about an expense, such as the payment and OCR number, as well as create expense accounts for and organize expenses by where they are due. Perfect if you have 2 bank accounts.',
			'expenses.webp',

			'## Debts & Loans',
			'No one wants to have debts, but if you have any, you can add these to and keep track of them. You can also state how much you pay or want to pay on the debt. '.$site_title.' will then give you a date as to when you have paid off the entire debt.',
			'You can also add loans to and from, for example, private individuals. State when the loan was started and when you or the other person will pay you back.',
			'debts.webp',
			'loans.webp',

			'## Share to other users',
			'Are you cohabiting and share, for example, rent and electricity? Add them to '.$site_title.' and share them with your partner. Specify how much the partner must pay (for example 50% of the amount), and whether he or she should, for example, be able to mark the expense as paid.',
			'Of course you can share debts and loans too.',
			'share.webp',

			'## Full control over your data',
			'With '.$site_title.' you have full control over the data you provide the website. You can export your data at any time, and also permanently delete your data or your entire account. You can then create a new account and import the data to then leave it exactly as you left it.',
			'export-import.webp',

			'## Your data is just that, your data',
			'When you create an account on '.$site_title.', the website creates a file with your encryption keys in it. These are included in the data export. We do not take any ownership, but all data you provide to '.$site_title.' is owned by you and no one else.',

			'## Open source and self-hosting',
			'Do you want to ensure that what we say about the service is true? Then go to [Codeberg](https://codeberg.org/airikr/'.$site_title.') and dig into '.$site_title.'\'s source code.',
			'You can always download the source code and install '.$site_title.' on your own server. With version control in the admin panel, you can ensure that you have the latest version.',
			'source-code.webp'
		],

		'demo' => [
			'# Demo',
			'The demo account exists for you to try '.$site_title.' first, before creating your own account.',
			'Anyone can log into the account at any time. If something you\'ve changed suddenly changes without you doing anything, it\'s not because of '.$site_title.', but because someone else made the change.',
			
			'## Reset every full hour',
			'We delete the demo account and re-create it every hour. So if you want to see what it\'s like to start from a new bullet, log in shortly after every full hour.',
			'If you are met with the account not existing when you try to log in to it, wait for the next full hour and try again.',

			'## The demo account is in Swedish?',
			"- When logged in, choose `Inställningar` in the menu.\n- Scroll down and change from `SE` to `GB` below `Språk`.\n- Save the change by hitting the button called `Spara` and then reload the page.",
			'Or simply click/tap on the Swedish flag in the top left corner.'
		],

		'overview' => [
			'balance' => [
				'expenses-notpayed' => 'You will have the following amounts, after all outstanding expenses have been paid:',
				'expenses-payed' => 'Your current amount is as follows:'
			]
		],

		'settings-info' => [
			'sessions' => [
				'Below you can see who has or has had access to your account.'
			]
		],

		'no-permissions' => [
			'You do not have any rights to visit this page.'
		],

		'privacy-policy' => [
			'#Privacy Policy',
			'## 1. About this policy',
			'The policy describes how we treat and manage your data (personal data included) on '.$site_domain.'. We will gradually improve the policy over time. You can see when it was last updated at the bottom of the page. However, we will never change the policy to the extent that it violates your privacy.',
			$site_title.' is a service created by a person and not a company.',

			'## 2. What data we store',
			"- The IP address of the device.\n- The user agent, which usually shows what operating system and browser you are using.\n- **The data you provide yourself ".$site_title.":**\n - Your username.\n - Your email address.\n - Your expenses, debts and loans.\n - Your income, your current amount, and your savings.\n  - Your budget.\n - Your notes.",

			'## 3. How we retrieve your data',
			'### 3.1. IP address and user agent',
			'The IP address and the user agent (where the IP address is personal data) are only retrieved when you log in to '.$site_title.'.',
			'### 3.2. Email address',
			'The email address is obtained via the registration form, or via the form for your account settings when you are logged in. [According to IMY](https://www.imy.se/verkehmsamt/dataskydd/det-har-galler-enligt-gdpr/introduktion-till-gdpr/personstäggung/) (link in Swedish) the e-mail address is considered personal data in Sweden, if it contains your first name and/or surname.',
			'### 3.3. Other data',
			'The other data, such as expenses and income amounts, are only retrieved when you give us this data yourself. '.$site_title.' is a service that does not collect any other information than stated about you automatically. You decide what information you want to give us.',

			'## 4. What we use the data for',
			'### 4.1. IP address and user agent',
			'The IP address can identify both location and Internet Service Provider (ISP) and is stored only to allow the user to see if someone unauthorized has logged in.',
			'The user agent can display the necessary additional information, specifically what browser and device the logged in person used.',
			'To clarify, this information is only stored at login.',
			'### 4.2. Email address',
			'If the user provides their email address, it will only be used for emailing such as reminders and password resets.',
			'### 4.3. Income, balance and savings',
			'Indicating your income and/or what you have in the way of money right now (balance), '.$site_title.' can show how much money you have left to live on during the selected period, after all expenses are paid.',
			'The savings amount is not used for anything more than just showing how much you have saved up for.',
			'### 4.4. Expenses, debts and loans',
			'The reason why '.$site_title.' should function as a financial overview page, is to indicate what you have in expenses during the selected period, and what you have for debts and loans. Based on the data, '.$site_title.' shows a clear overview of what the user\'s financial situation are.',
			'### 4.5. Budget',
			'If the user adds one or more budgets, '.$site_title.' help the user keep their savings in order. As with everything else on '.$site_title.', all input is done manually.',
			'### 4.6. Notes',
			'The notes only exist to allow the user to write down things concerning their financial situation, for example what to talk to customer service at, for example, Klarna about.',

			'## 5. How we store your data',
			'Every account on '.$site_title.' has its own encryption keys that the website uses. Any data that the user provides '.$site_title.' (except the username), is encrypted using these keys. And to preserve the security of users\' sensitive data, we use <mark>'.$config_encryptionmethod.' + salting</mark> as method.',

			'### 5.1. How the user\'s encryption keys are stored',
			'When a person creates their account at '.$site_title.', a JSON file is created and stored outside the root directory on the server. This file contains the encryption keys. It is impossible to know which JSON file belongs to which user, without some help.',
			'We at '.$site_title.' have zero interest or energy to find out who is who. It would also destroy everything we stand for.',

			'### 5.2. How the username is encrypted',
			'In order for the website to be able to identify the person logging in, the username is stored using the website\'s own encryption keys (the same method as the one used for the users\' data).',

			'## 6. Third party services and cookies',
			"- We have chosen not to <mark>use any analytics service</mark>, such as [Plausible](https://plausible.io/). We see no reason why we should use such a service.\n- The <mark >automated Apache HTTP Server logging has been turned off</mark> to protect your privacy.\n- We have carefully picked third-party services from GitHub that <mark>do not affects your privacy</mark>.\n- ".$site_title." <mark>stores a form of cookie called localStorage on your device</mark>, when you change the theme and location of the web page (see choices at the top left) as logged out. localStorage is cleared as soon as you log in. However, ".$site_title." stores <mark>no other cookies (cookies), apart from the session cookie when you log in</mark>.",

			'## 7. Your rights',
			'If you have questions about your rights, do not hesitate to contact us (see clause 10).',
			'### 7.1. Access',
			'You can at any time (when logged in) download your data (export) on the "Settings" page. You have the option of downloading your data in either clear text or encrypted.',
			'Your encryption keys are included in the export.',
			'### 7.2. Amendment',
			'You can at any time (when logged in) change the information you have given '.$site_title.'.',
			'We cannot currently handle your data. But in the future you can share your data with any user, and then decide which data will be handled by whom.',
			'### 7.3. Removal',
			'You can at any time (when logged in) delete all your data or your account on the "Settings" page. You can also delete individual data.',
			'We can\'t remove anything for you.',
			'### 7.4. Restrict processing',
			'You can (when logged in) limit what '.$site_title.' should get access to your current financial situation, by only adding data that you want to give '.$site_title.'.',
			'### 7.5. Objection to processing',
			'You can at any time (when logged in) decide what kind of personal data you want to give '.$site_title.', and choose what kind of personal data should be removed from the website.',
			'### 7.6. Data Portability',
			'You can at any time (when logged in) import previously exported data from '.$site_title.' from the "Settings" page.',

			'## 8. Sharing of your personal data',
			$site_title.' will never share any information to either people or companies, without the user\'s permission. It is the users themselves who decide when and how they want to share something with someone.',

			'## 10. How to contact us',
			'You can contact us at the following email address: <mark>info [at] keizai [dot] se</mark>',

			'----',
			'Last updated: September 28th, 2023'
		],



		'what-is-this' => [
			'overview-calendar' => [
				'# The calendar on the overview page',
				'The calendar shows 2 things: when the period starts and ends (if specified), and which days you have expenses, debts, and/or loans on.',
				
				'## When the period starts and ends',
				'It is not mandatory that you indicate when you receive money, and when you will receive more money next time. However, if you specify this on the settings page, the specified period will be highlighted on the calendar with a slightly lighter background color.',
				
				'## Which days you have any item on',
				'Regardless of whether you enter one or more expenses, debts, and/or loans, '.$site_title.' will show when these occur.',
				"- Expenses are highlighted when they are due.\n- Liabilities are highlighted when they were started.\n- Loans are highlighted when they were borrowed or lent.",
				'The colored dots on the days in the calendar are selectable, and will show you what expenses, debts, or loans are current for the selected date.',
				'These have different color codes: expenses are green, liabilities are purple and loans are orange.',
				
				'## The navigation over the calendar',
				'Above the calendar you can see 2 arrows on each side, with the current month\'s name and the year in the middle. These are optional. The left arrow goes back one month, while the right arrow goes forward one month. And if you select the current month and year, you will get a list of all the months of the current year.',
				'Each month in the month view is selectable and will display that month\'s calendar view.'
			],

			'settings-period' => [
				'# Period',
				'You have to choose either when the period starts and ends, or which day you receive your income. It is not possible to choose both. If you fill in the income date, the fields for "Begins" and "Ends" will be disabled, and so on.',
				'If the start period and/or end period is a holiday (Saturday or Sunday), '.$site_title.' will instead show the nearest weekday on the overview and expenses page.',
				
				'## Income day',
				'If you choose to fill in the day that you receive money, '.$site_title.' will itself determine when the period should start, based on the selected day. This is only for those who receive money once a month.',
				'**Valid value:** a number between 1 and 31.',
				
				'## Move recurring expenses forward',
				'If you have recurring expenses that must be brought forward to the correct period, select "Move recurring expenses forward". Then all expenses that have been marked as recurring will be moved forward the weeks, months, or years that you have chosen for each expense.',
				'**Example:** you have entered a period between October 18 and November 17. You also have more than one expense whose due date is before October 18, and they have been set to recur every month. Instead of selecting each expense, one by one, and changing the due date, you can select "Move recurring expenses forward" and move them all forward one month.'
			],

			'settings-sum-income' => [
				'# Income',
				'The amount refers to the amount you receive thanks to, for example, a job. If you receive, for example, SEK 10,000 during one day, and then SEK 600 after 1 week, you enter SEK 10,600 (without spaces).',
				'When you enter an amount in the text field, it will expand and the icon will change to a calculator. What you will type or have typed will appear as plain text below the text field. This is done so that you can more easily carry out mathematical calculations, like the one in Swish.',
				'**Valid value:** numbers and point or comma as decimal point. No spaces.'
			],

			'settings-sum-current' => [
				'# Current Amount',
				'The amount refers to the amount that you have right now and that you use for your bills and for other things, such as food and entertainment.',
				'When you enter an amount in the text field, it will expand and the icon will change to a calculator. What you will type or have typed will appear as plain text below the text field. This is done so that you can more easily carry out mathematical calculations, like the one in Swish.',
				'**Valid value:** numbers and point or comma as decimal point. No spaces.'
			],

			'settings-sum-savings' => [
				'# Saving',
				'The amount refers to the amount that you have deposited in a savings account, or that you have saved in cash.',
				'When you enter an amount in the text field, it will expand and the icon will change to a calculator. What you will type or have typed will appear as plain text below the text field. This is done so that you can more easily carry out mathematical calculations, like the one in Swish.',
				'**Valid value:** numbers and point or comma as decimal point. No spaces.'
			],

			'username' => [
				'# User name',
				'The username is unique and should be the name you should use when logging into your account here at '.$site_title.'. The username is also used when you want to share an item with another user.'
			],

			'email' => [
				'# Email address',
				'Adding your email address to your account is optional, but it has its benefits.',
				"- You can reset your password via \"Reset Password\" when logged out.\n- You can receive notifications when expenses are due (if you choose this in \"Settings\")."
			],

			'password' => [
				'# Password',
				'**NOTE! NEVER use any passwords shown here!** The passwords in the examples are public.',
				'It is very important that your account password is strong enough to prevent hackers from accessing your account for decades. We recommend that you use a so-called password phrase (passphrase) instead of a regular password. Passphrases are easier to remember and can shut out hackers for the aforementioned period.',
				
				'## Passphrases',
				'If you choose to use passphrases as passwords, it is recommended to use at least 4 words with a hyphen between each word. And to top it off, add a number to the end of the words. The words must be random and each word must start with a capital letter.',
				'Example: `Karlstad-Stockholm3-Kiruna-Tanumshede`',
				
				'## Password',
				'If you choose to use a regular password, it is important that it has the following criteria:',
				"- At least 10 characters long.\n- Uppercase and lowercase letters.\n- Numbers.\n- Preferably also special characters, but not important.",
				'A 10 character password (for example `g2Dr4gVAto`) can usually be cracked in 12 days. The same applies if you have the same password length, but use special characters (for example `e$fM78!z.3`).',
				'The more random characters, with or without special characters, the harder it will be for the hacker to crack it. For example, it takes centuries for hackers to crack the following password: `Dzt76XA673hm23253pWa`.',
				'It is highly recommended that you use [a password generator to create the password](https://bitwarden.com/password-generator/). Use a [password management service such as Bitwarden](https://bitwarden.com) to store the password in it.'
			],

			'settings-password-current' => [
				'# Current password',
				'In order to save the changes to the username, email address and/or the new password, you must enter your current password. This is required for security purposes.'
			],

			'item-number-phone' => [
				'# Mobile Number',
				'The mobile number must be a number at which you can reach the person or company (depending on the type of object).',
				
				'## Swish',
				'If it does not concern a debt, you can enter either the recipient\'s mobile number, or your mobile number. If, for example, you are the one lending, you can enter your mobile number and then share the object with the recipient. The recipient can then scan a QR code that '.$site_title.' shows, via the Swish app for easier repayment.'
			],

			'item-number-payment' => [
				'# Payment number',
				'The payment number is a bankgiro or a plusgiro number, i.e. the company\'s bank account. The number can be found near the payee on the payment receipt.'
			],

			'item-number-ocr' => [
				'# OCR number',
				'The OCR number is the reference number on the invoice. The number is usually the invoice number and/or the customer number, and is usually at the bottom of the payment slip.'
			],

			'item-contact-phone' => [
				'# Phone',
				'The telephone number must be for the company to which the debt is owed. If you choose to enter a number, you will be able to tap the phone number for and start calling from your phone app on your phone. This is not possible from Linux, Windows, or macOS.'
			],

			'item-contact-email' => [
				'# Email',
				'The email address must be for the company to which the debt is owed. You will then be able to click/press the address when viewing the details for a debt, for and start writing to them. This is possible from any operating system.'
			],

			'loggedout-email-forgotpw' => [
				'# Email address',
				'In order to reset the password for your account, you must have associated your email address with your account. If you haven\'t, you can\'t use the recovery.'
			],

			'loggedout-resetcode' => [
				'# Recovery Code',
				'When protecting your account with two-step verification (TFA, 2FA, or TOTP), '.$site_title.' will provide you with a recovery code. You can use it when you have been locked out of your account. The code will then remove the two-step verification so you can sign in without it.',
				'Unfortunately, if you do not have a recovery code and you cannot log in, please contact us at '.$config_email_contact.'. We can then try to help you with a security issue.'
			]
		]
	];

?>