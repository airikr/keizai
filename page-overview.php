<?php

	require_once 'site-header.php';



	if($period_start_empty == false AND $period_end_empty == false) {
		$period = "AND timestamp_date >= '".$period_start."' AND timestamp_date < '".$period_end."'";
		$period_extra = "AND timestamp_date >= '".$period_start."' AND timestamp_date < '".$period_end."'";
		$period_future = "AND timestamp_date >= '".$period_end."'";
		$period_total = "AND timestamp_date >= '".$period_start."' AND timestamp_date < '".$period_end."'";
		$period_next_future = "AND timestamp_date >= '".$period_start_nextmonth."' AND timestamp_date < '".$period_end_nextmonth."'";
		$period_next_recurrent = "AND timestamp_date <= '".$period_start_nextmonth."'";

	} elseif($period_incomeday_empty == false) {
		$period = "AND timestamp_date >= '".$period_incomeday."' AND timestamp_date < '".$period_incomeday_nextmonth_1."'";
		$period_extra = "AND timestamp_date >= '".$period_incomeday."' AND timestamp_date <= '".$period_incomeday_nextmonth_1."'";
		$period_future = "AND timestamp_date > '".$period_incomeday_nextmonth_1."'";
		$period_total = "AND timestamp_date >= '".$period_incomeday."' AND timestamp_date <= '".$period_incomeday_nextmonth_1."'";
		$period_next_future = "AND timestamp_date >= '".$period_incomeday_nextmonth_1."' AND timestamp_date < '".$period_incomeday_nextmonth_1."'";
		$period_next_recurrent = "AND timestamp_date <= '".$period_incomeday_nextmonth_1."'";
	}



	$expense =
	sql("SELECT (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
			 AND is_expense IS NOT NULL
			 AND check_expense_payed IS NULL
			 ".$period."
		 ) AS count_e_notpayed,
		 (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
			 AND is_expense IS NOT NULL
			 AND check_expense_payed IS NOT NULL
			 ".$period."
		 ) AS count_e_payed,
		 (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
			 AND is_expense IS NOT NULL
			 AND check_expense_extra IS NOT NULL
			 ".(isset($period_extra) ? $period_extra : '')."
		 ) AS count_e_extra,
		 (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
			 AND is_expense IS NOT NULL
			 ".(isset($period_future) ? $period_future : '')."
		 ) AS count_e_future,
		 (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
			 AND is_expense IS NOT NULL
		 ) AS count_e_total_a,
		 (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
			 AND is_expense IS NOT NULL
			 ".(isset($period_total) ? $period_total : '')."
		 ) AS count_e_total_p,
		 (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
			 AND check_expense_recurrent IS NOT NULL
			 AND is_expense IS NOT NULL
		 ) AS count_e_recurrent,
		 (
			 SELECT COUNT(s.id_user_with)
			 FROM items i
			 JOIN shares s
			 ON i.id = s.id_item
			 WHERE s.id_user_with = :_iduser
			 AND i.is_expense IS NOT NULL
		 ) AS count_e_shared
		", Array(
			'_iduser' => (int)$user['id']
		), 'fetch');

	$debt =
	sql("SELECT (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 AND is_debt IS NOT NULL
			 AND check_debt_company IS NULL
		 ) AS count_d_private,
		 (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 AND is_debt IS NOT NULL
			 AND check_debt_company IS NOT NULL
		 ) AS count_d_company,
		 (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 AND is_debt IS NOT NULL
		 ) AS count_d_total,
		 (
			 SELECT COUNT(s.id_user_with)
			 FROM items i
			 JOIN shares s
			 ON i.id = s.id_item
			 WHERE s.id_user_with = :_iduser
			 AND i.is_debt IS NOT NULL
		 ) AS count_d_shared
		", Array(
			'_iduser' => (int)$user['id']
		), 'fetch');

	$loan =
	sql("SELECT (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 AND is_loan IS NOT NULL
			 AND check_loan_out IS NULL
		 ) AS count_l_in,
		 (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 AND is_loan IS NOT NULL
			 AND check_loan_out IS NOT NULL
		 ) AS count_l_out,
		 (
			 SELECT COUNT(id_user)
			 FROM items
			 WHERE id_user = :_iduser
			 AND is_loan IS NOT NULL
		 ) AS count_l_total,
		 (
			 SELECT COUNT(s.id_user_with)
			 FROM items i
			 JOIN shares s
			 ON i.id = s.id_item
			 WHERE s.id_user_with = :_iduser
			 AND i.is_loan IS NOT NULL
		 ) AS count_l_shared
		", Array(
			'_iduser' => (int)$user['id']
		), 'fetch');



	$e_notpayed =
	sql("SELECT data_sum, data_currency
		 FROM items
		 WHERE id_user = :_iduser
		 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
		 AND check_expense_payed IS NULL
		 AND is_expense IS NOT NULL
		 ".$period."
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_e_notpayed = 0;
	$sum_e_notpayed_othercurrency = 0;
	foreach($e_notpayed AS $unpayed) {
		if($unpayed['data_currency'] == $user['data_currency']) {
			$sum_e_notpayed += endecrypt($unpayed['data_sum'], false);
		} else {
			$sum_e_notpayed_othercurrency += currency(endecrypt($unpayed['data_sum'], false), endecrypt($unpayed['data_currency'], false), null, true);
		}
	}


	$e_payed =
	sql("SELECT data_sum, data_currency
		 FROM items
		 WHERE id_user = :_iduser
		 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
		 AND check_expense_payed IS NOT NULL
		 AND is_expense IS NOT NULL
		 ".$period."
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_e_payed = 0;
	$sum_e_payed_othercurrency = 0;
	foreach($e_payed AS $payed) {
		if($payed['data_currency'] == $user['data_currency']) {
			$sum_e_payed += endecrypt($payed['data_sum'], false);
		} else {
			$sum_e_payed_othercurrency += currency(endecrypt($payed['data_sum'], false), endecrypt($payed['data_currency'], false), null, true);
		}
	}


	$e_extra =
	sql("SELECT data_sum, data_currency
		 FROM items
		 WHERE id_user = :_iduser
		 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
		 AND check_expense_extra IS NOT NULL
		 AND is_expense IS NOT NULL
		 ".(isset($period_extra) ? $period_extra : '')."
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_e_extra = 0;
	$sum_e_extra_othercurrency = 0;
	foreach($e_extra AS $extra) {
		if($extra['data_currency'] == $user['data_currency']) {
			$sum_e_extra += endecrypt($extra['data_sum'], false);
		} else {
			$sum_e_extra_othercurrency += currency(endecrypt($extra['data_sum'], false), endecrypt($extra['data_currency'], false), null, true);
		};
	}


	$e_future =
	sql("SELECT data_sum, data_currency
		 FROM items
		 WHERE id_user = :_iduser
		 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
		 AND is_expense IS NOT NULL
		 ".(isset($period_future) ? $period_future : '')."
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_e_future = 0;
	$sum_e_future_othercurrency = 0;
	foreach($e_future AS $future) {
		if($future['data_currency'] == $user['data_currency']) {
			$sum_e_future += endecrypt($future['data_sum'], false);
		} else {
			$sum_e_future_othercurrency += currency(endecrypt($future['data_sum'], false), endecrypt($future['data_currency'], false), null, true);
		}
	}


	$e_future_next =
	sql("SELECT data_name, data_sum, data_currency
		 FROM items
		 WHERE id_user = :_iduser
		 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
		 AND is_expense IS NOT NULL
		 ".(isset($period_next_future) ? $period_next_future : '')."
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_e_future_next = 0;
	$sum_e_future_othercurrency_next = 0;
	foreach($e_future_next AS $future_next) {
		if($future_next['data_currency'] == $user['data_currency']) {
			$sum_e_future_next += endecrypt($future_next['data_sum'], false);
		} else {
			$sum_e_future_othercurrency_next += currency(endecrypt($future_next['data_sum'], false), endecrypt($future_next['data_currency'], false), null, true);
		}
	}


	$e_total_a =
	sql("SELECT data_sum, data_currency
		 FROM items
		 WHERE id_user = :_iduser
		 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
		 AND is_expense IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_e_total_a = 0;
	$sum_e_total_a_othercurrency = 0;
	foreach($e_total_a AS $total_ea) {
		if($total_ea['data_currency'] == $user['data_currency']) {
			$sum_e_total_a += endecrypt($total_ea['data_sum'], false);
		} else {
			$sum_e_total_a_othercurrency += currency(endecrypt($total_ea['data_sum'], false), endecrypt($total_ea['data_currency'], false), null, true);
		}
	}


	$e_total_p =
	sql("SELECT data_sum, data_currency
		 FROM items
		 WHERE id_user = :_iduser
		 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
		 AND is_expense IS NOT NULL
		 ".(isset($period_total) ? $period_total : '')."
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_e_total_p = 0;
	$sum_e_total_p_othercurrency = 0;
	foreach($e_total_p AS $total_ep) {
		if($total_ep['data_currency'] == $user['data_currency']) {
			$sum_e_total_p += endecrypt($total_ep['data_sum'], false);
		} else {
			$sum_e_total_p_othercurrency += currency(endecrypt($total_ep['data_sum'], false), endecrypt($total_ep['data_currency'], false), null, true);
		}
	}


	$e_recurrent =
	sql("SELECT data_sum, data_currency
		 FROM items
		 WHERE id_user = :_iduser
		 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
		 AND check_expense_recurrent IS NOT NULL
		 AND is_expense IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_e_recurrent = 0;
	$sum_e_recurrent_othercurrency = 0;
	foreach($e_recurrent AS $recurrent) {
		if($recurrent['data_currency'] == $user['data_currency']) {
			$sum_e_recurrent += endecrypt($recurrent['data_sum'], false);
		} else {
			$sum_e_recurrent_othercurrency += currency(endecrypt($recurrent['data_sum'], false), endecrypt($recurrent['data_currency'], false), null, true);
		}
	}


	$e_recurrent_next =
	sql("SELECT data_sum, data_currency
		 FROM items
		 WHERE id_user = :_iduser
		 ".($count_accounts == 0 ? null : "AND id_account = ".(int)$default_account['id'])."
		 AND check_expense_recurrent IS NOT NULL
		 AND is_expense IS NOT NULL
		 ".(isset($period_next_recurrent) ? $period_next_recurrent : '')."
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_e_recurrent_next = 0;
	$sum_e_recurrent_othercurrency_next = 0;
	foreach($e_recurrent_next AS $recurrent_next) {
		if($recurrent_next['data_currency'] == $user['data_currency']) {
			$sum_e_recurrent_next += endecrypt($recurrent_next['data_sum'], false);
		} else {
			$sum_e_recurrent_othercurrency_next += currency(endecrypt($recurrent_next['data_sum'], false), endecrypt($recurrent_next['data_currency'], false), null, true);
		}
	}



	if($expense['count_e_shared'] != 0) {
		$e_shared =
		sql("SELECT i.data_sum, i.data_currency, s.id_user, s.id_user_with
			 FROM items i
			 JOIN shares s
			 ON i.id = s.id_item
			 WHERE s.id_user_with = :_iduser
			 AND i.is_expense IS NOT NULL
			", Array(
				'_iduser' => (int)$user['id']
			));

		$sum_e_shared = 0;
		$sum_e_shared_othercurrency = 0;
		foreach($e_shared AS $shared_e) {
			$usershare =
			sql("SELECT id, data_saltedstring_1, data_saltedstring_2, data_currency
				 FROM users
				 WHERE id = :_iduser
				", Array(
					'_iduser' => (int)$shared_e['id_user']
				), 'fetch');

			$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($usershare['data_saltedstring_1'] . (int)$usershare['id'] . $usershare['data_saltedstring_2']).'.json'), true);

			if($shared_e['data_currency'] == $user['data_currency']) {
				$sum_e_shared += endecrypt($shared_e['data_sum'], false);
			} else {
				$sum_e_shared_othercurrency += currency(endecrypt($shared_e['data_sum'], false), endecrypt($shared_e['data_currency'], false), null, true);
			}
		}
	}





	$c_subs =
	sql("SELECT COUNT(check_expense_subscription)
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_expense IS NOT NULL
		 AND check_expense_subscription IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		), 'count');

	$get_subs =
	sql("SELECT data_sum, data_currency, data_recurrence, data_recurrence_type
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_expense IS NOT NULL
		 AND check_expense_subscription IS NOT NULL
		 AND data_recurrence_type IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_subs_monthly = 0;
	$sum_subs_quarterly = 0;
	$sum_subs_halfyearly = 0;
	$sum_subs_yearly = 0;
	$sum_subs_monthly_othercurrency = 0;
	$sum_subs_quarterly_othercurrency = 0;
	$sum_subs_halfyearly_othercurrency = 0;
	$sum_subs_yearly_othercurrency = 0;

	if($c_subs != 0) {
		foreach($get_subs AS $sub) {
			if(endecrypt($sub['data_recurrence'], false) == '1' AND endecrypt($sub['data_recurrence_type'], false) == 'month') {
				if($sub['data_currency'] == $user['data_currency']) {
					$sum_subs_monthly += endecrypt($sub['data_sum'], false);
				} else {
					$sum_subs_monthly_othercurrency += currency(endecrypt($sub['data_sum'], false), endecrypt($sub['data_currency'], false), null, true);
				}

			} elseif(endecrypt($sub['data_recurrence'], false) == '3' AND endecrypt($sub['data_recurrence_type'], false) == 'month') {
				if($sub['data_currency'] == $user['data_currency']) {
					$sum_subs_quarterly += (endecrypt($sub['data_sum'], false) / 3);
				} else {
					$sum_subs_quarterly_othercurrency += currency((endecrypt($sub['data_sum'], false) / 3), endecrypt($sub['data_currency'], false), null, true);
				}

			} elseif(endecrypt($sub['data_recurrence'], false) == '6' AND endecrypt($sub['data_recurrence_type'], false) == 'month') {
				if($sub['data_currency'] == $user['data_currency']) {
					$sum_subs_halfyearly += (endecrypt($sub['data_sum'], false) / 6);
				} else {
					$sum_subs_halfyearly_othercurrency += currency((endecrypt($sub['data_sum'], false) / 6), endecrypt($sub['data_currency'], false), null, true);
				}

			} elseif(endecrypt($sub['data_recurrence'], false) == '1' AND endecrypt($sub['data_recurrence_type'], false) == 'year') {
				if($sub['data_currency'] == $user['data_currency']) {
					$sum_subs_yearly += endecrypt($sub['data_sum'], false);
				} else {
					$sum_subs_yearly_othercurrency += currency(endecrypt($sub['data_sum'], false), endecrypt($sub['data_currency'], false), null, true);
				}
			}
		}
	}





	$d_private =
	sql("SELECT data_sum
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_debt IS NOT NULL
		 AND check_debt_company IS NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_d_private = 0;
	foreach($d_private AS $private_d) {
		$sum_d_private += endecrypt($private_d['data_sum'], false);
	}


	$d_companies =
	sql("SELECT data_sum
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_debt IS NOT NULL
		 AND check_debt_company IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_d_companies = 0;
	foreach($d_companies AS $company_d) {
		$sum_d_companies += endecrypt($company_d['data_sum'], false);
	}


	$d_total =
	sql("SELECT data_sum
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_debt IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_d_total = 0;
	foreach($d_total AS $total_d) {
		$sum_d_total += endecrypt($total_d['data_sum'], false);
	}



	if($debt['count_d_shared'] != 0) {
		$d_shared =
		sql("SELECT s.data_payment, s.data_sum AS sum_share, i.data_sum AS sum_item, s.id_user
			 FROM items i
			 JOIN shares s
			 ON i.id = s.id_item
			 WHERE s.id_user_with = :_iduser
			 AND i.is_debt IS NOT NULL
			", Array(
				'_iduser' => (int)$user['id']
			));

		$sum_d_shared = 0;
		foreach($d_shared AS $shared_d) {
			$usershare =
			sql("SELECT id, data_saltedstring_1, data_saltedstring_2, data_currency
				 FROM users
				 WHERE id = :_iduser
				", Array(
					'_iduser' => (int)$shared_d['id_user']
				), 'fetch');

			$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($usershare['data_saltedstring_1'] . (int)$usershare['id'] . $usershare['data_saltedstring_2']).'.json'), true);

			if(empty($shared_d['data_payment']) OR endecrypt($shared_d['data_payment'], false) == 'sum') {
				$sum_d_shared += (empty($shared_d['sum_share']) ? endecrypt($shared_d['sum_item'], false) : endecrypt($shared_d['sum_share'], false));

			} elseif(!empty($shared_d['data_payment']) AND endecrypt($shared_d['data_payment'], false) == 'percent') {
				$sum_d_shared += endecrypt($shared_d['sum_item'], false) * (endecrypt($shared_d['sum_share'], false) / 100);

			} else {
				$sum_d_shared += endecrypt($item['data_sum'], false);
			}
		}
	}





	$l_in =
	sql("SELECT data_sum
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_loan IS NOT NULL
		 AND check_loan_out IS NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_l_in = 0;
	foreach($l_in AS $in_d) {
		$sum_l_in += endecrypt($in_d['data_sum'], false);
	}


	$l_out =
	sql("SELECT data_sum
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_loan IS NOT NULL
		 AND check_loan_out IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_l_out = 0;
	foreach($l_out AS $out_l) {
		$sum_l_out += endecrypt($out_l['data_sum'], false);
	}


	$l_total =
	sql("SELECT data_sum
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_loan IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$sum_l_total = 0;
	foreach($l_total AS $total_l) {
		$sum_l_total += endecrypt($total_l['data_sum'], false);
	}



	if($loan['count_l_shared'] != 0) {
		$l_shared =
		sql("SELECT i.data_currency, s.data_payment, s.data_sum AS sum_share, i.data_sum AS sum_item, s.id_user
			 FROM items i
			 JOIN shares s
			 ON i.id = s.id_item
			 WHERE s.id_user_with = :_iduser
			 AND i.is_loan IS NOT NULL
			", Array(
				'_iduser' => (int)$user['id']
			));

		$sum_l_shared = 0;
		foreach($l_shared AS $shared_l) {
			$usershare =
			sql("SELECT id, data_saltedstring_1, data_saltedstring_2, data_currency
				 FROM users
				 WHERE id = :_iduser
				", Array(
					'_iduser' => (int)$shared_l['id_user']
				), 'fetch');

			$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($usershare['data_saltedstring_1'] . (int)$usershare['id'] . $usershare['data_saltedstring_2']).'.json'), true);

			if(empty($shared_l['data_payment']) OR endecrypt($shared_l['data_payment'], false) == 'sum') {
				$sum_l_shared += (empty($shared_l['sum_share']) ? endecrypt($shared_l['sum_item'], false) : endecrypt($shared_l['sum_share'], false));

			} elseif(!empty($shared_l['data_payment']) AND endecrypt($shared_l['data_payment'], false) == 'percent') {
				$sum_l_shared += endecrypt($shared_l['sum_item'], false) * (endecrypt($shared_l['sum_share'], false) / 100);

			} else {
				$sum_l_shared += endecrypt($item['data_sum'], false);
			}
		}
	}







	$enc = json_decode(file_get_contents($dir_userfiles.'/'.md5($user['data_saltedstring_1'] . (int)$user['id'] . $user['data_saltedstring_2']).'.json'), true);

	$days_left = null;
	if($period_start_empty == false AND $period_end_empty == false AND empty($user['data_incomeday'])) {
		$days_left = x_left($period_start, $period_end);

	} elseif(empty($user['timestamp_period_start']) AND empty($user['timestamp_period_end']) AND $period_incomeday_empty == false) {
		$days_left = x_left($period_incomeday, $period_incomeday_nextmonth_1);
	}

	$days_left_next = null;
	if($period_start_empty == false AND $period_end_empty == false AND empty($user['data_incomeday'])) {
		$days_left_next = x_left($period_end, strtotime('+1 month', $period_end));

	} elseif(empty($user['timestamp_period_start']) AND empty($user['timestamp_period_end']) AND $period_incomeday_empty == false) {
		$days_left_next = x_left($period_incomeday, $period_incomeday_nextmonth_1);
	}


	$weeks_left = (empty($days_left) ? null : format_number(($days_left / 7), 0));
	$weeks_left_next = (empty($days_left_next) ? null : format_number(($days_left_next / 7), 0));

	if(!empty($user['data_sum_balance']) OR !empty($user['data_sum_income'])) {
		$balance = endecrypt((empty($user['data_sum_balance']) ? $user['data_sum_income'] : $user['data_sum_balance']), false) - ($sum_e_notpayed + $sum_e_notpayed_othercurrency);
		$balance_next = endecrypt($user['data_sum_income'], false) - (($sum_e_recurrent_next + $sum_e_recurrent_othercurrency_next) + ($sum_e_future_next + $sum_e_future_othercurrency_next));
	}


	$factory = new CalendR\Calendar;
	$month = $factory->getMonth(date('Y'), date('n'));


	$currency_str = '<span class="currency-website">'.svgicon('currency-'.mb_strtolower(endecrypt($user['data_currency'], false))).'</span>';
	$currency_str .= '<span class="currency-screenshot currency">'.mb_strtoupper(endecrypt($user['data_currency'], false)).'</span>';







	echo '<section id="overview">';
		echo '<div class="left">';
			echo screenshot('.balance', date('Y-m-d').' - '.$lang['screenshots']['overview-balance'], null, true);

			echo '<div class="balance">';
				echo '<div class="text">';
					if(($sum_e_notpayed + $sum_e_notpayed_othercurrency) != 0) {
						echo $lang['overview']['balance']['expenses-notpayed'];
					} else {
						echo $lang['overview']['balance']['expenses-payed'];
					}
				echo '</div>';

				echo '<div class="sum">';
					if(empty($user['data_sum_income'])) {
						echo format_number(0) . $currency_str;
					} else {
						if($currencies_exists != 0 AND !empty($sum_e_notpayed_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number($balance);
						echo $currency_str;
					}
				echo '</div>';
			echo '</div>';



			$c_expenses = 0;
			$c_debts = 0;
			$c_loans = 0;

			echo '<div class="calendar">';
				echo '<div class="dim"></div>';

				echo '<div class="icons side-by-side">';
					whatisthis('overview-calendar');
					echo screenshot('.calendar', $lang['screenshots']['overview-calendar']);
				echo '</div>';

				echo '<div class="top">';
					echo '<div class="left">';
						echo '<a href="javascript:void(0)" class="nav left" data-date="'.date('Y-m-d', strtotime('-1 month '.$month->format('Y-m-d'))).'">';
							echo svgicon('arrow-left');
						echo '</a>';

						echo '<div class="nav left wait">';
							echo svgicon('wait');
						echo '</div>';
					echo '</div>';

					echo '<div class="middle">';
						echo '<a href="javascript:void(0)" class="view-month '.$month->format('n').'" data-month="'.$month->format('n').'">';
							echo $lang['months'][mb_strtolower($month->format('F'))].' '.$month->format('Y');
						echo '</a>';

						echo '<div class="wait '.$month->format('n').'">';
							echo svgicon('wait');
						echo '</div>';
					echo '</div>';

					echo '<div class="right">';
						echo '<a href="javascript:void(0)" class="nav right" data-date="'.date('Y-m-d', strtotime('+1 month '.$month->format('Y-m-d'))).'">';
							echo svgicon('arrow-right');
						echo '</a>';

						echo '<div class="nav right wait">';
							echo svgicon('wait');
						echo '</div>';
					echo '</div>';
				echo '</div>';

				echo '<table cellspacing="0">';
					echo '<thead>';
						echo '<tr>';
							echo '<th>'.$lang['words']['week-short'].'</th>';
							foreach($lang['days-short'] AS $day) {
								echo '<th>'.$day.'</th>';
							}
						echo '</tr>';
					echo '</thead>';



					foreach($month AS $week) {
						echo '<tbody>';
							echo '<tr>';
								echo '<td class="week">'.$week->format('W').'</td>';

								foreach($week AS $day) {
									$has_expenses =
									sql("SELECT COUNT(timestamp_date)
										 FROM items
										 WHERE id_user = :_iduser
										 AND timestamp_date = :_date
										 AND is_expense IS NOT NULL
										", Array(
											'_iduser' => (int)$user['id'],
											'_date' => strtotime($day->format('Y-m-d'))
										), 'count');

									$has_debts =
									sql("SELECT COUNT(timestamp_date)
										 FROM items
										 WHERE id_user = :_iduser
										 AND timestamp_date = :_date
										 AND is_debt IS NOT NULL
										", Array(
											'_iduser' => (int)$user['id'],
											'_date' => strtotime($day->format('Y-m-d'))
										), 'count');

									$has_loans =
									sql("SELECT COUNT(timestamp_date)
										 FROM items
										 WHERE id_user = :_iduser
										 AND timestamp_date = :_date
										 AND is_loan IS NOT NULL
										", Array(
											'_iduser' => (int)$user['id'],
											'_date' => strtotime($day->format('Y-m-d'))
										), 'count');

									if($has_expenses != 0 AND $day->format('Y-m') == date('Y-m')) { $c_expenses += $has_expenses; }
									if($has_debts != 0 AND $day->format('Y-m') == date('Y-m')) { $c_debts += $has_debts; }
									if($has_loans != 0 AND $day->format('Y-m') == date('Y-m')) { $c_loans += $has_loans; }

									echo '<td'.(!$month->includes($day) ? ' class="out-of-month"' : '').'>';
										echo '<div class="digit';

										if($period_start_empty == false AND $period_end_empty == false AND empty($user['data_incomeday'])) {
											foreach(listalldates(date('Y-m-d', $period_start), date('Y-m-d', $period_end)) AS $date) {
												echo ($date->format('Y-m-d') == $day->format('Y-m-d') ? ' active' : '');
											}

										} elseif(empty($user['timestamp_period_start']) AND empty($user['timestamp_period_end']) AND $period_incomeday_empty == false) {
											foreach(listalldates(date('Y-m-d', $period_incomeday), date('Y-m-d', $period_incomeday_nextmonth_1)) AS $date) {
												echo ($date->format('Y-m-d') == $day->format('Y-m-d') ? ' active' : '');
											}
										}

										echo '">';
											echo '<a href="'.url('filter/year:'.$day->format('Y').'/month:'.$day->format('n').'/day:'.$day->format('j')).'">';
												echo $day->format('j');
											echo '</a>';
										echo '</div>';

										echo ($day->format('Y-m-d') == date('Y-m-d') ? '<div class="today"></div>' : '');

										echo '<div class="dots">';
											echo '<div class="dot expenses'.($has_expenses == 0 ? ' inactive' : '').'"></div>';
											echo '<div class="dot debts'.($has_debts == 0 ? ' inactive' : '').'"></div>';
											echo '<div class="dot loans'.($has_loans == 0 ? ' inactive' : '').'"></div>';
										echo '</div>';
									echo '</td>';
								}
							echo '</tr>';
						echo '</tbody>';
					}
				echo '</table>';



				echo '<div class="statistics">';
					echo '<div><div><div class="dot expenses"></div>'.$lang['words']['expenses'].'</div><div'.(empty($c_expenses) ? ' class="empty"' : '').'>'.(empty($c_expenses) ? 0 : $c_expenses).'</div></div>';
					echo '<div><div><div class="dot debts"></div>'.$lang['words']['debts'].'</div><div'.(empty($c_debts) ? ' class="empty"' : '').'>'.(empty($c_debts) ? 0 : $c_debts).'</div></div>';
					echo '<div><div><div class="dot loans"></div>'.$lang['words']['loans'].'</div><div'.(empty($c_loans) ? ' class="empty"' : '').'>'.(empty($c_loans) ? 0 : $c_loans).'</div></div>';
					echo '<div><div class="total">'.$lang['words']['total'].'</div><div'.((empty($c_expenses + $c_debts + $c_loans)) ? ' class="empty"' : '').'>'.($c_expenses + $c_debts + $c_loans).'</div></div>';
				echo '</div>';
			echo '</div>';
		echo '</div>';



		echo '<div class="right statistics">';
			echo '<div class="expenses">';
				echo '<h2>';
					echo '<div class="title">';
						echo screenshot('.statistics > .expenses', $lang['screenshots']['overview-expenses']);
						echo $lang['subtitles']['expenses'] . ($count_accounts == 0 ? '' : ' ('.mb_strtolower($lang['subtitles']['default-account']).')');
					echo '</div>';

					echo '<div class="new">';
						echo '<a href="'.url('add-expense'.($count_accounts == 0 ? '' : '/account:'.$default_account['id'])).'" class="color-green">'.$lang['words']['new'].'</a>';
					echo '</div>';
				echo '</h2>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo (((empty($user['timestamp_period_start']) AND empty($user['timestamp_period_start'])) AND empty($user['data_incomeday'])) ? '<span title="'.$lang['tooltips']['no-period'].'">' : '<a href="'.url('expenses'.($count_accounts == 0 ? null : '/account:'.(int)$default_account['id']).'/filter:period').'" title="'.$lang['tooltips']['showall-expenses-period'].'">');
							if($period_start_empty == false AND $period_end_empty == false) {
								echo date_($period_start, 'day-month').' - '.date_($period_end, 'day-month');

							} elseif($period_incomeday_empty == false) {
								echo date_($period_incomeday, 'day-month').' - '.date_($period_incomeday_nextmonth_1, 'day-month');

							} else {
								echo $lang['words']['current-period'];
							}

							echo ' ('.format_number($expense['count_e_total_p'], 0).')';
						echo (((empty($user['timestamp_period_start']) AND empty($user['timestamp_period_start'])) AND empty($user['data_incomeday'])) ? '</span>' : '</a>');
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($currencies_exists != 0 AND !empty($sum_e_total_p_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number(($sum_e_total_p + $sum_e_total_p_othercurrency));
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('expenses'.($count_accounts == 0 ? null : '/account:'.(int)$default_account['id']).'/filter:not-payed').'" title="'.$lang['tooltips']['showall-expenses-notpayed'].'">';
							echo $lang['words']['not-payed'].' ('.format_number($expense['count_e_notpayed'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($currencies_exists != 0 AND !empty($sum_e_notpayed_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number(($sum_e_notpayed + $sum_e_notpayed_othercurrency));
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('expenses'.($count_accounts == 0 ? null : '/account:'.(int)$default_account['id']).'/filter:payed').'" title="'.$lang['tooltips']['showall-expenses-payed'].'">';
							echo $lang['words']['payed'].' ('.format_number($expense['count_e_payed'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($currencies_exists != 0 AND !empty($sum_e_payed_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number(($sum_e_payed + $sum_e_payed_othercurrency));
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('expenses'.($count_accounts == 0 ? null : '/account:'.(int)$default_account['id']).'/filter:extra').'" title="'.$lang['tooltips']['showall-expenses-extra'].'">';
							echo $lang['words']['extra'].' ('.format_number($expense['count_e_extra'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($currencies_exists != 0 AND !empty($sum_e_extra_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number(($sum_e_extra + $sum_e_extra_othercurrency));
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('expenses'.($count_accounts == 0 ? null : '/account:'.(int)$default_account['id']).'/filter:future').'" title="'.$lang['tooltips']['showall-expenses-future'].'">';
							echo $lang['words']['future'].' ('.format_number($expense['count_e_future'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($currencies_exists != 0 AND !empty($sum_e_future_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number(($sum_e_future + $sum_e_future_othercurrency));
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('expenses'.($count_accounts == 0 ? null : '/account:'.(int)$default_account['id'])).'" title="'.$lang['tooltips']['showall-expenses'].'">';
							echo $lang['words']['total'].' ('.format_number($expense['count_e_total_a'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($currencies_exists != 0 AND !empty($sum_e_total_a_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number(($sum_e_total_a + $sum_e_total_a_othercurrency));
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('expenses'.($count_accounts == 0 ? null : '/account:'.(int)$default_account['id']).'/filter:recurrent').'" title="'.$lang['tooltips']['showall-expenses-recurrent'].'">';
							echo $lang['words']['recurrent'].' ('.format_number($expense['count_e_recurrent'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($currencies_exists != 0 AND !empty($sum_e_recurrent_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number(($sum_e_recurrent + $sum_e_recurrent_othercurrency));
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('expenses/filter:shared').'" title="'.$lang['tooltips']['showall-expenses-shared'].'">';
							echo $lang['words']['shared-overview'].' ('.format_number($expense['count_e_shared'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($expense['count_e_shared'] == 0) {
							echo format_number(0);
						} else {
							echo format_number(($sum_e_shared + $sum_e_shared_othercurrency));
						}

						echo $currency_str;
					echo '</div>';
				echo '</div>';
			echo '</div>';







			echo '<div class="subscriptions">';
				echo '<h2>';
					echo '<div class="title">';
						echo screenshot('.statistics > .subscriptions', $lang['screenshots']['overview-subscriptions']);
						echo $lang['subtitles']['subscriptions'];
					echo '</div>';
				echo '</h2>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('expenses/filter:subscriptions').'" title="'.$lang['tooltips']['showall-expenses-subscriptions'].'">';
							echo $lang['words']['active-subscriptions'];
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value">';
						echo format_number($c_subs, 0);
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['words']['monthly-cost'];
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($currencies_exists != 0 AND !empty($sum_subs_monthly_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number(($sum_subs_monthly + $sum_subs_monthly_othercurrency));
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['words']['quarterly-cost'];
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($currencies_exists != 0 AND !empty($sum_subs_quarterly_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number((($sum_subs_monthly + $sum_subs_monthly_othercurrency) * 3) + ($sum_subs_quarterly + $sum_subs_quarterly_othercurrency) * 3);
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['words']['halfyearly-cost'];
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($currencies_exists != 0 AND !empty($sum_subs_halfyearly_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number((($sum_subs_monthly + $sum_subs_monthly_othercurrency) * 6) + ($sum_subs_halfyearly + $sum_subs_halfyearly_othercurrency) * 6);
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['words']['yearly-cost'];
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($currencies_exists != 0 AND !empty($sum_subs_monthly_othercurrency)) {
							echo $lang['words']['approx'].' ';
						}

						echo format_number((($sum_subs_monthly + $sum_subs_monthly_othercurrency) * 12) + (($sum_subs_halfyearly + $sum_subs_halfyearly_othercurrency) * 6) + ($sum_subs_yearly + $sum_subs_yearly_othercurrency));
						echo $currency_str;
					echo '</div>';
				echo '</div>';
			echo '</div>';







			echo '<div class="debts">';
				echo '<h2>';
					echo '<div class="title">';
						echo screenshot('.statistics > .debts', $lang['screenshots']['overview-debts']);
						echo $lang['subtitles']['debts'];
					echo '</div>';

					echo '<div class="new">';
						echo '<a href="'.url('add-debt').'" class="color-green">'.$lang['words']['new'].'</a>';
					echo '</div>';
				echo '</h2>';

				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('debts/filter:private').'" title="'.$lang['tooltips']['showall-debts-individuals'].'">';
							echo $lang['words']['individuals'].' ('.format_number($debt['count_d_private'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						echo format_number($sum_d_private);
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('debts/filter:companies').'" title="'.$lang['tooltips']['showall-debts-companies'].'">';
							echo $lang['words']['companies'].' ('.format_number($debt['count_d_company'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						echo format_number($sum_d_companies);
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('debts').'" title="'.$lang['tooltips']['showall-debts'].'">';
							echo $lang['words']['total'].' ('.format_number($debt['count_d_total'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						echo format_number($sum_d_total);
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('debts/filter:shared').'" title="'.$lang['tooltips']['showall-debts-shared'].'">';
							echo $lang['words']['shared-overview'].' ('.format_number($debt['count_d_shared'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($debt['count_d_shared'] == 0) {
							echo format_number(0);
						} else {
							echo format_number($sum_d_shared);
						}

						echo $currency_str;
					echo '</div>';
				echo '</div>';
			echo '</div>';







			echo '<div class="loans">';
				echo '<h2>';
					echo '<div class="title">';
						echo screenshot('.statistics > .loans', $lang['screenshots']['overview-loans']);
						echo $lang['subtitles']['loans'];
					echo '</div>';

					echo '<div class="new">';
						echo '<a href="'.url('add-loan').'" class="color-green">'.$lang['words']['new'].'</a>';
					echo '</div>';
				echo '</h2>';

				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('loans/filter:private').'" title="'.$lang['tooltips']['showall-loans-individuals'].'">';
							echo $lang['words']['borrowed'].' ('.format_number($loan['count_l_in'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						echo format_number($sum_l_in);
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('loans/filter:companies').'" title="'.$lang['tooltips']['showall-loans-companies'].'">';
							echo $lang['words']['lend'].' ('.format_number($loan['count_l_out'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						echo format_number($sum_l_out);
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('loans').'" title="'.$lang['tooltips']['showall-loans'].'">';
							echo $lang['words']['total'].' ('.format_number($loan['count_l_total'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						echo format_number($sum_l_total);
						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('loans/filter:shared').'" title="'.$lang['tooltips']['showall-loans-shared'].'">';
							echo $lang['words']['shared-overview'].' ('.format_number($loan['count_l_shared'], 0).')';
						echo '</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if($loan['count_l_shared'] == 0) {
							echo format_number(0);
						} else {
							echo format_number($sum_l_shared);
						}

						echo $currency_str;
					echo '</div>';
				echo '</div>';
			echo '</div>';







			echo '<div class="other">';
				echo '<h2><div>';
					echo screenshot('.statistics > .other', $lang['screenshots']['overview-others']);
					echo $lang['subtitles']['others'];
				echo '</div></h2>';

				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('settings').'" title="'.$lang['tooltips']['update-income'].'">'.$lang['words']['sums']['income'].'</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if(empty($user['data_sum_income'])) {
							echo format_number(0);
						} else {
							echo format_number(endecrypt($user['data_sum_income'], false));
						}

						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('settings').'" title="'.$lang['tooltips']['update-current'].'">'.$lang['words']['sums']['balance'].'</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if(empty($user['data_sum_balance'])) {
							echo format_number(0);
						} else {
							echo format_number(endecrypt($user['data_sum_balance'], false));
						}

						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo '<a href="'.url('settings').'" title="'.$lang['tooltips']['update-savings'].'">'.$lang['words']['sums']['savings'].'</a>';
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if(empty($user['data_sum_savings'])) {
							echo format_number(0) . $currency_str;
						} else {
							echo format_number(endecrypt($user['data_sum_savings'], false));
							echo $currency_str;
						}
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['words']['sums']['weekly-budget'];
						if(!empty($days_left) AND $weeks_left != 0) {
							echo ' ('.$weeks_left.' '.mb_strtolower(($weeks_left == 1 ? $lang['words']['week'] : $lang['words']['weeks'])).')';
						}
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						if(empty($user['data_sum_income']) OR empty($days_left)) {
							echo format_number(0);
						} else {
							if($weeks_left == 0) {
								echo format_number($balance / format_number($days_left, 0));
							} else {
								echo format_number($balance / $weeks_left);
							}
						}

						echo $currency_str;
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['words']['sums']['daily-budget'];
						echo (empty($days_left) ? null : ' ('.$days_left.' '.mb_strtolower(($days_left == 1 ? $lang['words']['day'] : $lang['words']['days'])).')');
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value sum">';
						echo ((empty($user['data_sum_income']) OR empty($days_left)) ? format_number(0) : format_number($balance / $days_left));
						echo $currency_str;
					echo '</div>';
				echo '</div>';
			echo '</div>';







			echo '<div class="next-period">';
				echo '<h2><div>';
					echo screenshot('.statistics > .next-period', $lang['screenshots']['overview-nextperiod']);
					echo $lang['subtitles']['next-period'];
				echo '</div></h2>';



				if($period_start_empty == true AND $period_end_empty == true AND $period_incomeday_empty == true) {
					echo '<div class="message">';
						echo $lang['messages']['no-period'];
					echo '</div>';

				} else {
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['period'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value">';
							if($period_start_empty == false AND $period_end_empty == false) {
								echo date_($period_end, 'day-month');
								echo ' - ';
								echo date_($period_end_nextmonth, 'day-month');

							} elseif($period_incomeday_empty == false) {
								echo date_($period_incomeday_nextmonth_1, 'day-month');
								echo ' - ';
								echo date_($period_incomeday_nextmonth_2, 'day-month');

							} else {
								echo $lang['words']['current-period'];
							}
						echo '</div>';
					echo '</div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['sums']['income'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value sum">';
							if(empty($user['data_sum_income'])) {
								echo format_number(0);
							} else {
								echo format_number(endecrypt($user['data_sum_income'], false));
							}

							echo $currency_str;
						echo '</div>';
					echo '</div>';


					echo '<div class="space"></div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['recurrent-expenses'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value sum">';
							if($currencies_exists != 0 AND !empty($sum_e_recurrent_othercurrency_next)) {
								echo $lang['words']['approx'].' ';
							}

							echo format_number(($sum_e_recurrent_next + $sum_e_recurrent_othercurrency_next));
							echo $currency_str;
						echo '</div>';
					echo '</div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['future-expenses'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value sum">';
							if($currencies_exists != 0 AND !empty($sum_e_future_othercurrency_next)) {
								echo $lang['words']['approx'].' ';
							}

							echo format_number(($sum_e_future_next + $sum_e_future_othercurrency_next));
							echo $currency_str;
						echo '</div>';
					echo '</div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['total'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value sum">';
							if($currencies_exists != 0 AND !empty($sum_e_recurrent_othercurrency_next) OR $currencies_exists != 0 AND !empty($sum_e_future_othercurrency_next)) {
								echo $lang['words']['approx'].' ';
							}

							echo format_number(($sum_e_recurrent_next + $sum_e_recurrent_othercurrency_next) + ($sum_e_future_next + $sum_e_future_othercurrency_next));
							echo $currency_str;
						echo '</div>';
					echo '</div>';


					echo '<div class="space"></div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['sums']['money-left'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value sum">';
							if(empty($user['data_sum_income'])) {
								echo format_number(0);
							} else {
								if($currencies_exists != 0 AND !empty($sum_e_recurrent_othercurrency) OR $currencies_exists != 0 AND !empty($sum_e_future_othercurrency)) {
									echo $lang['words']['approx'].' ';
								}
		
								echo format_number(endecrypt($user['data_sum_income'], false) - (($sum_e_recurrent_next + $sum_e_recurrent_othercurrency_next) + ($sum_e_future_next + $sum_e_future_othercurrency_next)));
							}

							echo $currency_str;
						echo '</div>';
					echo '</div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['sums']['weekly-budget'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value sum">';
							if(empty($user['data_sum_income']) OR empty($days_left_next)) {
								echo format_number(0);
							} else {
								if($weeks_left_next == 0) {
									echo format_number($balance_next / format_number($days_left_next, 0));
								} else {
									echo format_number($balance_next / $weeks_left_next);
								}
							}

							echo $currency_str;
						echo '</div>';
					echo '</div>';


					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['words']['sums']['daily-budget'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="value sum">';
							echo ((empty($user['data_sum_income']) OR empty($days_left_next)) ? format_number(0) : format_number($balance_next / $days_left_next));
							echo $currency_str;
						echo '</div>';
					echo '</div>';
				}
			echo '</div>';
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>