<?php

	# Include the required file
	require_once 'site-settings.php';

	# Tell the browser to let this file to act as an JSON file
	header('Content-Type: application/json;charset=utf-8');

	# Fetch the GET variables
	$get_action = safetag($_GET['act']);

	# Check if the user ask for exporting the data in plain text or encrypted
	$plaintext = ($get_action == 'plaintext' ? true : false);

	# Set default values
	$file_name = date('Y-m-d, Hi').' - '.$site_title.' export ('.($plaintext == true ? 'plain text' : 'encrypted').').json';
	$arr_combined = null;
	$arr_readme = null;
	$arr_accounts = null;
	$arr_budgets = null;



	# Log the action so the user can review it afterwards
	# log_action() is a function that can be located in `functions/log-action.php`
	log_action(
		'exported data ('.$get_action.')',
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		1
	);





	# Count how many accounts the user have
	$count_accounts =
	sql("SELECT COUNT(id_user)
		 FROM item_accounts
		 WHERE id_user = :_iduser
		", Array(
			'_iduser' => (int)$user['id']
		), 'count');


	# The user have more than 1 account
	if($count_accounts != 0) {

		# Get all accounts that the user have added
		$get_accounts =
		sql("SELECT *
			 FROM item_accounts
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			));

	}



	# Count how many items the user have
	$count_items =
	sql("SELECT COUNT(id_user)
		 FROM items
		 WHERE id_user = :_iduser
		", Array(
			'_iduser' => (int)$user['id']
		), 'count');


	# The user have more than 1 item
	if($count_items != 0) {

		# Get all items that the user have added
		$get_items =
		sql("SELECT *
			 FROM items
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			));

	}



	# Count how many budgets the user have
	$count_budgets =
	sql("SELECT COUNT(id_user)
		 FROM budget
		 WHERE id_user = :_iduser
		", Array(
			'_iduser' => (int)$user['id']
		), 'count');


	# The user have more than 1 budget
	if($count_budgets != 0) {

		# Get all budgets that the user have added
		$get_budgets =
		sql("SELECT *
			 FROM budget
			 WHERE id_user = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			));

	}




	# The user wants to export the data in plain text
	if($plaintext == true) {
		$readme = 'This file contains all data that the user owns on '.$site_domain.'. All data in this file are in plain text. To export the data encrypted, please choose "export encrypted" on the account\'s settings page.';
		$file_info = 'Each user on '.$site_domain.' have their own encryption keys that '.$site_title.' have generated for them. The content of that file are visible here.';
		$username_info = 'This data are encrypted using '.$site_title.'\'s own encryption keys which are required for the website to identify the user upon login and resetting the password.';

	# The user wants to export the data encrypted
	} else {
		$readme = 'This file contains all data that the user owns on '.$site_domain.'. All data in this file are encrypted. To export the data in plain text, please choose "export in plain text" on the account\'s settings page.';
		$file_info = 'Each user on '.$site_domain.' have their own encryption keys that '.$site_title.' have generated for them. The content of that file are visible here.';
		$username_info = 'This data are encrypted using '.$site_title.'\'s own encryption keys which are required for the website to identify the user upon login and resetting the password.';
	}



	# The user have more than 1 account
	if($count_accounts != 0) {

		# Loop all the accounts and add the data to the array. The array was defined as null in the beginning of this file
		foreach($get_accounts AS $account) {
			$arr_accounts[] = [
				'name' => ($plaintext == true ? (empty($account['data_name']) ? null : endecrypt($account['data_name'], false)) : $account['data_name']),
				'is_default' => (empty($account['is_default']) ? false : true)
			];
		}

	}



	# The user have more than 1 item
	if($count_items != 0) {

		# Loop all the items and add the data to the array. The array was defined as null in the beginning of this file
		foreach($get_items AS $item) {

			# Get data about the account
			$account =
			sql("SELECT data_name
				 FROM item_accounts
				 WHERE id = :_idaccount
				 AND id_user = :_iduser
				", Array(
					'_idaccount' => (int)$item['id_account'],
					'_iduser' => (int)$user['id']
				), 'fetch');


			# Add data to the array
			$arr_items[] = [
				'account' => ($plaintext == true ? (empty($item['id_account']) ? null : endecrypt($account['data_name'], false)) : $account['data_name']),
				'name' => ($plaintext == true ? (empty($item['data_name']) ? null : endecrypt($item['data_name'], false)) : $item['data_name']),
				'sums' => [
					'sum' => ($plaintext == true ? (empty($item['data_sum']) ? null : (float)endecrypt($item['data_sum'], false)) : $item['data_sum']),
					'interest' => ($plaintext == true ? (empty($item['data_sum_interest']) ? null : (float)endecrypt($item['data_sum_interest'], false)) : $item['data_sum_interest']),
					'adminfee' => ($plaintext == true ? (empty($item['data_sum_adminfee']) ? null : (float)endecrypt($item['data_sum_adminfee'], false)) : $item['data_sum_adminfee']),
					'permonth' => ($plaintext == true ? (empty($item['data_sum_permonth']) ? null : (float)endecrypt($item['data_sum_permonth'], false)) : $item['data_sum_permonth']),
				],
				'type' => ($plaintext == true ? (endecrypt($item['data_type'], false) == 'no-expense' ? null : endecrypt($item['data_type'], false)) : $item['data_type']),
				'category' => ($plaintext == true ? (endecrypt($item['data_category'], false) == 'no-expense' ? null : endecrypt($item['data_category'], false)) : $item['data_category']),
				'currency' => ($plaintext == true ? (endecrypt($item['data_currency'], false) == 'no-expense' ? null : endecrypt($item['data_currency'], false)) : $item['data_currency']),
				'number-payment' => ($plaintext == true ? (empty($item['data_number_payment']) ? null : endecrypt($item['data_number_payment'], false)) : $item['data_number_payment']),
				'number-ocr' => ($plaintext == true ? (empty($item['data_number_ocr']) ? null : endecrypt($item['data_number_ocr'], false)) : $item['data_number_ocr']),
				'number-phone' => ($plaintext == true ? (empty($item['data_number_phone']) ? null : endecrypt($item['data_number_phone'], false)) : $item['data_number_phone']),
				'recurrence' => ($plaintext == true ? (empty($item['data_recurrence']) ? null : (int)endecrypt($item['data_recurrence'], false)) : $item['data_recurrence']),
				'recurrence-type' => ($plaintext == true ? (empty($item['data_recurrence_type']) ? null : endecrypt($item['data_recurrence_type'], false)) : $item['data_recurrence_type']),
				'notes' => ($plaintext == true ? (empty($item['data_notes']) ? null : endecrypt($item['data_notes'], false)) : $item['data_notes']),
				'checkboxes' => [
					'expense-payed' => $item['check_expense_payed'],
					'expense-extra' => $item['check_expense_extra'],
					'expense-recurrent' => $item['check_expense_recurrent'],
					'expense-correctinfo' => $item['check_expense_correctinfo'],
					'debt-company' => $item['check_debt_company'],
					'debt-paying' => $item['check_debt_paying'],
					'loan-out' => $item['check_loan_out']
				],
				'is-expense' => $item['is_expense'],
				'is-debt' => $item['is_debt'],
				'is-loan' => $item['is_loan'],
				'timestamps' => [
					'date' => $item['timestamp_date'],
					'date-end' => (empty($item['timestamp_date_end']) ? null : ($plaintext == false ? $item['timestamp_date_end'] : endecrypt($item['timestamp_date_end'], false))),
					'payed' => (empty($item['timestamp_payed']) ? null : ($plaintext == false ? $item['timestamp_payed'] : endecrypt($item['timestamp_payed'], false))),
					'added' => (empty($item['timestamp_added']) ? null : ($plaintext == false ? $item['timestamp_added'] : endecrypt($item['timestamp_added'], false))),
					'edited' => (empty($item['timestamp_edited']) ? null : ($plaintext == false ? $item['timestamp_edited'] : endecrypt($item['timestamp_edited'], false)))
				]
			];

		}

	}



	# The user have more than 1 budget
	if($count_budgets != 0) {

		# Loop all the budgets and add the data to the array. The array was defined as null in the beginning of this file
		foreach($get_budgets AS $budget) {
			$arr_budgets[] = [
				'name' => ($plaintext == true ? (empty($budget['data_name']) ? null : endecrypt($budget['data_name'], false)) : $budget['data_name']),
				'sums' => [
					'current' => ($plaintext == true ? (empty($budget['data_sum_current']) ? null : endecrypt($budget['data_sum_current'], false)) : $budget['data_sum_current']),
					'goal' => ($plaintext == true ? (empty($budget['data_sum_goal']) ? null : endecrypt($budget['data_sum_goal'], false)) : $budget['data_sum_goal'])
				],
				'notes' => ($plaintext == true ? (empty($budget['data_notes']) ? null : endecrypt($budget['data_notes'], false)) : $budget['data_notes']),
				'created' => (empty($budget['timestamp_created']) ? null : $budget['timestamp_created'])
			];
		}

	}



	/* -------- */



	# Combine all stored data into 1 array
	$arr_combined = [
		'README' => $readme,
		'export' => [
			'exported' => date('Y-m-d, H:i', time()),
			'encrypted' => ($plaintext == true ? false : true)
		],
		'file' => [
			'info' => $file_info,
			'content' => file_get_contents($dir_userfiles.'/'.md5($user['data_saltedstring_1'] . (int)$user['id'] . $user['data_saltedstring_2']).'.json')
		],
		'user' => [
			'id' => (int)$user['id'],
			'username' => [
				'info' => $username_info,
				'content' => ($plaintext == true ? endecrypt($user['data_username'], false, true) : $user['data_username'])
			],
			'email' => (empty($user['data_email']) ? null : ($plaintext == false ? $user['data_email'] : endecrypt($user['data_email'], false))),
			'password' => $user['data_password'],
			'two-factor' => $user['data_tfa'],
			'two-factor-backup' => $user['data_tfa_backup'],
			'salt' => [
				'info' => 'These 2 random string are used as salt for the hashed filename that contains your encryption keys.',
				'salt-1' => $user['data_saltedstring_1'],
				'salt-2' => $user['data_saltedstring_2']
			],
			'sums' => [
				'income' => (empty($user['data_sum_income']) ? null : ($plaintext == false ? $user['data_sum_income'] : (float)endecrypt($user['data_sum_income'], false))),
				'balance' => (empty($user['data_sum_balance']) ? null : ($plaintext == false ? $user['data_sum_balance'] : (float)endecrypt($user['data_sum_balance'], false))),
				'savings' => (empty($user['data_sum_savings']) ? null : ($plaintext == false ? $user['data_sum_savings'] : (float)endecrypt($user['data_sum_savings'], false)))
			],
			'income-day' => (empty($user['data_incomeday']) ? null : ($plaintext == false ? $user['data_incomeday'] : (int)endecrypt($user['data_incomeday'], false))),
			'currency' => ($plaintext == true ? endecrypt($user['data_currency'], false) : $user['data_currency']),
			'language' => ($plaintext == true ? endecrypt($user['data_language'], false) : $user['data_language']),
			'notes' => (empty($user['data_notes']) ? null : ($plaintext == false ? $user['data_notes'] : endecrypt($user['data_notes'], false))),
			'theme' => $user['data_theme'],
			'checkboxes' => [
				'email-expenses-expires' => $user['check_email_expensesexpires'],
				'hide-date-expenses' => $user['check_option_hidedate_expenses'],
				'hide-date-debts' => $user['check_option_hidedate_debts'],
				'hide-date-loans' => $user['check_option_hidedate_loans']
			],
			'timestamps' => [
				'registered' => ($plaintext == true ? endecrypt($user['timestamp_registered'], false) : $user['timestamp_registered']),
				'period-start' => (empty($user['timestamp_period_start']) ? null : ($plaintext == false ? $user['timestamp_period_start'] : endecrypt($user['timestamp_period_start'], false))),
				'period-end' => (empty($user['timestamp_period_end']) ? null : ($plaintext == false ? $user['timestamp_period_end'] : endecrypt($user['timestamp_period_end'], false)))
			]
		],
		'accounts' => ($count_accounts == 0 ? null : $arr_accounts),
		'items' => ($count_items == 0 ? null : $arr_items),
		'budget' => ($count_budgets == 0 ? null : $arr_budgets)
	];



	# Download the outputted data
	header('Content-disposition: attachment; filename="'.$file_name.'"');
	header('Content-type: application/json');

	# Print out for download
	echo json_encode($arr_combined);

?>