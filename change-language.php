<?php

	require_once 'site-settings.php';



	if(!empty($is_loggedin)) {
		sql("UPDATE users
			 SET data_language = :_language
			 WHERE id = :_iduser
			", Array(
				'_iduser' => (int)$user['id'],
				'_language' => endecrypt((endecrypt($user['data_language'], false) == 'se' ? 'gb' : 'se'))
			));

		header("Location: ".url(''));
		exit;
	}

?>