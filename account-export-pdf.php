<?php

	require_once 'site-settings.php';

	function cmp($a, $b){return strcmp($a['name'], $b['name']);}
	function cmp_e($a, $b){return strcmp($a['name'], $b['name']);}



	$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
	$fontDirs = $defaultConfig['fontDir'];
	
	$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
	$fontData = $defaultFontConfig['fontdata'];
	
	$mpdf = new \Mpdf\Mpdf([
		'fontDir' => array_merge($fontDirs, [__DIR__]),
		'fontdata' => $fontData + [
			'victormono' => [
				'R' => '/fonts/victor-mono-400.ttf',
				'B' => '/fonts/victor-mono-700.ttf'
			]
		],
		'default_font' => 'victormono',
		'default_font_size' => 8,
		'mode' => 'utf-8',
		'format' => [297, 210],
		'orientation' => 'L'
	]);

	$user_currency = mb_strtoupper(endecrypt($user['data_currency'], false));
	$user_period_start = date_(($period_incomeday_empty == false ? $period_incomeday : $period_start), 'date');
	$user_period_end = date_(($period_incomeday_empty == false ? $period_incomeday_nextmonth_1 : $period_end), 'date');

	$page_footer = '<table width="100%"><tr>';
	$page_footer .= '<td width="33%">Keizai (keizai.se'.($lang['metadata']['language'] == 'se' ? '' : '?lang=en').')</td>';
	$page_footer .= '<td width="33%" align="center">{PAGENO}/{nbpg}</td>';
	$page_footer .= '<td width="33%" style="text-align: right;">'.$lang['words']['refers-to'].' '.$user_period_start.' > '.$user_period_end.'</td>';
	$page_footer .= '</tr></table>';



	$mpdf->SetTitle('Keizai');
	$mpdf->SetCreator('Keizai');
	$mpdf->SetAuthor(endecrypt($user['data_username'], false, true));
	$mpdf->SetSubject($lang['metadata']['pdf']['subject']);
	$mpdf->SetKeywords(implode(',', $lang['metadata']['pdf']['keywords']));
	$mpdf->SetCompression(true);

	$stylesheet = file_get_contents('css/pdf.css');
	$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);

	#$mpdf->SetWatermarkText('Keizai');
	#$mpdf->showWatermarkText = true;


	$days_left = null;
	if($period_start_empty == false AND $period_end_empty == false AND empty($user['data_incomeday'])) {
		$days_left = x_left($period_start, $period_end);

	} elseif(empty($user['timestamp_period_start']) AND empty($user['timestamp_period_end']) AND $period_incomeday_empty == false) {
		$days_left = x_left($period_incomeday, $period_incomeday_nextmonth_1);
	}

	$weeks_left = (empty($days_left) ? null : format_number(($days_left / 7), 0));



	$get_expenses =
	sql("SELECT data_name, data_sum, data_currency, check_expense_payed, timestamp_date, timestamp_payed
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_expense IS NOT NULL
		 AND timestamp_date >= '".($period_start_empty == false ? $period_start : ($period_incomeday_empty == false ? $period_incomeday : null))."'
		 AND timestamp_date < '".($period_end_empty == false ? $period_end : ($period_incomeday_empty == false ? $period_incomeday_nextmonth_1 : null))."'
		", Array(
			'_iduser' => (int)$user['id']
		));

	$arr_expenses = [];
	$arr_expenses_othercurr = [];
	foreach($get_expenses AS $expense) {
		$rate =
		sql("SELECT data_rate
			 FROM currencies
			 WHERE data_currency_from = :_from
			 AND data_currency_to = :_to
			", Array(
				'_from' => endecrypt($expense['data_currency'], false),
				'_to' => endecrypt($user['data_currency'], false)
			), 'fetch');

		$is_diffcurrency = false;
		if(endecrypt($expense['data_currency'], false) != endecrypt($user['data_currency'], false)) {
			$is_diffcurrency = true;
		}

		$arr_expenses[] = [
			'date' => $expense['timestamp_date'],
			'payed' => (empty($expense['timestamp_payed']) ? null : date_(endecrypt($expense['timestamp_payed'], false), 'date')),
			'name' => endecrypt($expense['data_name'], false),
			'sum' => format_number(($is_diffcurrency == false ? endecrypt($expense['data_sum'], false) : (endecrypt($expense['data_sum'], false) * $rate['data_rate'])), 2, '.', ''),
			'sum_currency' => (endecrypt($expense['data_currency'], false) == endecrypt($user['data_currency'], false) ? '' : endecrypt($expense['data_sum'], false)),
			'currency' => $user_currency,
			'currency_other' => (endecrypt($expense['data_currency'], false) == endecrypt($user['data_currency'], false) ? '' : mb_strtoupper(endecrypt($expense['data_currency'], false)))
		];
	}



	$get_debts =
	sql("SELECT data_name, data_sum, check_debt_company, check_debt_paying, timestamp_date
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_debt IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$arr_debts = [];
	foreach($get_debts AS $debt) {
		$arr_debts[] = [
			'date' => $debt['timestamp_date'],
			'company' => $debt['check_debt_company'],
			'paying' => $debt['check_debt_paying'],
			'name' => endecrypt($debt['data_name'], false),
			'sum' => format_number(endecrypt($debt['data_sum'], false), 2, '.', '')
		];
	}



	$get_loans =
	sql("SELECT data_name, data_sum, timestamp_date
		 FROM items
		 WHERE id_user = :_iduser
		 AND is_loan IS NOT NULL
		", Array(
			'_iduser' => (int)$user['id']
		));

	$arr_loans = [];
	foreach($get_loans AS $loan) {
		$arr_loans[] = [
			'date' => $loan['timestamp_date'],
			'name' => endecrypt($loan['data_name'], false),
			'sum' => format_number(endecrypt($loan['data_sum'], false), 2, '.', '')
		];
	}



	usort($arr_expenses, 'cmp_e');
	usort($arr_debts, 'cmp');
	usort($arr_loans, 'cmp');



	$expenses = '';
	$total_expenses = 0;
	if(count($arr_expenses) != 0) {
		foreach($arr_expenses AS $expense) {
			$total_expenses += $expense['sum'];

			$expenses .= '<tr>';
				$expenses .= '<td class="date">'.date_($expense['date'], 'date').'</td>';
				$expenses .= '<td class="date payed">'.(empty($expense['payed']) ? null : $expense['payed']).'</td>';
				$expenses .= '<td class="name-expense">'.$expense['name'].'</td>';
				$expenses .= '<td class="sum">'.(empty($expense['sum_currency']) ? '' : format_number($expense['sum_currency']).' '.$expense['currency_other']).'</td>';
				$expenses .= '<td class="sum">'.format_number($expense['sum']).' '.$user_currency.'</td>';
			$expenses .= '</tr>';
		}
	}

	$debts = '';
	$total_debts = 0;
	if(count($arr_debts) != 0) {
		foreach($arr_debts AS $debt) {
			$total_debts += $debt['sum'];
			$debts .= '<tr>';
				$debts .= '<td class="date">'.date_($debt['date'], 'date').'</td>';
				$debts .= '<td class="company">'.(empty($debt['company']) ? $lang['words']['no'] : $lang['words']['yes']).'</td>';
				$debts .= '<td class="paying">'.(empty($debt['paying']) ? $lang['words']['no'] : $lang['words']['yes']).'</td>';
				$debts .= '<td class="name debt">'.$debt['name'].'</td>';
				$debts .= '<td class="sum">'.format_number($debt['sum']).' '.$user_currency.'</td>';
			$debts .= '</tr>';
		}
	}

	$loans = '';
	$total_loans = 0;
	if(count($arr_loans) != 0) {
		foreach($arr_loans AS $loan) {
			$total_loans += $loan['sum'];
			$loans .= '<tr>';
				$loans .= '<td class="date">'.date_($loan['date'], 'date').'</td>';
				$loans .= '<td class="name">'.$loan['name'].'</td>';
				$loans .= '<td class="sum">'.format_number($loan['sum']).' '.$user_currency.'</td>';
			$loans .= '</tr>';
		}
	}



	$mpdf->WriteHTML('
	<div class="overview">
		<table>
			<tbody>
				<tr>
					<td class="bold details-width">Inkomst:</td>
					<td class="sum">'.format_number((float)endecrypt($user['data_sum_income'], false)).' '.$user_currency.'</td>
				</tr>

				<tr>
					<td class="bold details-width">Sparande:</td>
					<td class="sum">'.format_number((float)endecrypt($user['data_sum_savings'], false)).' '.$user_currency.'</td>
				</tr>

				<tr>
					<td class="bold details-width">Veckobudget:</td>
					<td class="sum">'.format_number(((float)endecrypt($user['data_sum_income'], false) - $total_expenses) / $weeks_left).' '.$user_currency.'</td>
				</tr>

				<tr>
					<td class="bold details-width">Dagsbudget:</td>
					<td class="sum">'.format_number(((float)endecrypt($user['data_sum_income'], false) - $total_expenses) / $days_left).' '.$user_currency.'</td>
				</tr>
			</tbody>
		</table>
	</div>
	');



	if(empty($expenses)) {
		$mpdf->WriteHTML('
		<h1>'.$lang['words']['expenses'].'</h1>
		<div class="message">'.$lang['messages']['no-expenses'].'</div>
		');

	} else {
		$mpdf->WriteHTML('
		<h1>'.$lang['words']['expenses'].'</h1>
		<table>
			<thead>
				<tr>
					<td class="date">'.$lang['words']['dates']['expired'].'</td>
					<td class="date payed">'.$lang['words']['dates']['payed'].'</td>
					<td class="name expense">'.$lang['words']['name'].'</td>
					<td class="sum"></td>
					<td class="sum"></td>
				</tr>
			</thead>

			<tbody>'.$expenses.'</tbody>

			<tfoot>
				<tr>
					<td class="date"></td>
					<td class="date payed"></td>
					<td class="name expense">'.$lang['words']['total'].' '.count($arr_expenses).'</td>
					<td class="sum"></td>
					<td class="sum">'.format_number($total_expenses).' '.$user_currency.'</td>
				</tr>
			</tfoot>
		</table>
		');
	}



	if(count($arr_expenses) > 50) {
		$mpdf->SetHTMLFooter($page_footer);
		#$mpdf->addPage();
	}



	if(empty($debts)) {
		$mpdf->WriteHTML('
		<h1 class="debts">'.$lang['words']['debts'].'</h1>
		<div class="message">'.$lang['messages']['no-debts'].'</div>
		');

	} else {
		$mpdf->WriteHTML('
		<h1 class="debts">'.$lang['words']['debts'].'</h1>
		<table>
			<thead>
				<tr>
					<td class="date">'.$lang['words']['date'].'</td>
					<td class="company">'.$lang['words']['is-company'].'</td>
					<td class="paying">'.$lang['words']['is-paying'].'</td>
					<td class="name">'.$lang['words']['name'].'</td>
					<td class="sum"></td>
				</tr>
			</thead>

			<tbody>'.$debts.'</tbody>

			<tfoot>
				<tr>
					<td class="date"></td>
					<td class="company"></td>
					<td class="paying"></td>
					<td class="name">Totalt '.count($arr_debts).'</td>
					<td class="sum">'.format_number($total_debts).' '.$user_currency.'</td>
				</tr>
			</tfoot>
		</table>
		');
	}

	if(empty($loans)) {
		$mpdf->WriteHTML('
		<h1 class="loans">'.$lang['words']['loans'].'</h1>
		<div class="message">'.$lang['messages']['no-loans'].'</div>
		');

	} else {
		$mpdf->WriteHTML('
		<h1 class="loans">'.$lang['words']['loans'].'</h1>
		<table>
			<thead>
				<tr>
					<td class="date">'.$lang['words']['date'].'</td>
					<td class="name">'.$lang['words']['name'].'</td>
					<td class="sum"></td>
				</tr>
			</thead>
	
			<tbody>'.$loans.'</tbody>

			<tfoot>
				<tr>
					<td class="date"></td>
					<td class="name">Totalt '.count($arr_loans).'</td>
					<td class="sum">'.format_number($total_loans).' '.$user_currency.'</td>
				</tr>
			</tfoot>
		</table>
		');
	}

	$mpdf->SetHTMLFooter('
	<table width="100%">
		<tr>
			<td width="33%">Keizai (keizai.se'.($lang['metadata']['language'] == 'se' ? '' : '?lang=en').')</td>
			<td width="33%" align="center">{PAGENO}/{nbpg}</td>
			<td width="33%" style="text-align: right;">'.$lang['words']['refers-to'].' '.$user_period_start.' > '.$user_period_end.'</td>
		</tr>
	</table>
	');



	$mpdf->Output($user_period_start.' - '.$user_period_end.' - '.$site_title.'.pdf', 'I');

?>