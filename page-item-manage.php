<?php

	if(isset($_GET['pag']) AND $_GET['pag'] == 'delete') {

		require_once 'site-settings.php';

		$get_item = safetag($_GET['ite']);
		$get_idaccount = (isset($_GET['ida']) ? safetag($_GET['ida']) : null);
		$get_iditem = safetag($_GET['idi']);
		$is_shared = false;

		if($get_item == 'expense') {
			$link = 'expenses'.(empty($get_idaccount) ? '' : '/account:'.$get_idaccount);

		} elseif($get_item == 'debt') {
			$link = 'debts';

		} elseif($get_item == 'loan') {
			$link = 'loans';
		}



		$check_sharestatus =
		sql("SELECT COUNT(i.id_user)
			 FROM items i
			 JOIN shares s
			 ON i.id = s.id_item
			 WHERE s.id_item = :_iditem
			 AND s.id_user_with = :_iduser
			", Array(
				'_iditem' => (int)$get_iditem,
				'_iduser' => (int)$user['id']
			), 'count');

		/*if($check_sharestatus != 0) {
			$is_shared = true;

			$usershare =
			sql("SELECT u.id AS id_user
				 FROM users u
				 JOIN items i
				 ON u.id = i.id_user
				 WHERE i.id = :_iditem
				", Array(
					'_iditem' => (int)$get_iditem
				), 'fetch');
		}*/



		$check_deleteprm =
		sql("SELECT COUNT(allow_deletion)
			 FROM shares
			 WHERE id_item = :_iditem
			 AND id_user_with = :_iduser
			", Array(
				'_iditem' => (int)$get_iditem,
				'_iduser' => (int)$user['id']
			), 'count');


		if($check_sharestatus != 0 AND $check_deleteprm == 0) {
			header("Location: ".url($get_item.'s/filter:shared#'.$get_item.'-'.(int)$get_iditem));
			exit;


		} else {
			sql("DELETE FROM items
				 WHERE id = :_iditem
				 AND id_user = :_iduser
				", Array(
					'_iditem' => (int)$get_iditem,
					'_iduser' => (int)($is_shared == false ? $user['id'] : $usershare['id_user'])
				));

			header("Location: ".url($link));
			exit;

		}



	} else {

		require_once 'site-header.php';



		$get_page = safetag($_GET['pag']);
		$get_item = safetag($_GET['ite']);
		$get_iditem = (!isset($_GET['idi']) ? null : safetag($_GET['idi']));
		$get_idaccount = (isset($_GET['ida']) ? safetag($_GET['ida']) : null);

		$is_editing = false;
		$is_shared = false;
		$has_permissions = true;



		if($get_page == 'edit') {
			$is_editing = true;

			$check_editprm =
			sql("SELECT COUNT(id_user)
				 FROM items
				 WHERE id = :_iditem
				 AND id_user = :_iduser
				", Array(
					'_iditem' => (int)$get_iditem,
					'_iduser' => (int)$user['id']
				), 'count');

			if($check_editprm == 0) {
				$has_permissions = false;
			}


			$item =
			sql("SELECT *
				 FROM items
				 WHERE id = :_iditem
				 AND id_user = :_iduser
				", Array(
					'_iditem' => (int)$get_iditem,
					'_iduser' => (int)($is_shared == false ? $user['id'] : $usershare['id_user'])
				), 'fetch');
		}



		if($get_item == 'expense') {
			$cancel_link = 'expenses';
			$title = $lang['words']['an-expense'];
			$name = $lang['words']['the-expense'];
			$name_sum = $lang['words']['sums']['amount'];
			$date = $lang['words']['dates']['expires'];

			$count_accounts =
			sql("SELECT COUNT(id_user)
				 FROM item_accounts
				 WHERE id_user = :_iduser
				", Array(
					'_iduser' => (int)($is_shared == false ? $user['id'] : $usershare['id_user'])
				), 'count');

			if($count_accounts != 0) {
				$get_accounts =
				sql("SELECT *
					 FROM item_accounts
					 WHERE id_user = :_iduser
					", Array(
						'_iduser' => (int)($is_shared == false ? $user['id'] : $usershare['id_user'])
					));
			}

			$c_debts =
			sql("SELECT COUNT(id_user)
				 FROM items
				 WHERE id_user = :_iduser
				 AND is_debt IS NOT NULL
				", Array(
					'_iduser' => (int)($is_shared == false ? $user['id'] : $usershare['id_user'])
				), 'count');

			if($c_debts != 0) {
				$get_debts =
				sql("SELECT id, data_name, data_sum, timestamp_date
					 FROM items
					 WHERE id_user = :_iduser
					 AND is_debt IS NOT NULL
					", Array(
						'_iduser' => (int)$user['id']
					));
			}



		} elseif($get_item == 'debt') {
			$cancel_link = 'debts';
			$title = $lang['words']['a-debt'];
			$name = $lang['words']['the-debt'];
			$name_sum = $lang['words']['sums']['debt'];
			$date = $lang['words']['dates']['started'];


		} elseif($get_item == 'loan') {
			$cancel_link = 'loans';
			$title = $lang['words']['a-loan'];
			$name = $lang['words']['the-loan'];
			$name_sum = $lang['words']['sums']['loan'];
			$date = $lang['words']['dates']['loan-from'];
		}







		echo '<section id="manage-item">';
			if($has_permissions == false) {
				echo '<div class="no-permissions">';
					echo '<h1>'.$lang['titles']['no-permissions'].'</h1>';

					foreach($lang['no-permissions'] AS $content) {
						echo $Parsedown->text($content);
					}
				echo '</div>';



			} else {
				echo '<h1>';
					if($get_page == 'add') {
						echo $lang['words']['add-'.$get_item];
					} elseif($get_page == 'edit') {
						echo $lang['words']['edit'].' '.mb_strtolower($title);
					} else {
						echo 'Okänt';
					}
				echo '</h1>';

				echo '<div class="msg"></div>';


				echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate>';
					echo '<input type="hidden" name="hidden-object" value="'.$get_item.'">';

					if($get_page == 'edit') {
						echo '<input type="hidden" name="hidden-iduser" value="'.(int)$user['id'].'">';
						echo '<input type="hidden" name="hidden-iditem" value="'.(int)$get_iditem.'">';
					}


					echo '<div>';
						echo '<div class="required">';

							echo '<h2>'.$lang['subtitles']['required'].'</h2>';


							if($get_item == 'expense') {
								echo '<div class="side-by-side options">';
									echo '<div class="item">';
										echo '<div class="label">';
											echo $lang['words']['account'];
										echo '</div>';

										echo '<div class="field">';
											echo '<select name="list-accounts">';
												echo ($default_account['count_da'] == 0 ? '<option value=""></option>' : '');

												foreach($get_accounts AS $accountlist) {
													echo '<option value="'.(int)$accountlist['id'].'"';
													echo ($is_editing == false ? (((empty($get_idaccount) AND $default_account['id'] == $accountlist['id']) OR (!empty($get_idaccount) AND $get_idaccount == $accountlist['id'])) ? ' selected' : '') : ($accountlist['id'] == $item['id_account'] ? ' selected' : ''));
													echo '>'.endecrypt($accountlist['data_name'], false).'</option>';
												}
											echo '</select>';
										echo '</div>';
									echo '</div>';


									echo '<div class="item">';
										echo '<div class="label">';
											echo $lang['words']['type'];
										echo '</div>';

										echo '<div class="field">';
											echo '<select name="list-types">';
												echo '<option value=""></option>';

												asort($lang['types']);
												foreach($lang['types'] AS $key => $value) {
													echo '<option value="'.$key.'"';
													echo ($is_editing == false ? '' : ($key == endecrypt($item['data_type'], false) ? ' selected' : ''));
													echo '>'.$value.'</option>';
												}
											echo '</select>';
										echo '</div>';
									echo '</div>';


									echo '<div class="item">';
										echo '<div class="label">';
											echo $lang['words']['category'];
										echo '</div>';

										echo '<div class="field">';
											echo '<select name="list-categories">';
												echo '<option value=""></option>';

												asort($lang['categories']);
												foreach($lang['categories'] AS $key => $value) {
													echo '<option value="'.$key.'"';
													echo ($is_editing == false ? '' : ($key == endecrypt($item['data_category'], false) ? ' selected' : ''));
													echo '>'.$value.'</option>';
												}
											echo '</select>';
										echo '</div>';
									echo '</div>';
								echo '</div>';
							}



							echo '<div class="side-by-side">';
								echo '<div class="item name '.$get_item.'">';
									echo '<div class="label">';
										echo $lang['words']['name'];
									echo '</div>';

									echo '<div class="field">';
										echo '<div class="icon">'.svgicon('name').'</div>';
										echo '<input type="text" name="field-name" maxlength="35"';
										echo ($is_editing == false ? '' : ' value="'.endecrypt($item['data_name'], false).'"');
										echo '>';
									echo '</div>';
								echo '</div>';


								echo '<div class="item sum '.$get_item.'">';
									echo '<div class="label">';
										echo $name_sum;
									echo '</div>';

									echo '<div class="field sum">';
										echo '<div>';
											echo '<div class="icon">'.svgicon('money').'</div>';
											echo '<input type="text" inputmode="numeric" pattern="[0-9]{6}" name="field-sum" aria-label="sum"';
											echo ($get_item == 'expense' ? '' : ' class="no-currency"');
											echo ($is_editing == false ? '' : ' value="'.str_replace('.00', '', format_number(endecrypt($item['data_sum'], false), 2, '.', '')).'"');
											echo '>';

											if($get_item == 'expense') {
												echo '<select name="list-currencies">';
													foreach($arr_currencies AS $currency) {
														echo '<option value="'.mb_strtolower($currency).'"';
														if($is_editing == true AND mb_strtolower($currency) == endecrypt($item['data_currency'], false)) {
															echo ' selected';
														} elseif($is_editing == false AND mb_strtolower($currency) == endecrypt($user['data_currency'], false)) {
															echo ' selected';
														}
														echo '>';
															echo $currency;
														echo '</option>';
													}
												echo '</select>';
											}
										echo '</div>';

										echo '<div class="math">';
											echo '<span class="equals">=</span>';
											echo '<span class="sum"></span>';
										echo '</div>';
									echo '</div>';
								echo '</div>';
							echo '</div>';


							echo '<div class="item">';
								echo '<div class="label">';
									echo $date;
								echo '</div>';

								echo '<div class="field">';
									echo '<div class="icon">'.svgicon('calendar').'</div>';
									echo '<input type="date" name="field-date" maxlength="10"';
									echo ($is_editing == false ? '' : ' value="'.date_($item['timestamp_date'], 'date').'"');
									echo '>';
								echo '</div>';
							echo '</div>';

						echo '</div>';



						echo '<div class="optionally">';

							echo '<h2>'.$lang['subtitles']['optional'].'</h2>';


							if($get_item == 'expense') {
								echo '<div class="side-by-side">';
									echo '<div class="item">';
										echo '<div class="label">';
											echo $lang['words']['number-payment'];
										echo '</div>';

										echo '<div class="field with-explaination">';
											echo '<div class="icon">'.svgicon('number').'</div>';
											echo '<input type="text" inputmode="numeric" pattern="[0-9]{12}" name="field-number-payment" aria-label="num number-payment"';
											echo (($is_editing == false OR $is_editing == true AND empty($item['data_number_payment'])) ? '' : ' value="'.endecrypt($item['data_number_payment'], false).'"');
											echo ($is_editing == false ? '' : (endecrypt($item['data_type'], false) == 'swish' ? ' disabled' : ''));
											echo '>';

											echo '<div class="icon wit" data-element="item-number-payment" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
										echo '</div>';
									echo '</div>';


									echo '<div class="item ocr">';
										echo '<div class="label">';
											echo $lang['words']['number-ocr'];
										echo '</div>';

										echo '<div class="field with-explaination">';
											echo '<div class="icon">'.svgicon('number').'</div>';
											echo '<input type="text" inputmode="numeric" pattern="[0-9]+" name="field-number-ocr" aria-label="num ocr"';
											echo (($is_editing == false OR $is_editing == true AND empty($item['data_number_ocr'])) ? '' : ' value="'.endecrypt($item['data_number_ocr'], false).'"');
											echo ($is_editing == false ? '' : (endecrypt($item['data_type'], false) == 'swish' ? ' disabled' : ''));
											echo '>';

											echo '<div class="icon wit" data-element="item-number-ocr" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
										echo '</div>';
									echo '</div>';
								echo '</div>';


								echo '<div class="item">';
									echo '<div class="label">';
										echo $lang['words']['dates']['payed'];
									echo '</div>';

									echo '<div class="field">';
										echo '<div class="icon">'.svgicon('calendar').'</div>';
										echo '<input type="date" name="field-date-payed" maxlength="10"';
										echo (($is_editing == false OR $is_editing == true AND empty($item['timestamp_payed'])) ? ' disabled' : ' value="'.date_(endecrypt($item['timestamp_payed'], false), 'date').'"');
										echo '>';
									echo '</div>';
								echo '</div>';


								echo '<div class="item">';
									echo '<div class="label">';
										echo $lang['words']['recurrence'];
									echo '</div>';

									echo '<div class="field">';
										echo '<input type="text" inputmode="numeric" pattern="[0-9]{1,2}" name="field-recurrence" maxlength="2" class="no-icon"';
										echo ' value="'.($is_editing == true ? (empty($item['data_recurrence']) ? '1' : endecrypt($item['data_recurrence'], false)) : '1').'"';
										echo ($is_editing == true ? (empty($item['check_expense_recurrent']) ? ' disabled' : '') : ' disabled');
										echo '>';

										echo '<select name="list-recurrence-types"';
										echo ($is_editing == true ? (empty($item['check_expense_recurrent']) ? ' disabled' : '') : ' disabled');
										echo '>';
											echo '<option value="week"';
											echo ($is_editing == true ? (empty($item['data_recurrence_type']) ? '' : (endecrypt($item['data_recurrence_type'], false) == 'week' ? ' selected' : '')) : '');
											echo '>'.$lang['words']['week'].'</option>';

											echo '<option value="month"';
											echo ($is_editing == true ? (empty($item['data_recurrence_type']) ? ' selected' : (endecrypt($item['data_recurrence_type'], false) == 'month' ? ' selected' : '')) : ' selected');
											echo '>'.$lang['words']['month'].'</option>';

											echo '<option value="year"';
											echo ($is_editing == true ? (empty($item['data_recurrence_type']) ? '' : (endecrypt($item['data_recurrence_type'], false) == 'year' ? ' selected' : '')) : '');
											echo '>'.$lang['words']['year'].'</option>';
										echo '</select>';
									echo '</div>';
								echo '</div>';


								echo '<div class="item">';
									echo '<div class="label">';
										echo $lang['words']['belongs-to-debt'];
									echo '</div>';

									echo '<div class="field">';
										if($c_debts == 0) {
											echo '<select name="list-debts" disabled>';
												echo '<option value="">'.$lang['messages']['no-debts'].'</option>';
											echo '</select>';

										} else {
											echo '<select name="list-debts">';
												echo '<option value=""></option>';
												foreach($get_debts AS $debt) {
													echo '<option value="'.(int)$debt['id'].'"';
													echo ($is_editing == true ? (empty($item['data_belongsto_debt']) ? '' : ($item['data_belongsto_debt'] == $debt['id'] ? ' selected' : '')) : '');
													echo '>';
														echo date_($debt['timestamp_date'], 'date').': ';
														echo endecrypt($debt['data_name'], false);
														echo ' ('.format_number(endecrypt($debt['data_sum'], false)).')';
													echo '</option>';
												}
											echo '</select>';
										}
									echo '</div>';
								echo '</div>';


								echo '<div class="item phone">';
									echo '<div class="label">';
										echo $lang['words']['number-phone'];
									echo '</div>';

									echo '<div class="field with-explaination">';
										echo '<div class="icon">'.svgicon('number').'</div>';
										echo '<input type="text" inputmode="numeric" pattern="[0-9]{14}" name="field-number-phone" aria-label="num phone"';
										echo (($is_editing == false OR $is_editing == true AND empty($item['data_number_phone'])) ? '' : ' value="'.endecrypt($item['data_number_phone'], false).'"');
										echo ($is_editing == false ? '' : (endecrypt($item['data_type'], false) == 'swish' ? '' : ' disabled'));
										echo '>';

										echo '<div class="icon wit" data-element="item-number-phone" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
									echo '</div>';
								echo '</div>';



							} elseif($get_item == 'debt') {
								echo '<div class="side-by-side">';
									echo '<div class="item sum-permonth">';
										echo '<div class="label">';
											echo $lang['words']['sums']['monthly'];
										echo '</div>';

										echo '<div class="field">';
											echo '<div class="icon">'.svgicon('money').'</div>';
											echo '<input type="text" inputmode="numeric" pattern="[0-9]{6}" name="field-sum-permonth"';
											echo (($is_editing == false OR $is_editing == true AND empty($item['data_sum_permonth'])) ? '' : ' value="'.endecrypt($item['data_sum_permonth'], false).'"');
											echo '>';
										echo '</div>';
									echo '</div>';

									echo '<div class="item phone">';
										echo '<div class="label">';
											echo $lang['words']['contact-phone'];
										echo '</div>';

										echo '<div class="field with-explaination">';
											echo '<div class="icon">'.svgicon('number').'</div>';
											echo '<input type="text" inputmode="numeric" pattern="[0-9]{20}" name="field-contact-phone"';
											echo (($is_editing == false OR $is_editing == true AND empty($item['data_contact_phone'])) ? '' : ' value="'.endecrypt($item['data_contact_phone'], false).'"');
											echo '>';

											echo '<div class="icon wit" data-element="item-contact-phone" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
										echo '</div>';
									echo '</div>';
								echo '</div>';



								echo '<div class="item">';
									echo '<div class="label">';
										echo $lang['words']['contact-email'];
									echo '</div>';

									echo '<div class="field with-explaination">';
										echo '<div class="icon">'.svgicon('email').'</div>';
										echo '<input type="email" name="field-contact-email"';
										echo (($is_editing == false OR $is_editing == true AND empty($item['data_contact_email'])) ? '' : ' value="'.endecrypt($item['data_contact_email'], false).'"');
										echo '>';

										echo '<div class="icon wit" data-element="item-contact-email" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
									echo '</div>';
								echo '</div>';



							} elseif($get_item == 'loan') {
								echo '<div class="item">';
									echo '<div class="label">';
										echo $lang['words']['dates']['loan-to'];
									echo '</div>';

									echo '<div class="field">';
										echo '<div class="icon">'.svgicon('calendar').'</div>';
										echo '<input type="date" name="field-date-expires" maxlength="10"';
										echo (($is_editing == false OR $is_editing == true AND empty($item['timestamp_date_end'])) ? '' : ' value="'.date_(endecrypt($item['timestamp_date_end'], false), 'date').'"');
										echo '>';
									echo '</div>';
								echo '</div>';


								echo '<div class="item phone">';
									echo '<div class="label">';
										echo $lang['words']['number-phone'];
									echo '</div>';

									echo '<div class="field with-explaination">';
										echo '<div class="icon">'.svgicon('number').'</div>';
										echo '<input type="text" inputmode="numeric" pattern="[0-9]{14}" name="field-number-phone"';
										echo (($is_editing == false OR $is_editing == true AND empty($item['data_number_phone'])) ? '' : ' value="'.endecrypt($item['data_number_phone'], false).'"');
										#echo ($is_editing == false ? '' : (empty($item['check_loan_out']) ? '' : ' disabled'));
										echo '>';

										echo '<div class="icon wit" data-element="item-number-phone" title="'.$lang['tooltips']['what-is-this'].'">'.svgicon('question').'</div>';
									echo '</div>';
								echo '</div>';
							}


							echo '<div class="item notes">';
								echo '<div class="label">';
									echo $lang['words']['notes'];
								echo '</div>';

								echo '<div class="field">';
									echo '<textarea name="field-notes">';
										echo (($is_editing == false OR $is_editing == true AND empty($item['data_notes'])) ? '' : endecrypt($item['data_notes'], false));
									echo '</textarea>';
								echo '</div>';
							echo '</div>';


							echo '<div class="checkboxes">';
								if($get_item == 'expense') {
									echo checkbox($lang['forms']['checkboxes']['expense']['payed'], 'expense-payed', null, ($is_editing == false ? null : (empty($item['check_expense_payed']) ? null : true)));
									echo checkbox($lang['forms']['checkboxes']['expense']['extra'], 'expense-extra', null, ($is_editing == false ? null : (empty($item['check_expense_extra']) ? null : true)));
									echo checkbox($lang['forms']['checkboxes']['expense']['subscription'], 'expense-subscription', null, ($is_editing == false ? null : (empty($item['check_expense_subscription']) ? null : true)));
									echo checkbox($lang['forms']['checkboxes']['expense']['recurrent'], 'expense-recurrent', null, ($is_editing == false ? null : (empty($item['check_expense_recurrent']) ? null : true)));
									echo checkbox($lang['forms']['checkboxes']['expense']['correct-info'], 'expense-correctinfo', null, ($is_editing == false ? null : (empty($item['check_expense_correctinfo']) ? null : true)), ((empty($item['data_number']) AND empty($item['data_number_ocr'])) ? true : false));

								} elseif($get_item == 'debt') {
									echo checkbox($lang['forms']['checkboxes']['debt']['company'], 'debt-company', null, ($is_editing == false ? null : (empty($item['check_debt_company']) ? null : true)));
									echo checkbox($lang['forms']['checkboxes']['debt']['paying'], 'debt-currently-paying', null, ($is_editing == false ? null : (empty($item['check_debt_paying']) ? null : true)));

								} elseif($get_item == 'loan') {
									echo checkbox($lang['forms']['checkboxes']['loan-out'], 'loan-out', null, ($is_editing == false ? null : (empty($item['check_loan_out']) ? null : true)));
								}
							echo '</div>';

						echo '</div>';
					echo '</div>';



					echo '<div class="button">';
						if($is_editing == false) {
							echo '<input type="submit" name="button-add" value="'.$lang['words']['buttons']['add'].'">';

						} else {
							echo '<input type="submit" name="button-save" value="'.$lang['words']['buttons']['save'].'">';
							echo '<a href="'.url($cancel_link.'#'.$get_item.'-'.(int)$item['id']).'">'.$lang['words']['cancel'].'</a>';
						}
					echo '</div>';
				echo '</form>';
			}
		echo '</section>';







		require_once 'site-footer.php';

	}

?>